//package org.dppc.gateway.task;
//
//import org.apache.commons.lang.StringUtils;
//import org.bouncycastle.asn1.ocsp.ResponseData;
//import org.dppc.auth.client.feign.ServiceAuthFeign;
//import org.dppc.common.msg.ObjectRestResponse;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//@Component
//public class TokenScheduledTask {
//    private static Logger logger = LoggerFactory.getLogger(TokenScheduledTask.class);
//
//    public final static long ONE_HOUR = 1000 * 60 * 59;
//
//    @Autowired
//    private ServiceAuthFeign serviceAuthFeign;
//
//    /**
//     * 刷新Token
//     */
//    @Scheduled(fixedDelay = ONE_HOUR)
//    public void reloadApiToken() {
//        String token = this.getToken();
//        while (StringUtils.isBlank(token)) {
//            token = this.getToken();
//        }
//        System.setProperty("fangjia.auth.token", token);
//    }
//
//    public String getToken(String clientId, String secret) {
//        ObjectRestResponse objectRestResponse = serviceAuthFeign.getAccessToken(clientId, secret);
//        return objectRestResponse.getData() == null ? "" : String.valueOf(objectRestResponse.getData());
//    }
//}