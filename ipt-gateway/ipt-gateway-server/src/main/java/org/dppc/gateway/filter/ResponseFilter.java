package org.dppc.gateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.dppc.api.admin.dto.LogInfo;
import org.dppc.api.admin.dto.LogInterface;
import org.dppc.common.msg.ObjectRestResponse;
import org.dppc.common.utils.DateHelper;
import org.dppc.common.utils.JsonHelper;
import org.dppc.api.gateway.vo.RequestParams;
import org.dppc.gateway.feign.ILogInfoService;
import org.dppc.gateway.feign.ILogInterfaceService;
import org.dppc.gateway.utils.DBLogInfo;
import org.dppc.gateway.utils.DBLogInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @描述： 日志过滤和错误过滤器
 * @作者： 颜齐
 * @日期： 2017/10/19.
 * @时间： 15:13.
 */
@Component
public class ResponseFilter extends ZuulFilter {
    Logger log = LoggerFactory.getLogger(ResponseFilter.class);

    @Autowired
    private ILogInterfaceService logInterfaceService;

    @Autowired
    private ILogInfoService logInfoService;

    @Override
    public String filterType() {
        return "post";
    }

    @Override
    public int filterOrder() {
        return 2;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        RequestParams requestParams = null;
        LogInterface logInterface = null;
        Map resultMap = new HashMap();
        try {
            Object isContent = ctx.get("IS_CONTENT");
            if (isContent != null && (Boolean) isContent) {
                LogInfo logInfo = (LogInfo) ctx.get("LOG_INFO");
                saveLog(logInfo);
                return null;
            }
            Object isInterface = ctx.get("IS_INTERFACE");
            if (isInterface == null || !(Boolean) isInterface) {
                return null;
            }
            InputStream stream = ctx.getResponseDataStream();
            logInterface = (LogInterface) ctx.get("LOG_INTERFACE");
            requestParams = RequestParams.valueOf(ctx.get("REQUEST_PARAMS"));

            String responseBody = StreamUtils.copyToString(stream, Charset.forName("UTF-8"));
            ObjectRestResponse response = JsonHelper.readJson(responseBody, ObjectRestResponse.class);
            resultMap.put("BIZ_TYPE", requestParams.getRequestVO().getBIZ_TYPE());
            resultMap.put("RESP_TIME", DateHelper.getNowTime());
            resultMap.put("RESP_ID", String.valueOf(Calendar.getInstance().getTime().getTime()));
            resultMap.put("REQ_ID", requestParams.getRequestVO().getREQ_ID());
            resultMap.put("STATUS_CODE", response.getStatus());
            if (requestParams.getRequestVO().getBIZ_TYPE().equals("TRACE_CODE")) {//处理追溯码下发接口
                if(response.getStatus()==200)
                    resultMap.put("RESULT",  response.getData());
                else
                    resultMap.put("RESULT",  response.getMsg());
            }else{
                resultMap.put("RESULT",  response.getMsg());
            }
            responseBody = JsonHelper.toJSon(resultMap) + requestParams.getToken();
            ctx.setResponseBody(responseBody);
            saveLog(logInterface, resultMap);
        } catch (Exception e) {
            e.printStackTrace();
            setFailedRequest(requestParams, logInterface);
        } finally {
            return null;
        }
    }


    /**
     * 保存日志
     *
     * @param logInterface
     * @param resultMap
     */
    public void saveLog(LogInterface logInterface, Map resultMap) {
        logInterface.setRespId(String.valueOf(resultMap.get("RESP_ID")));
        logInterface.setResponseBody(JsonHelper.toJSon(resultMap));
        logInterface.setResponseStatus(String.valueOf(resultMap.get("STATUS_CODE")));
        logInterface.setErrorMsg(String.valueOf(resultMap.get("RESULT")));
        logInterface.setResponseTime(new Date());
        logInterface.setProcessTime((int) (new Date().getTime() - logInterface.getRequestTime().getTime()));
        logInterface.setCreateTime(new Date());
        DBLogInterface.getInstance().setLogInterfaceService(logInterfaceService).offerQueue(logInterface);
    }

    public void saveLog(LogInfo logInfo) {
        int processTime = (int) (Calendar.getInstance().getTime().getTime() - logInfo.getCreateTime().getTime());
        logInfo.setProcessTime(processTime);
        DBLogInfo.getInstance().setLogService(logInfoService).offerQueue(logInfo);
    }

    /**
     * 错误
     *
     * @param requestParams
     * @param logInterface
     */
    private void setFailedRequest(RequestParams requestParams, LogInterface logInterface) {
        RequestContext ctx = RequestContext.getCurrentContext();
        ctx.getResponse().setContentType("application/json;charset=UTF-8");
        Map resultMap = new HashMap();
        String respId = String.valueOf(new Date().getTime());
        resultMap.put("BIZ_TYPE", requestParams.getRequestVO().getBIZ_TYPE());
        resultMap.put("RESP_TIME", DateHelper.getNowTime());
        resultMap.put("RESP_ID", respId);
        resultMap.put("REQ_ID", requestParams.getRequestVO().getREQ_ID());
        resultMap.put("STATUS_CODE", 500);
        resultMap.put("RESULT", "参数错误");
        String responseBody = JsonHelper.toJSon(resultMap) + requestParams.getToken();
        ctx.setResponseStatusCode(500);
        ctx.setResponseBody(responseBody);
        saveLog(logInterface, resultMap);
    }
}
