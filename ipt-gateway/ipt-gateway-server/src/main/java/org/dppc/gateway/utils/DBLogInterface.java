package org.dppc.gateway.utils;


import org.dppc.api.admin.dto.LogInterface;
import org.dppc.gateway.feign.ILogInterfaceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @描述： 日志
 * @作者： 颜齐
 * @日期： 2017/10/25.
 * @时间： 16:23.
 */
public class DBLogInterface extends Thread {
    private static Logger logger = LoggerFactory.getLogger(DBLogInfo.class);

    private static DBLogInterface dbLogInterface = null;
    private static BlockingQueue<LogInterface> logInterfaceQueue = new LinkedBlockingQueue<LogInterface>(10240);

    private ILogInterfaceService logInterfaceService;

    public ILogInterfaceService getLogInterfaceService() {
        return logInterfaceService;
    }

    public DBLogInterface setLogInterfaceService(ILogInterfaceService logInterfaceService) {
        if(this.logInterfaceService==null)
            this.logInterfaceService = logInterfaceService;
        return this;
    }
    public static synchronized DBLogInterface getInstance() {
        if (dbLogInterface == null) {
            dbLogInterface = new DBLogInterface();
        }
        return dbLogInterface;
    }

    private DBLogInterface() {
        super("DBLogInterface");
    }

    public void offerQueue(LogInterface logInterface) {
        try {
            logInterfaceQueue.offer(logInterface);
        } catch (Exception e) {
            logger.error("日志写入失败", e);
        }
    }

    public void run() {
        List<LogInterface> bufferedLogList = new ArrayList<LogInterface>(); // 缓冲队列
        while (true) {
            try {
                bufferedLogList.add(logInterfaceQueue.take());
                logInterfaceQueue.drainTo(bufferedLogList);
                if (bufferedLogList != null && bufferedLogList.size() > 0) {
                    // 写入日志
                    for(LogInterface log:bufferedLogList){
                        logInterfaceService.saveLog(log);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Thread.sleep(1000);
                } catch (Exception eee) {
                }
            } finally {
                if (bufferedLogList != null && bufferedLogList.size() > 0) {
                    try {
                        bufferedLogList.clear();
                    } catch (Exception e) {
                    }
                }
            }
        }
    }
}