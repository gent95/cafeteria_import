package org.dppc.gateway.config;

import org.dppc.auth.client.interceptor.ServiceFeignInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 */
//@Configuration
public class ZuulConfig {
    @Bean
    ServiceFeignInterceptor getClientTokenInterceptor(){
        return new ServiceFeignInterceptor();
    }
}
