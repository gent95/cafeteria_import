package org.dppc.gateway.feign;

import org.dppc.api.admin.dto.LogInfo;
import org.dppc.gateway.config.ZuulConfig;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @描述： 日志接口
 * @作者： 颜齐
 * @日期： 2017/10/25.
 * @时间： 16:48.
 */
@FeignClient(value = "IPT-ADMIN", configuration = {ZuulConfig.class})
@RequestMapping("api/logInfo")
public interface ILogInfoService {
    /**
     * 保存日志
     *
     * @param info
     */
    @PutMapping(value = "/save")
    void saveLog(@RequestBody LogInfo info);
}
