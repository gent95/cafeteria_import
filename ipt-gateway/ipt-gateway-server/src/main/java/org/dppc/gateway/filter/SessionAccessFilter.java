package org.dppc.gateway.filter;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.apache.commons.lang3.StringUtils;
import org.dppc.api.admin.dto.LogInfo;
import org.dppc.api.admin.dto.Resource;
import org.dppc.api.authority.UserInfo;
import org.dppc.auth.client.config.ServiceAuthConfig;
import org.dppc.auth.client.config.UserAuthConfig;
import org.dppc.auth.client.feign.ServiceAuthFeign;
import org.dppc.auth.client.jwt.UserAuthUtil;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.context.ClientConstantHandler;
import org.dppc.common.enums.ExceptionEnum;
import org.dppc.common.enums.LogOptEnum;
import org.dppc.common.enums.LogTypeEnum;
import org.dppc.common.exception.BaseException;
import org.dppc.common.exception.auth.CommonErrorException;
import org.dppc.common.exception.auth.UserForbiddenException;
import org.dppc.common.jwt.IJWTInfo;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.msg.ObjectRestResponse;
import org.dppc.common.utils.ClientHelper;
import org.dppc.common.utils.JsonHelper;
import org.dppc.common.utils.ResponseHelper;
import org.dppc.common.utils.StringHelper;
import org.dppc.gateway.feign.ILogInfoService;
import org.dppc.gateway.feign.ILogInterfaceService;
import org.dppc.gateway.feign.IResourceService;
import org.dppc.gateway.feign.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Pattern;

/**
 * @描述： 平台权限拦截
 * @作者： 颜齐
 * @日期： 2017/10/19.
 * @时间： 15:13.
 */
@Component
public class SessionAccessFilter extends ZuulFilter {
    Logger log = LoggerFactory.getLogger(SessionAccessFilter.class);

    @Autowired
    private IUserService userService;

    @Autowired
    private ILogInfoService logService;

    @Autowired
    private ILogInterfaceService logInterfaceService;

    @Autowired
    private IResourceService resourceService;

    @Value("${gate.ignore.startWith}")
    private String ignoreStartWith;
    @Value("${gate.not-ignore.startWith}")
    private String notAllowStartWith;
    @Value("${zuul.prefix}")
    private String zuulPrefix;
    @Value("${zuul.ignored-log-method}")
    private String[] ignoredLogMethods;

    @Autowired
    private UserAuthUtil userAuthUtil;

    @Autowired
    private ServiceAuthConfig serviceAuthConfig;

    @Autowired
    private UserAuthConfig userAuthConfig;

    @Autowired
    private ServiceAuthFeign serviceAuthFeign;


    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        final String requestUri = request.getRequestURI().substring(zuulPrefix.length());
        final String method = request.getMethod();
        BaseContextHandler.setToken(null);


        if (isStartWith(requestUri)) // 不进行拦截的地址
            return null;
        IJWTInfo user;
        try {
            user = getJWTUser(request);//解析用户并存储
            ctx.addZuulRequestHeader(userAuthConfig.getTokenHeader(), BaseContextHandler.getToken());
        } catch (BaseException e) {
            setFailedRequest(e, 200);
            return null;
        }
        //判断用户是否有权限访问该接口
        boolean isAllow = checkAllow(requestUri, method, ctx, user.getUniqueName());
        if (!isAllow) return null;
        // 申请客户端密钥头
        if (StringUtils.isEmpty(ClientConstantHandler.CLIENT_TOKEN)) {
            BaseResponse resp = serviceAuthFeign.getAccessToken(serviceAuthConfig.getClientId(), serviceAuthConfig.getClientSecret());
            if (resp.getStatus() == 200) {
                ObjectRestResponse<String> clientToken = (ObjectRestResponse<String>) resp;
                ctx.addZuulRequestHeader(serviceAuthConfig.getTokenHeader(), clientToken.getData());
                ClientConstantHandler.CLIENT_TOKEN = clientToken.getData();
            } else {
                setFailedRequest(new CommonErrorException(ExceptionEnum.EX_CLIENT_INVALID), 200);
            }
        }
        return null;
    }

    /**
     * 获取目标权限资源
     *
     * @param
     * @param requestUri
     * @param method
     * @return
     */
    private Collection<Resource> getPermissionInfos(final String requestUri, final String method, List<Resource> permissionInfos) {
        return Collections2.filter(permissionInfos, new Predicate<Resource>() {
            @Override
            public boolean apply(Resource permissionInfo) {
                String[] permissions = permissionInfo.getPermission().split(",");
                if(permissions.length > 0){
                    boolean options;
                    for (int i = 0; i < permissions.length; i++) {
                        String url = permissions[i];
                        String uri = url.replaceAll("\\{\\*\\}", "[a-zA-Z\\\\d]+");
                        String regEx = "^" + uri + "$";
                        options = (Pattern.compile(regEx).matcher(requestUri).find() || requestUri.startsWith(url + "/"))
                                && ("ALL".equals(permissionInfo.getMethod()) || method.equals(permissionInfo.getMethod()));
                        if(options == true){
                            return true;
                        }
                    }
                }
                return false;
            }
        });
    }

    /**
     * 存储日志
     *
     * @param ctx
     * @param username
     */
    private void setCurrentUserInfoAndLog(RequestContext ctx, Resource resource, String username) {
        UserInfo info = userService.getUserByUsername(username);
        String host = ClientHelper.getClientIp(ctx.getRequest());
        ctx.addZuulRequestHeader("userId", info.getId());
        ctx.addZuulRequestHeader("userName", URLEncoder.encode(info.getName()));
        ctx.addZuulRequestHeader("userHost", ClientHelper.getClientIp(ctx.getRequest()));
        //保存日志
        LogInfo logInfo = new LogInfo(LogOptEnum.OTHER.name(), resource.getResourceName(),
                ctx.getRequest().getRequestURL().toString(),
                info.getUsername(), Calendar.getInstance(TimeZone.getTimeZone("GMT+:08:00")).getTime(), host,
                LogTypeEnum.USER.name(), resource.getMethod(), serviceAuthConfig.getClientId());
        ctx.set("LOG_INFO", logInfo);
        ctx.set("IS_CONTENT", true);
    }

    /**
     * 返回session中的用户信息
     *
     * @param request
     * @return
     */
    private IJWTInfo getJWTUser(HttpServletRequest request) throws BaseException {
        String authToken = request.getHeader(userAuthConfig.getTokenHeader());
        if (StringUtils.isBlank(authToken)) {
            authToken = request.getParameter("token");
        }
        if (StringUtils.isBlank(authToken)) {
            throw new BaseException(ExceptionEnum.EX_USER_INVALID);
        }
        BaseContextHandler.setToken(authToken);
        return userAuthUtil.getInfoFromToken(authToken);
    }

    /**
     * 读取用户权限
     *
     * @param request
     * @param username
     * @return
     */
    private List<Resource> getPermissionInfos(HttpServletRequest request, String username) {
        List<Resource> permissionInfos = userService.getPermissionByUsername(username);
//        if (request.getSession().getAttribute("permission") == null) {
//            permissionInfos = userService.getPermissionByUsername(username);
//            request.getSession().setAttribute("permission", permissionInfos);
//        } else {
//            permissionInfos = (List<Resource>) request.getSession().getAttribute("permission");
//        }
        return permissionInfos;
    }

    /**
     *
     * 效验不允许通过和允许通过的权限
     */
    private boolean checkPermission(final String requestUri, final String method, RequestContext ctx, String username) {
        Boolean result = checkNotAllow(requestUri);
        if (result)
            result = checkAllow(requestUri, method, ctx, username);
        return result;
    }

    /**
     *
     * @param requestUri
     * @return
     */
    private boolean checkNotAllow(final String requestUri) {
        Boolean result = StringHelper.isStartWith(requestUri, notAllowStartWith.split(","));
        return !result;//如果有复合的，返回false，说明不允许通过
    }

    /**
     * 权限校验
     *
     * @param requestUri
     * @param method
     */
    private boolean checkAllow(final String requestUri, final String method, RequestContext ctx, String username) {
        log.debug("uri：" + requestUri + "----method：" + method);
        Collection<Resource> result = null;
        List<Resource> permissionInfos = null;
        List<Resource> noFilterPermissionInfos = resourceService.findNoFilterPermission();
        Collection<Resource> noFilterPermissionInfoResult = getPermissionInfos(requestUri, method, noFilterPermissionInfos);
        if (noFilterPermissionInfoResult.size() <= 0) {//在去判断需要权限的能否访问
            permissionInfos = getPermissionInfos(ctx.getRequest(), username);//读取用户权限
            if (permissionInfos == null || permissionInfos.size() == 0) {
                setFailedRequest(new UserForbiddenException(), 200);
                return false;
            }
            result = getPermissionInfos(requestUri, method, permissionInfos);
            if (result.size() <= 0) {
                setFailedRequest(new UserForbiddenException(), 200);
                return false;
            }
        }
        for (String ignoredLogMethod : ignoredLogMethods) {
            if (method.equals(ignoredLogMethod)) {
                return true;
            }
        }
        setCurrentUserInfoAndLog(ctx, (Resource) result.toArray()[0], username);
        return true;
    }


    /**
     * URI是否以什么打头
     *
     * @param requestUri
     * @return
     */
    private boolean isStartWith(String requestUri) {
        return StringHelper.isStartWith(requestUri, ignoreStartWith.split(","));
    }

    /**
     * 有问题，需要修改
     *
     * @param
     * @param
     */
    private void setFailedRequest(BaseException be, int status) {
        RequestContext ctx = RequestContext.getCurrentContext();
        ctx.getResponse().setContentType("application/json;charset=UTF-8");
        ctx.setResponseStatusCode(status);
        ctx.setResponseBody(JsonHelper.toJSon(ResponseHelper.error(be)));
        ctx.setSendZuulResponse(false);
    }
}
