package org.dppc.gateway.feign;

import org.dppc.api.admin.dto.Resource;
import org.dppc.gateway.config.ZuulConfig;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/24.
 * @时间： 11:49.
 */
@FeignClient(value = "IPT-ADMIN", configuration = {ZuulConfig.class})
@RequestMapping("api/resource")
public interface IResourceService {

    @GetMapping(value = "/findFilterPermission")
    List<Resource> findFilterPermission();

    @GetMapping(value = "/findNoFilterPermission")
    List<Resource> findNoFilterPermission();
}
