package org.dppc.gateway.feign;

import org.dppc.api.admin.dto.Resource;
import org.dppc.api.authority.PermissionInfo;
import org.dppc.api.authority.UserInfo;
import org.dppc.gateway.config.ZuulConfig;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


/**
 * ${DESCRIPTION}
 *
 *
 * @create 2017-06-21 8:11
 */
@FeignClient(value = "IPT-ADMIN", configuration = {ZuulConfig.class})
@RequestMapping("api")
public interface IUserService {

    @RequestMapping(value = "/user/getUser/{username}", method = RequestMethod.GET)
    UserInfo getUserByUsername(@PathVariable("username") String username);

    @RequestMapping(value = "/user/getPermissions/{username}", method = RequestMethod.GET)
    List<Resource> getPermissionByUsername(@PathVariable("username") String username);
}
