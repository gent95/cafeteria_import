package org.dppc.gateway.feign;

import org.dppc.api.admin.dto.LogInfo;
import org.dppc.api.admin.dto.LogInterface;
import org.dppc.gateway.config.ZuulConfig;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @描述： 日志接口
 * @作者： 颜齐
 * @日期： 2017/10/25.
 * @时间： 16:48.
 */
@FeignClient(value = "IPT-ADMIN",configuration = {ZuulConfig.class})
@RequestMapping("api/logInterface")
public interface ILogInterfaceService {
    /**
     * 保存日志
     * @param logInterface
     */
    @PostMapping(value = "/save")
    void saveLog(@RequestBody LogInterface logInterface);
}
