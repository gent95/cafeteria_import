package org.dppc.gateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.dppc.api.admin.dto.LogInterface;
import org.dppc.gateway.feign.ILogInterfaceService;
import org.dppc.gateway.utils.DBLogInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;

/**
 * @描述： swagger api接口文档
 * @作者： 颜齐
 * @日期： 2017/10/19.
 * @时间： 15:13.
 */
@Component
public class ErrorFilter extends ZuulFilter {
    Logger log = LoggerFactory.getLogger(ErrorFilter.class);

    @Autowired
    private ILogInterfaceService logInterfaceService;

    @Override
    public String filterType() {
        return "error";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
//        RequestContext ctx = RequestContext.getCurrentContext();
//        LogInterface logInterface = (LogInterface) ctx.get("LOG_INTERFACE");
//        logInterface.setResponseBody(ctx.getResponseBody());
//        logInterface.setResponseStatus(String.valueOf(ctx.getResponseStatusCode()));
//        logInterface.setResponseTime(new Date());
//        logInterface.setProcessTime((int) (new Date().getTime() - logInterface.getRequestTime().getTime()));
//        logInterface.setCreateTime(new Date());
//        DBLogInterface.getInstance().setLogInterfaceService(logInterfaceService).offerQueue(logInterface);
        System.out.println("***************************)))))))))))))))))))))))))))))))))__________________________");
        return null;
    }
}
