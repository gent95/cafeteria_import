package org.dppc.gateway.utils;


import org.dppc.api.admin.dto.LogInfo;
import org.dppc.gateway.feign.ILogInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @描述： 日志
 * @作者： 颜齐
 * @日期： 2017/10/25.
 * @时间： 16:23.
 */
public class DBLogInfo extends Thread {
    private static Logger logger = LoggerFactory.getLogger(DBLogInfo.class);

    private static DBLogInfo dblog = null;
    private static BlockingQueue<LogInfo> logInfoQueue = new LinkedBlockingQueue<LogInfo>(1024);

    public ILogInfoService getLogService() {
        return logService;
    }

    public DBLogInfo setLogService(ILogInfoService logService) {
        if (this.logService == null)
            this.logService = logService;
        return this;
    }

    private ILogInfoService logService;

    public static synchronized DBLogInfo getInstance() {
        if (dblog == null) {
            dblog = new DBLogInfo();
        }
        return dblog;
    }

    private DBLogInfo() {
        super("DBLogInfo");
    }

    public void offerQueue(LogInfo logInfo) {
        try {
            logInfoQueue.offer(logInfo);
        } catch (Exception e) {
            logger.error("日志写入失败", e);
        }
    }

    public void run() {
        List<LogInfo> bufferedLogList = new ArrayList<LogInfo>(); // 缓冲队列
        while (true) {
            try {
                bufferedLogList.add(logInfoQueue.take());
                logInfoQueue.drainTo(bufferedLogList);
                if (bufferedLogList != null && bufferedLogList.size() > 0) {
                    // 写入日志
                    for (LogInfo log : bufferedLogList) {
                        logService.saveLog(log);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Thread.sleep(1000);
                } catch (Exception eee) {
                    eee.printStackTrace();
                }
            } finally {
                if (bufferedLogList != null && bufferedLogList.size() > 0) {
                    try {
                        bufferedLogList.clear();
                    } catch (Exception e) {
                    }
                }
            }
        }
    }
}