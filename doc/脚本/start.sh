#!/bin/bash
# author:yanqi 
# createTime:2018/04/19
PROJECT_NAME=$1
if [ -z $PROJECT_NAME ]
then
echo "runing fail ,please input you run filename:start.sh filename"
exit 1
fi
PROCESS_ID=`ps -ef | grep "$PROJECT_NAME" | grep -v "$0" | grep -v "grep" | awk '{print $2}'`
echo $PROCESS_ID
for id in $PROCESS_ID
do
	kill -9 $id
	echo "killed $id"  
done
echo "already kill runing process_id" 
echo  "start running:  "$PROJECT_NAME
TODAY=`date +%Y-%m-%d`
LOG_NAME="log-"$TODAY".txt"
FULL_LOG_NAME=logs/$LOG_NAME
if [ ! -d logs ]
then
	mkdir logs
fi
if [ ! -e $FULL_LOG_NAME ]
then
	touch $FULL_LOG_NAME
fi
nohup java -jar $PROJECT_NAME --info  >$FULL_LOG_NAME &
echo "running :"$PROJECT_NAME

