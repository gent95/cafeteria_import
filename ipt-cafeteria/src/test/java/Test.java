import org.dppc.cafeteria.CafeteriaAppliction;
import org.dppc.cafeteria.entity.basedata.DictType;
import org.dppc.cafeteria.entity.basedata.FoodMaterialsType;
import org.dppc.cafeteria.entity.machining.Meal;
import org.dppc.cafeteria.entity.machining.MealDish;
import org.dppc.cafeteria.entity.record.Utensil;
import org.dppc.cafeteria.entity.requisition.RequisitionDetail;
import org.dppc.cafeteria.service.basedata.DictTypeService;
import org.dppc.cafeteria.service.basedata.FoodMaterialsTypeService;
import org.dppc.cafeteria.service.machining.MealService;
import org.dppc.cafeteria.service.record.UtensilService;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @描述 ：
 * @作者 ：majt
 * @日期 ：2019/5/11
 * @时间 ：13:15
 */
@SpringBootTest(classes = CafeteriaAppliction.class)
@RunWith(SpringRunner.class)
public class Test {
    @Autowired
   private DictTypeService dictTypeService;

    @org.junit.Test
    public void test(){
        System.out.println(dictTypeService.findList(new DictType()));
    }
}
