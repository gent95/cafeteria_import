package org.dppc.cafeteria.feign;

import org.dppc.cafeteria.entity.emergency.EmergencySchoolReport;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Description 学校应急事件上报
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
@FeignClient("ipt-supervise")
@RequestMapping(value = "/api/schoolReport")
public interface SchoolReportService {

    /**
     * @Author GAOJ
     * @Description 应急事件上报监管系统
     * @Date 2019/05/27 14:18
     **/
    @PostMapping(value = "/report")
    Boolean report(EmergencySchoolReport emergencySchoolReport);
}