package org.dppc.cafeteria.utils;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import org.dppc.common.enums.stock.QGPUnitEnum;

import java.util.Date;

/**
 * @Description 库存相关工具类
 * @Author lhw
 * @Data 2019/5/11 19:15
 * @Version 1.0
 **/
public class StockUtil {
    /**
     * @Author lhw
     * @Description 根据商品编码获取大类编码
     * @Date 19:17 2019/5/11
     * @Param materialsCode
     **/
    public static String gainBigType(String materialsCode){
        return materialsCode.length() > 3 ? materialsCode.substring(0, 3) : materialsCode;
    }
    /**
     * @Author lhw
     * @Description 计算有效期至
     * @Date 9:32 2019/5/23
     **/
    public static String gainExpiryDate(Integer qualityGuaranteePeriod, Integer qualityGuaranteePeriodUnit, String produceDate) {
        Date date = DateUtil.parse(produceDate,DatePattern.NORM_DATE_PATTERN);
        Date expiryDate = DateUtil.date();
        if(qualityGuaranteePeriodUnit == QGPUnitEnum.DAY.getValue()){
            expiryDate = DateUtil.offset(date, DateField.DAY_OF_YEAR, qualityGuaranteePeriod);
        }else if(qualityGuaranteePeriodUnit == QGPUnitEnum.MONTH.getValue()){
            expiryDate = DateUtil.offset(date, DateField.MONTH, qualityGuaranteePeriod);
        }else if(qualityGuaranteePeriodUnit == QGPUnitEnum.YEAR.getValue()){
            expiryDate = DateUtil.offset(date, DateField.YEAR, qualityGuaranteePeriod);
        }
        return DateUtil.format(expiryDate, DatePattern.NORM_DATE_PATTERN);
    }

}
