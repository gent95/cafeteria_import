package org.dppc.cafeteria.utils;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.dppc.api.admin.dto.SchoolCafteria;
import org.dppc.cafeteria.entity.purchase.PurchaseBase;
import org.dppc.cafeteria.vo.CafeteriaInfoVO;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.enums.UserTypeEnum;
import org.dppc.common.exception.UserNoBindCafeteriaException;
import org.dppc.common.utils.NullHelper;
import org.dppc.dbexpand.util.BeanHelp;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Set;

/**
 * @Description 食堂端操作工具类
 * @Author lhw
 * @Data 2019/6/8 14:58
 * @Version 1.0
 **/
public class CafeteriaHandleUtil {
    /**
     * @Author lhw
     * @Description 验证用户查询食堂的权限
     * @Date 15:46 2019/6/8
     **/
    public static String validateAuth(String cafeteriaCode) {
        if (Integer.parseInt(BaseContextHandler.getUserType()) == UserTypeEnum.PROVINCE_USER.getValue()) {
            //食堂用户  进行判断，是否有绑定食堂
            Set<SchoolCafteria> schoolCafterias = BaseContextHandler.getSchoolCafterias();
            if (schoolCafterias != null && schoolCafterias.size() > 0) {
                Iterator<SchoolCafteria> iterable = schoolCafterias.iterator();
                SchoolCafteria schoolCafteria = iterable.next();
                if (StringUtils.isBlank(schoolCafteria.getCafeteriaCode())) {
                    //提示用户绑定食堂
                    throw  new UserNoBindCafeteriaException();
                }
                return schoolCafteria.getCafeteriaCode();
            }else {
                //提示用户绑定食堂
                throw  new UserNoBindCafeteriaException();
            }
        }
        //非食堂用户，直接返回原有值
        return cafeteriaCode;
    }
    /**
     * @Author lhw
     * @Description 食堂用进行添加数据时，进行食堂信息绑定
     * @Date 15:57 2019/6/8
     **/
    public static void bindCafeteriaInfo(Object object){
        if (Integer.parseInt(BaseContextHandler.getUserType()) == UserTypeEnum.PROVINCE_USER.getValue()) {
            Set<SchoolCafteria> schoolCafterias = BaseContextHandler.getSchoolCafterias();
            if (schoolCafterias != null && schoolCafterias.size() > 0) {
                Iterator<SchoolCafteria> iterable = schoolCafterias.iterator();
                SchoolCafteria schoolCafteria = iterable.next();
                CafeteriaInfoVO cafeteriaInfoVO = new CafeteriaInfoVO();
                if (StringUtils.isBlank(schoolCafteria.getCafeteriaCode())) {
                    //提示用户绑定食堂
                    throw  new UserNoBindCafeteriaException();
                }
                cafeteriaInfoVO.setSchoolCode(schoolCafteria.getSchoolCode());
                cafeteriaInfoVO.setCafeteriaCode(schoolCafteria.getCafeteriaCode());
                cafeteriaInfoVO.setCafeteriaName(schoolCafteria.getCafeteriaName());
                cafeteriaInfoVO.setSchoolName(schoolCafteria.getSchoolName());
                updateEntityExceptEmptyProps(object, cafeteriaInfoVO);
            }else {
                //提示用户绑定食堂
                throw new UserNoBindCafeteriaException();
            }
        }else {
            System.err.println("非食堂用户操作，食堂端添加功能");
        }
    }

    private static void updateEntityExceptEmptyProps(Object origin, Object update) {
        try {
            Class originClazz = origin.getClass();
            Class updateClazz = update.getClass();
            PropertyDescriptor[] originDescriptors = Introspector.getBeanInfo(originClazz).getPropertyDescriptors();
            PropertyDescriptor[] updateDescriptors = Introspector.getBeanInfo(updateClazz).getPropertyDescriptors();
            for (int i = 0; i < updateDescriptors.length; i++) {
                Method updateRMethod = updateDescriptors[i].getReadMethod();
                Method originRMethod = null;
                Method orginWMethod = null;
                for (int j = 0; j <originDescriptors.length; j++) {
                    if (updateDescriptors[i].getName().equals(originDescriptors[j].getName())){
                        originRMethod = originDescriptors[j].getReadMethod();
                        orginWMethod = originDescriptors[j].getWriteMethod();
                        break;
                    }
                }
                if (originRMethod != null && updateRMethod != null && orginWMethod != null) {
                    Object originProperty = originRMethod.invoke(origin);
                    Object updateProperty = updateRMethod.invoke(update);
                    orginWMethod.invoke(origin, NullHelper.isNull(updateProperty) ? originProperty : updateProperty);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
