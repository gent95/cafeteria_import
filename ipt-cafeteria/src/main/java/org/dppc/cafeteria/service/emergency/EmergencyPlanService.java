package org.dppc.cafeteria.service.emergency;

import org.dppc.cafeteria.entity.emergency.EmergencyPlan;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 应急预案业务层接口
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
public interface EmergencyPlanService extends BaseService<EmergencyPlan, Long> {

}
