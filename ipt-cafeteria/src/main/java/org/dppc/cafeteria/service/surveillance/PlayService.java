package org.dppc.cafeteria.service.surveillance;

import org.dppc.cafeteria.entity.surveillance.Play;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 业务层接口
 * @Author majt
 * @Data 2019/06/03 11:07
 * @Version 1.0
 **/
public interface PlayService extends BaseService<Play, Long> {

}
