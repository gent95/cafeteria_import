package org.dppc.cafeteria.service.requisition.impl;

import org.dppc.cafeteria.entity.requisition.RequisitionDetail;
import org.dppc.cafeteria.repository.requisition.RequisitionDetailRepository;
import org.dppc.cafeteria.service.requisition.RequisitionDetailService;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description 领用单明细业务层实现类
 * @Author GAOJ
 * @Data 2019/05/09 09:49
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class RequisitionDetailServiceImpl extends BaseServiceImpl<RequisitionDetail, Long> implements RequisitionDetailService {

    @Autowired
    private RequisitionDetailRepository requisitionDetailRepository;

}
