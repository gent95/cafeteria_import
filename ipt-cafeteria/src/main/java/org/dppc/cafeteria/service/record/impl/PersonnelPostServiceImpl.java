package org.dppc.cafeteria.service.record.impl;

import org.dppc.cafeteria.entity.record.PersonnelPost;
import org.dppc.cafeteria.repository.record.PersonnelPostRepository;
import org.dppc.cafeteria.service.record.PersonnelPostService;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description 人员职务业务层实现类
 * @Author GAOJ
 * @Data 2019/05/09 19:11
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class PersonnelPostServiceImpl extends BaseServiceImpl<PersonnelPost, Integer> implements PersonnelPostService {

    @Autowired
    private PersonnelPostRepository personnelPostRepository;

}
