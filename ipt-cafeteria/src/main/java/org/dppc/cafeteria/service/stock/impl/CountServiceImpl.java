package org.dppc.cafeteria.service.stock.impl;

import org.dppc.cafeteria.entity.stock.Count;
import org.dppc.cafeteria.repository.stock.CountRepository;
import org.dppc.cafeteria.service.stock.CountService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description 库存统计信息表业务层实现类
 * @Author lhw
 * @Data 2019/05/09 15:22
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class CountServiceImpl extends BaseServiceImpl<Count, Long> implements CountService {

    @Autowired
    private CountRepository countRepository;

    @Override
    public Double statFoodMaterialsNum(String materialsCode) {
        Double stockNum = countRepository.statFoodMaterialsNum(materialsCode, CafeteriaHandleUtil.validateAuth(""));
        if (stockNum != null) {
            return stockNum;
        }else {
            return 0.0;
        }

    }
}
