package org.dppc.cafeteria.service.stock;

import org.dppc.cafeteria.entity.stock.Info;
import org.dppc.dbexpand.service.BaseService;

import java.util.List;

/**
 * @Description 库存信息表业务层接口
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
public interface InfoService extends BaseService<Info, Long> {

    /**
     * @Author lhw
     * @Description 获取当前库存信息
     * @Date 16:32 2019/5/14
     **/
    List<Info> findCurrentStockInfo();
}
