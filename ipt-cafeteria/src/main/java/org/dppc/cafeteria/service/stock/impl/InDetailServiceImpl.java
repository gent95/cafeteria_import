package org.dppc.cafeteria.service.stock.impl;

import org.dppc.cafeteria.entity.stock.InDetail;
import org.dppc.cafeteria.service.stock.InDetailService;
import org.dppc.cafeteria.repository.stock.InDetailRepository;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description 原材料入库信息细表业务层实现类
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class InDetailServiceImpl extends BaseServiceImpl<InDetail, Long> implements InDetailService {

    @Autowired
    private InDetailRepository inDetailRepository;

}
