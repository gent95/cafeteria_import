package org.dppc.cafeteria.service.record.impl;

import org.dppc.cafeteria.entity.record.SupplierType;
import org.dppc.cafeteria.repository.record.SupplierTypeRepository;
import org.dppc.cafeteria.service.record.SupplierTypeService;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description 供应商类型业务层实现类
 * @Author GAOJ
 * @Data 2019/05/09 19:11
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class SupplierTypeServiceImpl extends BaseServiceImpl<SupplierType, Integer> implements SupplierTypeService {

    @Autowired
    private SupplierTypeRepository supplierTypeRepository;

}
