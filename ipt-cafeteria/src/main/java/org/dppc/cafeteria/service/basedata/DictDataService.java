package org.dppc.cafeteria.service.basedata;

import org.dppc.cafeteria.entity.basedata.DictData;
import org.dppc.cafeteria.entity.basedata.DictData;
import org.dppc.cafeteria.vo.DictDataVo;
import org.dppc.dbexpand.service.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @Description 字典数据业务层接口
 * @Author majt
 * @Data 2019/05/09 10:15
 * @Version 1.0
 **/
public interface DictDataService extends BaseService<DictData, Long> {
    /**
     * @描述 ：启用字典数据
     * @作者 ：majt
     * @日期 ：2017/11/30
     * @时间 ：15:46
     */
    DictData enableDictData(Long id);

    /**
     * @描述 ：禁用字典数据
     * @作者 ：majt
     * @日期 ：2017/11/30
     * @时间 ：15:46
     */
    DictData disableDictData(Long id);

    /**
     * @描述 ：删除字典数据
     * @作者 ：majt
     * @日期 ：2017/11/30
     * @时间 ：15:46
     */
    void delDictData(Long id);

    /**
     * @return
     * @Author majt
     * @Description 查询字典分页
     * @Date 2019/5/13 18:07
     * @Param
     **/
    Page<DictDataVo> findDictDataPage(DictData dictData, Pageable pageable);

    String dictNameFindByTypeCodeAndDictValue(String typeCode,String dictValue);
}
