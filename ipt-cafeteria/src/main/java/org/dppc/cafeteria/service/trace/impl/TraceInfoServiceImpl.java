package org.dppc.cafeteria.service.trace.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import org.dppc.cafeteria.entity.basedata.DictData;
import org.dppc.cafeteria.entity.basedata.DictType;
import org.dppc.cafeteria.entity.machining.Meal;
import org.dppc.cafeteria.entity.machining.MealDish;
import org.dppc.cafeteria.entity.requisition.RequisitionDetail;
import org.dppc.cafeteria.entity.stock.InBase;
import org.dppc.cafeteria.entity.stock.OutBase;
import org.dppc.cafeteria.entity.stock.OutDetail;
import org.dppc.cafeteria.repository.basedata.DictDataRepository;
import org.dppc.cafeteria.repository.stock.InBaseRepository;
import org.dppc.cafeteria.repository.stock.OutBaseRepository;
import org.dppc.cafeteria.repository.stock.OutDetailRepository;
import org.dppc.cafeteria.service.basedata.DictDataService;
import org.dppc.cafeteria.service.basedata.DictTypeService;
import org.dppc.cafeteria.service.machining.MealDishService;
import org.dppc.cafeteria.service.trace.TraceInfoService;
import org.dppc.cafeteria.vo.TraceInfoVO;
import org.dppc.cafeteria.vo.WeiXinTraceInfoVO;
import org.dppc.dbexpand.util.BeanHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description 追溯信息查询业务逻辑实现类
 * @Author lhw
 * @Data 2019/5/28 15:25
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class TraceInfoServiceImpl implements TraceInfoService {

    @Autowired
    private MealDishService mealDishService;
    @Autowired
    private DictDataService dictDataService;
    @Autowired
    private OutBaseRepository outBaseRepository;
    @Autowired
    private OutDetailRepository outDetailRepository;
    @Autowired
    private InBaseRepository inBaseRepository;
    @Autowired
    private DictTypeService dictTypeService;
    @Autowired
    private DictDataRepository dictDataRepository;

    @Override
    public TraceInfoVO findByDishId(Integer dishId) {
        TraceInfoVO mealTraceInfo = null;
        MealDish mealDish = mealDishService.getOneDataById(dishId);
        if (mealDish != null) {
            /* 菜品信息 */
            mealTraceInfo = new TraceInfoVO();
            mealTraceInfo.setTitle(mealDish.getDishName());
            mealTraceInfo.getAttrList().add(
                    new TraceInfoVO.Attr("时间", DateUtil.format(mealDish.getMeal().getTime()
                            , DatePattern.NORM_DATE_PATTERN)));
            mealTraceInfo.getAttrList().add(new TraceInfoVO.Attr("餐次", mealDish.getMeal().getMealTimes()));
            mealTraceInfo.getAttrList().add(new TraceInfoVO.Attr("菜品名称", mealDish.getDishName()));
            mealTraceInfo.getAttrList().add(new TraceInfoVO.Attr("菜品类型",
                    dictDataService.dictNameFindByTypeCodeAndDictValue("dishClassify", mealDish.getDishType().toString())));
            mealTraceInfo.getAttrList().add(new TraceInfoVO.Attr("是否留样",
                    dictDataService.dictNameFindByTypeCodeAndDictValue("retentionSample", mealDish.getRetentionSample().toString())));
            if (mealDish.getMaterialsList().size() > 0) {
                mealTraceInfo.setNextChains(new ArrayList<>());
            }
            for (RequisitionDetail requisitionDetail : mealDish.getMaterialsList()) {
                //根据领用信息查询出库信息
                OutBase outBase = outBaseRepository.findByRecordCode(requisitionDetail.getRecordCode());
                if (outBase != null) {
                    //根据出库信息查询所有出库细表
                    List<OutDetail> outDetailList = outDetailRepository.findByBaseIdAndMaterialsCode(
                            outBase.getBaseId(), requisitionDetail.getMaterialsCode());
                    for (OutDetail outDetail : outDetailList) {
                        /* 食材信息 */
                        TraceInfoVO foodMaterials = new TraceInfoVO();
                        foodMaterials.setTitle(requisitionDetail.getMaterialsName());
                        foodMaterials.getAttrList().add(new TraceInfoVO.Attr("食材名称", requisitionDetail.getMaterialsName()));
                        foodMaterials.getAttrList().add(new TraceInfoVO.Attr("批次号", outDetail.getBatchCode()));
                        foodMaterials.getAttrList().add(new TraceInfoVO.Attr("生产日期", outDetail.getProduceDate()));
                        foodMaterials.getAttrList().add(new TraceInfoVO.Attr("有效期至", outDetail.getExpiryDate()));
                        foodMaterials.getAttrList().add(new TraceInfoVO.Attr("品牌", outDetail.getBrand()));
                        //根据食材名称查询进场信息
                        InBase inBase = inBaseRepository.findByBatchCode(outDetail.getBatchCode());
                        if (inBase != null) {
                            /* 供应商信息 */
                            TraceInfoVO supplierTraceInfo = new TraceInfoVO();
                            supplierTraceInfo.setTitle(inBase.getSupplierName());
                            supplierTraceInfo.getAttrList().add(new TraceInfoVO.Attr("统一信用代码", inBase.getSocialCreditCode()));
                            supplierTraceInfo.getAttrList().add(new TraceInfoVO.Attr("供应商名称", inBase.getSupplierName()));
                            foodMaterials.setNextChains(new ArrayList<>());
                            foodMaterials.getNextChains().add(supplierTraceInfo);
                        }
                        mealTraceInfo.getNextChains().add(foodMaterials);
                    }
                }
            }
        }
        return mealTraceInfo;
    }

    @Override
    public MealDish findDishByDishId(Integer dishId) {
        MealDish mealDish = mealDishService.getOneDataById(dishId);
        if (mealDish != null && mealDish.getMaterialsList() != null && mealDish.getMaterialsList().size() > 0) {
            /**责任人*/
            Meal meal = mealDish.getMeal();
            if (meal != null) {
                mealDish.setPersonLiable(meal.getUserName());
            }
            /** 添加剂 */
            final String additiveTypeCode = "additive";
            DictType additiveType = dictTypeService.findByTypeCode(additiveTypeCode);
            List<DictData> additiveTypes = dictDataRepository.findAll((root, criteriaQuery, criteriaBuilder) ->
                    BeanHelp.getPredicate(root, new DictData(additiveType.getTypeId()), criteriaBuilder));
            String additive = mealDish.getAdditive();
            if (StrUtil.isNotBlank(additive)) {
                String newAdditive = "";
                String[] split = additive.split(",");
                out:
                for (String s : split) {
                    for (DictData type : additiveTypes) {
                        if (s.equals(type.getDictCode().toString())) {
                            newAdditive += type.getDictName() + ",";
                            continue out;
                        }
                    }
                }
                if (newAdditive.length() > 0) {
                    newAdditive = newAdditive.substring(0, newAdditive.length() - 1);
                    mealDish.setAdditiveStr(newAdditive);
                }
            }
            /** 添加剂结束*/
            List<WeiXinTraceInfoVO> voList = new ArrayList<>();
            for (RequisitionDetail requisitionDetail : mealDish.getMaterialsList()) {
                WeiXinTraceInfoVO vo = new WeiXinTraceInfoVO();
                vo.setMaterialsName(requisitionDetail.getMaterialsName());
                //根据领用信息查询出库信息
                OutBase outBase = outBaseRepository.findByRecordCode(requisitionDetail.getRecordCode());
                if (outBase != null) {
                    //根据出库信息查询所有出库细表
                    List<OutDetail> outDetailList = outDetailRepository.findByBaseIdAndMaterialsCode(
                            outBase.getBaseId(), requisitionDetail.getMaterialsCode());
                    if (outDetailList != null && outDetailList.size() > 0) {
                        OutDetail outDetail = outDetailList.get(0);
                        vo.setBatchCode(outDetail.getBatchCode());
                        InBase inBase = inBaseRepository.findByBatchCode(outDetail.getBatchCode());
                        if (inBase != null) {
                            vo.setPurchaseDate(inBase.getPurchaseDate());
                            vo.setInMan(inBase.getInMan());
                            vo.setSocialCreditCode(inBase.getSocialCreditCode());
                            vo.setSupplierName(inBase.getSupplierName());
                        }
                    }
                }
                voList.add(vo);
            }
            mealDish.setTraceInfo(voList);
        }
        return mealDish;
    }
}
