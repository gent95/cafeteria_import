package org.dppc.cafeteria.service.stock;

import org.dppc.cafeteria.entity.stock.OutDetail;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 原材料出库信息细表业务层接口
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
public interface OutDetailService extends BaseService<OutDetail, Long> {

}
