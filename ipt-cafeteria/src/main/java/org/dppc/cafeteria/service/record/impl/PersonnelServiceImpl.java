package org.dppc.cafeteria.service.record.impl;

import org.dppc.cafeteria.entity.record.Personnel;
import org.dppc.cafeteria.repository.record.PersonnelRepository;
import org.dppc.cafeteria.service.record.PersonnelService;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description 人员业务层实现类
 * @Author GAOJ
 * @Data 2019/05/09 09:45
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class PersonnelServiceImpl extends BaseServiceImpl<Personnel, Long> implements PersonnelService {

    @Autowired
    private PersonnelRepository personnelRepository;

    @Override
    public Personnel findByIdCode(String idCode) {
        List<Personnel> list = personnelRepository.findByIdCode(idCode);
        if (list != null && list.size() > 0) {
            return list.get(0);
        } else {
            return null;
        }
    }
}
