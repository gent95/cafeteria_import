package org.dppc.cafeteria.service.machining;

import org.dppc.cafeteria.entity.machining.Meal;
import org.dppc.cafeteria.entity.machining.MealDish;
import org.dppc.cafeteria.vo.MealDishVo;
import org.dppc.dbexpand.service.BaseService;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Description 餐次业务层接口
 * @Author majt
 * @Data 2019/05/20 18:05
 * @Version 1.0
 **/
public interface MealService extends BaseService<Meal, Long> {

    void saveMealDishVo(MealDishVo mealDishVo);

    Map<String, Map<String, List<MealDish>>> findBySchoolCodeAndDate(Meal meal);

    Map<String,Set<MealDish>> queryBySchoolCodeAndDate(Meal meal);
}
