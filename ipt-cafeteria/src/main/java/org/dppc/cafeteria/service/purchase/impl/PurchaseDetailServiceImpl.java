package org.dppc.cafeteria.service.purchase.impl;

import org.dppc.cafeteria.entity.purchase.PurchaseDetail;
import org.dppc.cafeteria.service.purchase.PurchaseDetailService;
import org.dppc.cafeteria.repository.purchase.PurchaseDetailRepository;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description 采购计划明细业务层实现类
 * @Author zmm
 * @Data 2019/05/14 16:43
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class PurchaseDetailServiceImpl extends BaseServiceImpl<PurchaseDetail, Integer> implements PurchaseDetailService {

    @Autowired
    private PurchaseDetailRepository purchaseDetailRepository;

}
