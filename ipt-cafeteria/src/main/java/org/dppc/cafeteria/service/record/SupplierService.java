package org.dppc.cafeteria.service.record;

import org.dppc.cafeteria.entity.record.Supplier;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 供应商业务层接口
 * @Author GAOJ
 * @Data 2019/05/09 09:45
 * @Version 1.0
 **/
public interface SupplierService extends BaseService<Supplier, Integer> {

    Supplier findBySocialCreditCode(String socialCreditCode);
}
