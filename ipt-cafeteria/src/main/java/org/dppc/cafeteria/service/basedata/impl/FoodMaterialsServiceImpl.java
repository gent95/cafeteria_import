package org.dppc.cafeteria.service.basedata.impl;

import cn.hutool.core.date.DateUtil;
import org.dppc.cafeteria.entity.basedata.FoodMaterials;
import org.dppc.cafeteria.entity.basedata.FoodMaterialsType;
import org.dppc.cafeteria.service.basedata.FoodMaterialsService;
import org.dppc.cafeteria.repository.basedata.FoodMaterialsRepository;
import org.dppc.cafeteria.service.basedata.FoodMaterialsTypeService;
import org.dppc.cafeteria.service.syscode.SysCodeService;
import org.dppc.common.exception.MessageException;
import org.dppc.common.utils.DateHelper;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

/**
 * @Description 食材表业务层实现类
 * @Author majt
 * @Data 2019/05/09 15:42
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class FoodMaterialsServiceImpl extends BaseServiceImpl<FoodMaterials, Integer> implements FoodMaterialsService {

    @Autowired
    private FoodMaterialsRepository foodMaterialsRepository;

    @Autowired
    private FoodMaterialsTypeService foodMaterialsTypeService;

    @Autowired
    private SysCodeService sysCodeService;

    @Override
    public FoodMaterials saveFoodMaterials(FoodMaterials foodMaterials) {
        FoodMaterialsType foodMaterialsType = foodMaterialsTypeService.findOneById(foodMaterials.getTypeId());
        if (foodMaterialsType == null){
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", foodMaterials.getTypeId()));
        }
        Sort sort = new Sort(Sort.Direction.DESC,"materialsId");
        Integer materialsId = foodMaterialsRepository.findAll(sort).get(0).getMaterialsId();
        foodMaterials.setMaterialsCode(sysCodeService.getMaterialsCode(foodMaterialsType.getTypeCode()));
        foodMaterials.setCreateTime(new Date());
        return foodMaterialsRepository.save(foodMaterials);
    }
}
