package org.dppc.cafeteria.service.record;

import org.dppc.cafeteria.entity.record.PersonnelPost;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 人员职务业务层接口
 * @Author GAOJ
 * @Data 2019/05/09 19:11
 * @Version 1.0
 **/
public interface PersonnelPostService extends BaseService<PersonnelPost, Integer> {

}
