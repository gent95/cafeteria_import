package org.dppc.cafeteria.service.peican;

import org.dppc.cafeteria.entity.peican.PeiCan;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 陪餐管理业务层接口
 * @Author zhumh
 * @Data 2019/05/10 17:11
 * @Version 1.0
 **/
public interface PeiCanService extends BaseService<PeiCan, Long> {

}
