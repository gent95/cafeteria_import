package org.dppc.cafeteria.service.record;

import org.dppc.cafeteria.entity.record.Personnel;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 人员业务层接口
 * @Author GAOJ
 * @Data 2019/05/09 09:45
 * @Version 1.0
 **/
public interface PersonnelService extends BaseService<Personnel, Long> {

    Personnel findByIdCode(String idCode);

}
