package org.dppc.cafeteria.service.basedata;

import org.dppc.cafeteria.entity.basedata.DictType;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 字典表业务层接口
 * @Author majt
 * @Data 2019/05/09 10:15
 * @Version 1.0
 **/
public interface DictTypeService extends BaseService<DictType, Long> {

    /**
     * @描述 ：启用字典类型
     * @作者 ：majt
     * @日期 ：2017/11/30
     * @时间 ：15:46
     */
    DictType enableDictType(Long id);

    /**
     * @描述 ：禁用字典类型
     * @作者 ：majt
     * @日期 ：2017/11/30
     * @时间 ：15:46
     */
    DictType disableDictType(Long id);

    /**
     * @描述 ：逻辑删除字典类型
     * @作者 ：majt
     * @日期 ：2017/11/30
     * @时间 ：15:46
     */
    void delDictType(Long id);

    DictType findByTypeCode(String typeCode);
}
