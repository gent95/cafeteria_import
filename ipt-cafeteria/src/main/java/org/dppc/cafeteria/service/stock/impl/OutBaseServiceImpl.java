package org.dppc.cafeteria.service.stock.impl;

import org.apache.commons.lang3.StringUtils;
import org.dppc.cafeteria.entity.basedata.FoodMaterials;
import org.dppc.cafeteria.entity.requisition.RequisitionDetail;
import org.dppc.cafeteria.entity.requisition.RequisitionRecord;
import org.dppc.cafeteria.entity.stock.OutBase;
import org.dppc.cafeteria.entity.stock.OutDetail;
import org.dppc.cafeteria.repository.requisition.RequisitionRecordRepository;
import org.dppc.cafeteria.repository.stock.OutBaseRepository;
import org.dppc.cafeteria.repository.stock.OutDetailRepository;
import org.dppc.cafeteria.service.stock.CountService;
import org.dppc.cafeteria.service.stock.InfoService;
import org.dppc.cafeteria.service.stock.OutBaseService;
import org.dppc.cafeteria.service.stock.StockService;
import org.dppc.cafeteria.service.syscode.SysCodeService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.cafeteria.vo.OutStockVO;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.enums.RequisitionStatusEnum;
import org.dppc.common.enums.stock.OutStatusEnum;
import org.dppc.common.exception.RepeatOperateException;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @Description 原材料出库信息主表业务层实现类
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class OutBaseServiceImpl extends BaseServiceImpl<OutBase, Long> implements OutBaseService {

    @Autowired
    private OutBaseRepository outBaseRepository;
    @Autowired
    private InfoService infoService;
    @Autowired
    private CountService countService;
    @Autowired
    private StockService stockService;
    @Autowired
    private OutDetailRepository outDetailRepository;
    @Autowired
    private SysCodeService sysCodeService;
    @Autowired
    private RequisitionRecordRepository requisitionRecordRepository;

    @Override
    public void outStock(OutStockVO outStockVO) {
        OutBase outBase = outStockVO.getOutBase();
        outBase.setOutManId(StringUtils.isBlank(BaseContextHandler.getUserID()) ? null : Long.parseLong(BaseContextHandler.getUserID()));
        outBase.setOutMan(BaseContextHandler.getUsername());
        outBase.setOutStatus(OutStatusEnum.OUT_STOCK.getValue());
        //TODO 指定食堂编码
        outBase.setOutCode(sysCodeService.getOutCode(""));
        outBase.setPurchaseDate(new Date());
        //判断单据是否重复操作,如果没有则进行提交
        this.flushOutBaseValidateIsRepeatOutStock(outBase);

        List<OutDetail> outDetailList = outStockVO.getOutDetailList();
        for (OutDetail outDetail : outDetailList) {
            if(outDetail.getOutNum() > 0){
                //更新详细库存
                stockService.updateStockInfo(outDetail.getBatchCode(), outDetail.getMaterialsCode(), outDetail.getOutNum());
                //更新库存统计信息
                stockService.updateStockCount(new FoodMaterials(outDetail.getMaterialsCode()), -outDetail.getOutNum());
            }
            outDetailRepository.saveAndFlush(outDetail);
        }
        RequisitionRecord requisitionRecord = requisitionRecordRepository.findByRecordCode(outBase.getRecordCode());
        if (requisitionRecord != null) {
            requisitionRecord.setStatus(RequisitionStatusEnum.OUT_BASE.getValue());
            requisitionRecordRepository.saveAndFlush(requisitionRecord);
        }
    }
    /**
     * @Author lhw
     * @Description 出库单出库，并判断是否重复出库
     *              如果重复出库，则抛出重复操作异常（在Rest中进行异常处理）
     *                            否则进行数据更新
     * @Date 12:07 2019/5/25
     **/
    private void flushOutBaseValidateIsRepeatOutStock(OutBase outBase) {
        OutBase dbOutBase = outBaseRepository.findOne(outBase.getBaseId());
        if(dbOutBase.getOutStatus() == OutStatusEnum.OUT_STOCK.getValue()){
            throw new RepeatOperateException();
        }
        outBaseRepository.saveAndFlush(outBase);
    }

    @Override
    public void saveFromRequisitionRecord(RequisitionRecord requisitionRecord) {
        OutBase outBase = new OutBase(requisitionRecord.getRecordCode()
                , requisitionRecord.getPersonnelId()
                , requisitionRecord.getName()
                , OutStatusEnum.STAY_OUT_STOCK.getValue()
                , requisitionRecord.getCreateDate());
        CafeteriaHandleUtil.bindCafeteriaInfo(outBase);
        outBaseRepository.save(outBase);
        for (RequisitionDetail requisitionDetail : requisitionRecord.getRequisitionDetail()) {
            OutDetail outDetail = new OutDetail(outBase.getBaseId()
                    , requisitionDetail.getMaterialsName()
                    , requisitionDetail.getMaterialsCode()
                    , requisitionDetail.getQuantity()
                    , requisitionDetail.getUnit());
            CafeteriaHandleUtil.bindCafeteriaInfo(outDetail);
            outDetailRepository.save(outDetail);
        }
    }

    @Override
    public void deleteOutStockInfoByRecordCode(String recordCode) {
        //根据领用单号查询出库主表信息
        OutBase outBase = outBaseRepository.findByRecordCode(recordCode);
        if (outBase == null) {
            //根据主表id 删除细表信息
            outDetailRepository.deleteByBaseId(outBase.getBaseId());
            outBaseRepository.delete(outBase);
        }
    }
}
