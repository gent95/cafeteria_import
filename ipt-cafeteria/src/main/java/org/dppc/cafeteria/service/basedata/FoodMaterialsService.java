package org.dppc.cafeteria.service.basedata;

import org.dppc.cafeteria.entity.basedata.FoodMaterials;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 食材表业务层接口
 * @Author majt
 * @Data 2019/05/09 15:42
 * @Version 1.0
 **/
public interface FoodMaterialsService extends BaseService<FoodMaterials, Integer> {
    /**
     * @return
     * @Author majt
     * @Description 食材添加
     * @Date 2019/5/13 15:46
     * @Param
     **/
    FoodMaterials saveFoodMaterials(FoodMaterials foodMaterials);

}
