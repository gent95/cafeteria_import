package org.dppc.cafeteria.service.basedata.impl;

import org.dppc.cafeteria.entity.basedata.DictData;
import org.dppc.cafeteria.entity.basedata.DictType;
import org.dppc.cafeteria.repository.basedata.DictDataRepository;
import org.dppc.cafeteria.service.basedata.DictDataService;
import org.dppc.cafeteria.service.basedata.DictTypeService;
import org.dppc.cafeteria.vo.DictDataVo;
import org.dppc.common.enums.EnableEnum;
import org.dppc.common.exception.MessageException;
import org.dppc.common.utils.NullHelper;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.dppc.dbexpand.util.BeanHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.*;

/**
 * @Description 字典数据业务层实现类
 * @Author majt
 * @Data 2019/05/09 10:15
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class DictDataServiceImpl extends BaseServiceImpl<DictData, Long> implements DictDataService {

    @Autowired
    private DictDataRepository dictDataRepository;
    @Autowired
    private DictTypeService dictTypeService;

    @Override
    public DictData enableDictData(Long typeId) {
        DictData DictData = dictDataRepository.findOne(typeId);
        if (DictData == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", typeId));
        }
        DictData.setStatus(EnableEnum.ENABLE.getValue());
        return dictDataRepository.saveAndFlush(DictData);
    }

    @Override
    public DictData disableDictData(Long typeId) {
        DictData DictData = dictDataRepository.findOne(typeId);
        if (DictData == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", typeId));
        }
        DictData.setStatus(EnableEnum.DISABLE.getValue());
        return dictDataRepository.saveAndFlush(DictData);
    }

    @Override
    public void delDictData(Long typeId) {
        DictData DictData = dictDataRepository.findOne(typeId);
        if (DictData == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", DictData));
        }
        dictDataRepository.delete(typeId);
    }

    @Override
    public Page<DictDataVo> findDictDataPage(DictData query, Pageable pageable) {
        Page<DictData> page = dictDataRepository.findAll((root, criteriaQuery, criteriaBuilder) -> BeanHelp.getPredicate(root, query, criteriaBuilder), pageable);
        return this.pageEntityToVO(page, pageable, query);
    }

    @Override
    public String dictNameFindByTypeCodeAndDictValue(String typeCode, String dictValue) {
        DictType dictType = dictTypeService.findByTypeCode(typeCode);
        DictData dict = new DictData();
        dict.setDictTypeId(dictType.getTypeId());
        dict.setDictValue(dictValue);
        List<DictData> dictDatas = this.findList(dict);
        if(dictDatas.size() > 0){
            return dictDatas.get(0).getDictName();
        }
        return "";
    }

    private Page<DictDataVo> pageEntityToVO(Page<DictData> page, Pageable pageable, DictData query) {
        List<DictDataVo> dictDataVoList = new ArrayList<>();
        List<DictData> dictDataList = page.getContent();
        Set<Long> departmentIdSet = new HashSet<>();
        for (DictData dictData : dictDataList) {
            DictDataVo dictDataVo = DictDataVo.entityToVO(dictData);
            dictDataVoList.add(dictDataVo);
        }
        return new PageImpl<>(dictDataVoList, pageable, page.getTotalElements());
    }
}
