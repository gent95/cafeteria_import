package org.dppc.cafeteria.service.stock.impl;

import org.dppc.cafeteria.entity.stock.Info;
import org.dppc.cafeteria.repository.stock.InfoRepository;
import org.dppc.cafeteria.service.stock.InfoService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.common.enums.IsDeleteEnum;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description 库存信息表业务层实现类
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class InfoServiceImpl extends BaseServiceImpl<Info, Long> implements InfoService {

    @Autowired
    private InfoRepository infoRepository;

    @Override
    public List<Info> findCurrentStockInfo() {
        // 获取所有 未删除的库存信息
        return infoRepository.findByIsDeleteAndCafeteriaCode(IsDeleteEnum.NOT_DELETE.getValue(), CafeteriaHandleUtil.validateAuth(""));
    }
}
