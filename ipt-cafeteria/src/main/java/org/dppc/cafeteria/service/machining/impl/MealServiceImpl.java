package org.dppc.cafeteria.service.machining.impl;

import org.dppc.cafeteria.entity.basedata.DictData;
import org.dppc.cafeteria.entity.basedata.DictType;
import org.dppc.cafeteria.entity.machining.Meal;
import org.dppc.cafeteria.entity.machining.MealDish;
import org.dppc.cafeteria.repository.basedata.DictDataRepository;
import org.dppc.cafeteria.repository.machining.MealRepository;
import org.dppc.cafeteria.service.basedata.DictTypeService;
import org.dppc.cafeteria.service.machining.MealDishService;
import org.dppc.cafeteria.service.machining.MealService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.cafeteria.vo.MealDishVo;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.dppc.dbexpand.util.BeanHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description 餐次业务层实现类
 * @Author majt
 * @Data 2019/05/20 18:05
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class MealServiceImpl extends BaseServiceImpl<Meal, Long> implements MealService {

    @Autowired
    private MealRepository mealRepository;

    @Autowired
    private MealDishService mealDishService;

    @Autowired
    private DictTypeService dictTypeService;
    @Autowired
    private DictDataRepository dictDataRepository;
    /**
     * 餐次
     */
    final String mealTimesType = "mealTimesType";
    /**
     * 菜品分类
     */
    final String dishClassify = "dishClassify";

    @Override
    public void saveMealDishVo(MealDishVo mealDishVo) {
        Meal meal = mealDishVo.getMeal();
        meal.setCreateTime(new Date());
        List<MealDish> mealDishList = mealDishVo.getMealDishList();
        CafeteriaHandleUtil.bindCafeteriaInfo(meal);
        mealRepository.save(meal);

        mealDishList.stream().forEach(mealDish -> {
            System.out.println(mealDish);
            CafeteriaHandleUtil.bindCafeteriaInfo(mealDish);
            mealDishService.save(mealDish);
        });
    }

    @Override
    public Map<String, Map<String, List<MealDish>>> findBySchoolCodeAndDate(Meal query) {
        DictType mealTime = dictTypeService.findByTypeCode(mealTimesType);
        DictType dish = dictTypeService.findByTypeCode(dishClassify);
        List<DictData> mealTimes = dictDataRepository.findAll((root, criteriaQuery, criteriaBuilder) ->
                BeanHelp.getPredicate(root, new DictData(mealTime.getTypeId()), criteriaBuilder));
        List<DictData> dishs = dictDataRepository.findAll((root, criteriaQuery, criteriaBuilder) ->
                BeanHelp.getPredicate(root, new DictData(dish.getTypeId()), criteriaBuilder));
        HashMap<String, Map<String, List<MealDish>>> map = new HashMap<>(mealTimes.size());
        List<Meal> list = findList(query);
        for (DictData time : mealTimes) {
            HashMap<String, List<MealDish>> timeDish = new HashMap<>(dishs.size());
            Optional<Meal> first = list.stream().filter(a -> time.getDictName().equals(a.getMealTimes())).findFirst();
            Meal meal = null;
            if (first.isPresent()) {
                meal = first.get();
            }
            for (DictData data : dishs) {
                if (meal != null && meal.getMealDishes() != null && meal.getMealDishes().size() > 0) {
                    List<MealDish> mealDishes = meal.getMealDishes().stream().filter(a -> data.getDictCode().equals(a.getDishType())).collect(Collectors.toList());
                    timeDish.put(data.getDictName(), mealDishes);
                } else {
                    timeDish.put(data.getDictName(), Collections.emptyList());
                }
            }
            map.put(time.getDictName(), timeDish);
        }
        return map;
    }

    @Override
    public Map<String, Set<MealDish>> queryBySchoolCodeAndDate(Meal query) {
        DictType mealTime = dictTypeService.findByTypeCode(mealTimesType);
        List<DictData> mealTimes = dictDataRepository.findAll((root, criteriaQuery, criteriaBuilder) ->
                BeanHelp.getPredicate(root, new DictData(mealTime.getTypeId()), criteriaBuilder));
        HashMap<String, Set<MealDish>> map = new HashMap<>(mealTimes.size());
        List<Meal> list = findList(query);
        for (DictData time : mealTimes) {
            Optional<Meal> first = list.stream().filter(a -> time.getDictName().equals(a.getMealTimes())).findFirst();
            Meal meal = null;
            if (first.isPresent()) {
                meal = first.get();
            }
            Set<MealDish> mealDishes;
            if (meal != null && meal.getMealDishes() != null && meal.getMealDishes().size() > 0) {
                mealDishes = meal.getMealDishes();
            } else {
                mealDishes = Collections.emptySet();
            }
            map.put(time.getDictName(), mealDishes);
        }
        return map;
    }
}
