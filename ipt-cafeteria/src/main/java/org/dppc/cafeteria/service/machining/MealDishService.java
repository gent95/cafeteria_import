package org.dppc.cafeteria.service.machining;

import org.dppc.cafeteria.entity.machining.MealDish;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 菜品业务层接口
 * @Author majt
 * @Data 2019/05/20 18:05
 * @Version 1.0
 **/
public interface MealDishService extends BaseService<MealDish, Integer> {

}
