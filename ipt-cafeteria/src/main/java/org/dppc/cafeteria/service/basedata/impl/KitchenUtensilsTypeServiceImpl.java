package org.dppc.cafeteria.service.basedata.impl;

import org.dppc.cafeteria.entity.basedata.KitchenUtensilsType;
import org.dppc.cafeteria.service.basedata.KitchenUtensilsTypeService;
import org.dppc.cafeteria.repository.basedata.KitchenUtensilsTypeRepository;
import org.dppc.common.exception.MessageException;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description 业务层实现类
 * @Author majt
 * @Data 2019/06/03 19:38
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class KitchenUtensilsTypeServiceImpl extends BaseServiceImpl<KitchenUtensilsType, Long> implements KitchenUtensilsTypeService {

    @Autowired
    private KitchenUtensilsTypeRepository kitchenUtensilsTypeRepository;

    @Override
    public List<KitchenUtensilsType> tree() {
        List<KitchenUtensilsType> kitchenUtensilsTypes =  kitchenUtensilsTypeRepository.findAll();
        List<KitchenUtensilsType> rootNodes = kitchenUtensilsTypes.stream().filter(kitchenUtensilsType -> "0".equals(kitchenUtensilsType.getParentCode())).collect(Collectors.toList());

        for (KitchenUtensilsType kitchenUtensilsType:rootNodes) {
            List<KitchenUtensilsType> nodes = kitchenUtensilsTypes.stream().filter(kitchenUtensilsType1 -> kitchenUtensilsType1.getParentCode().equals(kitchenUtensilsType.getTypeCode())).collect(Collectors.toList());
            kitchenUtensilsType.setChildren(nodes);
        }
        return rootNodes;
    }

    @Override
    public KitchenUtensilsType findOneById(Long typeId) {
        KitchenUtensilsType kitchenUtensilsType = kitchenUtensilsTypeRepository.findOne(typeId);
        if (null == kitchenUtensilsType){
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", typeId));
        }
        return kitchenUtensilsType;
    }
}
