package org.dppc.cafeteria.service.machining.impl;

import org.dppc.cafeteria.entity.machining.Enviroment;
import org.dppc.cafeteria.service.machining.EnviromentService;
import org.dppc.cafeteria.repository.machining.EnviromentRepository;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description 环境业务层实现类
 * @Author majt
 * @Data 2019/05/20 11:18
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class EnviromentServiceImpl extends BaseServiceImpl<Enviroment, Integer> implements EnviromentService {

    @Autowired
    private EnviromentRepository enviromentRepository;

}
