package org.dppc.cafeteria.service.purchase.impl;

import cn.hutool.core.date.DateUtil;
import org.dppc.cafeteria.entity.purchase.PurchaseBase;
import org.dppc.cafeteria.entity.purchase.PurchaseDetail;
import org.dppc.cafeteria.entity.stock.InBase;
import org.dppc.cafeteria.entity.stock.InDetail;
import org.dppc.cafeteria.repository.purchase.PurchaseDetailRepository;
import org.dppc.cafeteria.repository.stock.InBaseRepository;
import org.dppc.cafeteria.repository.stock.InDetailRepository;
import org.dppc.cafeteria.service.purchase.PurchaseBaseService;
import org.dppc.cafeteria.repository.purchase.PurchaseBaseRepository;
import org.dppc.cafeteria.service.syscode.SysCodeService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.cafeteria.vo.PurchaseVO;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.enums.AuditStatusEnum;
import org.dppc.common.enums.stock.InStatusEnum;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description 采购计划业务层实现类
 * @Author zmm
 * @Data 2019/05/17 15:37
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class PurchaseBaseServiceImpl extends BaseServiceImpl<PurchaseBase, Integer> implements PurchaseBaseService {

    @Autowired
    private PurchaseBaseRepository purchaseBaseRepository;
    @Autowired
    private PurchaseDetailRepository purchaseDetailRepository;
    @Autowired
    private SysCodeService sysCodeService;
    @Autowired
    private InBaseRepository inBaseRepository;
    @Autowired
    private InDetailRepository inDetailRepository;

    @Override
    public void savePurchaseVO(PurchaseVO purchaseVO) {
        PurchaseBase purchaseBase = purchaseVO.getPurchaseBase();
        CafeteriaHandleUtil.bindCafeteriaInfo(purchaseBase);
        purchaseBase.setPurchaseCode(sysCodeService.getPurchaseCode(CafeteriaHandleUtil.validateAuth("")));
        purchaseBase.setCreateTime(new Date());
        purchaseBase.setAuditStatus(AuditStatusEnum.NOT_AUDIT.getValue());
        purchaseBaseRepository.save(purchaseBase);
        for (PurchaseDetail purchaseDetail : purchaseVO.getPurchaseDetailList()) {
            CafeteriaHandleUtil.bindCafeteriaInfo(purchaseDetail);
            purchaseDetail.setPurchaseId(purchaseBase.getPurchaseId());
        }
        purchaseDetailRepository.save(purchaseVO.getPurchaseDetailList());
    }

    @Override
    public void checkPurchase(Integer id, Integer check) {
        PurchaseBase purchaseBase = purchaseBaseRepository.getOne(id);
        if(check == AuditStatusEnum.THROUGH.getValue()){
            /* 自动生成进场主表信息 */
            InBase inBase = new InBase(
                    purchaseBase.getPurchaseCode(),
                    purchaseBase.getSocialCreditCode(),
                    purchaseBase.getSupplierName(),
                    DateUtil.formatDate(purchaseBase.getCreateTime()),
                    InStatusEnum.STAY_TAKE_OVER.getValue()
            );
            CafeteriaHandleUtil.bindCafeteriaInfo(inBase);
            inBaseRepository.save(inBase);
            List<PurchaseDetail> purchaseDetails = purchaseDetailRepository.findByPurchaseId(id);
            List<InDetail> inDetails = new ArrayList<>();
            for (PurchaseDetail purchaseDetail : purchaseDetails) {
                /* 自动生成进场细表信息 */
                InDetail inDetail = new InDetail(
                        inBase.getBaseId(),
                        purchaseDetail.getMaterialsName(),
                        purchaseDetail.getMaterialsCode(),
                        purchaseDetail.getNum(),
                        purchaseDetail.getUnit());
                CafeteriaHandleUtil.bindCafeteriaInfo(inDetail);
                inDetails.add(inDetail);
            }
            inDetailRepository.save(inDetails);
        }
        purchaseBase.setAuditStatus(check);
        purchaseBaseRepository.saveAndFlush(purchaseBase);
    }

    @Override
    public void patchPurchaseVO(PurchaseVO purchaseVO) {
        PurchaseBase purchaseBase = purchaseVO.getPurchaseBase();
        purchaseBaseRepository.saveAndFlush(purchaseBase);
        for (PurchaseDetail purchaseDetail : purchaseVO.getPurchaseDetailList()) {
            purchaseDetailRepository.saveAndFlush(purchaseDetail);
        }
    }
}
