package org.dppc.cafeteria.service.record;

import org.dppc.cafeteria.entity.record.UtensilType;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 器具类型业务层接口
 * @Author GAOJ
 * @Data 2019/05/09 19:11
 * @Version 1.0
 **/
public interface UtensilTypeService extends BaseService<UtensilType, Integer> {

}
