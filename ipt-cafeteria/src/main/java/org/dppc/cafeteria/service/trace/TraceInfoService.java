package org.dppc.cafeteria.service.trace;

import org.dppc.cafeteria.entity.machining.MealDish;
import org.dppc.cafeteria.vo.TraceInfoVO;

public interface TraceInfoService {
    TraceInfoVO findByDishId(Integer dishId);

    MealDish findDishByDishId(Integer dishId);
}
