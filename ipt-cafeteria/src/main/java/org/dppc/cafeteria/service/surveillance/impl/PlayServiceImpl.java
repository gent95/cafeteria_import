package org.dppc.cafeteria.service.surveillance.impl;

import org.dppc.cafeteria.entity.surveillance.Play;
import org.dppc.cafeteria.service.surveillance.PlayService;
import org.dppc.cafeteria.repository.surveillance.PlayRepository;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description 业务层实现类
 * @Author majt
 * @Data 2019/06/03 11:07
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class PlayServiceImpl extends BaseServiceImpl<Play, Long> implements PlayService {

    @Autowired
    private PlayRepository playRepository;

}
