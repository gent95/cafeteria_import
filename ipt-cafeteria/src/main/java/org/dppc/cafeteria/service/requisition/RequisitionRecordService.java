package org.dppc.cafeteria.service.requisition;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.dppc.cafeteria.entity.requisition.RequisitionRecord;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 领用单业务层接口
 * @Author GAOJ
 * @Data 2019/05/09 09:49
 * @Version 1.0
 **/
public interface RequisitionRecordService extends BaseService<RequisitionRecord, Long> {

    void saveNew(RequisitionRecord requisitionRecord);

    HSSFWorkbook exportExcelRequisitionRecord(Long id);

}
