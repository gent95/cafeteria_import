package org.dppc.cafeteria.service.stock;

import org.dppc.cafeteria.entity.stock.InBase;
import org.dppc.cafeteria.vo.InStockVO;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 原材料入库信息主表业务层接口
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
public interface InBaseService extends BaseService<InBase, Long> {
    /**
     * @Author lhw
     * @Description 收货
     * @Date 12:37 2019/5/11
     **/
    void takeOver(InStockVO inStockVO);
}
