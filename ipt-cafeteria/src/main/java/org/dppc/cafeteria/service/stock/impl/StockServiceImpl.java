package org.dppc.cafeteria.service.stock.impl;

import org.dppc.cafeteria.entity.basedata.FoodMaterials;
import org.dppc.cafeteria.entity.stock.Count;
import org.dppc.cafeteria.entity.stock.Info;
import org.dppc.cafeteria.repository.basedata.FoodMaterialsRepository;
import org.dppc.cafeteria.repository.stock.CountRepository;
import org.dppc.cafeteria.repository.stock.InfoRepository;
import org.dppc.cafeteria.service.stock.StockService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.cafeteria.utils.StockUtil;
import org.dppc.common.enums.IsDeleteEnum;
import org.dppc.common.exception.StockNumberException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description 库存公共业务逻辑层实现类
 * 用于增减库存操作
 * @Author lhw
 * @Data 2019/5/24 15:46
 * @Version 1.0
 **/
@Service
public class StockServiceImpl implements StockService {
    @Autowired
    private InfoRepository infoRepository;
    @Autowired
    private CountRepository countRepository;
    @Autowired
    private FoodMaterialsRepository foodMaterialsRepository;

    @Override
    public synchronized void updateStockInfo(String batchCode, String materialsCode, Double outNum) {
        //根据 进货批次码和产品编码获取库存信息
        Info info = infoRepository.findByBatchCodeAndMaterialsCode(batchCode, materialsCode);

        if(info != null){
            double surplusNum = info.getNum() - outNum;
            if (surplusNum >= 0) {
                info.setNum(surplusNum);
                info.setIsDelete(surplusNum == 0 ? IsDeleteEnum.DELETED.getValue() : IsDeleteEnum.NOT_DELETE.getValue());
                infoRepository.saveAndFlush(info);
            } else {
                throw new StockNumberException(batchCode,info.getMaterialsName(),info.getNum());
            }
        }else {
            FoodMaterials foodMaterials = foodMaterialsRepository.findByMaterialsCode(materialsCode);
            //根据商品编码查询商品信息
            throw new StockNumberException(foodMaterials != null ? foodMaterials.getMaterialsName() : "--系统未录入商品--");
        }
    }

    @Override
    public synchronized void updateStockCount(FoodMaterials foodMaterials, Double num) {
        /* 库存如果存在，则更新库存，否则新增库存 */
        String materialsCode = foodMaterials.getMaterialsCode();
        Count count = countRepository.findByMaterialsCodeAndCafeteriaCode(materialsCode,CafeteriaHandleUtil.validateAuth(""));
        if (count != null) {
            double sum = count.getNum() + num;
            if (sum >= 0) {
                count.setNum(sum);
            } else {
                throw new StockNumberException(null,count.getMaterialsName(),count.getNum());
            }
        } else if (num > 0) {
            count = new Count();
            CafeteriaHandleUtil.bindCafeteriaInfo(count);
            count.setMaterialsName(foodMaterials.getMaterialsName());
            count.setMaterialsCode(materialsCode);
            count.setNum(num);
            count.setBigType(StockUtil.gainBigType(materialsCode));
            count.setUnit(foodMaterials.getUnit());
        } else {
            //根据商品编码查询商品信息
            throw new StockNumberException(foodMaterials.getMaterialsName());
        }
        countRepository.saveAndFlush(count);
    }
}
