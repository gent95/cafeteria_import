package org.dppc.cafeteria.service.machining;

import org.dppc.cafeteria.entity.machining.Enviroment;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 环境业务层接口
 * @Author majt
 * @Data 2019/05/20 11:18
 * @Version 1.0
 **/
public interface EnviromentService extends BaseService<Enviroment, Integer> {

}
