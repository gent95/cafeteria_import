package org.dppc.cafeteria.service.emergency.impl;

import org.dppc.cafeteria.entity.emergency.EmergencyType;
import org.dppc.cafeteria.repository.emergency.EmergencyTypeRepository;
import org.dppc.cafeteria.service.emergency.EmergencyTypeService;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description 应急事件类型业务层实现类
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class EmergencyTypeServiceImpl extends BaseServiceImpl<EmergencyType, Integer> implements EmergencyTypeService {

    @Autowired
    private EmergencyTypeRepository emergencyTypeRepository;

}
