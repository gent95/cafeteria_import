package org.dppc.cafeteria.service.basedata.impl;

import org.dppc.cafeteria.entity.basedata.FoodMaterialsType;
import org.dppc.cafeteria.repository.basedata.FoodMaterialsTypeRepository;
import org.dppc.cafeteria.service.basedata.FoodMaterialsTypeService;
import org.dppc.common.entity.Page;
import org.dppc.common.exception.MessageException;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.dppc.dbexpand.util.BeanHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description 食材类型表业务层实现类
 * @Author majt
 * @Data 2019/05/09 15:42
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class FoodMaterialsTypeServiceImpl extends BaseServiceImpl<FoodMaterialsType, Integer> implements FoodMaterialsTypeService {

    @Autowired
    private FoodMaterialsTypeRepository foodMaterialsTypeRepository;

    @Override
    public List<FoodMaterialsType> tree() {
       List<FoodMaterialsType> foodMaterialsTypes =  foodMaterialsTypeRepository.findAll();
       List<FoodMaterialsType> rootNodes = foodMaterialsTypes.stream().filter(foodMaterialsType -> "0".equals(foodMaterialsType.getParentCode())).collect(Collectors.toList());

       for (FoodMaterialsType foodMaterialsType:rootNodes) {
            List<FoodMaterialsType> nodes = foodMaterialsTypes.stream().filter(foodMaterialsType1 -> foodMaterialsType1.getParentCode().equals(foodMaterialsType.getTypeCode())).collect(Collectors.toList());
            foodMaterialsType.setChildren(nodes);
       }
        return rootNodes;
    }

    @Override
    public FoodMaterialsType findOneById(Integer typeId) {
        FoodMaterialsType foodMaterialsType = foodMaterialsTypeRepository.findOne(typeId);
        if (null == foodMaterialsType){
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", typeId));
        }
        return foodMaterialsType;
    }

    @Override
    public Page<FoodMaterialsType> findFoodMaterialsTypePage(FoodMaterialsType query, Pageable pageable) {
        Page<FoodMaterialsType> page = (Page<FoodMaterialsType>) foodMaterialsTypeRepository.findAll((root, criteriaQuery, criteriaBuilder) -> BeanHelp.getPredicate(root, query, criteriaBuilder), pageable);
        return page;
    }
}
