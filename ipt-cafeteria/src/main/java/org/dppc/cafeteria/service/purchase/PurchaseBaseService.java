package org.dppc.cafeteria.service.purchase;

import org.dppc.cafeteria.entity.purchase.PurchaseBase;
import org.dppc.cafeteria.vo.PurchaseVO;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 采购计划业务层接口
 * @Author zmm
 * @Data 2019/05/17 15:37
 * @Version 1.0
 **/
public interface PurchaseBaseService extends BaseService<PurchaseBase, Integer> {

    void savePurchaseVO(PurchaseVO purchaseVO);

    void checkPurchase(Integer id, Integer check);

    void patchPurchaseVO(PurchaseVO purchaseVO);
}
