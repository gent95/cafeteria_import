package org.dppc.cafeteria.service.stock;

import org.dppc.cafeteria.entity.basedata.FoodMaterials;

/**
 * @Description 库存公共业务逻辑层接口
 *              将更新库存接口统一管理，便于使用事务，事务统一交由调用方管理
 * @Author lhw
 * @Data 2019/5/24 15:45
 * @Version 1.0
 **/
public interface StockService {
    /**
     * @Author lhw
     * @Description 更新库存详情信息
     * @Date 15:45 2019/5/13
     **/
    void updateStockInfo(String batchCode, String materialsCode, Double outNum);
    /**
     * @Author lhw
     * @Description 更新库存统计信息
     * @Date 19:00 2019/5/11
     * @param materialsCode 商品名称
     * @param num 数量（加为 + ，减为 -）
     * @return void
     **/
    void updateStockCount(FoodMaterials materialsCode, Double num);
}
