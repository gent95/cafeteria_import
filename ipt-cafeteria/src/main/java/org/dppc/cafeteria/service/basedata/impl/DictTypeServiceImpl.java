package org.dppc.cafeteria.service.basedata.impl;

import org.dppc.cafeteria.entity.basedata.DictType;
import org.dppc.cafeteria.service.basedata.DictTypeService;
import org.dppc.cafeteria.repository.basedata.DictTypeRepository;
import org.dppc.common.enums.EnableEnum;
import org.dppc.common.exception.MessageException;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.MessageFormat;

/**
 * @Description 字典表业务层实现类
 * @Author majt
 * @Data 2019/05/09 10:15
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class DictTypeServiceImpl extends BaseServiceImpl<DictType, Long> implements DictTypeService {

    @Autowired
    private DictTypeRepository dictTypeRepository;

    @Override
    public DictType enableDictType(Long typeId) {
        DictType dictType = dictTypeRepository.findOne(typeId);
        if (dictType == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", typeId));
        }
        dictType.setStatus(EnableEnum.ENABLE.getValue());
        return dictTypeRepository.saveAndFlush(dictType);
    }

    @Override
    public DictType disableDictType(Long typeId) {
        DictType dictType = dictTypeRepository.findOne(typeId);
        if (dictType == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", typeId));
        }
        dictType.setStatus(EnableEnum.DISABLE.getValue());
        return dictTypeRepository.saveAndFlush(dictType);
    }

    @Override
    public void delDictType(Long typeId) {
        DictType dictType = dictTypeRepository.findOne(typeId);
        if (dictType == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", dictType));
        }
        dictTypeRepository.delete(typeId);
    }

    @Override
    public DictType findByTypeCode(String typeCode) {
        return dictTypeRepository.findByTypeCode(typeCode);
    }
}
