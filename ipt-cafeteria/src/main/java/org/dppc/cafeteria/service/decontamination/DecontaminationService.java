package org.dppc.cafeteria.service.decontamination;

import org.dppc.cafeteria.entity.decontamination.Decontamination;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 洗消管理业务层接口
 * @Author zhumh
 * @Data 2019/05/10 17:12
 * @Version 1.0
 **/
public interface DecontaminationService extends BaseService<Decontamination, Long> {

}
