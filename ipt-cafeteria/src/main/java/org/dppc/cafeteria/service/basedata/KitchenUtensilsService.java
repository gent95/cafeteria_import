package org.dppc.cafeteria.service.basedata;

import org.dppc.cafeteria.entity.basedata.KitchenUtensils;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 业务层接口
 * @Author majt
 * @Data 2019/06/03 19:38
 * @Version 1.0
 **/
public interface KitchenUtensilsService extends BaseService<KitchenUtensils, Long> {
    /**
     * @return
     * @Author majt
     * @Description 器材添加
     * @Date 2019/5/13 15:46
     * @Param
     **/
    KitchenUtensils saveKitchenUtensils(KitchenUtensils kitchenUtensils);
}
