package org.dppc.cafeteria.service.record;

import org.dppc.cafeteria.entity.record.SupplierType;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 供应商类型业务层接口
 * @Author GAOJ
 * @Data 2019/05/09 19:11
 * @Version 1.0
 **/
public interface SupplierTypeService extends BaseService<SupplierType, Integer> {

}
