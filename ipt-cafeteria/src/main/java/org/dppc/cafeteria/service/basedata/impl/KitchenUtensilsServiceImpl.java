package org.dppc.cafeteria.service.basedata.impl;

import org.dppc.cafeteria.entity.basedata.KitchenUtensils;
import org.dppc.cafeteria.entity.basedata.KitchenUtensilsType;
import org.dppc.cafeteria.repository.basedata.KitchenUtensilsRepository;
import org.dppc.cafeteria.service.basedata.KitchenUtensilsService;
import org.dppc.cafeteria.service.basedata.KitchenUtensilsTypeService;
import org.dppc.cafeteria.service.syscode.SysCodeService;
import org.dppc.common.exception.MessageException;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.Date;

/**
 * @Description 业务层实现类
 * @Author majt
 * @Data 2019/06/03 19:38
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class KitchenUtensilsServiceImpl extends BaseServiceImpl<KitchenUtensils, Long> implements KitchenUtensilsService {

    @Autowired
    private KitchenUtensilsRepository kitchenUtensilsRepository;

    @Autowired
    private KitchenUtensilsTypeService kitchenUtensilsTypeService;

    @Autowired
    private SysCodeService sysCodeService;

    @Override
    public KitchenUtensils saveKitchenUtensils(KitchenUtensils kitchenUtensils) {
        KitchenUtensilsType kitchenUtensilsType = kitchenUtensilsTypeService.findOneById(kitchenUtensils.getTypeId());
        if (kitchenUtensilsType == null){
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", kitchenUtensils.getTypeId()));
        }
        Sort sort = new Sort(Sort.Direction.DESC,"utensilsId");
        Long utensilsId = kitchenUtensilsRepository.findAll(sort).get(0).getUtensilsId();
        kitchenUtensils.setUtensilsCode(sysCodeService.getKitchenUtensilsCode(kitchenUtensilsType.getTypeCode()));
        kitchenUtensils.setCreateTime(new Date());
        return kitchenUtensilsRepository.save(kitchenUtensils);
    }
}
