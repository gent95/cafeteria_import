package org.dppc.cafeteria.service.stock;

import org.dppc.cafeteria.entity.requisition.RequisitionRecord;
import org.dppc.cafeteria.entity.stock.OutBase;
import org.dppc.cafeteria.vo.OutStockVO;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 原材料出库信息主表业务层接口
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
public interface OutBaseService extends BaseService<OutBase, Long> {
    /**
     * @Author lhw
     * @Description 出库
     * @Date 15:15 2019/5/13
     **/
    void outStock(OutStockVO outStockVO);
    /**
     * @Author gaoj
     * @Description 领用单转存出库信息
     * @Date 15:15 2019/5/13
     **/
    void saveFromRequisitionRecord(RequisitionRecord requisitionRecord);
    /**
     * @Author lhw
     * @Description 根据领用单号删除相应出库信息
     * @Date 10:56 2019/5/25
     * @Param recordCode 领用单号
     **/
    void deleteOutStockInfoByRecordCode(String recordCode);
}
