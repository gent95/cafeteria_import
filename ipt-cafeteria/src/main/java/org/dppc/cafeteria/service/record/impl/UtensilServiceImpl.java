package org.dppc.cafeteria.service.record.impl;

import org.dppc.cafeteria.entity.record.Utensil;
import org.dppc.cafeteria.repository.record.UtensilRepository;
import org.dppc.cafeteria.service.record.UtensilService;
import org.dppc.common.enums.AuditStatusEnum;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description 器具业务层实现类
 * @Author GAOJ
 * @Data 2019/05/09 09:45
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class UtensilServiceImpl extends BaseServiceImpl<Utensil, Long> implements UtensilService {

    @Autowired
    private UtensilRepository utensilRepository;

    @Override
    public List<Utensil> findByTypeAndAuditStatus(Integer type, Integer auditStatus) {
        return utensilRepository.findByTypeAndAuditStatus(type,auditStatus);
    }

    @Override
    public List<Utensil> findPlayList() {
        return findByTypeAndAuditStatus(7, AuditStatusEnum.THROUGH.getValue());
    }
}
