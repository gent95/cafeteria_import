package org.dppc.cafeteria.service.peican.impl;

import org.dppc.cafeteria.entity.peican.PeiCan;
import org.dppc.cafeteria.repository.peican.PeiCanRepository;
import org.dppc.cafeteria.service.peican.PeiCanService;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description 陪餐管理业务层实现类
 * @Author zhumh
 * @Data 2019/05/10 17:11
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class PeiCanServiceImpl extends BaseServiceImpl<PeiCan, Long> implements PeiCanService {

    @Autowired
    private PeiCanRepository peiCanRepository;

}
