package org.dppc.cafeteria.service.purchase;

import org.dppc.cafeteria.entity.purchase.PurchaseDetail;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 采购计划明细业务层接口
 * @Author zmm
 * @Data 2019/05/14 16:43
 * @Version 1.0
 **/
public interface PurchaseDetailService extends BaseService<PurchaseDetail, Integer> {

}
