package org.dppc.cafeteria.service.syscode;

/**
 * @Description 业务层接口
 * @Author GAOJ
 * @Data 2019/05/14 14:00
 * @Version 1.0
 **/
public interface SysCodeService {

    /** 生成领用单编码*/
    String getRequisitionRecordCode();

    /** 生成器具备案编码*/
    String getUtensilCode();

    /** 生成食材编码*/
    String getMaterialsCode(String prefix);
    /**
     * @Author lhw
     * @Description 生成食材进货批次码
     * @Date 9:34 2019/5/22
     **/
    String getBatchCode(String cafeteriaCode);
    /**
     * @Author lhw
     * @Description 获取出库编码
     * @Date 8:32 2019/5/23
     **/
    String getOutCode(String cafeteriaCode);
    /**
     * @Author lhw
     * @Description 获取盘库编码
     * @Date 8:32 2019/5/23
     **/
    String getInventoryCode(String cafeteriaCode);
    /**
     * @Author lhw
     * @Description 获取盘库编码
     * @Date 8:32 2019/5/23
     **/
    String getPurchaseCode(String cafeteriaCode);

    /**
     * @Author lhw
     * @Description 获取设备厨具编码
     * @Date 2019/6/4 9:47
     **/
    String getKitchenUtensilsCode(String typeCode);
}
