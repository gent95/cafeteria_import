package org.dppc.cafeteria.service.basedata;

import org.dppc.cafeteria.entity.basedata.FoodMaterialsType;
import org.dppc.common.entity.Page;
import org.dppc.dbexpand.service.BaseService;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @Description 食材类型表业务层接口
 * @Author majt
 * @Data 2019/05/09 15:42
 * @Version 1.0
 **/
public interface FoodMaterialsTypeService extends BaseService<FoodMaterialsType, Integer> {

    /**
     * @return
     * @Author majt
     * @Description 查询食材类目树
     * @Date 2019/5/11 11:40
     * @Param
     **/
    List<FoodMaterialsType> tree();

    /**
     * @return
     * @Author majt
     * @Description 根据主键id查询食材类型
     * @Date 2019/5/13 10:26
     * @Param
     **/
    FoodMaterialsType findOneById(Integer typeId);

    /**
     * @return
     * @Author majt
     * @Description 查询食材分页
     * @Date 2019/5/13 10:26
     * @Param
     **/
    Page<FoodMaterialsType> findFoodMaterialsTypePage(FoodMaterialsType foodMaterialsType,Pageable pageable);
}
