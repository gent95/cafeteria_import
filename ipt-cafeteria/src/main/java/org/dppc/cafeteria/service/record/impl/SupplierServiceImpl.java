package org.dppc.cafeteria.service.record.impl;

import org.dppc.cafeteria.entity.record.Supplier;
import org.dppc.cafeteria.repository.record.SupplierRepository;
import org.dppc.cafeteria.service.record.SupplierService;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description 供应商业务层实现类
 * @Author GAOJ
 * @Data 2019/05/09 09:45
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class SupplierServiceImpl extends BaseServiceImpl<Supplier, Integer> implements SupplierService {

    @Autowired
    private SupplierRepository supplierRepository;

    @Override
    public Supplier findBySocialCreditCode(String socialCreditCode) {
        List<Supplier> list = supplierRepository.findBySocialCreditCode(socialCreditCode);
        if (list != null && list.size() > 0) {
            return list.get(0);
        } else {
            return null;
        }
    }
}
