package org.dppc.cafeteria.service.stock;

import org.dppc.cafeteria.entity.stock.InventoryBase;
import org.dppc.cafeteria.vo.InventoryVO;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 盘货信息主表业务层接口
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
public interface InventoryBaseService extends BaseService<InventoryBase, Long> {
    /**
     * @Author lhw
     * @Description 根据库存信息生成盘库信息
     * @Date 16:24 2019/5/14
     **/
    InventoryVO createInventoryInfo();
    /**
     * @Author lhw
     * @Description 编辑库存信息
     * @Date 19:42 2019/5/14
     **/
    void inventoryEdit(InventoryVO inventoryVO);
}
