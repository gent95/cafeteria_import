package org.dppc.cafeteria.service.decontamination.impl;

import org.dppc.cafeteria.entity.decontamination.Decontamination;
import org.dppc.cafeteria.repository.decontamination.DecontaminationRepository;
import org.dppc.cafeteria.service.decontamination.DecontaminationService;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description 洗消管理业务层实现类
 * @Author zhumh
 * @Data 2019/05/10 17:12
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class DecontaminationServiceImpl extends BaseServiceImpl<Decontamination, Long> implements DecontaminationService {

    @Autowired
    private DecontaminationRepository decontaminationRepository;

}
