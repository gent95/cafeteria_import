package org.dppc.cafeteria.service.machining.impl;

import org.dppc.cafeteria.entity.machining.MealDish;
import org.dppc.cafeteria.service.machining.MealDishService;
import org.dppc.cafeteria.repository.machining.MealDishRepository;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description 菜品业务层实现类
 * @Author majt
 * @Data 2019/05/20 18:05
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class MealDishServiceImpl extends BaseServiceImpl<MealDish, Integer> implements MealDishService {

    @Autowired
    private MealDishRepository mealDishRepository;

}
