package org.dppc.cafeteria.service.surveillance;

import org.dppc.cafeteria.entity.record.Utensil;
import org.dppc.cafeteria.entity.surveillance.SensorData;
import org.dppc.cafeteria.vo.SensorInfoVO;
import org.dppc.dbexpand.service.BaseService;

import java.util.List;

/**
 * @Description 传感器数据业务层接口
 * @Author lhw
 * @Data 2019/06/04 16:07
 * @Version 1.0
 **/
public interface SensorDataService extends BaseService<SensorData, Long> {

    List<SensorInfoVO> findCurrenSensorInfoVO(Utensil utensil);
}
