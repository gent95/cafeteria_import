package org.dppc.cafeteria.service.record;

import org.dppc.cafeteria.entity.record.Utensil;
import org.dppc.dbexpand.service.BaseService;

import java.util.List;

/**
 * @Description 器具业务层接口
 * @Author GAOJ
 * @Data 2019/05/09 09:45
 * @Version 1.0
 **/
public interface UtensilService extends BaseService<Utensil, Long> {
    /**
     * @return
     * @Author majt
     * @Description 通过器具类型和审核状态查询
     * @Date 2019/6/5 10:42
     * @Param
     **/
    List<Utensil> findByTypeAndAuditStatus(Integer type,Integer auditStatus);

    /**
     * @return
     * @Author majt
     * @Description 获取所有直播设备信息
     * @Date 2019/6/5 14:13
     * @Param
     **/
    List<Utensil> findPlayList();
}
