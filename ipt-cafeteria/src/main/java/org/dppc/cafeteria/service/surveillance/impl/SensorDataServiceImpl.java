package org.dppc.cafeteria.service.surveillance.impl;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.StringUtil;
import org.dppc.cafeteria.entity.record.Utensil;
import org.dppc.cafeteria.entity.surveillance.SensorData;
import org.dppc.cafeteria.repository.surveillance.SensorDataRepository;
import org.dppc.cafeteria.service.record.UtensilService;
import org.dppc.cafeteria.service.surveillance.SensorDataService;
import org.dppc.cafeteria.vo.SensorInfoVO;
import org.dppc.common.enums.SensorStatusEnum;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description 传感器数据业务层实现类
 * @Author lhw
 * @Data 2019/06/04 16:07
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class SensorDataServiceImpl extends BaseServiceImpl<SensorData, Long> implements SensorDataService {

    @Autowired
    private SensorDataRepository sensorDataRepository;
    @Autowired
    private UtensilService utensilService;
    @Value("${sensor.intervalTime}")
    private String intervalTime;


    @Override
    public List<SensorInfoVO> findCurrenSensorInfoVO(Utensil utensil) {
        List<Utensil> utensils = utensilService.findList(utensil);
        List<SensorInfoVO> list = new ArrayList<>();
        for (Utensil item : utensils) {
            String utensilCode = item.getDeviceCode();
            if (utensilCode == null) {
                continue;
            }
            //根据传感器编码查询最后上传的数据
            List<SensorData> sensorDataList = sensorDataRepository.findCurrenInfo(Integer.parseInt(utensilCode));
            SensorInfoVO sensorInfoVO = new SensorInfoVO();
            sensorInfoVO.setDeviceDesc(item.getUses());
            sensorInfoVO.setDeviceId(Integer.parseInt(utensilCode));
            String jsonStr = item.getParamJson();
            //{"minTemp":"","maxTemp":"","minHum":"","maxHum":""}
            //jsonStr = "{\"minTemp\":\"\",\"maxTemp\":\"\",\"minHum\":\"\",\"maxHum\":\"\"}";
            if (StringUtils.isNotBlank(jsonStr)) {
                JSONObject object = JSONUtil.parseObj(jsonStr);
                sensorInfoVO.setMaxTemp(gainJsonObjectFloat(object,"maxTemp"));
                sensorInfoVO.setMinTemp(gainJsonObjectFloat(object,"minTemp"));
                sensorInfoVO.setMaxHum(gainJsonObjectFloat(object,"maxHum"));
                sensorInfoVO.setMinHum(gainJsonObjectFloat(object,"minHum"));
            }else {
                sensorInfoVO.setMaxTemp(null);
                sensorInfoVO.setMinTemp(null);
                sensorInfoVO.setMaxHum(null);
                sensorInfoVO.setMinHum(null);
            }
            sensorInfoVO.setIntervalTime(intervalTime);
            sensorInfoVO.setSchoolCode(item.getSchoolCode());
            sensorInfoVO.setSchoolName(item.getSchoolName());
            sensorInfoVO.setCafeteriaCode(item.getCafeteriaCode());
            sensorInfoVO.setCafeteriaName(item.getCafeteriaName());
            if (sensorDataList.size() > 0) {
                String tems = "";
                String hums = "";
                for (SensorData sensorData : sensorDataList) {
                    tems = tems + sensorData.getTem() + ",";
                    hums = hums +  sensorData.getHum() + ",";
                }
                sensorInfoVO.setTemps(tems.substring(0,tems.length()-1));
                sensorInfoVO.setHums(hums.substring(0,tems.length()-1));
                sensorInfoVO.setRecordTime(sensorDataList.get(0).getRecordTime());
                sensorInfoVO.setStatus(sensorInfoVO.gainStatus());
            }else {
                sensorInfoVO.setTemps(null);
                sensorInfoVO.setHums(null);
                sensorInfoVO.setRecordTime(null);
                sensorInfoVO.setStatus(SensorStatusEnum.STOP.getValue());
            }
            list.add(sensorInfoVO);
        }
        return list;
    }

    private Float gainJsonObjectFloat(JSONObject object,String key) {
        Object obj = object.get(key);
        if(obj != null) {
            try {
                Float s = Float.parseFloat(obj.toString());
                return s;
            } catch (NumberFormatException e) {
                return null;
            }
        }
        return null;
    }
}
