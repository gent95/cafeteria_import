package org.dppc.cafeteria.service.stock;

import org.dppc.cafeteria.entity.stock.InventoryDetail;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 盘库详情表业务层接口
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
public interface InventoryDetailService extends BaseService<InventoryDetail, Long> {

}
