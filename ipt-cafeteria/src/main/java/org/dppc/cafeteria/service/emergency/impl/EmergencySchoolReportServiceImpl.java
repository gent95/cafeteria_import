package org.dppc.cafeteria.service.emergency.impl;

import org.dppc.cafeteria.entity.emergency.EmergencySchoolReport;
import org.dppc.cafeteria.repository.emergency.EmergencySchoolReportRepository;
import org.dppc.cafeteria.service.emergency.EmergencySchoolReportService;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description 学校应急事件上报（学校直通车）业务层实现类
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class EmergencySchoolReportServiceImpl extends BaseServiceImpl<EmergencySchoolReport, Long> implements EmergencySchoolReportService {

    @Autowired
    private EmergencySchoolReportRepository emergencySchoolReportRepository;

}
