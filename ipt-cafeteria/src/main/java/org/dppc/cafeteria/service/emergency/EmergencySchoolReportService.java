package org.dppc.cafeteria.service.emergency;

import org.dppc.cafeteria.entity.emergency.EmergencySchoolReport;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 学校应急事件上报（学校直通车）业务层接口
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
public interface EmergencySchoolReportService extends BaseService<EmergencySchoolReport, Long> {

}
