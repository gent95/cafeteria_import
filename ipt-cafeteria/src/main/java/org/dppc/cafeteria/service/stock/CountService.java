package org.dppc.cafeteria.service.stock;

import org.dppc.cafeteria.entity.stock.Count;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 库存统计信息表业务层接口
 * @Author lhw
 * @Data 2019/05/09 15:22
 * @Version 1.0
 **/
public interface CountService extends BaseService<Count, Long> {
    /**
     * @Author lhw
     * @Description 统计库存数量
     * @Date 17:21 2019/5/31
     **/
    Double statFoodMaterialsNum(String materialsCode);
}
