package org.dppc.cafeteria.service.stock.impl;
import java.util.Date;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import org.dppc.cafeteria.entity.stock.Info;
import org.dppc.cafeteria.entity.stock.InventoryBase;
import org.dppc.cafeteria.entity.stock.InventoryDetail;
import org.dppc.cafeteria.repository.stock.InventoryDetailRepository;
import org.dppc.cafeteria.service.stock.InfoService;
import org.dppc.cafeteria.service.stock.InventoryBaseService;
import org.dppc.cafeteria.repository.stock.InventoryBaseRepository;
import org.dppc.cafeteria.service.syscode.SysCodeService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.cafeteria.vo.InventoryVO;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description 盘货信息主表业务层实现类
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class InventoryBaseServiceImpl extends BaseServiceImpl<InventoryBase, Long> implements InventoryBaseService {

    @Autowired
    private InventoryBaseRepository inventoryBaseRepository;
    @Autowired
    private InventoryDetailRepository inventoryDetailRepository;
    @Autowired
    private InfoService infoService;
    @Autowired
    private SysCodeService sysCodeService;

    @Override
    public InventoryVO createInventoryInfo() {
        //创建盘库主表信息
        InventoryBase inventoryBase = new InventoryBase();
        inventoryBase.setInventoryDay(DateUtil.format(new Date(), DatePattern.NORM_DATE_FORMAT));
        inventoryBase.setInventoryCode(sysCodeService.getInventoryCode(CafeteriaHandleUtil.validateAuth("")));
        inventoryBase.setCreateDate(new Date());
        CafeteriaHandleUtil.bindCafeteriaInfo(inventoryBase);
        inventoryBaseRepository.save(inventoryBase);
        //查询当前库存
        List<Info> infoList = infoService.findCurrentStockInfo();
        List<InventoryDetail> inventoryDetails = new ArrayList<>();
        for (Info info : infoList) {
            InventoryDetail inventoryDetail = inventoryDetailValueOf(info);
            inventoryDetail.setBaseId(inventoryBase.getBaseId());
            CafeteriaHandleUtil.bindCafeteriaInfo(inventoryDetail);
            inventoryDetails.add(inventoryDetail);
        }
        inventoryDetailRepository.save(inventoryDetails);
        InventoryVO inventoryVO = new InventoryVO();
        inventoryVO.setInventoryBase(inventoryBase);
        inventoryVO.setInventoryDetailList(inventoryDetails);
        return inventoryVO;
    }

    @Override
    public void inventoryEdit(InventoryVO inventoryVO) {
        InventoryBase inventoryBase = inventoryVO.getInventoryBase();
        List<InventoryDetail> inventoryDetails = inventoryVO.getInventoryDetailList();
        inventoryBaseRepository.saveAndFlush(inventoryBase);
        inventoryDetails.forEach(inventoryDetail -> inventoryDetailRepository.saveAndFlush(inventoryDetail));
    }

    /**
     * @Author lhw
     * @Description 将库存信息转换为盘库信息
     * @Date 16:51 2019/5/14
     **/
    private InventoryDetail inventoryDetailValueOf(Info info) {
        InventoryDetail inventoryDetail = new InventoryDetail();
        inventoryDetail.setMaterialsName(info.getMaterialsName());
        inventoryDetail.setMaterialsCode(info.getMaterialsCode());
        inventoryDetail.setCurrentNum(info.getNum());
        inventoryDetail.setBatchCode(info.getBatchCode());
        return inventoryDetail;
    }
}
