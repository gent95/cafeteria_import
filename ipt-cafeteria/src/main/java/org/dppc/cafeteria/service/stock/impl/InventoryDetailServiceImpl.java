package org.dppc.cafeteria.service.stock.impl;

import org.dppc.cafeteria.entity.stock.InventoryDetail;
import org.dppc.cafeteria.service.stock.InventoryDetailService;
import org.dppc.cafeteria.repository.stock.InventoryDetailRepository;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description 盘库详情表业务层实现类
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class InventoryDetailServiceImpl extends BaseServiceImpl<InventoryDetail, Long> implements InventoryDetailService {

    @Autowired
    private InventoryDetailRepository inventoryDetailRepository;

}
