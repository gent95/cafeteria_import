package org.dppc.cafeteria.service.requisition;

import org.dppc.cafeteria.entity.requisition.RequisitionDetail;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 领用单明细业务层接口
 * @Author GAOJ
 * @Data 2019/05/09 09:49
 * @Version 1.0
 **/
public interface RequisitionDetailService extends BaseService<RequisitionDetail, Long> {

}
