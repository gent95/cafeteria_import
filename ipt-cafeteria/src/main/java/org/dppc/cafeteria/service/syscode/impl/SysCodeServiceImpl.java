package org.dppc.cafeteria.service.syscode.impl;

import cn.hutool.core.date.DateUtil;
import org.dppc.cafeteria.entity.syscode.SysCode;
import org.dppc.cafeteria.repository.syscode.SysCodeRepository;
import org.dppc.cafeteria.service.syscode.SysCodeService;
import org.dppc.common.utils.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @Description 业务层实现类
 * @Author GAOJ
 * @Data 2019/05/14 14:00
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class SysCodeServiceImpl implements SysCodeService {

    @Autowired
    private SysCodeRepository sysCodeRepository;

    public void put(SysCode sysCode) {
        sysCodeRepository.saveAndFlush(sysCode);
    }

    @Override
    public synchronized String getRequisitionRecordCode() {
        String date = DateUtil.format(new Date(), "yyyyMMdd");
        SysCode sysCode = sysCodeRepository.findByCodeType("requisition_record_code");
        if (sysCode != null) {
            if (!date.equals(sysCode.getSeqDate())) {
                sysCode.setSeqDate(date);
                sysCode.setSeqNum(1);
            } else {
                sysCode.setSeqNum(sysCode.getSeqNum() + 1);
            }
            put(sysCode);
            return sysCode.getPrefix() + date + String.format("%0" + sysCode.getCompletion() + "d", sysCode.getSeqNum());
        }
        return "";
    }

    @Override
    public synchronized String getUtensilCode() {
        SysCode sysCode = sysCodeRepository.findByCodeType("utensil_code");
        if (sysCode != null) {
            sysCode.setSeqNum(sysCode.getSeqNum() + 1);
            put(sysCode);
            return sysCode.getPrefix() + String.format("%0" + sysCode.getCompletion() + "d", sysCode.getSeqNum());
        }
        return null;
    }

    @Override
    public String getMaterialsCode(String prefix) {
        //食材编码生成规则 食材类型编码+食材流水
        SysCode sysCode = sysCodeRepository.findByCodeType("materials_code");
        String date = DateUtil.format(new Date(), DateHelper.YYYYMMDD);
        if (null != sysCode){
            sysCode.setSeqNum(sysCode.getSeqNum() + 1);
            sysCode.setSeqDate(date);
        }else {
            sysCode = new SysCode();
            sysCode.setCodeName("食材编码");
            sysCode.setCodeType("materials_code");
            sysCode.setPrefix("F");
            sysCode.setSeqNum(0);
            sysCode.setCompletion(5);
            sysCode.setSeqDate(date);
        }
        put(sysCode);
        return prefix + String.format("%0" + sysCode.getCompletion() + "d", sysCode.getSeqNum());
    }

    @Override
    public String getBatchCode(String cafeteriaCode) {
        //进货批次号生成规则  前缀 + 食堂编码 + 时间 + 流水
        SysCode sysCode = sysCodeRepository.findByCodeType("batch_code");
        String date = DateUtil.format(new Date(), DateHelper.YYYYMMDD);
        if (sysCode != null) {
            sysCode.setSeqNum(sysCode.getSeqNum() + 1);
            put(sysCode);
        }else {
            sysCode = new SysCode();
            sysCode.setPrefix("B");
            sysCode.setCodeName("进货批次码");
            sysCode.setCodeType("batch_code");
            sysCode.setSeqNum(1);
            sysCode.setCompletion(3);
            put(sysCode);
        }
        if(cafeteriaCode == null){
            cafeteriaCode = "";
        }
        return sysCode.getPrefix() + cafeteriaCode + date + String.format("%0" + sysCode.getCompletion() + "d", sysCode.getSeqNum() + 1);
    }

    @Override
    public String getOutCode(String cafeteriaCode) {
        //出库单号生成规则  前缀 + 食堂编码 + 时间 + 流水
        SysCode sysCode = sysCodeRepository.findByCodeType("out_code");
        String date = DateUtil.format(new Date(), DateHelper.YYYYMMDD);
        if (sysCode != null) {
            sysCode.setSeqNum(sysCode.getSeqNum() + 1);
            put(sysCode);
        }else {
            sysCode = new SysCode();
            sysCode.setPrefix("O");
            sysCode.setCodeName("出库单号");
            sysCode.setCodeType("out_code");
            sysCode.setSeqNum(1);
            sysCode.setCompletion(3);
            put(sysCode);
        }
        if(cafeteriaCode == null){
            cafeteriaCode = "";
        }
        return sysCode.getPrefix() + cafeteriaCode + date + String.format("%0" + sysCode.getCompletion() + "d", sysCode.getSeqNum() + 1);
    }

    @Override
    public String getInventoryCode(String cafeteriaCode) {
        //盘库单号生成规则  前缀 + 食堂编码 + 时间 + 流水
        SysCode sysCode = sysCodeRepository.findByCodeType("inventory_code");
        String date = DateUtil.format(new Date(), DateHelper.YYYYMMDD);
        if (sysCode != null) {
            sysCode.setSeqNum(sysCode.getSeqNum() + 1);
            put(sysCode);
        }else {
            sysCode = new SysCode();
            sysCode.setPrefix("O");
            sysCode.setCodeName("盘库单号");
            sysCode.setCodeType("inventory_code");
            sysCode.setSeqNum(1);
            sysCode.setCompletion(2);
            put(sysCode);
        }
        if(cafeteriaCode == null){
            cafeteriaCode = "";
        }
        return sysCode.getPrefix() + cafeteriaCode + date + String.format("%0" + sysCode.getCompletion() + "d", sysCode.getSeqNum() + 1);
    }

    @Override
    public String getPurchaseCode(String cafeteriaCode) {
        //采购单号生成规则  前缀 + 食堂编码 + 时间 + 流水
        SysCode sysCode = sysCodeRepository.findByCodeType("purchase_code");
        String date = DateUtil.format(new Date(), DateHelper.YYYYMMDD);
        if (sysCode != null) {
            sysCode.setSeqNum(sysCode.getSeqNum() + 1);
            put(sysCode);
        }else {
            sysCode = new SysCode();
            sysCode.setPrefix("O");
            sysCode.setCodeName("采购单号");
            sysCode.setCodeType("purchase_code");
            sysCode.setSeqNum(1);
            sysCode.setCompletion(1);
            put(sysCode);
        }
        if(cafeteriaCode == null){
            cafeteriaCode = "";
        }
        return sysCode.getPrefix() + cafeteriaCode + date + String.format("%0" + sysCode.getCompletion() + "d", sysCode.getSeqNum() + 1);
    }

    @Override
    public String getKitchenUtensilsCode(String typeCode) {
        //设备厨具编码生成规则 设备厨具类型编码+器具流水
        SysCode sysCode = sysCodeRepository.findByCodeType("kitchen_utensils_code");
        String date = DateUtil.format(new Date(), DateHelper.YYYYMMDD);
        if (null != sysCode){
            sysCode.setSeqNum(sysCode.getSeqNum() + 1);
            sysCode.setSeqDate(date);
        }else {
            sysCode = new SysCode();
            sysCode.setCodeName("设备厨具编码");
            sysCode.setCodeType("kitchen_utensils_code");
            sysCode.setPrefix("K");
            sysCode.setSeqNum(0);
            sysCode.setCompletion(5);
            sysCode.setSeqDate(date);
        }
        put(sysCode);
        return typeCode + String.format("%0" + sysCode.getCompletion() + "d", sysCode.getSeqNum());
    }
}
