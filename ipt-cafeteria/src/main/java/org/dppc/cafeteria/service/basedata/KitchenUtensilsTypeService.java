package org.dppc.cafeteria.service.basedata;

import org.dppc.cafeteria.entity.basedata.KitchenUtensilsType;
import org.dppc.dbexpand.service.BaseService;

import java.util.List;

/**
 * @Description 业务层接口
 * @Author majt
 * @Data 2019/06/03 19:38
 * @Version 1.0
 **/
public interface KitchenUtensilsTypeService extends BaseService<KitchenUtensilsType, Long> {
    /**
     * @return
     * @Author majt
     * @Description 查询器材类目树
     * @Date 2019/5/11 11:40
     * @Param
     **/
    List<KitchenUtensilsType> tree();

    /**
     * @return
     * @Author majt
     * @Description 根据主键id查询器材类型
     * @Date 2019/5/13 10:26
     * @Param
     **/
    KitchenUtensilsType findOneById(Long typeId);
}
