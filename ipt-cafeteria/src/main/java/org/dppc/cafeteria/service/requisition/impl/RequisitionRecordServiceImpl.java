package org.dppc.cafeteria.service.requisition.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.dppc.cafeteria.entity.requisition.RequisitionDetail;
import org.dppc.cafeteria.entity.requisition.RequisitionRecord;
import org.dppc.cafeteria.repository.requisition.RequisitionDetailRepository;
import org.dppc.cafeteria.repository.requisition.RequisitionRecordRepository;
import org.dppc.cafeteria.service.requisition.RequisitionRecordService;
import org.dppc.cafeteria.service.stock.OutBaseService;
import org.dppc.cafeteria.service.syscode.SysCodeService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.enums.IsDeleteEnum;
import org.dppc.common.enums.IsUsedEnum;
import org.dppc.common.enums.RequisitionStatusEnum;
import org.dppc.common.utils.ExcelStyleHelper;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @Description 领用单业务层实现类
 * @Author GAOJ
 * @Data 2019/05/09 09:49
 * @Version 1.0
 **/
@Service
public class RequisitionRecordServiceImpl extends BaseServiceImpl<RequisitionRecord, Long> implements RequisitionRecordService {

    @Autowired
    private RequisitionRecordRepository requisitionRecordRepository;

    @Autowired
    private RequisitionDetailRepository requisitionDetailRepository;

    @Autowired
    private SysCodeService sysCodeService;

    @Autowired
    private OutBaseService outBaseService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveNew(RequisitionRecord requisitionRecord) {
        String recordCode = sysCodeService.getRequisitionRecordCode();
        requisitionRecord.setCreateDate(new Date());
        requisitionRecord.setCreateUserId(Long.valueOf(BaseContextHandler.getUserID()));
        requisitionRecord.setCreateUserName(BaseContextHandler.getName());
        requisitionRecord.setRecordCode(recordCode);
        requisitionRecord.setIsDelete(IsDeleteEnum.NOT_DELETE.getValue());
        requisitionRecord.setStatus(RequisitionStatusEnum.RECORD.getValue());
        CafeteriaHandleUtil.bindCafeteriaInfo(requisitionRecord);
        for (RequisitionDetail requisitionDetail : requisitionRecord.getRequisitionDetail()) {
            requisitionDetail.setRecordCode(recordCode);
            requisitionDetail.setIsUsed(IsUsedEnum.NOT_USED.getValue());
            CafeteriaHandleUtil.bindCafeteriaInfo(requisitionDetail);
            requisitionDetailRepository.save(requisitionDetail);
        }
        requisitionRecordRepository.save(requisitionRecord);
        outBaseService.saveFromRequisitionRecord(requisitionRecord);
    }

    @Override
    public HSSFWorkbook exportExcelRequisitionRecord(Long id) {
        RequisitionRecord record = requisitionRecordRepository.getOneById(id);
        String titleName = "食材领用单";
        String[] excelHeader = {"序号", "食材编码", "食材名称", "计量单位", "领用数量", "备注"};
        // 单元格列宽
        int[] excelHeaderWidth = {50, 120, 120, 100, 100, 120};
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("食材领用单");

        setHSSFWorkbook(wb, sheet, excelHeader, excelHeaderWidth, titleName);

        HSSFRow row1 = sheet.getRow(1);
        HSSFCell cell10 = row1.getCell(0);
        cell10.setCellValue("领用单编号：" + record.getRecordCode()
                + "        领用日期：" + DateUtil.format(record.getCreateDate(), DatePattern.NORM_DATE_PATTERN)
                + "        领用人：" + record.getName());

        HSSFCellStyle style = ExcelStyleHelper.getCellStyle(wb, HorizontalAlignment.CENTER, VerticalAlignment.CENTER,
                ExcelStyleHelper.getFont(wb, "宋体", (short) 13));
        ExcelStyleHelper.setBorder(style, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);

        List<RequisitionDetail> orderDetails = record.getRequisitionDetail();
        int i = 0;
        for (int j = 0; j < orderDetails.size(); j++) {
            RequisitionDetail detail = orderDetails.get(j);
            HSSFRow row = sheet.createRow(i + 3);
            ExcelStyleHelper.setCell(row, 0, j + 1, style);
            ExcelStyleHelper.setCell(row, 1, detail.getMaterialsCode(), style);
            ExcelStyleHelper.setCell(row, 2, detail.getMaterialsName(), style);
            ExcelStyleHelper.setCell(row, 3, detail.getUnit(), style);
            ExcelStyleHelper.setCell(row, 4, detail.getQuantity(), style);
            ExcelStyleHelper.setCell(row, 5, detail.getRemarks(), style);
            i++;
        }
        return wb;
    }

    /**
     * 设置表格样式
     */
    private void setHSSFWorkbook(HSSFWorkbook wb, HSSFSheet sheet,
                                 String[] excelHeader, int[] excelHeaderWidth, String titleName) {
        //标题样式
        HSSFCellStyle style1 = ExcelStyleHelper.getCellStyle(wb, HorizontalAlignment.CENTER, VerticalAlignment.CENTER,
                ExcelStyleHelper.getFont(wb, "宋体", (short) 18));

        //列名样式
        HSSFCellStyle style2 = ExcelStyleHelper.getCellStyle(wb, HorizontalAlignment.CENTER, VerticalAlignment.CENTER,
                ExcelStyleHelper.getFont(wb, "宋体", (short) 14));
        ExcelStyleHelper.setBorder(style2, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN, BorderStyle.THIN);

        //信息行样式
        HSSFCellStyle style3 = ExcelStyleHelper.getCellStyle(wb, HorizontalAlignment.CENTER, VerticalAlignment.CENTER,
                ExcelStyleHelper.getFont(wb, "宋体", (short) 14));

        //合并
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, excelHeader.length - 1));
        //标题行
        HSSFRow row = sheet.createRow(0);
        row.setHeight((short) 800);
        HSSFCell cell = row.createCell(0);
        cell.setCellValue(titleName);
        cell.setCellStyle(style1);

        sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, excelHeader.length - 1));
        row = sheet.createRow(1);
        HSSFCell cell1 = row.createCell(0);
        cell1.setCellStyle(style3);

        //列表标题行
        row = sheet.createRow(2);

        // 设置列宽度（像素）
        for (int i = 0; i < excelHeaderWidth.length; i++) {
            sheet.setColumnWidth(i, 50 * excelHeaderWidth[i]);
        }
        // 添加表格头
        for (int i = 0; i < excelHeader.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(excelHeader[i]);
            cell.setCellStyle(style2);
        }
    }
}
