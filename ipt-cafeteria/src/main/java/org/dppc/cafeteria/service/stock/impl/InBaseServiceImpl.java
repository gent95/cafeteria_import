package org.dppc.cafeteria.service.stock.impl;

import org.apache.commons.lang3.StringUtils;
import org.dppc.cafeteria.entity.basedata.FoodMaterials;
import org.dppc.cafeteria.entity.stock.InBase;
import org.dppc.cafeteria.entity.stock.InDetail;
import org.dppc.cafeteria.entity.stock.Info;
import org.dppc.cafeteria.entity.stock.OutBase;
import org.dppc.cafeteria.repository.stock.InDetailRepository;
import org.dppc.cafeteria.repository.stock.InfoRepository;
import org.dppc.cafeteria.service.stock.InBaseService;
import org.dppc.cafeteria.repository.stock.InBaseRepository;
import org.dppc.cafeteria.service.stock.StockService;
import org.dppc.cafeteria.service.syscode.SysCodeService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.cafeteria.utils.StockUtil;
import org.dppc.cafeteria.vo.InStockVO;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.enums.IsDeleteEnum;
import org.dppc.common.enums.stock.InStatusEnum;
import org.dppc.common.enums.stock.OutStatusEnum;
import org.dppc.common.exception.RepeatOperateException;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description 原材料入库信息主表业务层实现类
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class InBaseServiceImpl extends BaseServiceImpl<InBase, Long> implements InBaseService {

    @Autowired
    private InBaseRepository inBaseRepository;
    @Autowired
    private InDetailRepository inDetailRepository;
    @Autowired
    private InfoRepository infoRepository;
    @Autowired
    private StockService stockService;
    @Autowired
    private SysCodeService sysCodeService;

    @Override
    public void takeOver(InStockVO inStockVO) {
        InBase inBase = inStockVO.getInBase();
        List<InDetail> inDetailList = inStockVO.getInDetailList();
        inBase.setBatchCode(sysCodeService.getBatchCode(null));
        inBase.setInDate(new Date());
        inBase.setInStatus(InStatusEnum.TAKE_OVER.getValue());
        inBase.setInMan(BaseContextHandler.getUsername());
        inBase.setInManId(StringUtils.isBlank(BaseContextHandler.getUserID()) ? null : Long.parseLong(BaseContextHandler.getUserID()));
        Double sumPrice = 0.0;
        List<Info> infoList = new ArrayList<>();
        //计算总价格  细表入库
        for (InDetail inDetail : inDetailList) {
            //判断进货数量是否大于0
            if(inDetail.getInNum() > 0) {
                sumPrice += inDetail.getSumPrice();
                inDetail.setExpiryDate(
                        StockUtil.gainExpiryDate(
                                inDetail.getQualityGuaranteePeriod(), inDetail.getQualityGuaranteePeriodUnit()
                                ,inDetail.getProduceDate())
                );
                //根据入库信息 添加 库存详细记录信息
                Info info = infoValueOf(inBase, inDetail);
                CafeteriaHandleUtil.bindCafeteriaInfo(info);
                infoList.add(info);

                /* 更新库存统计信息 */
                FoodMaterials foodMaterials = new FoodMaterials();
                foodMaterials.setMaterialsCode(inDetail.getMaterialsCode());
                foodMaterials.setMaterialsName(inDetail.getMaterialsName());
                foodMaterials.setUnit(inDetail.getUnit());
                stockService.updateStockCount(foodMaterials,inDetail.getInNum());
            }
            inDetail.setBatchCode(inBase.getBatchCode());
            inDetailRepository.saveAndFlush(inDetail);
        }
        //更新入库主表信息
        inBase.setSumPrice(sumPrice);
        //出库单出库，并判断是否重复出库
        this.flushOutBaseValidateIsRepeatOutStock(inBase);
        //添加库存详细记录信息
        infoRepository.save(infoList);
    }

    /**
     * @Author lhw
     * @Description 出库单出库，并判断是否重复出库
     *              如果重复出库，则抛出重复操作异常(在Rest中进行异常处理)
     *                            否则进行数据更新
     * @Date 12:07 2019/5/25
     **/
    private void flushOutBaseValidateIsRepeatOutStock(InBase inBase) {
        InBase dbOutBase = inBaseRepository.findOne(inBase.getBaseId());
        if(dbOutBase.getInStatus() == InStatusEnum.TAKE_OVER.getValue()){
            throw new RepeatOperateException();
        }
        inBaseRepository.saveAndFlush(inBase);
    }

    private Info infoValueOf(InBase inBase, InDetail inDetail) {
        Info info = new Info();
        info.setMaterialsName(inDetail.getMaterialsName());
        info.setMaterialsCode(inDetail.getMaterialsCode());
        info.setNum(inDetail.getInNum());
        info.setBatchCode(inBase.getBatchCode());
        info.setPurchaseDate(inBase.getPurchaseDate());
        info.setProduceDate(inDetail.getProduceDate());
        info.setQualityGuaranteePeriod(inDetail.getQualityGuaranteePeriod());
        info.setQualityGuaranteePeriodUnit(inDetail.getQualityGuaranteePeriodUnit());
        info.setBigType(inDetail.getMaterialsCode().substring(0,3));
        info.setIsDelete(IsDeleteEnum.NOT_DELETE.getValue());
        info.setUnit(inDetail.getUnit());
        info.setExpiryDate(inDetail.getExpiryDate());
        return info;
    }
}
