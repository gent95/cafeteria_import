package org.dppc.cafeteria.entity.record;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 器具
 * @Author GAOJ
 * @Data 2019/05/09 09:45
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_record_utensil")
public class Utensil implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "utensil_id")
    private Long utensilId;

    /** 器具类型 */
    @Column(name = "type")
    @PredicateInfo
    private Integer type;

    /** 品牌 */
    @Column(name = "brand", length = 30)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String brand;

    /** 型号 */
    @Column(name = "model", length = 30)
    private String model;

    /** 数量 */
    @Column(name = "quantity")
    private Integer quantity;

    /** 备案编码 */
    @Column(name = "utensil_code", length = 20)
    private String utensilCode;

    /** 设备编码 */
    @Column(name = "device_code", length = 20)
    private String deviceCode;

    /** 用途 */
    @Column(name = "uses", length = 200)
    private String uses;

    /** 备案人id */
    @Column(name = "create_user_id")
    private Long createUserId;

    /** 备案人姓名 */
    @Column(name = "create_user_name", length = 30)
    private String createUserName;

    /** 备案日期 */
    @Column(name = "create_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_MINUTE_PATTERN, timezone = "GMT+8")
    private Date createDate;

    /** 审核状态 */
    @Column(name = "audit_status")
    @PredicateInfo
    private Integer auditStatus;

    /** 审核人id */
    @Column(name = "audit_user_id")
    private Long auditUserId;

    /** 审核人姓名 */
    @Column(name = "audit_user_name", length = 30)
    private String auditUserName;

    /** 审核日期 */
    @Column(name = "audit_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date auditDate;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;
    /** 器具属性（json字符串）温湿度传感器: {"minTemp":"","maxTemp":"","minHum":"","maxHum":""} ,摄像头:{"sources":[{"type":"","src":""}]}*/
    @Column(name = "param_json")
    private String paramJson;
}