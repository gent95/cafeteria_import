package org.dppc.cafeteria.entity.stock;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;

/**
 * @Description 库存统计信息表
 * @Author lhw
 * @Data 2019/05/09 15:22
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_stock_count")
public class Count implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "count_id")
    private Long countId;

    /** 食材名称 */
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    @Column(name = "materials_name", length = 50)
    private String materialsName;

    /** 食材编码 */
    @Column(name = "materials_code", length = 20)
    private String materialsCode;

    /** 数量 */
    @Column(name = "num")
    private Double num;

    /** 大类编码 */
    @PredicateInfo
    @Column(name = "big_type", length = 5)
    private String bigType;

    /** 计量单位 */
    @Column(name = "unit")
    private String unit;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;
}