package org.dppc.cafeteria.entity.stock;

import java.io.Serializable;
import java.util.Date;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;

/**
 * @Description 盘货信息主表
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_stock_inventory_base")
public class InventoryBase implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "base_id")
    private Long baseId;

    /** 盘库时间 */
    @Column(name = "inventory_day", length = 10)
    private String inventoryDay;

    /** 盘库单号 */
    @Column(name = "inventory_code", length = 10)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String inventoryCode;

    /** 初盘人 */
    @Column(name = "first_man", length = 20)
    private String firstMan;

    /** 初盘人id */
    @Column(name = "first_man_id")
    @PredicateInfo
    private Long firstManId;

    /** 复盘人 */
    @Column(name = "repeat_man", length = 20)
    private String repeatMan;

    /** 复盘人id */
    @Column(name = "repeat_man_id")
    @PredicateInfo
    private Long repeatManId;

    /** 创建时间 */
    @Column(name = "create_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN,timezone = "GMT+8")
    private Date createDate;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;

}