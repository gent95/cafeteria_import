package org.dppc.cafeteria.entity.record;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.common.enums.IsDeleteEnum;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @Description 人员职务
 * @Author GAOJ
 * @Data 2019/05/09 19:11
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_record_personnel_post")
public class PersonnelPost implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "personnel_post_id")
    private Integer personnelPostId;

    /** 职务名称 */
    @Column(name = "post_name", length = 50)
    private String postName;

    /** 删除状态 */
    @Column(name = "is_delete")
    @PredicateInfo
    private Integer isDelete = IsDeleteEnum.NOT_DELETE.getValue();


}