package org.dppc.cafeteria.entity.stock;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;

/**
 * @Description 盘库详情表
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_stock_inventory_detail")
public class InventoryDetail implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "detail_id")
    private Long detailId;

    /** 主表id */
    @Column(name = "base_id")
    @PredicateInfo
    private Long baseId;

    /** 食材名称 */
    @Column(name = "materials_name", length = 50)
    private String materialsName;

    /** 食材编码 */
    @Column(name = "materials_code", length = 20)
    private String materialsCode;

    /** 当前库存 */
    @Column(name = "current_num")
    private Double currentNum;

    /** 初盘数量 */
    @Column(name = "first_num")
    private Double firstNum;

    /** 复盘数量 */
    @Column(name = "repeat_num")
    private Double repeatNum;

    /** 备注 */
    @Column(name = "remarks", length = 200)
    private String remarks;

    /** 进货批次号 */
    @Column(name = "batch_code", length = 20)
    private String batchCode;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;
}