package org.dppc.cafeteria.entity.stock;

import java.io.Serializable;
import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

/**
 * @Description 原材料入库信息主表
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_stock_in_base")
public class InBase implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "base_id")
    private Long baseId;

    /** 补货编码 */
    @Column(name = "replenish_code", length = 20)
    private String replenishCode;

    /** 供货商编码（统一社会信用代码） */
    @Column(name = "social_credit_code", length = 18)
    @PredicateInfo
    private String socialCreditCode;

    /** 供应商名称 */
    @Column(name = "supplier_name", length = 50)
    private String supplierName;

    /** 进货批次号 */
    @Column(name = "batch_code", length = 20)
    private String batchCode;

    /** 采购日期 */
    @Column(name = "purchase_date")
    private String purchaseDate;

    /** 入库时间 */
    @Column(name = "in_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN,timezone = "GMT+8")
    private Date inDate;

    /** 收货状态 0:待收货 1:已收货*/
    @PredicateInfo
    @Column(name = "in_status")
    private Integer inStatus;

    /** 总价 */
    @Column(name = "sum_price")
    private Double sumPrice;

    /** 入库人 */
    @Column(name = "in_man", length = 20)
    private String inMan;

    /** 入库人ID */
    @Column(name = "in_man_id")
    private Long inManId;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;

    public InBase(String replenishCode, String socialCreditCode, String supplierName, String purchaseDate, Integer inStatus) {
        this.replenishCode = replenishCode;
        this.socialCreditCode = socialCreditCode;
        this.supplierName = supplierName;
        this.purchaseDate = purchaseDate;
        this.inStatus = inStatus;
    }
}