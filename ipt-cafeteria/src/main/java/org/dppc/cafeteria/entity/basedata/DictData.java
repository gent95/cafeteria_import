package org.dppc.cafeteria.entity.basedata;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 字典数据
 * @Author majt
 * @Data 2019/05/09 10:15
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_dict_data")
public class DictData implements Serializable {

    @Id
    /** 主键id */
    @GeneratedValue
    @Column(name = "dict_id")
    private Long dictId;

    /** 字典编码 */
    @Column(name = "dict_code", length = 11)
    @PredicateInfo(queryType = PredicateInfo.QueryType.BASIC)
    private Integer dictCode;

    /** 字典名 */
    @Column(name = "dict_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String dictName;

    /** 字典键值 */
    @Column(name = "dict_value", length = 50)
    private String dictValue;

    /** 状态（1启用 0禁用） */
    @Column(name = "status")
    private Integer status;

    /** 描述 */
    @Column(name = "remarks", length = 50)
    private String remarks;

    /** 创建时间 */
    @Column(name = "create_time")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN,timezone = "GMT+8")
    private Date createTime;

    /** 创建人 */
    @Column(name = "create_user_id")
    private Long createUserId;

    /** 字典类型id */
    @Column(name = "dict_type_id")
    @PredicateInfo(queryType = PredicateInfo.QueryType.BASIC)
    private Long dictTypeId;

    /** 排序 */
    @Column(name = "sort")
    private Integer sort;

    public DictData(Long dictTypeId) {
        this.dictTypeId = dictTypeId;
    }
}
