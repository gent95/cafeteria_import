package org.dppc.cafeteria.entity.basedata;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @Description 
 * @Author majt
 * @Data 2019/06/03 19:38
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_kitchen_utensils_type")
public class KitchenUtensilsType implements Serializable {

    @Id
    /** 自增主键 */
    @GeneratedValue
    @Column(name = "type_id")
    private Long typeId;

    /** 类型编码 */
    @Column(name = "type_code", length = 8)
    private String typeCode;

    /** 类型名称 */
    @Column(name = "type_name", length = 50)
    private String typeName;

    /** 父亲节点编码 */
    @Column(name = "parent_code", length = 8)
    private String parentCode;

    @Transient
    private List<KitchenUtensilsType> children;
}