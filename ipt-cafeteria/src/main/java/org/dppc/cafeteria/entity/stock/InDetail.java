package org.dppc.cafeteria.entity.stock;

import java.io.Serializable;
import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

/**
 * @Description 原材料入库信息细表
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_stock_in_detail")
public class InDetail implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "detail_id")
    private Long detailId;

    /** 主表id */
    @Column(name = "base_id")
    @PredicateInfo
    private Long baseId;

    /** 进货批次号 */
    @Column(name = "batch_code", length = 20)
    private String batchCode;

    /** 食材名称 */
    @Column(name = "materials_name", length = 50)
    private String materialsName;

    /** 食材编码 */
    @Column(name = "materials_code", length = 20)
    private String materialsCode;

    /** 品牌 */
    @Column(name = "brand", length = 50)
    private String brand;

    /** 订单数量 */
    @Column(name = "order_num")
    private Double orderNum;

    /** 进货数量 */
    @Column(name = "in_num")
    private Double inNum;

    /** 单价 */
    @Column(name = "price")
    private Double price;

    /** 总价 */
    @Column(name = "sum_price")
    private Double sumPrice;

    /** 生产日期 */
    @Column(name = "produce_date")
    private String produceDate;

    /** 保质期 */
    @Column(name = "quality_guarantee_period", length = 8)
    private Integer qualityGuaranteePeriod;

    /** 保质期单位 */
    @Column(name = "quality_guarantee_period_unit", length = 1)
    private Integer qualityGuaranteePeriodUnit;

    /** 有效期至 */
    @Column(name = "expiry_date", length = 10)
    private String expiryDate;

    /** 上游追溯码 */
    @Column(name = "up_tran_code", length = 50)
    private String upTranCode;

    /** 检验证号 */
    @Column(name = "quarantine_code", length = 30)
    private String quarantineCode;

    /** 计量单位 */
    @Column(name = "unit")
    private String unit;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;

    public InDetail(Long baseId, String materialsName, String materialsCode, Double orderNum, String unit) {
        this.baseId = baseId;
        this.materialsName = materialsName;
        this.materialsCode = materialsCode;
        this.orderNum = orderNum;
        this.unit = unit;
    }
}