package org.dppc.cafeteria.entity.emergency;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.common.enums.IsDeleteEnum;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @Description 应急事件类型
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_emergency_type")
public class EmergencyType implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "emergency_type_id")
    private Integer emergencyTypeId;

    /** 名称 */
    @Column(name = "emergency_type_name", length = 30)
    private String emergencyTypeName;

    /** 是否删除1.未删除0.已删除 */
    @Column(name = "is_delete")
    @PredicateInfo
    private Integer isDelete = IsDeleteEnum.NOT_DELETE.getValue();

}