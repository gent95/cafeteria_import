package org.dppc.cafeteria.entity.peican;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 陪餐管理
 * @Author zhumh
 * @Data 2019/05/10 17:11
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_pei_can")
public class PeiCan implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "pei_can_id")
    private Long peiCanId;

    /** 陪餐时间 */
    @Column(name = "pei_can_time")
    @DateTimeFormat(pattern = DatePattern.NORM_DATE_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATE_PATTERN, timezone = "GMT+8")
    @PredicateInfo
    private Date peiCanTime;

    /** 餐次类型 mealTimesEnum*/
    @Column(name = "meal_times")
    @PredicateInfo
    private Integer mealTimes;

    /** 陪餐人 */
    @Column(name = "pei_can_user_id")
    private Long peiCanUserId;

    /** 陪餐人 */
    @Column(name = "pei_can_name")
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String peiCanName;

    /** 创建时间 */
    @Column(name = "create_time")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date createTime;

    /** 创建用户 */
    @Column(name = "create_user_id")
    private Long createUserId;

    /** 创建用户 */
    @Column(name = "create_user_name")
    private String createUserName;

    /** 陪餐描述 */
    @Column(name = "pei_can_desc", length = 200)
    private String peiCanDesc;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;

}