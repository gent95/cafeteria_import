package org.dppc.cafeteria.entity.basedata;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

/**
 * @Description 食材类型表
 * @Author majt
 * @Data 2019/05/09 15:42
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_food_materials_type")
public class FoodMaterialsType implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "type_id")
    private Integer typeId;

    /** 类型编码 */
    @Column(name = "type_code", length = 8)
    private String typeCode;

    /** 类型名称 */
    @Column(name = "type_name", length = 50)
    private String typeName;

    /** 父类编码 */
    @Column(name = "parent_code", length = 8)
    private String parentCode;

    @Transient
    private List<FoodMaterialsType> children;
}