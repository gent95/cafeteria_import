package org.dppc.cafeteria.entity.basedata;

import java.io.Serializable;
import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

/**
 * @Description 
 * @Author majt
 * @Data 2019/06/03 19:38
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_kitchen_utensils")
public class KitchenUtensils implements Serializable {

    @Id
    /** 自增主键 */
    @GeneratedValue
    @Column(name = "utensils_id")
    private Long utensilsId;

    /** 器具名称 */
    @Column(name = "utensils_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String utensilsName;

    /** 器具编码 */
    @Column(name = "utensils_code", length = 8)
    private String utensilsCode;

    /** 类型编号 */
    @Column(name = "type_id")
    @PredicateInfo(queryType = PredicateInfo.QueryType.BASIC)
    private Long typeId;

    /** 创建时间 */
    @Column(name = "create_time")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private Date createTime;


}