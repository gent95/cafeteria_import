package org.dppc.cafeteria.entity.stock;

import java.io.Serializable;
import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

/**
 * @Description 库存信息表
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_stock_info")
public class Info implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "stock_id")
    private Long stockId;

    /** 食材名称 */
    @Column(name = "materials_name", length = 50)
    private String materialsName;

    /** 食材编码 */
    @PredicateInfo
    @Column(name = "materials_code", length = 20)
    private String materialsCode;

    /** 数量 */
    @Column(name = "num")
    private Double num;

    /** 进货批次号 */
    @Column(name = "batch_code", length = 20)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String batchCode;

    /** 采购日期 */
    @Column(name = "purchase_date")
    private String purchaseDate;

    /** 生产日期 */
    @Column(name = "produce_date")
    private String produceDate;

    /** 保质期 */
    @Column(name = "quality_guarantee_period", length = 8)
    private Integer qualityGuaranteePeriod;

    /** 保质期单位 */
    @Column(name = "quality_guarantee_period_unit", length = 1)
    private Integer qualityGuaranteePeriodUnit;

    /** 有效期至 */
    @Column(name = "expiry_date", length = 10)
    private String expiryDate;

    /** 大类编码 */
    @Column(name = "big_type", length = 5)
    private String bigType;

    /** 是否删除  （1：未删除；0：已删除） */
    @Column(name = "is_delete")
    @PredicateInfo
    private Integer isDelete;

    /** 计量单位 */
    @Column(name = "unit")
    private String unit;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;

}