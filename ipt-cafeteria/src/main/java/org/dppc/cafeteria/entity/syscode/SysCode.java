package org.dppc.cafeteria.entity.syscode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @Description 
 * @Author GAOJ
 * @Data 2019/05/14 14:00
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_sys_code")
public class SysCode implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "code_id")
    private Integer codeId;

    /** 编码描述 */
    @Column(name = "code_name", length = 30)
    private String codeName;

    /** 编码类型 */
    @Column(name = "code_type", length = 30)
    private String codeType;

    /** 补全位数 */
    @Column(name = "completion")
    private Integer completion;

    /** 编码前缀 */
    @Column(name = "prefix", length = 10)
    private String prefix;

    /** 编码日期 */
    @Column(name = "seq_date", length = 11)
    private String seqDate;

    /** 编码序列 */
    @Column(name = "seq_num")
    private Integer seqNum;

}