package org.dppc.cafeteria.entity.basedata;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 食材表
 * @Author majt
 * @Data 2019/05/09 15:42
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_food_materials")
public class FoodMaterials implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "materials_id")
    private Integer materialsId;

    /** 食材名称 */
    @Column(name = "materials_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String materialsName;

    /** 食材编码 */
    @Column(name = "materials_code", length = 20)
    private String materialsCode;

    /** 计量单位 */
    @Column(name = "unit", length = 20)
    private String unit;

    @Column(name="type_id")
    @PredicateInfo(queryType = PredicateInfo.QueryType.BASIC)
    private Integer typeId;

    /** 创建时间 */
    @Column(name = "create_time")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN,timezone = "GMT+8")
    private Date createTime;

    public FoodMaterials(String materialsCode) {
        this.materialsCode = materialsCode;
    }
}