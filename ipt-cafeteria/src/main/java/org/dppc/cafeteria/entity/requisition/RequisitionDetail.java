package org.dppc.cafeteria.entity.requisition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @Description 领用单明细
 * @Author GAOJ
 * @Data 2019/05/09 09:49
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_requisition_detail")
public class RequisitionDetail implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "detail_id")
    private Long detailId;

    /** 领用单编号 */
    @Column(name = "record_code", length = 30)
    @PredicateInfo
    private String recordCode;

    /** 食材id */
    @Column(name = "materials_id")
    private Integer materialsId;

    /** 食材编码 */
    @Column(name = "materials_code", length = 20)
    private String materialsCode;

    /** 食材名称 */
    @Column(name = "materials_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String materialsName;

    /** 领用数量 */
    @Column(name = "quantity")
    private Double quantity;

    /** 计量单位 */
    @Column(name = "unit")
    private String unit;

    /** 备注 */
    @Column(name = "remarks", length = 100)
    private String remarks;

    /** 使用状态、0 未使用、1 已使用 IsUsedEnum*/
    @Column(name = "is_used")
    @PredicateInfo
    private Integer isUsed;

    /** 使用状态、0 未删除、1 已删除 IsUsedEnum*/
    @Column(name = "is_delete")
    @PredicateInfo
    private Integer isDelete;

    @Transient
    private boolean checked = false;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;
}