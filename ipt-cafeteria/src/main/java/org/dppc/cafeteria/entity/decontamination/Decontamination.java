package org.dppc.cafeteria.entity.decontamination;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 洗消管理
 * @Author zhumh
 * @Data 2019/05/10 17:12
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_decontamination")
public class Decontamination implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "decontamination_id")
    private Long decontaminationId;

    @NotNull
    /** 清洁时间 */
    @Column(name = "decontamination_datetime")
    @DateTimeFormat(pattern = DatePattern.NORM_DATE_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATE_PATTERN, timezone = "GMT+8")
    @PredicateInfo
    private Date decontaminationDatetime;

    @NotNull
    /** 餐次类型 mealTimesEnum*/
    @Column(name = "meal_times")
    @PredicateInfo
    private Integer mealTimes;

    @NotNull
    /** 餐具数量 */
    @Column(name = "tableware_number")
    private Integer tablewareNumber;

    @NotNull
    /** 清洗状态 DecontaminationStatusEnum*/
    @Column(name = "decontamination_status")
    @PredicateInfo
    private Integer decontaminationStatus;

    @NotNull
    /** 消毒状态 DisinfectStatusEnum*/
    @Column(name = "disinfect_status")
    @PredicateInfo
    private Integer disinfectStatus;

    @NotNull
    /** 负责人 */
    @Column(name = "duty_officer_id")
    private Long dutyOfficerId;

    /** 负责人 */
    @Column(name = "duty_officer_name")
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String dutyOfficerName;

    /** 创建时间 */
    @Column(name = "create_time")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date createTime;

    /** 创建用户 */
    @Column(name = "create_user_id")
    private Long createUserId;

    /** 创建用户 */
    @Column(name = "create_user_name")
    private String createUserName;

    /** 洗消描述 */
    @Column(name = "decontamination_desc", length = 200)
    private String decontaminationDesc;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;


}