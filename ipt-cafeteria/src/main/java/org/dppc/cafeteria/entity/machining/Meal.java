package org.dppc.cafeteria.entity.machining;

import java.io.Serializable;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import org.dppc.cafeteria.entity.record.Personnel;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.*;

import javax.persistence.*;

/**
 * @Description 餐次
 * @Author majt
 * @Data 2019/05/20 18:05
 * @Version 1.0
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_machining_meal")
public class Meal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "meal_id")
    private Long mealId;

    /**
     * 时间
     */
    @Column(name = "time")
    @DateTimeFormat(pattern = DatePattern.NORM_DATE_PATTERN)
    @JsonFormat(pattern =DatePattern.NORM_DATE_PATTERN,timezone = "GMT+8")
    @PredicateInfo
    private Date time;

    /**
     * 餐次
     */
    @Column(name = "meal_times")
    @PredicateInfo(queryType = PredicateInfo.QueryType.BASIC)
    private String mealTimes;


    /**
     * 责任人
     */
    @Column(name = "user_id")
    @PredicateInfo(queryType = PredicateInfo.QueryType.BASIC)
    private Long userId;

    @Column(name = "user_name")
    private String userName;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date createTime;

    @OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REFRESH,CascadeType.MERGE},fetch=FetchType.LAZY)
    @JoinColumn(name = "meal_id")
    private Set<MealDish> mealDishes;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;
}