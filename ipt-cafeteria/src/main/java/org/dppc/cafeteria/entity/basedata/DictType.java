package org.dppc.cafeteria.entity.basedata;

import java.io.Serializable;
import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

/**
 * @Description 字典表
 * @Author majt
 * @Data 2019/05/09 10:15
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_dict_type")
public class DictType implements Serializable {

    @Id
    /** 主键id */
    @GeneratedValue
    @Column(name = "type_id")
    private Long typeId;

    /** 类型编码 */
    @Column(name = "type_code", length = 11)
    @PredicateInfo(queryType = PredicateInfo.QueryType.BASIC)
    private String typeCode;

    /** 类型名称 */
    @Column(name = "type_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String typeName;

    /** 状态（1启用 0禁用） */
    @Column(name = "status")
    private Integer status;

    /** 添加时间 */
    @Column(name = "create_time")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN,timezone = "GMT+8")
    private Date createTime;

    /** 添加人 */
    @Column(name = "create_user_id")
    private Long createUserId;

    /** 描述 */
    @Column(name = "remarks", length = 50)
    private String remarks;

    /** 是否系统字典(0:是,1:否) */
    @Column(name = "is_sys")
    @PredicateInfo(queryType = PredicateInfo.QueryType.BASIC)
    private Integer isSys;

    /** 排序 */
    @Column(name = "sort")
    private Integer sort;

    @OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REFRESH,CascadeType.MERGE})
    @JoinColumn(name = "dict_type_id")
    private Set<DictData> dictDataSet;
}
