package org.dppc.cafeteria.entity.machining;

import java.io.Serializable;
import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

/**
 * @Description 环境
 * @Author majt
 * @Data 2019/05/20 11:18
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_machining_enviroment")
public class Enviroment implements Serializable {

    @Id
    /** 主键id */
    @GeneratedValue
    @Column(name = "environment_id")
    private Integer environmentId;

    /** 检查项目 */
    @Column(name = "check_item", length = 255)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String checkItem;

    /** 保洁时间 */
    @Column(name = "cleaning_time")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
   @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN,timezone = "GMT+8")
    private Date cleaningTime;

    /** 检查时间 */
    @Column(name = "check_time")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
   @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN,timezone = "GMT+8")
    private Date checkTime;

    /** 状态(1:已检查,0:未检查) */
    @Column(name = "status", length = 255)
    private Integer status;

    /** 检查结果 */
    @Column(name = "check_result", length = 255)
    private String checkResult;

    /** 负责人 */
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "user_name")
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String userName;

    /** 检查人 */
    @Column(name = "check_user_id")
    private Long checkUserId;

    @Column(name = "checkUserName")
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String checkUserName;

    /** 得分 */
    @Column(name = "score", length = 255)
    private Integer score;

    /** b备注 */
    @Column(name = "remark", length = 255)
    private String remark;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;
}