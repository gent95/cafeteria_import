package org.dppc.cafeteria.entity.surveillance;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

/**
 * @Description 
 * @Author majt
 * @Data 2019/06/03 11:07
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_surveillance_play")
public class Play implements Serializable {

    @Id
    /** 自增主键 */
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dppc_surveillance_play_SEQ")
    @SequenceGenerator(name = "dppc_surveillance_play_SEQ", sequenceName = "dppc_surveillance_play_SEQ", allocationSize = 1)
    @Column(name = "play_id")
    private Long playId;

    /** 直播名称 */
    @Column(name = "play_name", length = 50)
    private String playName;

    /** 直播流地址 */
    @Column(name = "play_url", length = 1000)
    private String playUrl;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    private String schoolCode;

    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;

    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    private String cafeteriaCode;

    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    private String cafeteriaName;


}