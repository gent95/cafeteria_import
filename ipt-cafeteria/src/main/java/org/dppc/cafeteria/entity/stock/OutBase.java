package org.dppc.cafeteria.entity.stock;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 原材料出库信息主表
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_stock_out_base")
public class OutBase implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "base_id")
    private Long baseId;

    /** 领用记录编码 */
    @Column(name = "record_code", length = 20)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String recordCode;

    /** 领用人id */
    @Column(name = "consuming_man_id")
    private Long consumingManId;

    /** 领用人 */
    @Column(name = "consuming_man", length = 18)
    private String consumingMan;

    /** 出库人id */
    @Column(name = "out_man_id")
    private Long outManId;

    /** 出库人 */
    @Column(name = "out_man", length = 50)
    private String outMan;

    /** 出库状态 0:待出库 1:已出库 */
    @PredicateInfo
    @Column(name = "out_status")
    private Integer outStatus;

    /** 出库编码 */
    @Column(name = "out_code", length = 20)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String outCode;

    /** 出库时间 */
    @Column(name = "purchase_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN,timezone = "GMT+8")
    private Date purchaseDate;

    /** 创建时间 */
    @Column(name = "create_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN,timezone = "GMT+8")
    private Date createDate;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;

    public OutBase(String recordCode, Long consumingManId, String consumingMan, Integer outStatus, Date createDate) {
        this.recordCode = recordCode;
        this.consumingManId = consumingManId;
        this.consumingMan = consumingMan;
        this.outStatus = outStatus;
        this.createDate = createDate;
    }
}