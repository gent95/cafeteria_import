package org.dppc.cafeteria.entity.machining;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.cafeteria.entity.requisition.RequisitionDetail;
import org.dppc.cafeteria.vo.WeiXinTraceInfoVO;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @Description 菜品
 * @Author majt
 * @Data 2019/05/20 18:05
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_machining_dish")
public class MealDish implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dish_id")
    private Integer dishId;

    /**
     * 菜品名称
     */
    @Column(name = "dish_name", length = 255)
    private String dishName;

    /**
     * 菜品类型
     */
    @Column(name = "dish_type")
    private Integer dishType;

    /**
     * 加工温度
     */
    @Column(name = "maching_temperature", length = 255)
    private String machingTemperature;

    /**
     * 添加剂
     */
    @Column(name = "additive", length = 500)
    private String additive;

    /**
     * 留样
     */
    @Column(name = "retention_sample")
    private Integer retentionSample;

    /**
     * 菜品图片
     */
    @Column(name = "dish_image_url", length = 100)
    private String dishImageUrl;

    /**
     * 餐次编号
     */
    @ManyToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinTable(name = "dppc_machining_dish_materials", joinColumns = {@JoinColumn(name = "dish_id", referencedColumnName = "dish_id")},
            inverseJoinColumns = {@JoinColumn(name = "detail_id", referencedColumnName = "detail_id")})
    private Set<RequisitionDetail> materialsList;

    @ManyToOne(cascade={CascadeType.REFRESH,CascadeType.PERSIST},optional=false)
    @JoinColumn(name = "meal_id")
    @JsonIgnore
    private Meal meal;

    @Transient
    private List<WeiXinTraceInfoVO> traceInfo;

    /**责任人*/
    @Transient
    private String personLiable;
    /**添加剂*/
    @Transient
    private String additiveStr;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;
}