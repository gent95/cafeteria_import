package org.dppc.cafeteria.entity.purchase;

import java.io.Serializable;
import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

/**
 * @Description 采购计划
 * @Author zmm
 * @Data 2019/05/17 15:37
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_purchase_base")
public class PurchaseBase implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "purchase_id")
    private Integer purchaseId;

    /** 补货单编码 */
    @Column(name = "purchase_code", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String purchaseCode;

    /** 供应商名称 */
    @PredicateInfo
    @Column(name = "supplier_name", length = 50)
    private String supplierName;

    /** 统一社会信用代码 */
    @Column(name = "social_credit_code", length = 18)
    @PredicateInfo
    private String socialCreditCode;

    /** 发起人id */
    @Column(name = "user_id")
    private Long userId;

    /** 发起人名称 */
    @Column(name = "user_name", length = 20)
    private String userName;

    /** 采购时间 */
    @Column(name = "create_time")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private Date createTime;

    /** 审核状态：0待审核；1通过；2未通过 */
    @Column(name = "audit_status")
    @PredicateInfo
    private Integer auditStatus;

    /** 审核时间 */
    @Column(name = "check_time")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private Date checkTime;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;

}