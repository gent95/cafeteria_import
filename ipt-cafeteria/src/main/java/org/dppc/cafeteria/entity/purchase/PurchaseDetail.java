package org.dppc.cafeteria.entity.purchase;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;

/**
 * @Description 采购计划明细
 * @Author zmm
 * @Data 2019/05/14 16:43
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_purchase_detail")
public class PurchaseDetail implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "detail_id")
    private Integer detailId;

    /** 采购计划 */
    @Column(name = "purchase_id")
    @PredicateInfo
    private Integer purchaseId;

    /** 食材编码 */
    @Column(name = "materials_code", length = 20)
    private String materialsCode;

    /** 食材名称 */
    @Column(name = "materials_name", length = 50)
    private String materialsName;

    /** 单位 */
    @Column(name = "unit")
    private String unit;

    /** 库存数量 */
    @Column(name = "stock_num")
    private Double stockNum;

    /** 采购数量 */
    @Column(name = "num")
    private Double num;

    /** 单价 */
    @Column(name = "price")
    private Double price;

    /** 总价 */
    @Column(name = "sum_price")
    private Double sumPrice;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;
}