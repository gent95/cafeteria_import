package org.dppc.cafeteria.entity.record;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 供应商
 * @Author GAOJ
 * @Data 2019/05/09 09:45
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_record_supplier")
public class Supplier implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "supplier_id")
    private Integer supplierId;

    /** 统一社会信用代码 */
    @Column(name = "social_credit_code", length = 18)
    @PredicateInfo
    private String socialCreditCode;

    /** 供应商名称 */
    @Column(name = "supplier_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String supplierName;

    /** 供应商类型 */
    @Column(name = "supplier_type")
    @PredicateInfo
    private Integer supplierType;

    /** 企业法人 */
    @Column(name = "legal_person", length = 30)
    private String legalPerson;

    /** 经营地址 */
    @Column(name = "address", length = 50)
    private String address;

    /** 联系人 */
    @Column(name = "contact", length = 30)
    private String contact;

    /** 联系电话 */
    @Column(name = "phone", length = 11)
    private String phone;

    /** 备案人id */
    @Column(name = "create_user_id")
    private Long createUserId;

    /** 备案人姓名 */
    @Column(name = "create_user_name", length = 30)
    private String createUserName;

    /** 备案日期 */
    @Column(name = "create_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_MINUTE_PATTERN, timezone = "GMT+8")
    private Date createDate;

    /** 审核状态 */
    @Column(name = "audit_status")
    @PredicateInfo
    private Integer auditStatus;

    /** 审核人id */
    @Column(name = "audit_user_id")
    private Long auditUserId;

    /** 审核人姓名 */
    @Column(name = "audit_user_name", length = 30)
    private String auditUserName;

    /** 审核日期 */
    @Column(name = "audit_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_MINUTE_PATTERN, timezone = "GMT+8")
    private Date auditDate;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;
}