package org.dppc.cafeteria.entity.record;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.common.enums.IsDeleteEnum;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @Description 器具类型
 * @Author GAOJ
 * @Data 2019/05/09 19:11
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_record_utensil_type")
public class UtensilType implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "utensil_type_id")
    private Integer utensilTypeId;

    /** 类型名称 */
    @Column(name = "type_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String typeName;

    /** 删除状态 */
    @Column(name = "is_delete")
    @PredicateInfo
    private Integer isDelete = IsDeleteEnum.NOT_DELETE.getValue();


}
