package org.dppc.cafeteria.entity.requisition;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Description 领用单
 * @Author GAOJ
 * @Data 2019/05/09 09:49
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_requisition_record")
public class RequisitionRecord implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "record_id")
    private Long recordId;

    /** 领用单编号 */
    @Column(name = "record_code", length = 30)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String recordCode;

    /** 领用人id */
    @Column(name = "personnel_id")
    @PredicateInfo
    private Long personnelId;

    /** 领用人姓名 */
    @Column(name = "name", length = 30)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String name;

    /** 创建时间 */
    @Column(name = "create_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_MINUTE_PATTERN, timezone = "GMT+8")
    private Date createDate;

    /** 创建人id */
    @Column(name = "create_user_id")
    private Long createUserId;

    /** 创建人姓名 */
    @Column(name = "create_user_name", length = 30)
    private String createUserName;

    /** 领用单状态、1 已填单、2 已出库 RequisitionStatusEnum*/
    @Column(name = "status")
    @PredicateInfo
    private Integer status;

    /** 逻辑删除、0 删除、1 正常 */
    @Column(name = "is_delete")
    @PredicateInfo
    private Integer isDelete;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    @JoinColumn(name = "record_code", referencedColumnName = "record_code")
    private List<RequisitionDetail> requisitionDetail;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;
}