package org.dppc.cafeteria.entity.record;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 人员
 * @Author GAOJ
 * @Data 2019/05/09 09:45
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_record_personnel")
public class Personnel implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "personnel_id")
    @PredicateInfo
    private Long personnelId;

    /** 身份证号码 */
    @Column(name = "id_code", length = 18)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String idCode;

    /** 姓名 */
    @Column(name = "name", length = 30)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String name;

    /** 性别 */
    @Column(name = "gender")
    private Integer gender;

    /** 出生日期 */
    @Column(name = "birthday")
    @DateTimeFormat(pattern = DatePattern.NORM_DATE_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATE_PATTERN, timezone = "GMT+8")
    private Date birthday;

    /** 婚姻状况 */
    @Column(name = "marital_status")
    private Integer maritalStatus;

    /** 人员职务 */
    @Column(name = "post")
    @PredicateInfo
    private Integer post;

    /** 手机号码 */
    @Column(name = "phone", length = 11)
    private String phone;

    /** 电子邮箱 */
    @Column(name = "email", length = 30)
    private String email;

    /** 健康证编号 */
    @Column(name = "health_code", length = 30)
    private String healthCode;

    /** 身份证正面 */
    @Column(name = "id_front_url", length = 100)
    private String idFrontUrl;

    /** 身份证反面 */
    @Column(name = "id_back_url", length = 100)
    private String idBackUrl;

    /** 健康证图片 */
    @Column(name = "health_url", length = 100)
    private String healthUrl;

    /** 备案人id */
    @Column(name = "create_user_id")
    private Long createUserId;

    /** 备案人姓名 */
    @Column(name = "create_user_name", length = 30)
    private String createUserName;

    /** 备案日期 */
    @Column(name = "create_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_MINUTE_PATTERN, timezone = "GMT+8")
    private Date createDate;

    /** 审核状态 */
    @Column(name = "audit_status")
    @PredicateInfo
    private Integer auditStatus;

    /** 审核人id */
    @Column(name = "audit_user_id")
    private Long auditUserId;

    /** 审核人姓名 */
    @Column(name = "audit_user_name", length = 30)
    private String auditUserName;

    /** 审核日期 */
    @Column(name = "audit_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date auditDate;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;
    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;
    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;


}