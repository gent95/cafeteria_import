package org.dppc.cafeteria.annotation;

import java.lang.annotation.*;

/**
 * @Description 食堂端添加数据注解
 * @Author lhw
 * @Data 2019/05/09 15:22
 * @Version 1.0
 **/
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CafeteriaAddData {
}
