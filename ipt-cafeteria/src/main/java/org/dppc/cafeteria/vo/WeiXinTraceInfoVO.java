package org.dppc.cafeteria.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description 追溯信息查询vo
 * @Author lhw
 * @Data 2019/5/28 15:15
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeiXinTraceInfoVO {
    private String materialsName;
    private String batchCode;
    private String purchaseDate;
    private String inMan;
    private String SocialCreditCode;
    private String SupplierName;
}
