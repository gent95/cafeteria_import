package org.dppc.cafeteria.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.cafeteria.entity.stock.InventoryBase;
import org.dppc.cafeteria.entity.stock.InventoryDetail;

import java.util.List;

/**
 * @Description 盘库VO
 * @Author lhw
 * @Data 2019/5/13 15:04
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InventoryVO {
    private InventoryBase inventoryBase;
    private List<InventoryDetail> inventoryDetailList;
}
