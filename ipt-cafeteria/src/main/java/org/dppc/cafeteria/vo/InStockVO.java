package org.dppc.cafeteria.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.cafeteria.entity.stock.InBase;
import org.dppc.cafeteria.entity.stock.InDetail;

import java.util.List;

/**
 * @Description 入库信息VO
 * @Author lhw
 * @Data 2019/5/11 13:43
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InStockVO {
    private InBase inBase;
    private List<InDetail> inDetailList;
}
