package org.dppc.cafeteria.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.cafeteria.entity.machining.Meal;
import org.dppc.cafeteria.entity.machining.MealDish;

import java.util.List;

/**
 * @描述 ：餐次菜品Vo
 * @作者 ：majt
 * @日期 ：2019/5/22
 * @时间 ：9:34
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MealDishVo {
    private Meal meal;
    private List<MealDish> mealDishList;
}
