package org.dppc.cafeteria.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.cafeteria.entity.stock.OutBase;
import org.dppc.cafeteria.entity.stock.OutDetail;

import java.util.List;

/**
 * @Description 出库VO
 * @Author lhw
 * @Data 2019/5/13 15:04
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OutStockVO {
    private OutBase outBase;
    private List<OutDetail> outDetailList;
}
