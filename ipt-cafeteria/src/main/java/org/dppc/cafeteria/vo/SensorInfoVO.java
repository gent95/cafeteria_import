package org.dppc.cafeteria.vo;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.common.enums.SensorStatusEnum;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Description 监控实时信息查看VO
 * @Author lhw
 * @Data 2019/6/4 15:22
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SensorInfoVO {
    /**
     * 设备名称
     */
    private String deviceDesc;
    /**
     * 设备地址
     */
    private Integer deviceId;
    /**
     * 温度
     */
    private String temps;
    /**
     * 最小温度
     */
    private Float maxTemp;
    /**
     * 最大温度
     */
    private Float minTemp;
    /**
     * 湿度
     */
    private String hums;
    /**
     * 最小温度
     */
    private Float maxHum;
    /**
     * 最大温度
     */
    private Float minHum;
    /**
     * 记录时间
     */
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN, timezone = "GMT+8")
    private Date recordTime;
    /**
     * 间隔时间 (秒)
     */
    private String intervalTime;
    /**
     * 状态  SensorStatusEnum
     */
    private Integer status;
    /** 学校编码 */
    private String schoolCode;
    /** 学校名称 */
    private String schoolName;
    /** 食堂编码 */
    private String cafeteriaCode;
    /** 食堂名称 */
    private String cafeteriaName;

    public Integer gainStatus() {
        //判断是否离线
        if (recordTime != null) {
            if (DateUtil.between(recordTime, DateUtil.date(), DateUnit.SECOND) > Integer.parseInt(intervalTime)) {
                //上传时间与现在系统时间的间隔大于 区间时间  则代码设备离线
                return SensorStatusEnum.STOP.getValue();
            } else {
                if (maxTemp != null && minTemp != null && maxHum != null && minHum != null) {
                    String[] tempStrs = this.getTemps().split(",");
                    String[] humStrs = this.getHums().split(",");
                    for (int i = 0; i < tempStrs.length; i++) {
                        Float tem = Float.parseFloat(tempStrs[i]);
                        Float hum = Float.parseFloat(humStrs[i]);
                        if (tem < minTemp || tem > maxTemp || hum < minHum || hum > maxHum) {
                            return SensorStatusEnum.EARLY_WARNING.getValue();
                        }
                    }
                    return SensorStatusEnum.NORMAL.getValue();
                } else {
                    System.out.println("区间值不规范无法进行预警判断");
                    return SensorStatusEnum.NORMAL.getValue();
                }
            }
        } else {
            return SensorStatusEnum.STOP.getValue();
        }
    }
}
