package org.dppc.cafeteria.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description 食堂信息VO
 * @Author lhw
 * @Data 2019/6/8 15:51
 * @Version 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CafeteriaInfoVO {
    /**
     * 学校编码
     */
    private String schoolCode;

    /**
     * 食堂编码
     */
    private String cafeteriaCode;
    /**
     * 食堂名称
     */
    private String cafeteriaName;
    /**
     * 学校名称
     */
    private String schoolName;
}
