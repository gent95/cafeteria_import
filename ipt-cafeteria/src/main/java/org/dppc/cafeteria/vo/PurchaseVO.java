package org.dppc.cafeteria.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.cafeteria.entity.purchase.PurchaseBase;
import org.dppc.cafeteria.entity.purchase.PurchaseDetail;

import java.util.List;

/**
 * @Description 采购信息VO
 * @Author lhw
 * @Data 2019/5/31 17:54
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseVO {
    private PurchaseBase purchaseBase;
    private List<PurchaseDetail> purchaseDetailList;
}
