package org.dppc.cafeteria.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description 追溯信息查询vo
 * @Author lhw
 * @Data 2019/5/28 15:15
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TraceInfoVO {
    private String title;
    private List<Attr> attrList = new ArrayList<>();
    private List<TraceInfoVO> nextChains = null;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Attr {
        private String name;
        private String value;
    }
}
