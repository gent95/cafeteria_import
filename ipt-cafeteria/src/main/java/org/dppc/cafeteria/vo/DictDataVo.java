package org.dppc.cafeteria.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.dppc.cafeteria.entity.basedata.DictData;
import org.dppc.common.enums.EnableEnum;
import org.dppc.common.enums.UserTypeEnum;
import org.dppc.common.utils.NullHelper;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：字典
 * @作者 ：majt
 * @日期 ：2019/5/13
 * @时间 ：14:39
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DictDataVo extends  DictData{
    private String statusName;
    public static DictDataVo entityToVO(DictData dictData) {
        DictDataVo dictDataVo = new DictDataVo();
        BeanUtils.copyProperties(dictData, dictDataVo);
        dictDataVo.setStatusName(0 == dictDataVo.getStatus()?EnableEnum.DISABLE.getName():EnableEnum.ENABLE.getName());
        return dictDataVo;
    }
}
