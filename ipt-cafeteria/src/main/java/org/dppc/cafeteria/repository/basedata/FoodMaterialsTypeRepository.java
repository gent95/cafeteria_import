package org.dppc.cafeteria.repository.basedata;

import org.dppc.cafeteria.entity.basedata.FoodMaterialsType;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 食材类型表数据库操作层
 * @Author majt
 * @Data 2019/05/09 15:42
 * @Version 1.0
 **/
public interface FoodMaterialsTypeRepository extends BaseRepository<FoodMaterialsType, Integer> {

}
