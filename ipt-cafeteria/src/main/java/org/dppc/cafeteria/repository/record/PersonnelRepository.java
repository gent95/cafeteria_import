package org.dppc.cafeteria.repository.record;

import org.dppc.cafeteria.entity.record.Personnel;
import org.dppc.dbexpand.repository.BaseRepository;

import java.util.List;

/**
 * @Description 人员数据库操作层
 * @Author GAOJ
 * @Data 2019/05/09 09:45
 * @Version 1.0
 **/
public interface PersonnelRepository extends BaseRepository<Personnel, Long> {

    List<Personnel> findByIdCode(String idCode);
}
