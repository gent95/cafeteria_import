package org.dppc.cafeteria.repository.machining;

import org.dppc.cafeteria.entity.machining.Meal;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 餐次数据库操作层
 * @Author majt
 * @Data 2019/05/20 18:05
 * @Version 1.0
 **/
public interface MealRepository extends BaseRepository<Meal, Long> {

}
