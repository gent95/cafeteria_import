package org.dppc.cafeteria.repository.basedata;

import org.dppc.cafeteria.entity.basedata.DictData;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 字典数据数据库操作层
 * @Author majt
 * @Data 2019/05/09 10:15
 * @Version 1.0
 **/
public interface DictDataRepository extends BaseRepository<DictData, Long> {

}
