package org.dppc.cafeteria.repository.purchase;

import org.dppc.cafeteria.entity.purchase.PurchaseDetail;
import org.dppc.dbexpand.repository.BaseRepository;

import java.util.List;

/**
 * @Description 采购计划明细数据库操作层
 * @Author zmm
 * @Data 2019/05/14 16:43
 * @Version 1.0
 **/
public interface PurchaseDetailRepository extends BaseRepository<PurchaseDetail, Integer> {

    List<PurchaseDetail> findByPurchaseId(Integer id);
}
