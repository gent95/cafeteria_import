package org.dppc.cafeteria.repository.requisition;

import org.dppc.cafeteria.entity.requisition.RequisitionDetail;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 领用单明细数据库操作层
 * @Author GAOJ
 * @Data 2019/05/09 09:49
 * @Version 1.0
 **/
public interface RequisitionDetailRepository extends BaseRepository<RequisitionDetail, Long> {

}
