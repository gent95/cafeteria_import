package org.dppc.cafeteria.repository.requisition;

import org.dppc.cafeteria.entity.requisition.RequisitionRecord;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 领用单数据库操作层
 * @Author GAOJ
 * @Data 2019/05/09 09:49
 * @Version 1.0
 **/
public interface RequisitionRecordRepository extends BaseRepository<RequisitionRecord, Long> {

    RequisitionRecord findByRecordCode(String recordCode);
}
