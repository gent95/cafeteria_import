package org.dppc.cafeteria.repository.emergency;

import org.dppc.cafeteria.entity.emergency.EmergencyPlan;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 应急预案数据库操作层
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
public interface EmergencyPlanRepository extends BaseRepository<EmergencyPlan, Long> {

}
