package org.dppc.cafeteria.repository.decontamination;

import org.dppc.cafeteria.entity.decontamination.Decontamination;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 洗消管理数据库操作层
 * @Author zhumh
 * @Data 2019/05/10 17:12
 * @Version 1.0
 **/
public interface DecontaminationRepository extends BaseRepository<Decontamination, Long> {

}
