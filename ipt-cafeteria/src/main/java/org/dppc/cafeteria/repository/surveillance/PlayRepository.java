package org.dppc.cafeteria.repository.surveillance;

import org.dppc.cafeteria.entity.surveillance.Play;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 数据库操作层
 * @Author majt
 * @Data 2019/06/03 11:07
 * @Version 1.0
 **/
public interface PlayRepository extends BaseRepository<Play, Long> {

}
