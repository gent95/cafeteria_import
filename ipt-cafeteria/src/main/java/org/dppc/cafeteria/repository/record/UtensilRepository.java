package org.dppc.cafeteria.repository.record;

import org.dppc.cafeteria.entity.record.Utensil;
import org.dppc.dbexpand.repository.BaseRepository;

import java.util.List;

/**
 * @Description 器具数据库操作层
 * @Author GAOJ
 * @Data 2019/05/09 09:45
 * @Version 1.0
 **/
public interface UtensilRepository extends BaseRepository<Utensil, Long> {
   /**
    * @return
    * @Author majt
    * @Description 通过器具类型和审核状态查询
    * @Date 2019/6/5 10:42
    * @Param
    **/
    List<Utensil> findByTypeAndAuditStatus(Integer type,Integer auditStatus);
}
