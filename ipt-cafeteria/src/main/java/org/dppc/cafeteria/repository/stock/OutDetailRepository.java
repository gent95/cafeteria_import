package org.dppc.cafeteria.repository.stock;

import org.dppc.cafeteria.entity.stock.OutDetail;
import org.dppc.dbexpand.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @Description 原材料出库信息细表数据库操作层
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
public interface OutDetailRepository extends BaseRepository<OutDetail, Long> {
    @Query("delete from OutDetail where baseId =?1")
    void deleteByBaseId(Long baseId);

    List<OutDetail> findByBaseIdAndMaterialsCode(Long baseId, String materialsCode);
}
