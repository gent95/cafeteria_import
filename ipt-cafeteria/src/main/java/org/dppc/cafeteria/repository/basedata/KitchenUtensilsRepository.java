package org.dppc.cafeteria.repository.basedata;

import org.dppc.cafeteria.entity.basedata.KitchenUtensils;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 数据库操作层
 * @Author majt
 * @Data 2019/06/03 19:38
 * @Version 1.0
 **/
public interface KitchenUtensilsRepository extends BaseRepository<KitchenUtensils, Long> {

}
