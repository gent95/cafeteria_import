package org.dppc.cafeteria.repository.peican;

import org.dppc.cafeteria.entity.peican.PeiCan;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 陪餐管理数据库操作层
 * @Author zhumh
 * @Data 2019/05/10 17:11
 * @Version 1.0
 **/
public interface PeiCanRepository extends BaseRepository<PeiCan, Long> {

}
