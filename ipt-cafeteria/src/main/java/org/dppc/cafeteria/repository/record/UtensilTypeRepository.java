package org.dppc.cafeteria.repository.record;

import org.dppc.cafeteria.entity.record.UtensilType;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 器具类型数据库操作层
 * @Author GAOJ
 * @Data 2019/05/09 19:11
 * @Version 1.0
 **/
public interface UtensilTypeRepository extends BaseRepository<UtensilType, Integer> {

}
