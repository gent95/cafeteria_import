package org.dppc.cafeteria.repository.stock;

import org.dppc.cafeteria.entity.stock.Info;
import org.dppc.dbexpand.repository.BaseRepository;

import java.util.List;

/**
 * @Description 库存信息表数据库操作层
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
public interface InfoRepository extends BaseRepository<Info, Long> {

    Info findByBatchCodeAndMaterialsCode(String batchCode, String materialsCode);

    List<Info> findByIsDeleteAndCafeteriaCode(int value,String feteriaCode);
}
