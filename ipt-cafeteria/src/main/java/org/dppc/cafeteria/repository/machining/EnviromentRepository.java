package org.dppc.cafeteria.repository.machining;

import org.dppc.cafeteria.entity.machining.Enviroment;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 环境数据库操作层
 * @Author majt
 * @Data 2019/05/20 11:18
 * @Version 1.0
 **/
public interface EnviromentRepository extends BaseRepository<Enviroment, Integer> {

}
