package org.dppc.cafeteria.repository.surveillance;

import org.dppc.cafeteria.entity.surveillance.SensorData;
import org.dppc.dbexpand.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @Description 传感器数据数据库操作层
 * @Author lhw
 * @Data 2019/06/04 16:07
 * @Version 1.0
 **/
public interface SensorDataRepository extends BaseRepository<SensorData, Long> {
    @Query("select s from SensorData s where s.deviceId = ?1 " +
            "and s.recordTime = (select max(recordTime) from SensorData where deviceId = ?1)")
    List<SensorData> findCurrenInfo(Integer deviceId);
}
