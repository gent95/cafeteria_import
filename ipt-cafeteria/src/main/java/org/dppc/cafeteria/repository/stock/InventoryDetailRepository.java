package org.dppc.cafeteria.repository.stock;

import org.dppc.cafeteria.entity.stock.InventoryDetail;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 盘库详情表数据库操作层
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
public interface InventoryDetailRepository extends BaseRepository<InventoryDetail, Long> {

}
