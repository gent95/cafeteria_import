package org.dppc.cafeteria.repository.record;

import org.dppc.cafeteria.entity.record.PersonnelPost;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 人员职务数据库操作层
 * @Author GAOJ
 * @Data 2019/05/09 19:11
 * @Version 1.0
 **/
public interface PersonnelPostRepository extends BaseRepository<PersonnelPost, Integer> {

}
