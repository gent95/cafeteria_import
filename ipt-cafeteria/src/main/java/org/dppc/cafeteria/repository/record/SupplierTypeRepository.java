package org.dppc.cafeteria.repository.record;

import org.dppc.cafeteria.entity.record.SupplierType;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 供应商类型数据库操作层
 * @Author GAOJ
 * @Data 2019/05/09 19:11
 * @Version 1.0
 **/
public interface SupplierTypeRepository extends BaseRepository<SupplierType, Integer> {

}
