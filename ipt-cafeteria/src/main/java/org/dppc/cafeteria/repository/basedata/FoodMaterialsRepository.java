package org.dppc.cafeteria.repository.basedata;

import org.dppc.cafeteria.entity.basedata.FoodMaterials;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 食材表数据库操作层
 * @Author majt
 * @Data 2019/05/09 15:42
 * @Version 1.0
 **/
public interface FoodMaterialsRepository extends BaseRepository<FoodMaterials, Integer> {

    FoodMaterials findByMaterialsCode(String materialsCode);
}
