package org.dppc.cafeteria.repository.purchase;

import org.dppc.cafeteria.entity.purchase.PurchaseBase;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 采购计划数据库操作层
 * @Author zmm
 * @Data 2019/05/17 15:37
 * @Version 1.0
 **/
public interface PurchaseBaseRepository extends BaseRepository<PurchaseBase, Integer> {

}
