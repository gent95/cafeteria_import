package org.dppc.cafeteria.repository.syscode;

import org.dppc.cafeteria.entity.syscode.SysCode;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 数据库操作层
 * @Author GAOJ
 * @Data 2019/05/14 14:00
 * @Version 1.0
 **/
public interface SysCodeRepository extends BaseRepository<SysCode, Integer> {

    SysCode findByCodeType(String codeType);

}
