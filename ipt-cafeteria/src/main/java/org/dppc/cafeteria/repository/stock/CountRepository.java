package org.dppc.cafeteria.repository.stock;

import org.dppc.cafeteria.entity.stock.Count;
import org.dppc.dbexpand.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @Description 库存统计信息表数据库操作层
 * @Author lhw
 * @Data 2019/05/09 15:22
 * @Version 1.0
 **/
public interface CountRepository extends BaseRepository<Count, Long> {

    Count findByMaterialsCodeAndCafeteriaCode(String materialsCode, String cafeteriaCode);
    @Query("select sum(num) from Count where materialsCode = ?1 and cafeteriaCode = ?2")
    Double statFoodMaterialsNum(String materialsCode, String cafeteriaCode);
}
