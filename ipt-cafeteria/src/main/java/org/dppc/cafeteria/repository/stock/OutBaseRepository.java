package org.dppc.cafeteria.repository.stock;

import org.dppc.cafeteria.entity.stock.OutBase;
import org.dppc.dbexpand.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @Description 原材料出库信息主表数据库操作层
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
public interface OutBaseRepository extends BaseRepository<OutBase, Long> {

    OutBase findByRecordCode(String recordCode);
}
