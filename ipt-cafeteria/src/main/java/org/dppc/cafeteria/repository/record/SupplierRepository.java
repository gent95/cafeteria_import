package org.dppc.cafeteria.repository.record;

import org.dppc.cafeteria.entity.record.Supplier;
import org.dppc.dbexpand.repository.BaseRepository;

import java.util.List;

/**
 * @Description 供应商数据库操作层
 * @Author GAOJ
 * @Data 2019/05/09 09:45
 * @Version 1.0
 **/
public interface SupplierRepository extends BaseRepository<Supplier, Integer> {

    List<Supplier> findBySocialCreditCode(String socialCreditCode);

}
