package org.dppc.cafeteria.repository.machining;

import org.dppc.cafeteria.entity.machining.MealDish;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 菜品数据库操作层
 * @Author majt
 * @Data 2019/05/20 18:05
 * @Version 1.0
 **/
public interface MealDishRepository extends BaseRepository<MealDish, Integer> {

}
