package org.dppc.cafeteria.repository.emergency;

import org.dppc.cafeteria.entity.emergency.EmergencyType;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 应急事件类型数据库操作层
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
public interface EmergencyTypeRepository extends BaseRepository<EmergencyType, Integer> {

}
