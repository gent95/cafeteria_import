package org.dppc.cafeteria.rest.decontamination;

import lombok.extern.java.Log;
import org.dppc.cafeteria.entity.decontamination.Decontamination;
import org.dppc.cafeteria.service.decontamination.DecontaminationService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.entity.Pager;
import org.dppc.common.enums.DecontaminationStatusEnum;
import org.dppc.common.enums.DisinfectStatusEnum;
import org.dppc.common.enums.MealTimesEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;

/**
 * @Description 洗消管理
 * @Author zhumh
 * @Data 2019/05/10 17:12
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/decontamination")
public class DecontaminationRest {

    @Autowired
    private DecontaminationService decontaminationService;

    /**
     * @Author zhumh
     * @Description 根据条件分页查询洗消管理
     * @Date 2019/05/10 17:12
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, Decontamination decontamination) {
        /** 根据条件分页查询集合 **/
        decontamination.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(decontamination.getCafeteriaCode()));
        Page<Decontamination> page = decontaminationService.findPageList(decontamination,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        HashMap<Object, Object> map = new HashMap<>(54);
        if (pager.getPage() == 1) {
            map.put("mealTimesEnum", MealTimesEnum.values());
            map.put("disinfectStatusEnum", DisinfectStatusEnum.values());
            map.put("decontaminationStatusEnum", DecontaminationStatusEnum.values());
        }
        return ResponseHelper.success(page, map);
    }

    /**
     * @Author zhumh
     * @Description 根据id查询洗消管理
     * @Date 2019/05/10 17:12
     **/
    @GetMapping("/{id}")
    public BaseResponse findDecontamination(@PathVariable Long id) {
        return ResponseHelper.success(decontaminationService.getOneDataById(id));
    }

    /**
     * @Author zhumh
     * @Description 添加洗消管理
     * @Date 2019/05/10 17:12
     **/
    @PostMapping
    public BaseResponse saveDecontamination(@RequestBody Decontamination decontamination, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            decontamination.setCreateTime(new Date());
            decontamination.setCreateUserId(Long.valueOf(BaseContextHandler.getUserID()));
            decontamination.setCreateUserName(BaseContextHandler.getName());
            CafeteriaHandleUtil.bindCafeteriaInfo(decontamination);
            decontaminationService.save(decontamination);
            return ResponseHelper.success(decontamination);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author zhumh
     * @Description 局部更新洗消管理
     * @Date 2019/05/10 17:12
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchDecontamination(@PathVariable Long id, @RequestBody Decontamination decontamination) {
        try {
            boolean result = decontaminationService.patch(decontamination, id);
            if (result) {
                return ResponseHelper.success(decontamination);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author zhumh
     * @Description 根据id删除洗消管理
     * @Date 2019/05/10 17:12
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteDecontamination(@PathVariable Long id) {
        try {
            decontaminationService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
