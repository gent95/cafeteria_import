package org.dppc.cafeteria.rest.peican;

import lombok.extern.java.Log;
import org.dppc.cafeteria.entity.peican.PeiCan;
import org.dppc.cafeteria.service.peican.PeiCanService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.entity.Pager;
import org.dppc.common.enums.MealTimesEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;

/**
 * @Description 陪餐管理
 * @Author zhumh
 * @Data 2019/05/10 17:11
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/peiCan")
public class PeiCanRest {

    @Autowired
    private PeiCanService peiCanService;

    /**
     * @Author zhumh
     * @Description 根据条件分页查询陪餐管理
     * @Date 2019/05/10 17:11
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, PeiCan peiCan) {
        /** 根据条件分页查询集合 **/
        peiCan.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(peiCan.getCafeteriaCode()));
        Page<PeiCan> page = peiCanService.findPageList(peiCan,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        HashMap<Object, Object> map = new HashMap<>(2);
        if (pager.getPage() == 1) {
            map.put("mealTimesEnum", MealTimesEnum.values());
        }
        return ResponseHelper.success(page, map);
    }

    /**
     * @Author zhumh
     * @Description 根据id查询陪餐管理
     * @Date 2019/05/10 17:11
     **/
    @GetMapping("/{id}")
    public BaseResponse findPeiCan(@PathVariable Long id) {
        return ResponseHelper.success(peiCanService.getOneDataById(id));
    }

    /**
     * @Author zhumh
     * @Description 添加陪餐管理
     * @Date 2019/05/10 17:11
     **/
    @PostMapping
    public BaseResponse savePeiCan(@Valid @RequestBody PeiCan peiCan, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            peiCan.setCreateTime(new Date());
            peiCan.setCreateUserId(Long.valueOf(BaseContextHandler.getUserID()));
            peiCan.setCreateUserName(BaseContextHandler.getName());
            CafeteriaHandleUtil.bindCafeteriaInfo(peiCan);
            peiCanService.save(peiCan);
            return ResponseHelper.success(peiCan);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author zhumh
     * @Description 局部更新陪餐管理
     * @Date 2019/05/10 17:11
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchPeiCan(@PathVariable Long id, @RequestBody PeiCan peiCan) {
        try {
            boolean result = peiCanService.patch(peiCan, id);
            if (result) {
                return ResponseHelper.success(peiCan);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author zhumh
     * @Description 根据id删除陪餐管理
     * @Date 2019/05/10 17:11
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deletePeiCan(@PathVariable Long id) {
        try {
            peiCanService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
