package org.dppc.cafeteria.rest.record;

import lombok.extern.java.Log;
import org.dppc.cafeteria.entity.record.Personnel;
import org.dppc.cafeteria.entity.record.PersonnelPost;
import org.dppc.cafeteria.service.record.PersonnelPostService;
import org.dppc.cafeteria.service.record.PersonnelService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.entity.Pager;
import org.dppc.common.enums.GenderEnum;
import org.dppc.common.enums.MaritalStatusEnum;
import org.dppc.common.enums.RecordAuditStatusEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

/**
 * @Description 人员
 * @Author GAOJ
 * @Data 2019/05/09 09:45
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/personnel")
public class PersonnelRest {

    @Autowired
    private PersonnelService personnelService;

    @Autowired
    private PersonnelPostService personnelPostService;

    /**
     * @Author GAOJ
     * @Description 根据条件分页查询人员
     * @Date 2019/05/09 09:45
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, Personnel personnel) {
        personnel.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(personnel.getCafeteriaCode()));
        /** 根据条件分页查询集合 **/
        Page<Personnel> page = personnelService.findPageList(personnel,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        HashMap<Object, Object> map = new HashMap<>(5);
        map.put("postList", personnelPostService.findList(new PersonnelPost()));
        map.put("auditStatusEnums", RecordAuditStatusEnum.values());
        map.put("genderEnum", GenderEnum.values());
        map.put("maritalStatusEnum", MaritalStatusEnum.values());
        return ResponseHelper.success(page, map);
    }

    /**
     * @Author GAOJ
     * @Description 根据条件查询人员
     * @Date 2019/05/09 09:45
     **/
    @GetMapping("/list")
    public BaseResponse list(Personnel personnel) {
        personnel.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(personnel.getCafeteriaCode()));
        List<Personnel> list = personnelService.findList(personnel);
        return ResponseHelper.success(list);
    }

    /**
     * @Author GAOJ
     * @Description 根据id查询人员
     * @Date 2019/05/09 09:45
     **/
    @GetMapping("/{id}")
    public BaseResponse findPersonnel(@PathVariable Long id) {
        return ResponseHelper.success(personnelService.getOneDataById(id));
    }

    /**
     * @Author GAOJ
     * @Description 添加人员
     * @Date 2019/05/09 09:45
     **/
    @PostMapping
    public BaseResponse savePersonnel(@Valid @RequestBody Personnel personnel, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            personnel.setAuditStatus(RecordAuditStatusEnum.NOT_AUDIT.getValue());
            personnel.setCreateDate(new Date());
            personnel.setCreateUserId(Long.valueOf(BaseContextHandler.getUserID()));
            personnel.setCreateUserName(BaseContextHandler.getName());
            CafeteriaHandleUtil.bindCafeteriaInfo(personnel);
            personnelService.save(personnel);
            return ResponseHelper.success(personnel);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author GAOJ
     * @Description 局部更新人员
     * @Date 2019/05/09 09:45
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchPersonnel(@PathVariable Long id, @RequestBody Personnel personnel) {
        try {
            boolean result = personnelService.patch(personnel, id);
            if (result) {
                return ResponseHelper.success(personnel);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 审核人员
     * @Date 2019/05/09 09:45
     **/
    @PatchMapping(value = "/audit")
    public BaseResponse patchPersonnel(@RequestBody Personnel personnel) {
        try {
            personnel.setAuditDate(new Date());
            personnel.setAuditUserId(Long.valueOf(BaseContextHandler.getUserID()));
            personnel.setAuditUserName(BaseContextHandler.getName());
            boolean result = personnelService.patch(personnel, personnel.getPersonnelId());
            if (result) {
                return ResponseHelper.success(personnel);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 根据id删除人员
     * @Date 2019/05/09 09:45
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deletePersonnel(@PathVariable Long id) {
        try {
            Personnel personnel = personnelService.getOneDataById(id);
            if (personnel != null && RecordAuditStatusEnum.AUDIT_PASSED.getValue() != personnel.getAuditStatus()) {
                personnelService.delete(id);
                return ResponseHelper.success(id);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    @GetMapping("/checkCode")
    public boolean checkCode(String idCode, Long personnelId) {
        try {
            Personnel personnel = personnelService.findByIdCode(idCode);
            if (personnel == null) {
                return true;
            }
            if (personnelId != null && personnel.getPersonnelId().equals(personnelId)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
