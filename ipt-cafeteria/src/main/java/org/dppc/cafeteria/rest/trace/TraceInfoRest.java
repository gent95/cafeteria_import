package org.dppc.cafeteria.rest.trace;

import lombok.extern.java.Log;
import org.dppc.cafeteria.entity.machining.MealDish;
import org.dppc.cafeteria.service.trace.TraceInfoService;
import org.dppc.cafeteria.vo.TraceInfoVO;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description 追溯信息查询
 * @Author lhw
 * @Data 2019/5/28 15:22
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/traceInfo")
public class TraceInfoRest {
    @Autowired
    private TraceInfoService traceInfoService;

    /**
     * @Author lhw
     * @Description 根据食材编号查询食材信息
     * @Date 17:36 2019/5/28
     **/
    @GetMapping("/{dishId}")
    public BaseResponse findRequisitionDetail(@PathVariable Integer dishId) {
        TraceInfoVO traceInfoVO = traceInfoService.findByDishId(dishId);
        return ResponseHelper.success(traceInfoVO);
    }

    /**
     * @Author gaojing
     * @Description 微信公众号获取菜品追溯信息
     * @Date 17:36 2019/5/28
     **/
    @GetMapping("/weixin/{dishId}")
    public BaseResponse weixnRequisitionDetail(@PathVariable Integer dishId) {
        MealDish dish = traceInfoService.findDishByDishId(dishId);
        return ResponseHelper.success(dish);
    }
}
