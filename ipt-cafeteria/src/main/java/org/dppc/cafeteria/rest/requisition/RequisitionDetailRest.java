package org.dppc.cafeteria.rest.requisition;

import lombok.extern.java.Log;
import org.dppc.cafeteria.entity.requisition.RequisitionDetail;
import org.dppc.cafeteria.service.requisition.RequisitionDetailService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 领用单明细
 * @Author GAOJ
 * @Data 2019/05/09 09:49
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/requisitionDetail")
public class RequisitionDetailRest {

    @Autowired
    private RequisitionDetailService requisitionDetailService;

    /**
     * @Author GAOJ
     * @Description 根据条件分页查询领用单明细
     * @Date 2019/05/09 09:49
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, RequisitionDetail requisitionDetail) {
        /** 根据条件分页查询集合 **/
        requisitionDetail.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(requisitionDetail.getCafeteriaCode()));
        Page<RequisitionDetail> page = requisitionDetailService.findPageList(requisitionDetail,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author GAOJ
     * @Description 根据id查询领用单明细
     * @Date 2019/05/09 09:49
     **/
    @GetMapping("/{id}")
    public BaseResponse findRequisitionDetail(@PathVariable Long id) {
        return ResponseHelper.success(requisitionDetailService.getOneDataById(id));
    }

    /**
     * @Author GAOJ
     * @Description 添加领用单明细
     * @Date 2019/05/09 09:49
     **/
    @PostMapping
    public BaseResponse saveRequisitionDetail(@Valid @RequestBody RequisitionDetail requisitionDetail, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            requisitionDetailService.save(requisitionDetail);
            return ResponseHelper.success(requisitionDetail);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author GAOJ
     * @Description 局部更新领用单明细
     * @Date 2019/05/09 09:49
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchRequisitionDetail(@PathVariable Long id, @RequestBody RequisitionDetail requisitionDetail) {
        try {
            boolean result = requisitionDetailService.patch(requisitionDetail, id);
            if (result) {
                return ResponseHelper.success(requisitionDetail);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 根据id删除领用单明细
     * @Date 2019/05/09 09:49
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteRequisitionDetail(@PathVariable Long id) {
        try {
            requisitionDetailService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
