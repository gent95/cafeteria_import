package org.dppc.cafeteria.rest.stock;

import org.dppc.cafeteria.entity.stock.Count;
import org.dppc.cafeteria.service.stock.CountService;
import lombok.extern.java.Log;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 库存统计信息表
 * @Author lhw
 * @Data 2019/05/09 15:22
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/stock/count")
public class CountRest {

    @Autowired
    private CountService countService;

    /**
     * @Author lhw
     * @Description 根据条件分页查询库存统计信息表
     * @Date 2019/05/09 15:22
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, Count count) {
        count.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(count.getCafeteriaCode()));
        /** 根据条件分页查询集合 **/
        Page<Count> page = countService.findPageList(count,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author lhw
     * @Description 根据条件分页查询库存统计信息表
     * @Date 2019/05/09 15:22
     **/
    @GetMapping("/statFoodMaterialsNum/")
    public BaseResponse statFoodMaterialsNum(String materialsCode) {
        return ResponseHelper.success(countService.statFoodMaterialsNum(materialsCode));
    }

    /**
     * @Author lhw
     * @Description 根据id查询库存统计信息表
     * @Date 2019/05/09 15:22
     **/
    @GetMapping("/{id}")
    public BaseResponse findCount(@PathVariable Long id) {
        return ResponseHelper.success(countService.getOneDataById(id));
    }

    /**
     * @Author lhw
     * @Description 添加库存统计信息表
     * @Date 2019/05/09 15:22
     **/
    @PostMapping
    public BaseResponse saveCount(@Valid @RequestBody Count count, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            countService.save(count);
            return ResponseHelper.success(count);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author lhw
     * @Description 局部更新库存统计信息表
     * @Date 2019/05/09 15:22
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchCount(@PathVariable Long id, @RequestBody Count count) {
        try {
            boolean result = countService.patch(count, id);
            if (result) {
                return ResponseHelper.success(count);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author lhw
     * @Description 根据id删除库存统计信息表
     * @Date 2019/05/09 15:22
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteCount(@PathVariable Long id) {
        try {
            countService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
