package org.dppc.cafeteria.rest.stock;

import lombok.extern.java.Log;
import org.dppc.cafeteria.annotation.CafeteriaQueryData;
import org.dppc.cafeteria.entity.stock.InBase;
import org.dppc.cafeteria.service.stock.InBaseService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.cafeteria.vo.InStockVO;
import org.dppc.common.entity.Pager;
import org.dppc.common.enums.stock.InStatusEnum;
import org.dppc.common.enums.stock.QGPUnitEnum;
import org.dppc.common.exception.RepeatOperateException;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 原材料入库信息主表
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/stock/inBase")
public class InBaseRest {

    @Autowired
    private InBaseService inBaseService;

    /**
     * @Author lhw
     * @Description 根据条件分页查询原材料入库信息主表
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/page")
    @CafeteriaQueryData
    public BaseResponse page(Pager pager, InBase inBase) {
        inBase.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(inBase.getCafeteriaCode()));
        /** 根据条件分页查询集合 **/
        Page<InBase> page = inBaseService.findPageList(inBase,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author lhw
     * @Description 根据id查询原材料入库信息主表
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/{id}")
    public BaseResponse findInBase(@PathVariable Long id) {
        return ResponseHelper.success(inBaseService.getOneDataById(id));
    }

    /**
     * @Author lhw
     * @Description 查询入库状态枚举集合
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/inStatusEnums/")
    public BaseResponse gatInStatusEnums() {
        return ResponseHelper.success(InStatusEnum.getEnumList());
    }

    /**
     * @Author lhw
     * @Description 查询计量单位枚举集合
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/QGPUnitEnums/")
    public BaseResponse getQGPUnitEnums() {
        return ResponseHelper.success(QGPUnitEnum.getEnumList());
    }


    /**
     * @Author lhw
     * @Description 添加原材料入库信息主表
     * @Date 2019/05/09 11:34
     **/
    @PostMapping
    public BaseResponse saveInBase(@Valid @RequestBody InBase inBase, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            inBaseService.save(inBase);
            return ResponseHelper.success(inBase);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author lhw
     * @Description 局部更新原材料入库信息主表
     * @Date 2019/05/09 11:34
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchInBase(@PathVariable Long id, @RequestBody InBase inBase) {
        try {
            boolean result = inBaseService.patch(inBase, id);
            if (result) {
                return ResponseHelper.success(inBase);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author lhw
     * @Description 收货
     * @Date 2019/05/09 11:34
     **/
    @PatchMapping(value = "/takeOver/")
    public BaseResponse patchInBase(@RequestBody InStockVO inStockVO) {
        try {
            inBaseService.takeOver(inStockVO);
            return ResponseHelper.success("收货成功");
        } catch (RepeatOperateException repeatOperateExe) {
            return ResponseHelper.error("该入库单已经收货，不可重复出库");
        } catch (Exception e) {
            e.printStackTrace();
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error("收货失败");
    }

    /**
     * @Author lhw
     * @Description 根据id删除原材料入库信息主表
     * @Date 2019/05/09 11:34
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteInBase(@PathVariable Long id) {
        try {
            inBaseService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
