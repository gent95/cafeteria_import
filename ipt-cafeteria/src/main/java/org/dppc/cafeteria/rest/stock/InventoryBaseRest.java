package org.dppc.cafeteria.rest.stock;

import org.dppc.cafeteria.entity.stock.InventoryBase;
import org.dppc.cafeteria.service.stock.InventoryBaseService;
import lombok.extern.java.Log;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.cafeteria.vo.InventoryVO;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 盘货信息主表
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/stock/inventoryBase")
public class InventoryBaseRest {

    @Autowired
    private InventoryBaseService inventoryBaseService;

    /**
     * @Author lhw
     * @Description 根据条件分页查询盘货信息主表
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, InventoryBase inventoryBase) {
        /** 根据条件分页查询集合 **/
        inventoryBase.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(inventoryBase.getCafeteriaCode()));
        Page<InventoryBase> page = inventoryBaseService.findPageList(inventoryBase,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author lhw
     * @Description 根据id查询盘货信息主表
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/{id}")
    public BaseResponse findInventoryBase(@PathVariable Long id) {
        return ResponseHelper.success(inventoryBaseService.getOneDataById(id));
    }

    /**
     * @Author lhw
     * @Description 添加盘货信息主表
     * @Date 2019/05/09 11:34
     **/
    @PostMapping
    public BaseResponse saveInventoryBase(@Valid @RequestBody InventoryBase inventoryBase, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            inventoryBaseService.save(inventoryBase);
            return ResponseHelper.success(inventoryBase);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author lhw
     * @Description 生成盘库信息
     * @Date 2019/05/09 11:34
     **/
    @PostMapping("/createInventoryInfo")
    public BaseResponse createInventoryInfo() {
        try {
            InventoryVO inventoryVO = inventoryBaseService.createInventoryInfo();
            return ResponseHelper.success(inventoryVO);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author lhw
     * @Description 修改
     * @Date 2019/05/09 11:34
     **/
    @PatchMapping(value = "/inventoryEdit/")
    public BaseResponse inventoryEdit(@RequestBody InventoryVO inventoryVO) {
        try {
            inventoryBaseService.inventoryEdit(inventoryVO);
            return ResponseHelper.success("修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error("修改失败");
    }

    /**
     * @Author lhw
     * @Description 根据id删除盘货信息主表
     * @Date 2019/05/09 11:34
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteInventoryBase(@PathVariable Long id) {
        try {
            inventoryBaseService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
