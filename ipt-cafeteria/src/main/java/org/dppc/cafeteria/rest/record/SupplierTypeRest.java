package org.dppc.cafeteria.rest.record;

import lombok.extern.java.Log;
import org.dppc.cafeteria.entity.record.SupplierType;
import org.dppc.cafeteria.service.record.SupplierTypeService;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 供应商类型
 * @Author GAOJ
 * @Data 2019/05/09 19:11
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/supplierType")
public class SupplierTypeRest {

    @Autowired
    private SupplierTypeService supplierTypeService;

    /**
     * @Author GAOJ
     * @Description 根据条件分页查询供应商类型
     * @Date 2019/05/09 19:11
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, SupplierType supplierType) {
        /** 根据条件分页查询集合 **/
        Page<SupplierType> page = supplierTypeService.findPageList(supplierType,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author GAOJ
     * @Description 根据id查询供应商类型
     * @Date 2019/05/09 19:11
     **/
    @GetMapping("/{id}")
    public BaseResponse findSupplierType(@PathVariable Integer id) {
        return ResponseHelper.success(supplierTypeService.getOneDataById(id));
    }

    /**
     * @Author GAOJ
     * @Description 添加供应商类型
     * @Date 2019/05/09 19:11
     **/
    @PostMapping
    public BaseResponse saveSupplierType(@Valid @RequestBody SupplierType supplierType, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            supplierTypeService.save(supplierType);
            return ResponseHelper.success(supplierType);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author GAOJ
     * @Description 局部更新供应商类型
     * @Date 2019/05/09 19:11
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchSupplierType(@PathVariable Integer id, @RequestBody SupplierType supplierType) {
        try {
            boolean result = supplierTypeService.patch(supplierType, id);
            if (result) {
                return ResponseHelper.success(supplierType);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 根据id删除供应商类型
     * @Date 2019/05/09 19:11
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteSupplierType(@PathVariable Integer id) {
        try {
            supplierTypeService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
