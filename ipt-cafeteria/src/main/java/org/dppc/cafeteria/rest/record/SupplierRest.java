package org.dppc.cafeteria.rest.record;

import lombok.extern.java.Log;
import org.dppc.cafeteria.entity.record.Supplier;
import org.dppc.cafeteria.entity.record.SupplierType;
import org.dppc.cafeteria.service.record.SupplierService;
import org.dppc.cafeteria.service.record.SupplierTypeService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.entity.Pager;
import org.dppc.common.enums.RecordAuditStatusEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;

/**
 * @Description 供应商
 * @Author GAOJ
 * @Data 2019/05/09 09:45
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/supplier")
public class SupplierRest {

    @Autowired
    private SupplierService supplierService;
    @Autowired
    private SupplierTypeService supplierTypeService;

    /**
     * @Author GAOJ
     * @Description 根据条件分页查询供应商
     * @Date 2019/05/09 09:45
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, Supplier supplier) {
        /** 根据条件分页查询集合 **/
        supplier.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(supplier.getCafeteriaCode()));
        Page<Supplier> page = supplierService.findPageList(supplier,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        HashMap<Object, Object> map = new HashMap<>(3);
        map.put("typeList", supplierTypeService.findList(new SupplierType()));
        map.put("auditStatusEnums", RecordAuditStatusEnum.values());
        return ResponseHelper.success(page, map);
    }

    /**
     * @Author GAOJ
     * @Description 根据条件查询供应商集合
     * @Date 2019/05/09 09:45
     **/
    @GetMapping
    public BaseResponse list(Supplier supplier) {
        supplier.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(supplier.getCafeteriaCode()));
        return ResponseHelper.success(supplierService.findList(supplier));
    }

    /**
     * @Author GAOJ
     * @Description 根据id查询供应商
     * @Date 2019/05/09 09:45
     **/
    @GetMapping("/{id}")
    public BaseResponse findSupplier(@PathVariable Integer id) {
        return ResponseHelper.success(supplierService.getOneDataById(id));
    }

    /**
     * @Author GAOJ
     * @Description 添加供应商
     * @Date 2019/05/09 09:45
     **/
    @PostMapping
    public BaseResponse saveSupplier(@Valid @RequestBody Supplier supplier, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        supplier.setCreateUserId(Long.valueOf(BaseContextHandler.getUserID()));
        supplier.setCreateUserName(BaseContextHandler.getName());
        supplier.setAuditStatus(RecordAuditStatusEnum.NOT_AUDIT.getValue());
        supplier.setCreateDate(new Date());
        CafeteriaHandleUtil.bindCafeteriaInfo(supplier);
        try {
            supplierService.save(supplier);
            return ResponseHelper.success(supplier);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author GAOJ
     * @Description 局部更新供应商
     * @Date 2019/05/09 09:45
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchSupplier(@PathVariable Integer id, @RequestBody Supplier supplier) {
        try {
            boolean result = supplierService.patch(supplier, id);
            if (result) {
                return ResponseHelper.success(supplier);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 审核供应商
     * @Date 2019/05/09 09:45
     **/
    @PatchMapping(value = "/audit")
    public BaseResponse patchSupplier(@RequestBody Supplier supplier) {
        try {
            supplier.setAuditUserId(Long.valueOf(BaseContextHandler.getUserID()));
            supplier.setAuditUserName(BaseContextHandler.getName());
            supplier.setAuditDate(new Date());
            boolean result = supplierService.patch(supplier, supplier.getSupplierId());
            if (result) {
                return ResponseHelper.success(supplier);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 根据id删除供应商
     * @Date 2019/05/09 09:45
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteSupplier(@PathVariable Integer id) {
        try {
            Supplier supplier = supplierService.getOneDataById(id);
            if (supplier != null && RecordAuditStatusEnum.AUDIT_PASSED.getValue() != supplier.getAuditStatus()) {
                supplierService.delete(id);
                return ResponseHelper.success(id);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    @GetMapping("/checkCode")
    public boolean checkCode(String socialCreditCode, Integer supplierId) {
        try {
            Supplier supplier = supplierService.findBySocialCreditCode(socialCreditCode);
            if (supplier == null) {
                return true;
            }
            if (supplierId != null && supplier.getSupplierId().equals(supplierId)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
