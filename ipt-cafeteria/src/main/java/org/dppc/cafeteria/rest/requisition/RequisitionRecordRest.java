package org.dppc.cafeteria.rest.requisition;

import lombok.extern.java.Log;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.dppc.cafeteria.entity.requisition.RequisitionDetail;
import org.dppc.cafeteria.entity.requisition.RequisitionRecord;
import org.dppc.cafeteria.service.requisition.RequisitionDetailService;
import org.dppc.cafeteria.service.requisition.RequisitionRecordService;
import org.dppc.cafeteria.service.stock.OutBaseService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.common.entity.Pager;
import org.dppc.common.enums.IsDeleteEnum;
import org.dppc.common.enums.IsUsedEnum;
import org.dppc.common.enums.RequisitionStatusEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

/**
 * @Description 领用单
 * @Author GAOJ
 * @Data 2019/05/09 09:49
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/requisitionRecord")
public class RequisitionRecordRest {

    @Autowired
    private RequisitionRecordService requisitionRecordService;

    @Autowired
    private OutBaseService outBaseService;

    @Autowired
    private RequisitionDetailService requisitionDetailService;

    /**
     * @Author GAOJ
     * @Description 根据条件分页查询领用单
     * @Date 2019/05/09 09:49
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, RequisitionRecord requisitionRecord) {
        requisitionRecord.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(requisitionRecord.getCafeteriaCode()));
        /** 根据条件分页查询集合 **/
        Page<RequisitionRecord> page = requisitionRecordService.findPageList(requisitionRecord,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        HashMap<Object, Object> map = new HashMap<>(3);
        map.put("isUsedEnum", IsUsedEnum.values());
        map.put("requisitionStatusEnum", RequisitionStatusEnum.values());
        return ResponseHelper.success(page, map);
    }

    /**
     * @Author GAOJ
     * @Description 根据id查询领用单
     * @Date 2019/05/09 09:49
     **/
    @GetMapping("/{id}")
    public BaseResponse findRequisitionRecord(@PathVariable Long id) {
        return ResponseHelper.success(requisitionRecordService.getOneDataById(id));
    }

    /**
     * @Author GAOJ
     * @Description 添加领用单
     * @Date 2019/05/09 09:49
     **/
    @PostMapping
    public BaseResponse saveRequisitionRecord(@Valid @RequestBody RequisitionRecord requisitionRecord, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        if (requisitionRecord.getRequisitionDetail() == null || requisitionRecord.getRequisitionDetail().size() <= 0) {
            return ResponseHelper.error();
        }
        try {
            requisitionRecordService.saveNew(requisitionRecord);
            return ResponseHelper.success(requisitionRecord);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author GAOJ
     * @Description 局部更新领用单
     * @Date 2019/05/09 09:49
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchRequisitionRecord(@PathVariable Long id, @RequestBody RequisitionRecord requisitionRecord) {
        try {
            boolean result = requisitionRecordService.patch(requisitionRecord, id);
            if (result) {
                return ResponseHelper.success(requisitionRecord);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 根据id删除领用单
     * @Date 2019/05/09 09:49
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteRequisitionRecord(@PathVariable Long id) {
        try {
            RequisitionRecord one = requisitionRecordService.getOneDataById(id);
            if (one != null && RequisitionStatusEnum.RECORD.getValue() == one.getStatus()) {
                one.setIsDelete(IsDeleteEnum.DELETED.getValue());
                requisitionRecordService.patch(one, id);
                outBaseService.deleteOutStockInfoByRecordCode(one.getRecordCode());
                RequisitionDetail requisitionDetail = new RequisitionDetail();
                requisitionDetail.setRecordCode(one.getRecordCode());
                List<RequisitionDetail> list = requisitionDetailService.findList(requisitionDetail);
                list.stream().forEach(element->{
                    element.setIsDelete(IsDeleteEnum.DELETED.getValue());
                    requisitionDetailService.patch(element,element.getDetailId());
                });
                return ResponseHelper.success(id);
            } else {
                return ResponseHelper.error();
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    @GetMapping(value = "/export/{id}")
    public void exportExcel(@PathVariable Long id, HttpServletResponse response) {
        HSSFWorkbook hw = requisitionRecordService.exportExcelRequisitionRecord(id);
        response.setContentType("application/octet-stream");
        response.setHeader("Content-disposition", "attachment;filename=requisitionRecord.xls");
        try {
            OutputStream outputStream = response.getOutputStream();
            hw.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
