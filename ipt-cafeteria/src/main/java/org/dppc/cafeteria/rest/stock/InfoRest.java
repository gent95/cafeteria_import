package org.dppc.cafeteria.rest.stock;

import org.dppc.cafeteria.entity.stock.Info;
import org.dppc.cafeteria.service.stock.InfoService;
import lombok.extern.java.Log;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 库存信息表
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/stock/info")
public class InfoRest {

    @Autowired
    private InfoService infoService;

    /**
     * @Author lhw
     * @Description 根据条件分页查询库存信息表
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, Info info) {
        info.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(info.getCafeteriaCode()));
        /** 根据条件分页查询集合 **/
        Page<Info> page = infoService.findPageList(info,
                new PageRequest(pager.getPage() - 1, pager.getSize(), Sort.Direction.ASC,"expiryDate"));
        return ResponseHelper.success(page);
    }

    /**
     * @Author lhw
     * @Description 根据id查询库存信息表
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/{id}")
    public BaseResponse findInfo(@PathVariable Long id) {
        return ResponseHelper.success(infoService.getOneDataById(id));
    }

    /**
     * @Author lhw
     * @Description 添加库存信息表
     * @Date 2019/05/09 11:34
     **/
    @PostMapping
    public BaseResponse saveInfo(@Valid @RequestBody Info info, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            infoService.save(info);
            return ResponseHelper.success(info);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author lhw
     * @Description 局部更新库存信息表
     * @Date 2019/05/09 11:34
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchInfo(@PathVariable Long id, @RequestBody Info info) {
        try {
            boolean result = infoService.patch(info, id);
            if (result) {
                return ResponseHelper.success(info);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author lhw
     * @Description 根据id删除库存信息表
     * @Date 2019/05/09 11:34
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteInfo(@PathVariable Long id) {
        try {
            infoService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
