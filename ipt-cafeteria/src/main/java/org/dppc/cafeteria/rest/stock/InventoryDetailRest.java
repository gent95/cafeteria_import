package org.dppc.cafeteria.rest.stock;

import org.dppc.cafeteria.entity.stock.InventoryDetail;
import org.dppc.cafeteria.service.stock.InventoryDetailService;
import lombok.extern.java.Log;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 盘库详情表
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/stock/inventoryDetail")
public class InventoryDetailRest {

    @Autowired
    private InventoryDetailService inventoryDetailService;

    /**
     * @Author lhw
     * @Description 根据条件分页查询盘库详情表
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, InventoryDetail inventoryDetail) {
        /** 根据条件分页查询集合 **/
        Page<InventoryDetail> page = inventoryDetailService.findPageList(inventoryDetail,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author lhw
     * @Description 根据id查询盘库详情表
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/{id}")
    public BaseResponse findInventoryDetail(@PathVariable Long id) {
        return ResponseHelper.success(inventoryDetailService.getOneDataById(id));
    }

    /**
     * @Author lhw
     * @Description 添加盘库详情表
     * @Date 2019/05/09 11:34
     **/
    @PostMapping
    public BaseResponse saveInventoryDetail(@Valid @RequestBody InventoryDetail inventoryDetail, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            inventoryDetailService.save(inventoryDetail);
            return ResponseHelper.success(inventoryDetail);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author lhw
     * @Description 局部更新盘库详情表
     * @Date 2019/05/09 11:34
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchInventoryDetail(@PathVariable Long id, @RequestBody InventoryDetail inventoryDetail) {
        try {
            boolean result = inventoryDetailService.patch(inventoryDetail, id);
            if (result) {
                return ResponseHelper.success(inventoryDetail);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author lhw
     * @Description 根据id删除盘库详情表
     * @Date 2019/05/09 11:34
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteInventoryDetail(@PathVariable Long id) {
        try {
            inventoryDetailService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
