package org.dppc.cafeteria.rest.basedata;

import org.dppc.cafeteria.entity.basedata.KitchenUtensils;
import org.dppc.cafeteria.service.basedata.KitchenUtensilsService;
import lombok.extern.java.Log;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 
 * @Author majt
 * @Data 2019/06/03 19:38
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/kitchenUtensils")
public class KitchenUtensilsRest {

    @Autowired
    private KitchenUtensilsService kitchenUtensilsService;

    /**
     * @Author majt
     * @Description 根据条件分页查询
     * @Date 2019/06/03 19:38
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, KitchenUtensils kitchenUtensils) {
        /** 根据条件分页查询集合 **/
        Page<KitchenUtensils> page = kitchenUtensilsService.findPageList(kitchenUtensils,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author majt
     * @Description 根据id查询
     * @Date 2019/06/03 19:38
     **/
    @GetMapping("/{id}")
    public BaseResponse findKitchenUtensils(@PathVariable Long id) {
        return ResponseHelper.success(kitchenUtensilsService.getOneDataById(id));
    }

    /**
     * @Author majt
     * @Description 添加
     * @Date 2019/06/03 19:38
     **/
    @PostMapping
    public BaseResponse saveKitchenUtensils(@Valid @RequestBody KitchenUtensils kitchenUtensils, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            kitchenUtensilsService.saveKitchenUtensils(kitchenUtensils);
            return ResponseHelper.success(kitchenUtensils);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author majt
     * @Description 局部更新
     * @Date 2019/06/03 19:38
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchKitchenUtensils(@PathVariable Long id, @RequestBody KitchenUtensils kitchenUtensils) {
        try {
            boolean result = kitchenUtensilsService.patch(kitchenUtensils, id);
            if (result) {
                return ResponseHelper.success(kitchenUtensils);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author majt
     * @Description 根据id删除
     * @Date 2019/06/03 19:38
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteKitchenUtensils(@PathVariable Long id) {
        try {
            kitchenUtensilsService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
