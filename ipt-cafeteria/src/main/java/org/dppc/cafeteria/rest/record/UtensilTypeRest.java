package org.dppc.cafeteria.rest.record;

import lombok.extern.java.Log;
import org.dppc.cafeteria.entity.record.UtensilType;
import org.dppc.cafeteria.service.record.UtensilTypeService;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 器具类型
 * @Author GAOJ
 * @Data 2019/05/09 19:11
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/utensilType")
public class UtensilTypeRest {

    @Autowired
    private UtensilTypeService utensilTypeService;

    /**
     * @Author GAOJ
     * @Description 根据条件分页查询器具类型
     * @Date 2019/05/09 19:11
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, UtensilType utensilType) {
        /** 根据条件分页查询集合 **/
        Page<UtensilType> page = utensilTypeService.findPageList(utensilType,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author GAOJ
     * @Description 根据id查询器具类型
     * @Date 2019/05/09 19:11
     **/
    @GetMapping("/{id}")
    public BaseResponse findUtensilType(@PathVariable Integer id) {
        return ResponseHelper.success(utensilTypeService.getOneDataById(id));
    }

    /**
     * @Author GAOJ
     * @Description 添加器具类型
     * @Date 2019/05/09 19:11
     **/
    @PostMapping
    public BaseResponse saveUtensilType(@Valid @RequestBody UtensilType utensilType, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            utensilTypeService.save(utensilType);
            return ResponseHelper.success(utensilType);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author GAOJ
     * @Description 局部更新器具类型
     * @Date 2019/05/09 19:11
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchUtensilType(@PathVariable Integer id, @RequestBody UtensilType utensilType) {
        try {
            boolean result = utensilTypeService.patch(utensilType, id);
            if (result) {
                return ResponseHelper.success(utensilType);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 根据id删除器具类型
     * @Date 2019/05/09 19:11
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteUtensilType(@PathVariable Integer id) {
        try {
            utensilTypeService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
