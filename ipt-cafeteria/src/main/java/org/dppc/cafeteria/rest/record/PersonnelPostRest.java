package org.dppc.cafeteria.rest.record;

import lombok.extern.java.Log;
import org.dppc.cafeteria.entity.record.PersonnelPost;
import org.dppc.cafeteria.service.record.PersonnelPostService;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 人员职务
 * @Author GAOJ
 * @Data 2019/05/09 19:11
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/personnelPost")
public class PersonnelPostRest {

    @Autowired
    private PersonnelPostService personnelPostService;

    /**
     * @Author GAOJ
     * @Description 根据条件分页查询人员职务
     * @Date 2019/05/09 19:11
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, PersonnelPost personnelPost) {
        /** 根据条件分页查询集合 **/
        Page<PersonnelPost> page = personnelPostService.findPageList(personnelPost,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author GAOJ
     * @Description 根据id查询人员职务
     * @Date 2019/05/09 19:11
     **/
    @GetMapping("/{id}")
    public BaseResponse findPersonnelPost(@PathVariable Integer id) {
        return ResponseHelper.success(personnelPostService.getOneDataById(id));
    }

    /**
     * @Author GAOJ
     * @Description 添加人员职务
     * @Date 2019/05/09 19:11
     **/
    @PostMapping
    public BaseResponse savePersonnelPost(@Valid @RequestBody PersonnelPost personnelPost, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            personnelPostService.save(personnelPost);
            return ResponseHelper.success(personnelPost);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author GAOJ
     * @Description 局部更新人员职务
     * @Date 2019/05/09 19:11
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchPersonnelPost(@PathVariable Integer id, @RequestBody PersonnelPost personnelPost) {
        try {
            boolean result = personnelPostService.patch(personnelPost, id);
            if (result) {
                return ResponseHelper.success(personnelPost);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 根据id删除人员职务
     * @Date 2019/05/09 19:11
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deletePersonnelPost(@PathVariable Integer id) {
        try {
            personnelPostService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
