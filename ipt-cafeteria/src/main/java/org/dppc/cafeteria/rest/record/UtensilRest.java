package org.dppc.cafeteria.rest.record;

import lombok.extern.java.Log;
import org.dppc.cafeteria.entity.record.Utensil;
import org.dppc.cafeteria.entity.record.UtensilType;
import org.dppc.cafeteria.service.record.UtensilService;
import org.dppc.cafeteria.service.record.UtensilTypeService;
import org.dppc.cafeteria.service.syscode.SysCodeService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.entity.Pager;
import org.dppc.common.enums.RecordAuditStatusEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

/**
 * @Description 器具
 * @Author GAOJ
 * @Data 2019/05/09 09:45
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/utensil")
public class UtensilRest {

    @Autowired
    private UtensilService utensilService;
    @Autowired
    private UtensilTypeService utensilTypeService;
    @Autowired
    private SysCodeService sysCodeService;

    /**
     * @Author GAOJ
     * @Description 根据条件分页查询器具
     * @Date 2019/05/09 09:45
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, Utensil utensil) {
        /** 根据条件分页查询集合 **/
        utensil.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(utensil.getCafeteriaCode()));
        Page<Utensil> page = utensilService.findPageList(utensil,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        HashMap<Object, Object> map = new HashMap<>(3);
        map.put("utensilTypeList", utensilTypeService.findList(new UtensilType()));
        map.put("auditStatusEnums", RecordAuditStatusEnum.values());
        return ResponseHelper.success(page, map);
    }

    /**
     * @Author GAOJ
     * @Description 根据id查询器具
     * @Date 2019/05/09 09:45
     **/
    @GetMapping("/{id}")
    public BaseResponse findUtensil(@PathVariable Long id) {
        return ResponseHelper.success(utensilService.getOneDataById(id));
    }

    /**
     * @Author GAOJ
     * @Description 添加器具
     * @Date 2019/05/09 09:45
     **/
    @PostMapping
    public BaseResponse saveUtensil(@Valid @RequestBody Utensil utensil, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            utensil.setAuditStatus(RecordAuditStatusEnum.NOT_AUDIT.getValue());
            utensil.setCreateUserId(Long.valueOf(BaseContextHandler.getUserID()));
            utensil.setCreateUserName(BaseContextHandler.getName());
            utensil.setCreateDate(new Date());
            utensil.setUtensilCode(sysCodeService.getUtensilCode());
            CafeteriaHandleUtil.bindCafeteriaInfo(utensil);
            utensilService.save(utensil);
            return ResponseHelper.success(utensil);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author GAOJ
     * @Description 局部更新器具
     * @Date 2019/05/09 09:45
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchUtensil(@PathVariable Long id, @RequestBody Utensil utensil) {
        try {
            boolean result = utensilService.patch(utensil, id);
            if (result) {
                return ResponseHelper.success(utensil);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 审核更新器具
     * @Date 2019/05/09 09:45
     **/
    @PatchMapping(value = "/audit")
    public BaseResponse patchUtensil(@RequestBody Utensil utensil) {
        try {
            utensil.setAuditUserId(Long.valueOf(BaseContextHandler.getUserID()));
            utensil.setAuditUserName(BaseContextHandler.getName());
            utensil.setAuditDate(new Date());
            boolean result = utensilService.patch(utensil, utensil.getUtensilId());
            if (result) {
                return ResponseHelper.success(utensil);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 根据id删除器具
     * @Date 2019/05/09 09:45
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteUtensil(@PathVariable Long id) {
        try {
            Utensil utensil = utensilService.getOneDataById(id);
            if (utensil != null && RecordAuditStatusEnum.AUDIT_PASSED.getValue() != utensil.getAuditStatus()) {
                utensilService.delete(id);
                return ResponseHelper.success(id);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @return
     * @Author majt
     * @Description 查询所有
     * @Date 2019/6/5 11:09
     * @Param
     **/
    @GetMapping("/players")
    public BaseResponse findPlayer(Utensil utensil) {
        utensil.setType(7);
        utensil.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(utensil.getCafeteriaCode()));
        List<Utensil> utensilList = utensilService.findList(utensil);
        return ResponseHelper.success(utensilList);
    }
}
