package org.dppc.cafeteria.rest.basedata;

import org.dppc.cafeteria.entity.basedata.KitchenUtensilsType;
import org.dppc.cafeteria.service.basedata.KitchenUtensilsTypeService;
import lombok.extern.java.Log;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 
 * @Author majt
 * @Data 2019/06/03 19:38
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/kitchenUtensilsType")
public class KitchenUtensilsTypeRest {

    @Autowired
    private KitchenUtensilsTypeService kitchenUtensilsTypeService;

    /**
     * @Author majt
     * @Description 根据条件分页查询
     * @Date 2019/06/03 19:38
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, KitchenUtensilsType kitchenUtensilsType) {
        /** 根据条件分页查询集合 **/
        Page<KitchenUtensilsType> page = kitchenUtensilsTypeService.findPageList(kitchenUtensilsType,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author majt
     * @Description 根据id查询
     * @Date 2019/06/03 19:38
     **/
    @GetMapping("/{id}")
    public BaseResponse findKitchenUtensilsType(@PathVariable Long id) {
        return ResponseHelper.success(kitchenUtensilsTypeService.getOneDataById(id));
    }

    /**
     * @Author majt
     * @Description 添加
     * @Date 2019/06/03 19:38
     **/
    @PostMapping
    public BaseResponse saveKitchenUtensilsType(@Valid @RequestBody KitchenUtensilsType kitchenUtensilsType, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            kitchenUtensilsTypeService.save(kitchenUtensilsType);
            return ResponseHelper.success(kitchenUtensilsType);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author majt
     * @Description 局部更新
     * @Date 2019/06/03 19:38
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchKitchenUtensilsType(@PathVariable Long id, @RequestBody KitchenUtensilsType kitchenUtensilsType) {
        try {
            boolean result = kitchenUtensilsTypeService.patch(kitchenUtensilsType, id);
            if (result) {
                return ResponseHelper.success(kitchenUtensilsType);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author majt
     * @Description 根据id删除
     * @Date 2019/06/03 19:38
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteKitchenUtensilsType(@PathVariable Long id) {
        try {
            kitchenUtensilsTypeService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @return  org.dppc.common.msg.BaseResponse
     * @Author majt
     * @Description 查询器材厨具类目树
     * @Date 2019/5/11 11:38
     * @Param []
     **/
    @GetMapping("tree")
    public BaseResponse tree(){
        try{
            return ResponseHelper.success(kitchenUtensilsTypeService.tree());
        }catch (Exception e){
            return ResponseHelper.error();
        }
    }
}
