package org.dppc.cafeteria.rest.surveillance;

import org.dppc.cafeteria.entity.surveillance.Play;
import org.dppc.cafeteria.service.surveillance.PlayService;
import lombok.extern.java.Log;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 
 * @Author majt
 * @Data 2019/06/03 11:07
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/surveillancePlay")
public class PlayRest {

    @Autowired
    private PlayService playService;

    /**
     * @Author majt
     * @Description 根据条件分页查询
     * @Date 2019/06/03 11:07
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, Play play) {
        /** 根据条件分页查询集合 **/
        Page<Play> page = playService.findPageList(play,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author majt
     * @Description 根据id查询
     * @Date 2019/06/03 11:07
     **/
    @GetMapping("/{id}")
    public BaseResponse findPlay(@PathVariable Long id) {
        return ResponseHelper.success(playService.getOneDataById(id));
    }

    /**
     * @Author majt
     * @Description 添加
     * @Date 2019/06/03 11:07
     **/
    @PostMapping
    public BaseResponse savePlay(@Valid @RequestBody Play play, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            playService.save(play);
            return ResponseHelper.success(play);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author majt
     * @Description 局部更新
     * @Date 2019/06/03 11:07
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchPlay(@PathVariable Long id, @RequestBody Play play) {
        try {
            boolean result = playService.patch(play, id);
            if (result) {
                return ResponseHelper.success(play);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author majt
     * @Description 根据id删除
     * @Date 2019/06/03 11:07
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deletePlay(@PathVariable Long id) {
        try {
            playService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
