package org.dppc.cafeteria.rest.basedata;

import org.dppc.cafeteria.entity.basedata.DictType;
import org.dppc.cafeteria.service.basedata.DictTypeService;
import lombok.extern.java.Log;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Date;
import java.util.logging.Level;

/**
 * @Description 字典类型
 * @Author majt
 * @Data 2019/05/09 10:15
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/dictType")
public class DictTypeRest {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private DictTypeService dictTypeService;

    /**
     * @Author majt
     * @Description 根据条件分页查询字典类型
     * @Date 2019/05/09 10:15
     **/
    @GetMapping("/page")
    public BaseResponse page(@ModelAttribute DictType dictType,@ModelAttribute Pager pager) {
        /** 根据条件分页查询集合 **/
        Page<DictType> page = dictTypeService.findPageList(dictType,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author majt
     * @Description 根据id查询字典类型
     * @Date 2019/05/09 10:15
     **/
    @GetMapping("/{id}")
    public BaseResponse findDictType(@PathVariable Long id) {
        return ResponseHelper.success(dictTypeService.getOneDataById(id));
    }

    /**
     * @Author majt
     * @Description 添加字典类型
     * @Date 2019/05/09 10:15
     **/
    @PostMapping
    public BaseResponse saveDictType(@Valid @RequestBody DictType dictType, BindingResult result) {
        DictType dictTypeTmp = dictTypeService.findByTypeCode(dictType.getTypeCode());
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }else if (null != dictTypeTmp) {
            return ResponseHelper.error("编码已存在!");
        }
        try {
            dictType.setStatus(1);
            dictType.setCreateTime(new Date());
            dictTypeService.save(dictType);
            return ResponseHelper.success(dictType);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author majt
     * @Description 局部更新字典类型
     * @Date 2019/05/09 10:15
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchDictType(@PathVariable Long id, @RequestBody DictType dictType) {
        try {
            boolean result = dictTypeService.patch(dictType, id);
            if (result) {
                return ResponseHelper.success(dictType);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author majt
     * @Description 根据id删除字典类型
     * @Date 2019/05/09 10:15
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteDictType(@PathVariable Long id) {
        try {
            dictTypeService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @return  org.dppc.common.msg.BaseResponse
     * @Author majt
     * @Description 启用字典类型
     * @Date 2019/5/10 11:27
     * @Param [id]
     **/
    @PatchMapping(value = "/enableDictType/{id}")
    public BaseResponse enableDictType(@PathVariable Long id){
        try {
            dictTypeService.enableDictType(id);
            return ResponseHelper.success("启用字典类型成功");
        } catch (Exception e) {
            logger.error("启用字典类型异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @return  org.dppc.common.msg.BaseResponse
     * @Author majt
     * @Description 禁用字典类型
     * @Date 2019/5/10 11:27
     * @Param [id]
     **/
    @PatchMapping(value = "/disableDictType/{id}")
    public BaseResponse disableDictType(@PathVariable Long id){
        try {
            dictTypeService.disableDictType(id);
            return ResponseHelper.success("禁用字典类型成功");
        } catch (Exception e) {
            logger.error("禁用字典类型异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @return  org.dppc.common.msg.BaseResponse
     * @Author majt
     * @Description 逻辑删除字典类型
     * @Date 2019/5/10 11:27
     * @Param [id]
     **/
    @DeleteMapping(value = "/delDictType/{id}")
    public BaseResponse delDictType(@PathVariable Long id){
        try {
            dictTypeService.delDictType(id);
            return ResponseHelper.success("删除字典类型成功");
        } catch (Exception e) {
            logger.error("删除字典类型异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @return  boolean
     * @Author majt
     * @Description 验证字典类型编码是否已存在
     * @Date 2019/5/15 13:48
     * @Param [typeCode]
     **/
    @GetMapping("/typeCode")
    public boolean checkTypeCode(String typeCode){
        DictType dictType = dictTypeService.findByTypeCode(typeCode);
        if (null == dictType){
            return true;
        }else {
            return false;
        }
    }
}
