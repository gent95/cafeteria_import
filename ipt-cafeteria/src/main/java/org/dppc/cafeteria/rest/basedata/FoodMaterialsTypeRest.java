package org.dppc.cafeteria.rest.basedata;

import org.dppc.cafeteria.entity.basedata.FoodMaterialsType;
import org.dppc.cafeteria.service.basedata.FoodMaterialsTypeService;
import lombok.extern.java.Log;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 食材类型表
 * @Author majt
 * @Data 2019/05/09 15:42
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/foodMaterialsType")
public class FoodMaterialsTypeRest {

    @Autowired
    private FoodMaterialsTypeService foodMaterialsTypeService;

    /**
     * @Author majt
     * @Description 根据条件分页查询食材类型表
     * @Date 2019/05/09 15:42
     **/
    @GetMapping("/page")
    public BaseResponse page(@ModelAttribute Pager pager,@ModelAttribute FoodMaterialsType foodMaterialsType) {
        /** 根据条件分页查询集合 **/
        Page<FoodMaterialsType> page = foodMaterialsTypeService.findPageList(foodMaterialsType,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }
    /**
     * @Author majt
     * @Description 根据条件查询食材类型集合
     * @Date 2019/05/09 15:42
     **/
    @GetMapping
    public BaseResponse list(FoodMaterialsType foodMaterialsType) {
        /** 根据条件查询集合 **/
        return ResponseHelper.success(foodMaterialsTypeService.findList(foodMaterialsType));
    }

    /**
     * @Author majt
     * @Description 根据id查询食材类型表
     * @Date 2019/05/09 15:42
     **/
    @GetMapping("/{id}")
    public BaseResponse findFoodMaterialsType(@PathVariable Integer id) {
        return ResponseHelper.success(foodMaterialsTypeService.getOneDataById(id));
    }

    /**
     * @Author majt
     * @Description 添加食材类型表
     * @Date 2019/05/09 15:42
     **/
    @PostMapping
    public BaseResponse saveFoodMaterialsType(@Valid @RequestBody FoodMaterialsType foodMaterialsType, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            foodMaterialsTypeService.save(foodMaterialsType);
            return ResponseHelper.success(foodMaterialsType);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author majt
     * @Description 局部更新食材类型表
     * @Date 2019/05/09 15:42
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchFoodMaterialsType(@PathVariable Integer id, @RequestBody FoodMaterialsType foodMaterialsType) {
        try {
            boolean result = foodMaterialsTypeService.patch(foodMaterialsType, id);
            if (result) {
                return ResponseHelper.success(foodMaterialsType);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author majt
     * @Description 根据id删除食材类型表
     * @Date 2019/05/09 15:42
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteFoodMaterialsType(@PathVariable Integer id) {
        try {
            foodMaterialsTypeService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @return  org.dppc.common.msg.BaseResponse
     * @Author majt
     * @Description 查询食材类目树
     * @Date 2019/5/11 11:38
     * @Param []
     **/
    @GetMapping("tree")
    public BaseResponse tree(){
        try{
            return ResponseHelper.success(foodMaterialsTypeService.tree());
        }catch (Exception e){
            return ResponseHelper.error();
        }
    }
}
