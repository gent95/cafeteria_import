package org.dppc.cafeteria.rest.basedata;

import org.dppc.cafeteria.entity.basedata.DictData;
import org.dppc.cafeteria.entity.basedata.DictType;
import org.dppc.cafeteria.service.basedata.DictDataService;
import lombok.extern.java.Log;
import org.dppc.cafeteria.service.basedata.DictTypeService;
import org.dppc.cafeteria.vo.DictDataVo;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

/**
 * @Description 字典数据
 * @Author majt
 * @Data 2019/05/09 10:15
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/dictData")
public class DictDataRest {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private DictDataService dictDataService;

    @Autowired
    private DictTypeService dictTypeService;
    /**
     * @Author majt
     * @Description 根据条件分页查询字典数据
     * @Date 2019/05/09 10:15
     **/
    @GetMapping("/page")
    public BaseResponse page(@ModelAttribute Pager pager, @ModelAttribute DictData dictData) {
        /** 根据条件分页查询集合 **/
        Page<DictDataVo> page = dictDataService.findDictDataPage(dictData,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author majt
     * @Description 根据id查询字典数据
     * @Date 2019/05/09 10:15
     **/
    @GetMapping("/{id}")
    public BaseResponse findDictData(@PathVariable Long id) {
        return ResponseHelper.success(dictDataService.getOneDataById(id));
    }

    /**
     * @Author majt
     * @Description 添加字典数据
     * @Date 2019/05/09 10:15
     **/
    @PostMapping
    public BaseResponse saveDictData(@Valid @RequestBody DictData dictData, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            dictData.setStatus(1);
            dictData.setCreateTime(new Date());
            dictDataService.save(dictData);
            return ResponseHelper.success(dictData);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author majt
     * @Description 局部更新字典数据
     * @Date 2019/05/09 10:15
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchDictData(@PathVariable Long id, @RequestBody DictData dictData) {
        try {
            boolean result = dictDataService.patch(dictData, id);
            if (result) {
                return ResponseHelper.success(dictData);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author majt
     * @Description 根据id删除字典数据
     * @Date 2019/05/09 10:15
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteDictData(@PathVariable Long id) {
        try {
            dictDataService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @return  org.dppc.common.msg.BaseResponse
     * @Author majt
     * @Description 启用字典类型
     * @Date 2019/5/10 11:27
     * @Param [id]
     **/
    @PatchMapping(value = "/enableDictData/{id}")
    public BaseResponse enableDictData(@PathVariable Long id){
        try {
            dictDataService.enableDictData(id);
            return ResponseHelper.success("启用字典类型成功");
        } catch (Exception e) {
            logger.error("启用字典类型异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @return  org.dppc.common.msg.BaseResponse
     * @Author majt
     * @Description 禁用字典类型
     * @Date 2019/5/10 11:27
     * @Param [id]
     **/
    @PatchMapping(value = "/disableDictData/{id}")
    public BaseResponse disableDictData(@PathVariable Long id){
        try {
            dictDataService.disableDictData(id);
            return ResponseHelper.success("禁用字典类型成功");
        } catch (Exception e) {
            logger.error("禁用字典类型异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @return  org.dppc.common.msg.BaseResponse
     * @Author majt
     * @Description 逻辑删除字典类型
     * @Date 2019/5/10 11:27
     * @Param [id]
     **/
    @DeleteMapping(value = "/delDictData/{id}")
    public BaseResponse delDictData(@PathVariable Long id){
        try {
            dictDataService.delDictData(id);
            return ResponseHelper.success("删除字典类型成功");
        } catch (Exception e) {
            logger.error("删除字典类型异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    @GetMapping("/dictTypeCode")
    public BaseResponse findByDictTypeCode(String dictTypeCode){
        DictType dictType = dictTypeService.findByTypeCode(dictTypeCode);
        if (null == dictType){
            return ResponseHelper.error("字典类型不存在!");
        }else {
            DictData dictData = new DictData();
            dictData.setDictTypeId(dictType.getTypeId());
            List<DictData> dictDataList = dictDataService.findList(dictData);
            return ResponseHelper.success(dictDataList);
        }
    }
}
