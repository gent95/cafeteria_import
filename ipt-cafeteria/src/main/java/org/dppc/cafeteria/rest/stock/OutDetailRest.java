package org.dppc.cafeteria.rest.stock;

import org.dppc.cafeteria.entity.stock.OutDetail;
import org.dppc.cafeteria.service.stock.OutDetailService;
import lombok.extern.java.Log;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 原材料出库信息细表
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/stock/outDetail")
public class OutDetailRest {

    @Autowired
    private OutDetailService outDetailService;

    /**
     * @Author lhw
     * @Description 根据条件分页查询原材料出库信息细表
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, OutDetail outDetail) {
        /** 根据条件分页查询集合 **/
        Page<OutDetail> page = outDetailService.findPageList(outDetail,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author lhw
     * @Description 根据id查询原材料出库信息细表
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/{id}")
    public BaseResponse findOutDetail(@PathVariable Long id) {
        return ResponseHelper.success(outDetailService.getOneDataById(id));
    }

    /**
     * @Author lhw
     * @Description 添加原材料出库信息细表
     * @Date 2019/05/09 11:34
     **/
    @PostMapping
    public BaseResponse saveOutDetail(@Valid @RequestBody OutDetail outDetail, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            outDetailService.save(outDetail);
            return ResponseHelper.success(outDetail);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author lhw
     * @Description 局部更新原材料出库信息细表
     * @Date 2019/05/09 11:34
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchOutDetail(@PathVariable Long id, @RequestBody OutDetail outDetail) {
        try {
            boolean result = outDetailService.patch(outDetail, id);
            if (result) {
                return ResponseHelper.success(outDetail);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author lhw
     * @Description 根据id删除原材料出库信息细表
     * @Date 2019/05/09 11:34
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteOutDetail(@PathVariable Long id) {
        try {
            outDetailService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
