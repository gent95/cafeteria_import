package org.dppc.cafeteria.rest.machining;

import org.dppc.cafeteria.entity.machining.Enviroment;
import org.dppc.cafeteria.service.machining.EnviromentService;
import lombok.extern.java.Log;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Date;
import java.util.logging.Level;

/**
 * @Description 环境
 * @Author majt
 * @Data 2019/05/20 11:18
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/enviroment")
public class EnviromentRest {

    @Autowired
    private EnviromentService enviromentService;

    /**
     * @Author majt
     * @Description 根据条件分页查询环境
     * @Date 2019/05/20 11:18
     **/
    @GetMapping("/page")
    public BaseResponse page(@ModelAttribute Enviroment enviroment, @ModelAttribute Pager pager) {
        /** 根据条件分页查询集合 **/
        enviroment.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(enviroment.getCafeteriaCode()));
        Page<Enviroment> page = enviromentService.findPageList(enviroment,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author majt
     * @Description 根据id查询环境
     * @Date 2019/05/20 11:18
     **/
    @GetMapping("/{id}")
    public BaseResponse findEnviroment(@PathVariable Integer id) {
        return ResponseHelper.success(enviromentService.getOneDataById(id));
    }

    /**
     * @Author majt
     * @Description 添加环境
     * @Date 2019/05/20 11:18
     **/
    @PostMapping
    public BaseResponse saveEnviroment(@Valid @RequestBody Enviroment enviroment, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            enviroment.setCleaningTime(new Date());
            enviroment.setStatus(0);
            CafeteriaHandleUtil.bindCafeteriaInfo(enviroment);
            enviromentService.save(enviroment);
            return ResponseHelper.success(enviroment);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author majt
     * @Description 局部更新环境
     * @Date 2019/05/20 11:18
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchEnviroment(@PathVariable Integer id, @RequestBody Enviroment enviroment) {
        try {
            enviroment.setStatus(0);
            enviroment.setScore(null);
            boolean result = enviromentService.patch(enviroment, id);
            if (result) {
                return ResponseHelper.success(enviroment);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author majt
     * @Description 根据id删除环境
     * @Date 2019/05/20 11:18
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteEnviroment(@PathVariable Integer id) {
        try {
            enviromentService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    @PatchMapping(value = "/checkEnviroment/{id}")
    public BaseResponse checkEnviroment(@PathVariable Integer id, @RequestBody Enviroment enviroment) {
        try {
            enviroment.setStatus(1);
            enviroment.setCheckTime(new Date());
            boolean result = enviromentService.patch(enviroment, id);
            if (result) {
                return ResponseHelper.success(enviroment);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }
}
