package org.dppc.cafeteria.rest.emergency;

import lombok.extern.java.Log;
import org.dppc.cafeteria.entity.emergency.EmergencyType;
import org.dppc.cafeteria.service.emergency.EmergencyTypeService;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 应急事件类型
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/emergencyType")
public class EmergencyTypeRest {

    @Autowired
    private EmergencyTypeService emergencyTypeService;

    /**
     * @Author GAOJ
     * @Description 根据条件分页查询应急事件类型
     * @Date 2019/05/27 14:18
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, EmergencyType emergencyType) {
        /** 根据条件分页查询集合 **/
        Page<EmergencyType> page = emergencyTypeService.findPageList(emergencyType,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author GAOJ
     * @Description 根据id查询应急事件类型
     * @Date 2019/05/27 14:18
     **/
    @GetMapping("/{id}")
    public BaseResponse findEmergencyType(@PathVariable Integer id) {
        return ResponseHelper.success(emergencyTypeService.getOneDataById(id));
    }

    /**
     * @Author GAOJ
     * @Description 添加应急事件类型
     * @Date 2019/05/27 14:18
     **/
    @PostMapping
    public BaseResponse saveEmergencyType(@Valid @RequestBody EmergencyType emergencyType, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            emergencyTypeService.save(emergencyType);
            return ResponseHelper.success(emergencyType);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author GAOJ
     * @Description 局部更新应急事件类型
     * @Date 2019/05/27 14:18
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchEmergencyType(@PathVariable Integer id, @RequestBody EmergencyType emergencyType) {
        try {
            boolean result = emergencyTypeService.patch(emergencyType, id);
            if (result) {
                return ResponseHelper.success(emergencyType);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 根据id删除应急事件类型
     * @Date 2019/05/27 14:18
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteEmergencyType(@PathVariable Integer id) {
        try {
            emergencyTypeService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
