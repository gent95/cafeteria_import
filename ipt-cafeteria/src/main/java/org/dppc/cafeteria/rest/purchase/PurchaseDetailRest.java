package org.dppc.cafeteria.rest.purchase;

import org.dppc.cafeteria.entity.purchase.PurchaseDetail;
import org.dppc.cafeteria.service.purchase.PurchaseDetailService;
import lombok.extern.java.Log;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 采购计划明细
 * @Author zmm
 * @Data 2019/05/14 16:43
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/purchaseDetail")
public class PurchaseDetailRest {

    @Autowired
    private PurchaseDetailService purchaseDetailService;

    /**
     * @Author zmm
     * @Description 根据条件分页查询采购计划明细
     * @Date 2019/05/14 16:43
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, PurchaseDetail purchaseDetail) {
        /** 根据条件分页查询集合 **/
        Page<PurchaseDetail> page = purchaseDetailService.findPageList(purchaseDetail,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author zmm
     * @Description 根据id查询采购计划明细
     * @Date 2019/05/14 16:43
     **/
    @GetMapping("/{id}")
    public BaseResponse findPurchaseDetail(@PathVariable Integer id) {
        return ResponseHelper.success(purchaseDetailService.getOneDataById(id));
    }

    /**
     * @Author zmm
     * @Description 添加采购计划明细
     * @Date 2019/05/14 16:43
     **/
    @PostMapping
    public BaseResponse savePurchaseDetail(@Valid @RequestBody PurchaseDetail purchaseDetail, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            purchaseDetailService.save(purchaseDetail);
            return ResponseHelper.success(purchaseDetail);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author zmm
     * @Description 局部更新采购计划明细
     * @Date 2019/05/14 16:43
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchPurchaseDetail(@PathVariable Integer id, @RequestBody PurchaseDetail purchaseDetail) {
        try {
            boolean result = purchaseDetailService.patch(purchaseDetail, id);
            if (result) {
                return ResponseHelper.success(purchaseDetail);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author zmm
     * @Description 根据id删除采购计划明细
     * @Date 2019/05/14 16:43
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deletePurchaseDetail(@PathVariable Integer id) {
        try {
            purchaseDetailService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
