package org.dppc.cafeteria.rest.machining;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.java.Log;
import org.dppc.cafeteria.entity.machining.Meal;
import org.dppc.cafeteria.entity.machining.MealDish;
import org.dppc.cafeteria.service.machining.MealService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.cafeteria.vo.MealDishVo;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

/**
 * @Description 餐次
 * @Author majt
 * @Data 2019/05/20 18:05
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/meal")
public class MealRest {

    @Autowired
    private MealService mealService;

    /**
     * @Author majt
     * @Description 根据条件分页查询餐次
     * @Date 2019/05/20 18:05
     **/
    @GetMapping("/page")
    public BaseResponse page(@ModelAttribute Pager pager, @ModelAttribute Meal meal) {
        /** 根据条件分页查询集合 **/
        meal.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(meal.getCafeteriaCode()));
        Page<Meal> page = mealService.findPageList(meal,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author majt
     * @Description 根据id查询餐次
     * @Date 2019/05/20 18:05
     **/
    @GetMapping("/{id}")
    public BaseResponse findMeal(@PathVariable Long id) {
        return ResponseHelper.success(mealService.getOneDataById(id));
    }

    /**
     * @Author majt
     * @Description 添加餐次
     * @Date 2019/05/20 18:05
     **/
    @PostMapping
    public BaseResponse saveMeal(@Valid @RequestBody Meal meal, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            meal.setCreateTime(new Date());
            CafeteriaHandleUtil.bindCafeteriaInfo(meal);
            mealService.save(meal);
            return ResponseHelper.success(meal);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author majt
     * @Description 局部更新餐次
     * @Date 2019/05/20 18:05
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchMeal(@PathVariable Long id, @RequestBody Meal meal) {
        try {
//            boolean result = mealService.patch(meal, id);
            Meal result = mealService.update(meal);
            if (null != result) {
                return ResponseHelper.success(meal);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author majt
     * @Description 根据id删除餐次
     * @Date 2019/05/20 18:05
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteMeal(@PathVariable Long id) {
        try {
            mealService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @return org.dppc.common.msg.BaseResponse
     * @Author majt
     * @Description 保存餐次和菜品
     * @Date 2019/5/22 9:49
     * @Param [mealDishVo, result]
     **/
    @PostMapping("/saveMealDishVo")
    public BaseResponse saveMealDishVo(@Valid @RequestBody MealDishVo mealDishVo, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            mealService.saveMealDishVo(mealDishVo);
            return ResponseHelper.success(mealDishVo);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author gaoj
     * @Description 微信公众号用户获取餐饮信息
     * @Date 2019/06/05 18:05
     **/
    @GetMapping("/weixin")
    public BaseResponse findMeal(String schoolCode, String date) {
        if (StrUtil.isBlank(schoolCode)) {
            return ResponseHelper.error();
        }
        try {
            Meal meal = new Meal();
            DateTime parse;
            if (StrUtil.isNotBlank(date)) {
                parse = DateUtil.parse(date, DatePattern.NORM_DATE_PATTERN);
            } else {
                parse = DateUtil.parse(DateUtil.format(new Date(), DatePattern.NORM_DATE_PATTERN));
            }
            meal.setTime(new Date(parse.getTime()));
            meal.setSchoolCode(schoolCode);
            Map<String, Map<String, List<MealDish>>> map = mealService.findBySchoolCodeAndDate(meal);
            return ResponseHelper.success(map);
        } catch (Exception e) {
            return ResponseHelper.error();
        }
    }

    /**
     * @Author gaoj
     * @Description 微信公众号用户获取餐饮信息
     * @Date 2019/06/05 18:05
     **/
    @GetMapping("/weixin/queryMeal")
    public BaseResponse queryMeal(String schoolCode, String date) {
        if (StrUtil.isBlank(schoolCode)) {
            return ResponseHelper.error();
        }
        try {
            Meal meal = new Meal();
            DateTime parse;
            if (StrUtil.isNotBlank(date)) {
                parse = DateUtil.parse(date, DatePattern.NORM_DATE_PATTERN);
            } else {
                parse = DateUtil.parse(DateUtil.format(new Date(), DatePattern.NORM_DATE_PATTERN));
            }
            meal.setTime(new Date(parse.getTime()));
            meal.setSchoolCode(schoolCode);
            Map<String, Set<MealDish>> map = mealService.queryBySchoolCodeAndDate(meal);
            return ResponseHelper.success(map);
        } catch (Exception e) {
            return ResponseHelper.error();
        }
    }

    @GetMapping("/checkMeal")
    public boolean checkMeal(Meal meal) {
        meal.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(meal.getCafeteriaCode()));
        List<Meal> meals = mealService.findList(meal);
        if (meals.size() > 0) {
            return false;
        } else {
            return true;
        }
    }
}
