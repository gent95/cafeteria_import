package org.dppc.cafeteria.rest.surveillance;

import org.dppc.cafeteria.entity.record.Utensil;
import org.dppc.cafeteria.entity.surveillance.SensorData;
import org.dppc.cafeteria.service.surveillance.SensorDataService;
import lombok.extern.java.Log;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.cafeteria.vo.SensorInfoVO;
import org.dppc.common.entity.Pager;
import org.dppc.common.enums.AuditStatusEnum;
import org.dppc.common.enums.RecordAuditStatusEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.logging.Level;

/**
 * @Description 传感器数据
 * @Author lhw
 * @Data 2019/06/04 16:07
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/sensorData")
public class SensorDataRest {

    @Autowired
    private SensorDataService sensorDataService;

    /**
     * @Author lhw
     * @Description 根据条件分页查询传感器数据
     * @Date 2019/06/04 16:07
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, SensorData sensorData) {
        /** 根据条件分页查询集合 **/
        Page<SensorData> page = sensorDataService.findPageList(sensorData,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author lhw
     * @Description 查询当前所有设备信息
     * @Date 2019/06/04 16:07
     **/
    @GetMapping("/current/")
    public BaseResponse currentInfo(Utensil utensil) {
        //设备类型为传感器
        utensil.setType(8);
        utensil.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(utensil.getCafeteriaCode()));
        utensil.setAuditStatus(RecordAuditStatusEnum.AUDIT_PASSED.getValue());
        List<SensorInfoVO> list = sensorDataService.findCurrenSensorInfoVO(utensil);
        return ResponseHelper.success(list);
    }

    /**
     * @Author lhw
     * @Description 根据id查询传感器数据
     * @Date 2019/06/04 16:07
     **/
    @GetMapping("/{id}")
    public BaseResponse findSensorData(@PathVariable Long id) {
        return ResponseHelper.success(sensorDataService.getOneDataById(id));
    }

    /**
     * @Author lhw
     * @Description 添加传感器数据
     * @Date 2019/06/04 16:07
     **/
    @PostMapping
    public BaseResponse saveSensorData(@Valid @RequestBody SensorData sensorData, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            sensorDataService.save(sensorData);
            return ResponseHelper.success(sensorData);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author lhw
     * @Description 局部更新传感器数据
     * @Date 2019/06/04 16:07
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchSensorData(@PathVariable Long id, @RequestBody SensorData sensorData) {
        try {
            boolean result = sensorDataService.patch(sensorData, id);
            if (result) {
                return ResponseHelper.success(sensorData);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author lhw
     * @Description 根据id删除传感器数据
     * @Date 2019/06/04 16:07
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteSensorData(@PathVariable Long id) {
        try {
            sensorDataService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
