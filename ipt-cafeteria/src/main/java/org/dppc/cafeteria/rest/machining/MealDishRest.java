package org.dppc.cafeteria.rest.machining;

import org.dppc.cafeteria.entity.machining.MealDish;
import org.dppc.cafeteria.service.machining.MealDishService;
import lombok.extern.java.Log;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 菜品
 * @Author majt
 * @Data 2019/05/20 18:05
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/mealDish")
public class MealDishRest {

    @Autowired
    private MealDishService mealDishService;

    /**
     * @Author majt
     * @Description 根据条件分页查询菜品
     * @Date 2019/05/20 18:05
     **/
    @GetMapping("/page")
    public BaseResponse page(@ModelAttribute Pager pager, @ModelAttribute MealDish mealDish) {
        /** 根据条件分页查询集合 **/
        mealDish.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(mealDish.getCafeteriaCode()));
        Page<MealDish> page = mealDishService.findPageList(mealDish,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author majt
     * @Description 根据id查询菜品
     * @Date 2019/05/20 18:05
     **/
    @GetMapping("/{id}")
    public BaseResponse findMealDish(@PathVariable Integer id) {
        return ResponseHelper.success(mealDishService.getOneDataById(id));
    }

    /**
     * @Author majt
     * @Description 添加菜品
     * @Date 2019/05/20 18:05
     **/
    @PostMapping
    public BaseResponse saveMealDish(@Valid @RequestBody MealDish mealDish, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            CafeteriaHandleUtil.bindCafeteriaInfo(mealDish);
            mealDishService.save(mealDish);
            return ResponseHelper.success(mealDish);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author majt
     * @Description 局部更新菜品
     * @Date 2019/05/20 18:05
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchMealDish(@PathVariable Integer id, @RequestBody MealDish mealDish) {
        try {
            boolean result = mealDishService.patch(mealDish, id);
            if (result) {
                return ResponseHelper.success(mealDish);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author majt
     * @Description 根据id删除菜品
     * @Date 2019/05/20 18:05
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteMealDish(@PathVariable Integer id) {
        try {
            mealDishService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
