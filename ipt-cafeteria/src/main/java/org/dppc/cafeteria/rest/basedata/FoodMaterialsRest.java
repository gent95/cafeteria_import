package org.dppc.cafeteria.rest.basedata;

import org.dppc.cafeteria.entity.basedata.FoodMaterials;
import org.dppc.cafeteria.service.basedata.FoodMaterialsService;
import lombok.extern.java.Log;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 食材表
 * @Author majt
 * @Data 2019/05/09 15:42
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/foodMaterials")
public class FoodMaterialsRest {

    @Autowired
    private FoodMaterialsService foodMaterialsService;

    /**
     * @Author majt
     * @Description 根据条件分页查询食材表
     * @Date 2019/05/09 15:42
     **/
    @GetMapping("/page")
    public BaseResponse page(@ModelAttribute FoodMaterials foodMaterials,@ModelAttribute Pager pager) {
        /** 根据条件分页查询集合 **/
        Page<FoodMaterials> page = foodMaterialsService.findPageList(foodMaterials,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author majt
     * @Description 根据id查询食材表
     * @Date 2019/05/09 15:42
     **/
    @GetMapping("/{id}")
    public BaseResponse findFoodMaterials(@PathVariable Integer id) {
        return ResponseHelper.success(foodMaterialsService.getOneDataById(id));
    }

    /**
     * @Author majt
     * @Description 添加食材表
     * @Date 2019/05/09 15:42
     **/
    @PostMapping
    public BaseResponse saveFoodMaterials(@Valid @RequestBody FoodMaterials foodMaterials, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            foodMaterialsService.saveFoodMaterials(foodMaterials);
            return ResponseHelper.success(foodMaterials);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author majt
     * @Description 局部更新食材表
     * @Date 2019/05/09 15:42
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchFoodMaterials(@PathVariable Integer id, @RequestBody FoodMaterials foodMaterials) {
        try {
            boolean result = foodMaterialsService.patch(foodMaterials, id);
            if (result) {
                return ResponseHelper.success(foodMaterials);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author majt
     * @Description 根据id删除食材表
     * @Date 2019/05/09 15:42
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteFoodMaterials(@PathVariable Integer id) {
        try {
            foodMaterialsService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
