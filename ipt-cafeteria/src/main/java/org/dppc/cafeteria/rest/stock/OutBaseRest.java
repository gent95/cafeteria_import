package org.dppc.cafeteria.rest.stock;

import com.sun.corba.se.pept.transport.OutboundConnectionCache;
import lombok.extern.java.Log;
import org.dppc.cafeteria.entity.stock.OutBase;
import org.dppc.cafeteria.service.stock.OutBaseService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.cafeteria.vo.OutStockVO;
import org.dppc.common.entity.Pager;
import org.dppc.common.enums.stock.OutStatusEnum;
import org.dppc.common.exception.RepeatOperateException;
import org.dppc.common.exception.StockNumberException;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 原材料出库信息主表
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/stock/outBase")
public class OutBaseRest {

    @Autowired
    private OutBaseService outBaseService;

    /**
     * @Author lhw
     * @Description 根据条件分页查询原材料出库信息主表
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, OutBase outBase) {
        /** 根据条件分页查询集合 **/
        outBase.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(outBase.getCafeteriaCode()));
        Page<OutBase> page = outBaseService.findPageList(outBase,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author lhw
     * @Description 根据id查询原材料出库信息主表
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/{id}")
    public BaseResponse findOutBase(@PathVariable Long id) {
        return ResponseHelper.success(outBaseService.getOneDataById(id));
    }

    /**
     * @Author lhw
     * @Description 查询入库状态枚举集合
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/outStatusEnums/")
    public BaseResponse gatOutStatusEnums() {
        return ResponseHelper.success(OutStatusEnum.getEnumList());
    }


    /**
     * @Author lhw
     * @Description 出库
     * @Date 2019/05/09 11:34
     **/
    @PatchMapping(value = "/outStock/")
    public BaseResponse outStock(@RequestBody OutStockVO outStockVO) {
        try {
            outBaseService.outStock(outStockVO);
            return ResponseHelper.success("出库成功");
        } catch (RepeatOperateException repeatOperateExe){
            return ResponseHelper.error("该出库单已经出库，不可重复出库");
        } catch (StockNumberException stockExe) {
            return ResponseHelper.error(stockExe.getCustomMessage());
        } catch (Exception e) {
            e.printStackTrace();
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error("出库失败");
    }

    /**
     * @Author lhw
     * @Description 添加原材料出库信息主表
     * @Date 2019/05/09 11:34
     **/
    @PostMapping
    public BaseResponse saveOutBase(@Valid @RequestBody OutBase outBase, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            outBaseService.save(outBase);
            return ResponseHelper.success(outBase);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author lhw
     * @Description 局部更新原材料出库信息主表
     * @Date 2019/05/09 11:34
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchOutBase(@PathVariable Long id, @RequestBody OutBase outBase) {
        try {
            boolean result = outBaseService.patch(outBase, id);
            if (result) {
                return ResponseHelper.success(outBase);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author lhw
     * @Description 根据id删除原材料出库信息主表
     * @Date 2019/05/09 11:34
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteOutBase(@PathVariable Long id) {
        try {
            outBaseService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
