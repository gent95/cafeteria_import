package org.dppc.cafeteria.rest.purchase;

import org.dppc.cafeteria.entity.purchase.PurchaseBase;
import org.dppc.cafeteria.service.purchase.PurchaseBaseService;
import lombok.extern.java.Log;
import org.dppc.cafeteria.service.record.PersonnelService;
import org.dppc.cafeteria.service.record.SupplierService;
import org.dppc.cafeteria.utils.CafeteriaHandleUtil;
import org.dppc.cafeteria.vo.PurchaseVO;
import org.dppc.common.entity.Pager;
import org.dppc.common.enums.AuditStatusEnum;
import org.dppc.common.enums.RecordAuditStatusEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.logging.Level;

/**
 * @Description 采购计划
 * @Author zmm
 * @Data 2019/05/17 15:37
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/purchaseBase")
public class PurchaseBaseRest {

    @Autowired
    private PurchaseBaseService purchaseBaseService;
    @Autowired
    private SupplierService supplierService;

    /**
     * @Author zmm
     * @Description 根据条件分页查询采购计划
     * @Date 2019/05/17 15:37
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, PurchaseBase purchaseBase) {
        purchaseBase.setCafeteriaCode(CafeteriaHandleUtil.validateAuth(purchaseBase.getCafeteriaCode()));
        /** 根据条件分页查询集合 **/
        Page<PurchaseBase> page = purchaseBaseService.findPageList(purchaseBase,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        HashMap<Object, Object> map = new HashMap<>(3);
        map.put("auditStatusEnums", AuditStatusEnum.values());
        map.put("supplierList", supplierService.getAllList());
        return ResponseHelper.success(page,map);
    }

    /**
     * @Author zmm
     * @Description 根据id查询采购计划
     * @Date 2019/05/17 15:37
     **/
    @GetMapping("/{id}")
    public BaseResponse findPurchaseBase(@PathVariable Integer id) {
        return ResponseHelper.success(purchaseBaseService.getOneDataById(id));
    }

    /**
     * @Author zmm
     * @Description 添加采购计划
     * @Date 2019/05/17 15:37
     **/
    @PostMapping
    public BaseResponse savePurchaseBase(@RequestBody PurchaseVO purchaseVO) {
        try {
            purchaseBaseService.savePurchaseVO(purchaseVO);
            return ResponseHelper.success("添加成功");
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error("添加失败");
        }
    }

    /**
     * @Author zmm
     * @Description 采购计划审核
     * @Date 2019/05/17 15:37
     **/
    @PatchMapping(value = "/audit/{id}")
    public BaseResponse checkPurchase(@PathVariable Integer id, @RequestBody PurchaseBase purchaseBase) {
        try {
            purchaseBaseService.checkPurchase(id,purchaseBase.getAuditStatus());
            return ResponseHelper.success("审核成功");
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error("审核失败");
        }
    }
    /**
     * @Author zmm
     * @Description 修改采购信息
     * @Date 2019/05/17 15:37
     **/
    @PutMapping(value = "/purchaseVO/")
    public BaseResponse patchPurchaseVO(@RequestBody PurchaseVO purchaseVO) {
        try {
            purchaseBaseService.patchPurchaseVO(purchaseVO);
            return ResponseHelper.success("操作成功");
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error("操作失败");
        }
    }

    /**
     * @Author zmm
     * @Description 局部更新采购计划
     * @Date 2019/05/17 15:37
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchPurchaseBase(@PathVariable Integer id, @RequestBody PurchaseBase purchaseBase) {
        try {
            boolean result = purchaseBaseService.patch(purchaseBase, id);
            if (result) {
                return ResponseHelper.success(purchaseBase);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author zmm
     * @Description 根据id删除采购计划
     * @Date 2019/05/17 15:37
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deletePurchaseBase(@PathVariable Integer id) {
        try {
            purchaseBaseService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
