package org.dppc.cafeteria.rest.stock;

import org.dppc.cafeteria.entity.stock.InDetail;
import org.dppc.cafeteria.service.stock.InDetailService;
import lombok.extern.java.Log;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 原材料入库信息细表
 * @Author lhw
 * @Data 2019/05/09 11:34
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/stock/inDetail")
public class InDetailRest {

    @Autowired
    private InDetailService inDetailService;

    /**
     * @Author lhw
     * @Description 根据条件分页查询原材料入库信息细表
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, InDetail inDetail) {
        /** 根据条件分页查询集合 **/
        Page<InDetail> page = inDetailService.findPageList(inDetail,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author lhw
     * @Description 根据id查询原材料入库信息细表
     * @Date 2019/05/09 11:34
     **/
    @GetMapping("/{id}")
    public BaseResponse findInDetail(@PathVariable Long id) {
        return ResponseHelper.success(inDetailService.getOneDataById(id));
    }

    /**
     * @Author lhw
     * @Description 添加原材料入库信息细表
     * @Date 2019/05/09 11:34
     **/
    @PostMapping
    public BaseResponse saveInDetail(@Valid @RequestBody InDetail inDetail, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            inDetailService.save(inDetail);
            return ResponseHelper.success(inDetail);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author lhw
     * @Description 局部更新原材料入库信息细表
     * @Date 2019/05/09 11:34
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchInDetail(@PathVariable Long id, @RequestBody InDetail inDetail) {
        try {
            boolean result = inDetailService.patch(inDetail, id);
            if (result) {
                return ResponseHelper.success(inDetail);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author lhw
     * @Description 根据id删除原材料入库信息细表
     * @Date 2019/05/09 11:34
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteInDetail(@PathVariable Long id) {
        try {
            inDetailService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
