package org.dppc.auth.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.dppc.auth.service.AuthService;
import org.dppc.auth.util.interfaces.JwtInterfaceResponse;
import org.dppc.auth.util.user.JwtAuthenticationRequest;
import org.dppc.auth.util.user.JwtAuthenticationResponse;
import org.dppc.auth.vo.FrontUser;
import org.dppc.common.enums.ExceptionEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.Base64Helper;
import org.dppc.common.utils.JsonpUtil;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
@Api(value="jwt权限控制")
@RestController
@RequestMapping("/security")
public class AuthRest {
    @Value("${jwt.token-header}")
    private String tokenHeader;

    @Autowired
    private AuthService authService;

    @ApiOperation(value = "后台登录", notes = "后台登录")
    @GetMapping(value = "/{username}/{password}")
    public String login(String callback, @PathVariable("username")String username, @PathVariable("password")String password) {
        try {
            if (password == null) {
                return JsonpUtil.getResult(callback, ResponseHelper.error("参数错误"));
            }
            String token = authService.login(new String(Base64Helper.decode(username)), new String(Base64Helper.decode(password)));
            if(org.springframework.util.StringUtils.isEmpty(token)){
                return JsonpUtil.getResult(callback, ResponseHelper.error("登陆失败,请检查用户名、密码是否填写正确"));
            }
            return JsonpUtil.getResult(callback, ResponseHelper.success(token));
        } catch (Exception e) {
            return JsonpUtil.getResult(callback, ResponseHelper.error("登陆失败"));
        }
    }


    @ApiOperation(value = "用户登录",notes = "username:用户名，password：密码")
    @RequestMapping(value = "/token", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest) throws Exception {
        final String token = authService.login(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        if(StringUtils.isEmpty(token)){
            return ResponseHelper.error(ExceptionEnum.EX_LOGIN_ERROR);
        }else {
            return ResponseHelper.success(token);
        }
    }

    @ApiOperation(value = "接口用户登录",notes = "username:用户名，password：密码")
    @RequestMapping(value = "access_token", method = RequestMethod.POST)
    @ResponseBody
    public JwtInterfaceResponse createInterfaceToken(
            @RequestBody JwtAuthenticationRequest authenticationRequest) throws Exception {
        JwtInterfaceResponse j = authService.interfaceLogin(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        return j;
    }

    @RequestMapping(value = "refresh", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(
            HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        String refreshedToken = authService.refresh(token);
        if(refreshedToken == null) {
            return ResponseEntity.badRequest().body(null);
        } else {
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));
        }
    }

    @RequestMapping(value = "verify", method = RequestMethod.GET)
    public ResponseEntity<?> verify(String token) throws Exception {
        authService.validate(token);
        return ResponseEntity.ok(true);
    }

    @RequestMapping(value = "invalid", method = RequestMethod.POST)
    public ResponseEntity<?> invalid(@RequestHeader("access-token") String token){
        authService.invalid(token);
        return ResponseEntity.ok(true);
    }

    @RequestMapping(value = "user", method = RequestMethod.GET)
    public ResponseEntity<?> getUserInfo(String token) throws Exception {
        FrontUser userInfo = authService.getUserInfo(token);
        if(userInfo==null)
            return ResponseEntity.status(401).body(false);
        else
            return ResponseEntity.ok(userInfo);
    }

    @GetMapping(value="test")
    public BaseResponse test(){
        return ResponseHelper.success("中文乱码吗");
    }
}
