package org.dppc.auth.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.apache.commons.lang3.StringUtils;
import org.dppc.auth.configuration.ClientConfig;
import org.dppc.auth.service.ClientService;
import org.dppc.common.context.ClientConstantHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 请求拦截器
 */
public class ClientTokenInterceptor implements RequestInterceptor {
    private Logger logger = LoggerFactory.getLogger(ClientTokenInterceptor.class);
    @Autowired
    private ClientConfig clientConfig;
    @Autowired
    private ClientService clientService;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        try {
            if (StringUtils.isEmpty(ClientConstantHandler.CLIENT_TOKEN))
                ClientConstantHandler.CLIENT_TOKEN = clientService.apply(clientConfig.getClientId(), clientConfig.getClientSecret());
            requestTemplate.header(clientConfig.getClientTokenHeader(), ClientConstantHandler.CLIENT_TOKEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




}
