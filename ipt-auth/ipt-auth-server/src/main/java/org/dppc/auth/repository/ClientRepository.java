package org.dppc.auth.repository;

import org.dppc.auth.entity.GateClient;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @描述： client
 * @作者： 颜齐
 * @日期： 2017/10/18.
 * @时间： 16:42.
 */
public interface ClientRepository extends BaseRepository<GateClient,Long> {
    /**
     * 根据code查询
     * @param code
     * @return
     */
    GateClient findByCode(String code);

    /**
     *
     * @return
     */


}
