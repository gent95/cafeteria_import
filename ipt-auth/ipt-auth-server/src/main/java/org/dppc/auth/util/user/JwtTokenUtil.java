package org.dppc.auth.util.user;

import org.dppc.common.enums.UserTypeEnum;
import org.dppc.common.jwt.IJWTInfo;
import org.dppc.common.utils.JWTHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by ace on 2017/9/10.
 */
@Component
public class JwtTokenUtil {

    @Value("${jwt.sys-user-expire}")
    private int sysUserExpire;
    @Value("${jwt.interface-user-expire}")
    private int interfaceUserExpire;
    @Value("${jwt.pri-key.path}")
    private String priKeyPath;
    @Value("${jwt.pub-key.path}")
    private String pubKeyPath;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 根据用户类型生成token
     * @param jwtInfo 用户信息
     * @param type 用户类型
     * @return
     * @throws Exception
     */
    public String generateToken(IJWTInfo jwtInfo, UserTypeEnum type) throws Exception {
        /*if (type == UserTypeEnum.INTERFACE_USER || type == UserTypeEnum.INTERFACE_CITY_USER)
            return JWTHelper.generateToken(jwtInfo, priKeyPath, interfaceUserExpire);
        else*/ //其他用户，时间为5个小时
            return JWTHelper.generateToken(jwtInfo, priKeyPath, sysUserExpire);
    }

    public IJWTInfo getInfoFromToken(String token) throws Exception {
        return JWTHelper.getInfoFromToken(token, pubKeyPath);
    }


}
