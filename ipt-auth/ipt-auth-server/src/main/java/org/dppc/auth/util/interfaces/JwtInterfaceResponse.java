package org.dppc.auth.util.interfaces;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

public class JwtInterfaceResponse implements Serializable {
    private static final long serialVersionUID = 1250166508152483573L;
    @JsonProperty(value = "STATUS_CODE")
    private Integer statusCode;
    @JsonProperty(value = "AUTH_ID")
    private String authId;
    @JsonProperty(value = "TOKEN")
    private String token;


    public JwtInterfaceResponse(String token, String authId) {
        this.token = token;
        this.authId = authId;
        if (StringUtils.isEmpty(toString())) {
            this.statusCode = 40109;
        } else {
            this.statusCode = 200;
        }
    }


    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
