package org.dppc.auth.rpc;

import org.dppc.auth.service.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @描述 :   门户登陆（ipt-home-web模块请求）
 * @作者 :   GAOJ
 * @日期 :   2017/11/14 0014
 * @时间 :   11:12
 */
@RestController
@RequestMapping(value = "/api/login")
public class LoginApi {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private AuthService authService;

    /**
     * @描述 :   门户登陆
     * @作者 :   GAOJ
     * @日期 :   2017/11/14 0014
     * @时间 :   11:12
     */
    @PostMapping(value = "token/{username}/{password}")
    public String token(@PathVariable("username")String username,@PathVariable("password")String password) {
        String token = null;
        try {
            token = authService.login(username, password);
        }catch (Exception e){
            logger.error("门户登陆用户获取token出错");
        }
        return token;
    }

}
