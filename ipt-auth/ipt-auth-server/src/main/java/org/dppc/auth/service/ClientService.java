package org.dppc.auth.service;


import org.dppc.auth.entity.GateClient;
import org.dppc.dbexpand.service.BaseService;

import java.util.List;

/**
 * Created by ace on 2017/9/10.
 */
public interface ClientService extends BaseService<GateClient, Long> {
    String apply(String clientId, String secret) throws Exception;

    /**
     * 获取授权的客户端列表
     *
     * @param serviceId
     * @param secret
     * @return
     */
    List<String> getAllowedClient(String serviceId, String secret);

    void registryClient();
}
