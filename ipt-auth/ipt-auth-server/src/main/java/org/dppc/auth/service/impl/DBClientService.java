package org.dppc.auth.service.impl;

import org.dppc.auth.bean.ClientInfo;
import org.dppc.auth.entity.GateClient;
import org.dppc.auth.entity.GateClientService;
import org.dppc.auth.repository.ClientRepository;
import org.dppc.auth.repository.ClientServiceRepository;
import org.dppc.auth.service.ClientService;
import org.dppc.auth.util.client.ClientTokenUtil;
import org.dppc.common.exception.auth.ClientInvalidException;
import org.dppc.common.utils.UUIDHelper;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/18.
 * @时间： 16:42.
 */
@Service
@Transactional
public class DBClientService extends BaseServiceImpl<GateClient, Long> implements ClientService {
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private ClientServiceRepository clientServiceRepository;
    @Autowired
    private ClientTokenUtil clientTokenUtil;
    @Autowired
    private DiscoveryClient discovery;

    @Override
    public String apply(String clientId, String secret) throws Exception {
        GateClient client = getClient(clientId, secret);
        return clientTokenUtil.generateToken(new ClientInfo(client.getCode(), client.getName(), client.getClientId().toString()));
    }

    private GateClient getClient(String clientCode, String secret) {
        GateClient client = clientRepository.findByCode(clientCode);
        if (client == null || !client.getSecret().equals(secret)) {
            throw new ClientInvalidException();
        }
        return client;
    }

    @Override
    public List<String> getAllowedClient(String clientId, String secret) {
        GateClient client = this.getClient(clientId, secret);
        List<String> clientCodes = new ArrayList<>();
        List<GateClientService> gateClientServiceList = clientServiceRepository.findByServiceId(client.getClientId());
        if (gateClientServiceList == null || gateClientServiceList.size() == 0)
            return null;
        for (GateClientService gateClientService : gateClientServiceList) {
            clientCodes.add(clientRepository.findOne(gateClientService.getClientId()).getCode());
        }
        return clientCodes;
    }

    @Override
    @Scheduled(cron = "0 0/10 * * * ?")
    public void registryClient() {
        // 自动注册节点
        discovery.getServices().forEach((name) -> {
            GateClient client = new GateClient();
            client.setName(name);
            client.setCode(name);
            if (clientRepository.findByCode(name) == null) {
                client.setSecret(UUIDHelper.generateShortUuid());
                clientRepository.save(client);
            }
        });
    }
}
