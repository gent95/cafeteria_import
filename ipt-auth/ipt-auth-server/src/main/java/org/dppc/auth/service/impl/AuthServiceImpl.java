package org.dppc.auth.service.impl;

import org.dppc.api.admin.dto.Resource;
import org.dppc.api.admin.dto.User;
import org.dppc.auth.feign.UserService;
import org.dppc.auth.service.AuthService;
import org.dppc.auth.util.interfaces.JwtInterfaceResponse;
import org.dppc.auth.util.user.JwtTokenUtil;
import org.dppc.auth.vo.FrontUser;
import org.dppc.common.enums.UserTypeEnum;
import org.dppc.common.jwt.JWTInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthServiceImpl implements AuthService {

    private JwtTokenUtil jwtTokenUtil;
    private UserService userService;
    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);

    @Autowired
    public AuthServiceImpl(
            JwtTokenUtil jwtTokenUtil,
            UserService userService) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.userService = userService;
    }


    @Override
    public User getUserByUsername(String username) {
        return userService.getUserByUsername(username);
    }

    @Override
    public String login(String username, String password) throws Exception {
        User info = userService.getUserByUsername(username);
        String token = "";
        if (encoder.matches(password, info.getPassword())) {
            token = jwtTokenUtil.generateToken(new JWTInfo(info.getUsername(), info.getUserId() + "",
                    info.getName()), UserTypeEnum.valueOf(info.getUserType()));
        }
        return token;
    }

    @Override
    public JwtInterfaceResponse interfaceLogin(String username, String password) throws Exception {
        User info = userService.getUserByUsername(username);
        String token = "";
        if (encoder.matches(password, info.getPassword())) {
            token = jwtTokenUtil.generateToken(new JWTInfo(info.getUsername(), info.getUserId() + "",
                    info.getName()), UserTypeEnum.valueOf(info.getUserType()));
        }
        return new JwtInterfaceResponse(token, String.valueOf(info.getUserId()));
    }

    @Override
    public void validate(String token) throws Exception {
        jwtTokenUtil.getInfoFromToken(token);
    }

    @Override
    public FrontUser getUserInfo(String token) throws Exception {
        String username = jwtTokenUtil.getInfoFromToken(token).getUniqueName();
        if (username == null)
            return null;
        User user = userService.getUserByUsername(username);
        FrontUser frontUser = new FrontUser();
        BeanUtils.copyProperties(user, frontUser);
        List<Resource> permissionInfos = userService.getPermissionByUsername(username);
//        Stream<Resource> menus = permissionInfos.parallelStream().filter((permission) -> {
//            return permission.getType().equals(CommonConstants.RESOURCE_TYPE_MENU);
//        });
//        frontUser.setMenus(menus.collect(Collectors.toList()));
//        Stream<Resource> elements = permissionInfos.parallelStream().filter((permission) -> {
//            return !permission.getType().equals(CommonConstants.RESOURCE_TYPE_MENU);
//        });
//        frontUser.setElements(elements.collect(Collectors.toList()));
        return frontUser;
    }

    @Override
    public Boolean invalid(String token) {
        // TODO: 2017/9/11 注销token
        return null;
    }

    @Override
    public String refresh(String oldToken) {
        // TODO: 2017/9/11 刷新token
        return null;
    }
}
