package org.dppc.auth.feign;

import org.dppc.api.admin.dto.Resource;
import org.dppc.api.authority.PermissionInfo;
import org.dppc.auth.configuration.FeignConfiguration;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/23.
 * @时间： 17:46.
 */
@FeignClient(value = "ipt-admin",configuration = FeignConfiguration.class)
@RequestMapping(value="api/resource")
public interface ResourceService {

    /**
     * 查询所有权限
     * @return
     */
    @RequestMapping(value = "/findAllPermission", method = RequestMethod.GET)
    List<Resource> findAllPermission();

}
