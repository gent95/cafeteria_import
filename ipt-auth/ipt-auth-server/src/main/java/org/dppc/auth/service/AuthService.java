package org.dppc.auth.service;


import org.dppc.api.admin.dto.User;
import org.dppc.auth.util.interfaces.JwtInterfaceResponse;
import org.dppc.auth.vo.FrontUser;

public interface AuthService {
    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    User getUserByUsername(String username);

    String login(String username, String password) throws Exception;

    JwtInterfaceResponse interfaceLogin(String username, String password)  throws Exception ;

    String refresh(String oldToken);

    void validate(String token) throws Exception;

    FrontUser getUserInfo(String token) throws Exception;

    Boolean invalid(String token);
}
