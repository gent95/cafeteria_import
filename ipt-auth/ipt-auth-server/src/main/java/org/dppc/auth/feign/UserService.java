package org.dppc.auth.feign;

import org.dppc.api.admin.dto.Resource;
import org.dppc.api.admin.dto.User;
import org.dppc.api.authority.PermissionInfo;
import org.dppc.api.authority.UserInfo;
import org.dppc.auth.configuration.FeignConfiguration;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-21 8:11
 */
@FeignClient(value = "ipt-admin", configuration = FeignConfiguration.class)
@RequestMapping(value = "api/user")
public interface UserService {
    @RequestMapping(value = "/getUser/{username}", method = RequestMethod.GET)
    User getUserByUsername(@PathVariable("username") String username);

    @RequestMapping(value = "/getPermissions/{username}", method = RequestMethod.GET)
    List<Resource> getPermissionByUsername(@PathVariable("username") String username);

}
