package org.dppc.auth.service;

import org.dppc.auth.configuration.ClientConfig;
import org.dppc.common.context.ClientConstantHandler;
import org.dppc.common.msg.ObjectRestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/25.
 * @时间： 10:59.
 */
@Component
public class ScheduleService {
    Logger logger = LoggerFactory.getLogger(ScheduleService.class);
    @Autowired
    private ClientConfig clientConfig;
    @Autowired
    private ClientService clientService;

    /**
     * 定时更新客户端token
     */
    @Scheduled(cron = "0 0 0/4 * * ?")
    public void refresh() {
        try {
            logger.info("refresh client token.....");
            ClientConstantHandler.CLIENT_TOKEN = clientService.apply(clientConfig.getClientId(), clientConfig.getClientSecret());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
