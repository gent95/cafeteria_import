package org.dppc.auth.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/19.
 * @时间： 9:56.
 */
@Entity
@Table(name = "SYS_GATE_CLIENT_SERVICE")
public class GateClientService implements Serializable{
    @Id

    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;
    @Column(name="SERVICE_ID")
    private Long serviceId;
    @Column(name="CLIENT_ID")
    private Long clientId;
    @Column(name="DESCRIPTION")
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
