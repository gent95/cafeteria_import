package org.dppc.auth.repository;

import org.dppc.auth.entity.GateClientService;
import org.dppc.dbexpand.repository.BaseRepository;

import java.util.List;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/19.
 * @时间： 10:18.
 */
public interface ClientServiceRepository extends BaseRepository<GateClientService,Long> {

    /**
     *
     * @param serviceId
     * @return
     */
    List<GateClientService> findByServiceId(Long serviceId);
}
