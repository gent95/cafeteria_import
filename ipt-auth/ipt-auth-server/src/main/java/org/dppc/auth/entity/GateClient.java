package org.dppc.auth.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SYS_GATE_CLIENT")
public class GateClient {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)

    @Column(name = "CLIENT_ID")
    private Long clientId;

    @Column(name = "CODE",length = 30)
    private String code;

    @Column(name = "SECRET",length = 50)
    private String secret;

    @Column(name = "NAME",length = 30)
    private String name;

    @Column(name = "LOCKED",length = 30)
    private String locked = "0";

    @Column(name = "DESCRIPTION",length = 30)
    private String description;

    @Column(name = "CRT_TIME")
    private Date crtTime;

    @Column(name = "CRT_USER",length = 30)
    private String crtUser;

    @Column(name = "CRT_NAME",length = 30)
    private String crtName;

    @Column(name = "CRT_HOST",length = 30)
    private String crtHost;

    @Column(name = "UPD_TIME")
    private Date updTime;

    @Column(name = "UPD_USER",length = 30)
    private String updUser;

    @Column(name = "UPD_NAME",length = 30)
    private String updName;

    @Column(name = "UPD_HOST",length = 30)
    private String updHost;


    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocked() {
        return locked;
    }

    public void setLocked(String locked) {
        this.locked = locked;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCrtTime() {
        return crtTime;
    }

    public void setCrtTime(Date crtTime) {
        this.crtTime = crtTime;
    }

    public String getCrtUser() {
        return crtUser;
    }

    public void setCrtUser(String crtUser) {
        this.crtUser = crtUser;
    }

    public String getCrtName() {
        return crtName;
    }

    public void setCrtName(String crtName) {
        this.crtName = crtName;
    }

    public String getCrtHost() {
        return crtHost;
    }

    public void setCrtHost(String crtHost) {
        this.crtHost = crtHost;
    }

    public Date getUpdTime() {
        return updTime;
    }

    public void setUpdTime(Date updTime) {
        this.updTime = updTime;
    }

    public String getUpdUser() {
        return updUser;
    }

    public void setUpdUser(String updUser) {
        this.updUser = updUser;
    }

    public String getUpdName() {
        return updName;
    }

    public void setUpdName(String updName) {
        this.updName = updName;
    }

    public String getUpdHost() {
        return updHost;
    }

    public void setUpdHost(String updHost) {
        this.updHost = updHost;
    }
}