package org.dppc.auth.client.annotation;

/**
 * 忽略服务鉴权
 */
public @interface IgnoreClientToken {
}
