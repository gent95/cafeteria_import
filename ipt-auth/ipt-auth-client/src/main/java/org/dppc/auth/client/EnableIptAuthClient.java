package org.dppc.auth.client;

import org.dppc.auth.client.configuration.AutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @描述： 针对平台开启用户拦截
 * @作者： 颜齐
 * @日期： 2017/10/19.
 * @时间： 15:13.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(AutoConfiguration.class)
@Documented
@Inherited
public @interface EnableIptAuthClient {
}
