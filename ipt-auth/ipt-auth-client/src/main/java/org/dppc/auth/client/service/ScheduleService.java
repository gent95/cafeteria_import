package org.dppc.auth.client.service;

import org.dppc.auth.client.config.ServiceAuthConfig;
import org.dppc.auth.client.feign.ServiceAuthFeign;
import org.dppc.auth.client.jwt.ServiceAuthUtil;
import org.dppc.common.context.ClientConstantHandler;
import org.dppc.common.msg.ObjectRestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @描述： 定时更新客户端token
 * @作者： 颜齐
 * @日期： 2017/10/25.
 * @时间： 10:59.
 */
@Component
public class ScheduleService {
    Logger logger = LoggerFactory.getLogger(ScheduleService.class);
    @Autowired
    private ServiceAuthFeign serviceAuthFeign;
    @Autowired
    private ServiceAuthUtil serviceAuthUtil;
    @Autowired
    private ServiceAuthConfig serviceAuthConfig;

    /**
     * 定时更新客户端token
     */
    @Scheduled(cron = "0 0 0/4 * * ?")
    public void refresh() {
        try {
            logger.info("refresh client token.....");
            serviceAuthUtil.getInfoFromToken(ClientConstantHandler.CLIENT_TOKEN);
        } catch (Exception e) {
            ObjectRestResponse resp;
            resp = serviceAuthFeign.getAccessToken(serviceAuthConfig.getClientId(), serviceAuthConfig.getClientSecret());
            logger.info(resp.toString());
            if (resp.getStatus() == 200) {
                ClientConstantHandler.CLIENT_TOKEN = String.valueOf(resp.getData());
            }
        }
    }
}
