package org.dppc.auth.client.feign;

import org.dppc.common.msg.ObjectRestResponse;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 *
 */
@FeignClient(value = "IPT-AUTH")
public interface ServiceAuthFeign {
    /**
     * 根据服务名称查询允许访问的客户端
     * @param serviceId
     * @param secret
     * @return
     */
    @RequestMapping(value = "/client/myClient")
    ObjectRestResponse<List<String>> getAllowedClient(@RequestParam("serviceId") String serviceId, @RequestParam("secret") String secret);

    /**
     * 根据服务名称和密码，获取最新token
     * @param clientId
     * @param secret
     * @return
     */
    @RequestMapping(value = "/client/token", method = RequestMethod.POST)
    ObjectRestResponse getAccessToken(@RequestParam("clientId") String clientId, @RequestParam("secret") String secret);

}
