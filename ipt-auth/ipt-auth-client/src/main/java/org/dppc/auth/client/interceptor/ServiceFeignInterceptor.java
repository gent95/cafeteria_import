package org.dppc.auth.client.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.apache.commons.lang3.StringUtils;
import org.dppc.auth.client.config.ServiceAuthConfig;
import org.dppc.auth.client.config.UserAuthConfig;
import org.dppc.auth.client.feign.ServiceAuthFeign;
import org.dppc.auth.client.jwt.ServiceAuthUtil;
import org.dppc.auth.client.service.ScheduleService;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.context.ClientConstantHandler;
import org.dppc.common.msg.ObjectRestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * 在feign请求的里面加入token
 */
public class ServiceFeignInterceptor implements RequestInterceptor {
    private Logger logger = LoggerFactory.getLogger(ServiceFeignInterceptor.class);
    @Autowired
    private ServiceAuthConfig serviceAuthConfig;
    @Autowired
    private UserAuthConfig userAuthConfig;
//    @Autowired
//    private ServiceAuthFeign serviceAuthFeign;
//    @Autowired
//    private ServiceAuthUtil serviceAuthUtil;
    @Autowired
    private ScheduleService scheduleService;


    @Override
    public void apply(RequestTemplate requestTemplate) {
        if (StringUtils.isEmpty(ClientConstantHandler.CLIENT_TOKEN))
            scheduleService.refresh();
        requestTemplate.header(serviceAuthConfig.getTokenHeader(), ClientConstantHandler.CLIENT_TOKEN);
        requestTemplate.header(userAuthConfig.getTokenHeader(), BaseContextHandler.getToken());
    }

}