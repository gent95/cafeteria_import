package org.dppc.auth.client.jwt;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import org.dppc.auth.client.config.UserAuthConfig;
import org.dppc.common.exception.BaseException;
import org.dppc.common.exception.auth.JwtIllegalArgumentException;
import org.dppc.common.exception.auth.JwtSignatureException;
import org.dppc.common.exception.auth.JwtTokenExpiredException;
import org.dppc.common.exception.auth.TokenErrorException;
import org.dppc.common.jwt.IJWTInfo;
import org.dppc.common.utils.JWTHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * token解析
 */
@Configuration
public class UserAuthUtil {
    Logger logger = LoggerFactory.getLogger(UserAuthUtil.class.getName());
    @Autowired
    private UserAuthConfig userAuthConfig;
    public IJWTInfo getInfoFromToken(String token) throws BaseException {
        try {
            return JWTHelper.getInfoFromToken(token, userAuthConfig.getPubKeyPath());
        } catch (ExpiredJwtException ex){
            logger.info("登陆的用户超时");
            throw new JwtTokenExpiredException();
        } catch (SignatureException ex){
            logger.info("签名出现异常");
            throw new JwtSignatureException();
        } catch (IllegalArgumentException ex){
            logger.info("参数出现错误");
            throw new JwtIllegalArgumentException();
        } catch (Exception e){
            logger.info("token位置错误");
            throw new TokenErrorException();
        }
    }
}
