package org.dppc.auth.client.interceptor;

import org.apache.commons.lang3.StringUtils;
import org.dppc.api.admin.dto.User;
import org.dppc.auth.client.annotation.IgnoreUserToken;
import org.dppc.auth.client.config.UserAuthConfig;
import org.dppc.auth.client.feign.UserFeign;
import org.dppc.auth.client.jwt.UserAuthUtil;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.jwt.IJWTInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 针对用户做拦截，并记录用户
 */
public class UserAuthRestInterceptor extends HandlerInterceptorAdapter {
    private Logger logger = LoggerFactory.getLogger(UserAuthRestInterceptor.class);

    @Value("${auth.user.ignore.startWith}")
    private String ignoreStartWith;

    @Autowired
    private UserAuthUtil userAuthUtil;

    @Autowired
    private UserAuthConfig userAuthConfig;

    @Autowired
    private UserFeign userFeign;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        // 配置该注解，说明不进行用户拦截
        IgnoreUserToken annotation = handlerMethod.getBeanType().getAnnotation(IgnoreUserToken.class);
        if (annotation == null)
            annotation = handlerMethod.getMethodAnnotation(IgnoreUserToken.class);
        if (annotation != null)
            return super.preHandle(request, response, handler);
        if (isStartWith(request.getRequestURI(), ignoreStartWith)) {
            return super.preHandle(request, response, handler);
        }
        String token = request.getHeader(userAuthConfig.getTokenHeader());
        IJWTInfo infoFromToken = userAuthUtil.getInfoFromToken(token);
        BaseContextHandler.setUsername(infoFromToken.getUniqueName());
        BaseContextHandler.setName(infoFromToken.getName());
        BaseContextHandler.setUserID(infoFromToken.getId());
        User user = userFeign.getUserByUsername(infoFromToken.getUniqueName());
        BaseContextHandler.setCityCode(user.getCityCode());
        BaseContextHandler.setUserType(user.getUserType());
        BaseContextHandler.setSchoolCafterias(user.getSchoolCafterias());
        return super.preHandle(request, response, handler);
    }

    private boolean isStartWith(String requestURI, String ignoreStartWith) {
        if (StringUtils.isEmpty(ignoreStartWith)) {
            return false;
        }
        String[] ignoreStartWiths = ignoreStartWith.split(",");
        for (String startWith : ignoreStartWiths) {
            if (requestURI.startsWith(startWith.trim())) {
                return true;
            }
        }
        return false;
    }
}
