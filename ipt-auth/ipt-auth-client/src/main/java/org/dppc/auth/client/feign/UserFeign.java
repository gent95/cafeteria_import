package org.dppc.auth.client.feign;

import org.dppc.api.admin.dto.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @描述：
 * @作者： yanqi
 * @日期： 2018/6/5
 */
@FeignClient(value = "IPT-ADMIN")
public interface UserFeign {

    @RequestMapping(value = "/api/user/getUser/{username}", method = RequestMethod.GET)
    User getUserByUsername(@PathVariable("username") String username);
}
