package org.dppc.auth.client.annotation;

/**
 * 忽略用户鉴权
 */
public @interface IgnoreUserToken {
}
