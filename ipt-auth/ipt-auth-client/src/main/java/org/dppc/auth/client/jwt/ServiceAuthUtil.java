
package org.dppc.auth.client.jwt;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import org.dppc.auth.client.config.ServiceAuthConfig;
import org.dppc.common.exception.auth.JwtIllegalArgumentException;
import org.dppc.common.exception.auth.JwtSignatureException;
import org.dppc.common.exception.auth.JwtTokenExpiredException;
import org.dppc.common.jwt.IJWTInfo;
import org.dppc.common.utils.JWTHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 */
@Configuration
public class ServiceAuthUtil {
    @Autowired
    private ServiceAuthConfig serviceAuthConfig;

    public IJWTInfo getInfoFromToken(String token) throws Exception {
        try {
            return JWTHelper.getInfoFromToken(token, serviceAuthConfig.getPubKeyPath());
        }catch (ExpiredJwtException ex){
            throw new JwtTokenExpiredException();
        }catch (SignatureException ex){
            throw new JwtSignatureException();
        }catch (IllegalArgumentException ex){
            throw new JwtIllegalArgumentException();
        }
    }
}