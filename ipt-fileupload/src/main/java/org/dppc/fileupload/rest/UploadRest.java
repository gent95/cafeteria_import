package org.dppc.fileupload.rest;

import lombok.extern.slf4j.Slf4j;
import org.dppc.fileupload.entity.FileChunk;
import org.dppc.fileupload.entity.FileInfo;
import org.dppc.fileupload.service.FileChunkService;
import org.dppc.fileupload.service.FileInfoService;
import org.dppc.fileupload.utils.ftp.FtpOptionProperties;
import org.dppc.fileupload.utils.ftp.IotFtpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import static org.dppc.fileupload.utils.FileUtils.generatePath;
import static org.dppc.fileupload.utils.FileUtils.merge;

/**
 * @描述 :  分片上传文件
 * @作者 :	zhumh
 * @日期 :	2019/5/21
 * @时间 :	13:38
 */
@RestController
@Slf4j
public class UploadRest {

    private String uploadFolder = System.getProperty("user.dir");
    @Autowired
    private FileInfoService fileInfoService;
    @Autowired
    private FileChunkService chunkService;
    @Autowired
    private IotFtpService iotFtpService;
    @Autowired
    private FtpOptionProperties ftpOptionProperties;

    /**
     * 上传文件且保存到数据库
     * @param fileChunk
     * @return
     */
    @PostMapping("/chunk")
    public String uploadChunk(FileChunk fileChunk) {
        MultipartFile file = fileChunk.getFile();
        log.debug("file originName: {}, chunkNumber: {}", file.getOriginalFilename(), fileChunk.getChunkNumber());

        try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(generatePath(uploadFolder, fileChunk));
            //文件写入指定路径
            Files.write(path, bytes);
            log.debug("文件 {} 写入成功, uuid:{}", fileChunk.getFileName(), fileChunk.getIdentifier());
            chunkService.save(fileChunk);

            return "文件上传成功";
        } catch (IOException e) {
            e.printStackTrace();
            return "后端异常...";
        }
    }

    /**
     * 判断唯一标识
     * @param response
     * @return
     */
    @GetMapping("/chunk")
    public Object checkChunk(FileChunk fileChunk, HttpServletResponse response) {
        if (chunkService.checkChunk(fileChunk.getIdentifier(), fileChunk.getChunkNumber())) {
            response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
        }

        return fileChunk;
    }

    /**
     *  合并分片文件（所有分片上传结束后调用）
     * @param fileInfo
     * @return
     */
    @PostMapping("/mergeFile")
    public String mergeFile(FileInfo fileInfo) throws Exception {
        String filename = fileInfo.getFileName();
        String path = uploadFolder + "/" + fileInfo.getIdentifier() + "/" + fileInfo.getFileName();
        String folder = uploadFolder + "/" + fileInfo.getIdentifier();
        merge(path, folder,filename);
        Map<String,String> ftpdata = iotFtpService.uploadFile(fileInfo.getFunName(),filename,new FileInputStream(new File(path)));
        if(ftpdata.get("code") == "200" ){
            fileInfo.setLocation(ftpdata.get("location"));
//            fileInfo.setFileName(filename);
            fileInfo.setFtpFileName(ftpdata.get("ftpFileName"));
            fileInfoService.save(fileInfo);
        }
        return "合并成功";
    }

    /**
     *  单个文件上传
     * @param file
     * @return
     */
    @PostMapping("/fileUpload")
    public Map<String,String> fileUpload(MultipartFile file,String funName) throws IOException {
        FileInfo fileInfo = new FileInfo();
        fileInfo.setFunName(funName);//功能模块名
        fileInfo.setFileName(file.getOriginalFilename());//文件名称
        Map<String,String> ftpdata = iotFtpService.uploadFile(fileInfo.getFunName(),
                fileInfo.getFileName(), file.getInputStream());
        if(ftpdata.get("code") == "200" ){
            fileInfo.setLocation(ftpdata.get("location")); //ftp上传路径
            fileInfo.setFtpFileName(ftpdata.get("ftpFileName")); //ftp文件名称
            fileInfo.setType(file.getContentType());
            Long s = file.getSize();
            fileInfo.setTotalSize(s.intValue());
            ftpdata.put("host", ftpOptionProperties.getHttp());
            fileInfoService.save(fileInfo);
        }
        return ftpdata;
    }
}
