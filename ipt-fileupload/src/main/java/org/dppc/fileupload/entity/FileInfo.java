package org.dppc.fileupload.entity;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

/**
 * @Description 文件上传基本信息表
 * @Author zhumh
 * @Data 2019/05/21 11:45
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_file_info")
public class FileInfo implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "file_id")
    private Integer fileId;

    /** 文件名称 */
    @Column(name = "file_name", length = 100)
    private String fileName;

    /** 文件编号 */
    @Column(name = "identifier", length = 100)
    private String identifier;

    /** 文件大小 */
    @Column(name = "total_size")
    private Integer totalSize;

    /** 文件类型 */
    @Column(name = "type", length = 50)
    private String type;

    /** 文件地址 */
    @Column(name = "location", length = 1000)
    private String location;

    /** ftp存储文件名称 */
    @Column(name = "ftp_file_name", length = 100)
    private String ftpFileName;

    /** 功能模块名称（英文） */
    @Transient
    private String funName;



}