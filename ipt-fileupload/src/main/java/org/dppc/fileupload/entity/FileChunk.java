package org.dppc.fileupload.entity;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

/**
 * @Description 文件分片表
 * @Author zhumh
 * @Data 2019/05/21 11:45
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_file_chunk")
public class FileChunk implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "chunk_id")
    private Integer chunkId;

    /** 当前文件快（从1开始） */
    @Column(name = "chunk_number")
    private Integer chunkNumber;

    /** 分块大小 */
    @Column(name = "chunk_size")
    private Integer chunkSize;

    /** 当前分块大小 */
    @Column(name = "current_chunk_Size")
    private Integer currentChunkSize;

    /** 总大小 */
    @Column(name = "total_size")
    private Integer totalSize;

    /** 文件标识 */
    @Column(name = "identifier", length = 50)
    private String identifier;

    /** 文件名称 */
    @Column(name = "file_name", length = 50)
    private String fileName;

    /** 相对路径 */
    @Column(name = "relative_path", length = 1000)
    private String relativePath;

    /** 总块数 */
    @Column(name = "total_chunks")
    private Integer totalChunks;

    /** 文件类型 */
    @Column(name = "type", length = 50)
    private String type;

    @Transient
    private MultipartFile file;


}