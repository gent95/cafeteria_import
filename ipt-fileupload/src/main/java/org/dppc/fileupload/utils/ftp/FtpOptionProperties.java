package org.dppc.fileupload.utils.ftp;

import lombok.Data;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @描述 :  ftp连接信息
 * @作者 :	zhumh
 * @日期 :	2019/5/21
 * @时间 :	14:32
 */
@Component
@ConfigurationProperties(prefix = "iot-ftp")
@Data
public class FtpOptionProperties {
    private String host;
    private int port = FTPClient.DEFAULT_PORT;
    private String username;
    private String password;
    private int bufferSize = 8096;
    /**
     * 初始化连接数
     */
    private Integer initialSize = 10;
    private String encoding;
    private String http;

}