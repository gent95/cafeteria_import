package org.dppc.fileupload.service;

import org.dppc.fileupload.entity.FileChunk;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 业务层接口
 * @Author zhumh
 * @Data 2019/05/21 11:45
 * @Version 1.0
 **/
public interface FileChunkService extends BaseService<FileChunk, Integer> {
    /**
     * 验证是否唯一
     * @param identifier
     * @param chunkNumber
     * @return
     */
    boolean checkChunk(String identifier, Integer chunkNumber);

}
