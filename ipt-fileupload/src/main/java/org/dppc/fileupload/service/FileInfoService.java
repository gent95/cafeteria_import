package org.dppc.fileupload.service;

import org.dppc.fileupload.entity.FileInfo;
import org.dppc.dbexpand.service.BaseService;

/**
 * @Description 业务层接口
 * @Author zhumh
 * @Data 2019/05/21 11:45
 * @Version 1.0
 **/
public interface FileInfoService extends BaseService<FileInfo, Integer> {

}
