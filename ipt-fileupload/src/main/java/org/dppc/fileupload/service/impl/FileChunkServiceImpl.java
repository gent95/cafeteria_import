package org.dppc.fileupload.service.impl;

import org.dppc.fileupload.entity.FileChunk;
import org.dppc.fileupload.service.FileChunkService;
import org.dppc.fileupload.repository.FileChunkRepository;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description 业务层实现类
 * @Author zhumh
 * @Data 2019/05/21 11:45
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class FileChunkServiceImpl extends BaseServiceImpl<FileChunk, Integer> implements FileChunkService {

    @Autowired
    private FileChunkRepository fileChunkRepository;

    @Override
    public boolean checkChunk(String identifier, Integer chunkNumber) {
        Specification<FileChunk> specification = (Specification<FileChunk>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("identifier"), identifier));
            predicates.add(criteriaBuilder.equal(root.get("chunkNumber"), chunkNumber));
            return criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()])).getRestriction();
        };

        return fileChunkRepository.findOne(specification) == null;
    }
}
