package org.dppc.fileupload.service.impl;

import org.dppc.fileupload.entity.FileInfo;

import org.dppc.fileupload.entity.FileInfo;
import org.dppc.fileupload.service.FileInfoService;
import org.dppc.fileupload.repository.FileInfoRepository;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description 业务层实现类
 * @Author zhumh
 * @Data 2019/05/21 11:45
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class FileInfoServiceImpl extends BaseServiceImpl<FileInfo, Integer> implements FileInfoService {

    @Autowired
    private FileInfoRepository fileInfoRepository;

}
