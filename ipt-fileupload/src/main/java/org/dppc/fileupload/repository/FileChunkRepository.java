package org.dppc.fileupload.repository;

import org.dppc.fileupload.entity.FileChunk;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 数据库操作层
 * @Author zhumh
 * @Data 2019/05/21 11:45
 * @Version 1.0
 **/
public interface FileChunkRepository extends BaseRepository<FileChunk, Integer> {

}
