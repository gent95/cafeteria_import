package org.dppc.api.gateway.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestVO {

    /** 2018/3/13 10:06 Yang Gang 业务类型 */
    private String BIZ_TYPE;
    /** 2018/3/13 10:06 Yang Gang 请求时间 */
    private String REQ_TIME;
    /** 2018/3/13 10:06 Yang Gang 请求编码 */
    private String REQ_ID;
    /** 2018/3/13 10:06 Yang Gang 认证编码 */
    private String AUTH_ID;
    /** 2018/3/13 10:06 Yang Gang 请求数据参数 */
    private Object PARAM;
}
