package org.dppc.api.gateway.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseVO {
    /** 2018/3/13 10:06 Yang Gang 业务类型 */
    private String BIZ_TYPE;
    /** 2018/3/13 10:06 Yang Gang 响应时间 */
    private String RESP_TIME;
    /** 2018/3/13 10:06 Yang Gang 响应编码 */
    private String RESP_ID;
    /** 2018/3/13 10:06 Yang Gang 请求编码 */
    private String REQ_ID;
    /** 2018/3/13 10:06 Yang Gang 响应状态 */
    private Integer STATUS_CODE;
    /** 2018/3/13 10:06 Yang Gang 响应数据参数 */
    private Object RESULT;

}
