package org.dppc.api.gateway.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestParams {
    private RequestVO requestVO;
    private String token;

    public static RequestParams valueOf(Object object) {
        if (object == null) {
            return null;
        }
        return (RequestParams) object;
    }
}
