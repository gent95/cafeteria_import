package org.dppc.api.cmsHome;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @描述 :   门户 - 企业注册VO
 * @作者 :   GAOJ
 * @日期 :   2018/5/25 0019
 * @时间 :   13:58
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterVO {

    /**  统一社会信用代码 若无统一社会信用代码填写组织机构代码*/
    private String socialCreditCode;

    /**  企业名称*/
    private String compName;

    /**  所属地区*/
    private String cityCode;

    /**  联系电话*/
    private String phone;

    /**  用户名*/
    private String username;

    /**  密码*/
    private String password;

    /**  企业ID  BaseNode ID,BaseNode数据保存后获得*/
    private Long enterpriseId;

}
