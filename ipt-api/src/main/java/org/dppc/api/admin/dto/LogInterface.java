package org.dppc.api.admin.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.api.gateway.vo.RequestVO;

import java.util.*;

/**
 * @描述： 接口日志
 * @作者： 颜齐
 * @日期： 2017/10/25.
 * @时间： 16:23.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LogInterface {

    private Long logInterfaceId;

    private String url;

    private String bizType;

    private String requestMethod;

    private Date requestTime;

    private String requestParams;

    private int uploadCount;

    private int processTime;

    private Date responseTime;

    private String responseBody;

    private String responseStatus;

    private int interfaceType;

    private String host;

    private String username;

    private Date createTime;

    private String reqId;

    private String respId;

    private String errorMsg;


}
