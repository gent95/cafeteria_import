package org.dppc.api.admin.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * @描述 :   数据传输异常记录表
 * @作者 :   GAOJ
 * @日期 :   2018/4/16 0027
 * @时间 :   10:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogDataTransport implements Serializable{

    /**
     * 异常ID
     */
    private Long exceptionId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 异常类型 DataTransportExceptionTypeEnum
     */
    private Integer type;

    /**
     * 异常表 TraceTableEnum
     */
    private String traceTableName;

    /**
     * 异常ids(对应nx_ipt_interface_dev库的接口数据id)
     */
    private String ids;

    /**
     * 异常数据
     */
    private String data;

    /**
     * 报错信息
     */
    private String exceptionInfo;

    /**
     * 错误是否解决(枚举) YesOrNoEnum
     */
    private Integer isSolve;

    public LogDataTransport(Integer type, String traceTableName, String ids, String data, String exceptionInfo, Integer isSolve) {
        this.type = type;
        this.traceTableName = traceTableName;
        this.ids = ids;
        this.data = data;
        this.exceptionInfo = exceptionInfo;
        this.isSolve = isSolve;
        this.createTime = Calendar.getInstance().getTime();
    }
}
