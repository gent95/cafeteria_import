package org.dppc.api.admin.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @描述 :
 * @作者 :	zhumh
 * @日期 :	2019/6/2
 * @时间 :	16:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SchoolCafteria implements Serializable {

    /**
     * 主键id
     */
    private Long schoolCafteriaId;

    /**
     * 学校编码
     */
    private String schoolCode;

    /**
     * 食堂编码
     */
    private String cafeteriaCode;

    /**
     * 食堂名称
     */
    private String cafeteriaName;
    /**
     * 学校名称
     */
    private String schoolName;
    /**
     * 省级编码
     */
    private String provinceCode;
    /**
     * 市级编码
     */
    private String cityCode;
    /**
     * 县级编码
     */
    private String areaCode;
}
