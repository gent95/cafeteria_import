package org.dppc.api.admin.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/7/27.
 * @时间： 11:36.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {


    private Long userId;
    private String username;
    private String password;
    private String email;
    private Integer enable;
    private Integer userType;
    private String name;
    private List<Role> roles;
    private Long departmentId;
    private String phone;
    private String cityCode;
    private Set<SchoolCafteria> schoolCafterias;
}
