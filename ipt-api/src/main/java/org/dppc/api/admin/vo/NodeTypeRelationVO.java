package org.dppc.api.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @描述 :   流通节点类别管理 VO
 * @作者 :   GAOJ
 * @日期 :   2017/12/26 0019
 * @时间 :   13:58
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NodeTypeRelationVO {

    /** GAOJ 2017/12/22 主键ID */
    private Long relationId;

    /** GAOJ 2017/12/22 节点类型编码 */
    private String nodeTypeNo;

    /** GAOJ 2017/12/22 节点类型名称 */
    private String nodeTypeName;

    List<NodeTypeRelationVO> children;

    public NodeTypeRelationVO(String nodeTypeNo, String nodeTypeName) {
        this.nodeTypeNo = nodeTypeNo;
        this.nodeTypeName = nodeTypeName;
    }

}
