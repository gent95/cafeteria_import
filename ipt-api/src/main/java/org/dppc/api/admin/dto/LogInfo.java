package org.dppc.api.admin.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @描述： 日志
 * @作者： 颜齐
 * @日期： 2017/10/25.
 * @时间： 16:23.
 */
@Data
public class LogInfo {
    /**
     * 主键id
     */
    /**
     * 操作
     */
    private String opt;
    /**
     * 信息
     */
    private String msg;
    /**
     * 访问路径
     */
    private String url;
    /**
     * 登录用户
     */
    private String username;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * 访问者ip
     */
    private String host;
    /**
     * 日志类型
     */
    private String type;

    private String method;

    private String module;

    private int processTime;


    public LogInfo(String opt, String msg, String url, String username, Date createTime, String host, String type, String method, String module) {
        this.opt = opt;
        this.msg = msg;
        this.url = url;
        this.username = username;
        this.createTime = createTime;
        this.host = host;
        this.type = type;
        this.method = method;
        this.module = module;
    }


}
