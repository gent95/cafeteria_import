package org.dppc.api.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @描述：
 * @作者： gaoj
 * @日期： 2017/7/27.
 * @时间： 11:41.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResourceVO implements Serializable {

    private Long resourceId;

    private Long parentId;

    private String resourceName;

    private String resourceUrl;

    private String icon;

    private List<ResourceVO> children = new ArrayList<>();

}
