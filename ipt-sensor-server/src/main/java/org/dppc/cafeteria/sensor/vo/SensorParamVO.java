package org.dppc.cafeteria.sensor.vo;

import com.jnrsmcu.sdk.netdevice.ParamItem;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description TODO
 * @Author lhw
 * @Data 2019/6/4 11:12
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
public class SensorParamVO {
    List<ParamItem> parameters = new ArrayList<>();
}
