package org.dppc.cafeteria.sensor.service.impl;

import org.dppc.cafeteria.sensor.entity.SensorData;
import org.dppc.cafeteria.sensor.repository.SensorDataRepository;
import org.dppc.cafeteria.sensor.service.SensorDataService;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @Description 传感器数据业务层实现类
 * @Author lhw
 * @Data 2019/06/03 17:48
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class SensorDataServiceImpl extends BaseServiceImpl<SensorData, Long> implements SensorDataService {

    @Autowired
    private SensorDataRepository sensorDataRepository;

    @Override
    public void saveSensorDataList(List<SensorData> sensorDataList) {
        sensorDataRepository.save(sensorDataList);
    }
}
