package org.dppc.cafeteria.sensor.repository;

import org.dppc.cafeteria.sensor.entity.SensorData;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @Description 传感器数据数据库操作层
 * @Author lhw
 * @Data 2019/06/03 17:48
 * @Version 1.0
 **/
public interface SensorDataRepository extends BaseRepository<SensorData, Long> {

}
