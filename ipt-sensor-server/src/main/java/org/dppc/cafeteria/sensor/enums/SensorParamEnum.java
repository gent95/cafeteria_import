package org.dppc.cafeteria.sensor.enums;

/**
 * @Description 传感器参数枚举
 * @Author lhw
 * @Data 2019/6/2 19:40
 * @Version 1.0
 **/
public enum SensorParamEnum {
    SERVER_URL(2, "网口服务器1URL地址"),
    SERVER_PORT(3, "网口服务器1源端口"),
    HEARTBEAT_INTERVAL(30, "网口登陆帧间隔（秒）"),
    IP_GAIN_TYPE(16, "网口IP获取方式"),
    LOGIN_INTERVAL(31, "网口心跳帧间隔（秒）"),
    DATA_INTERVAL(32, "网口数据帧间隔（秒）"),
    LNG(33, "标识设备坐标经度其中-180-0代表西经"),
    LAT(34, "标识设备坐标维度其中-90-0代表南维");

    private int pararmId;
    private String desc;

    SensorParamEnum(int pararmId, String desc) {
        this.pararmId = pararmId;
        this.desc = desc;
    }

    public int getPararmId() {
        return pararmId;
    }

    public void setPararmId(int pararmId) {
        this.pararmId = pararmId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
