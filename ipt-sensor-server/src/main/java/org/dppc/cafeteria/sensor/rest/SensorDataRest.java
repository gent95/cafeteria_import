package org.dppc.cafeteria.sensor.rest;

import org.dppc.cafeteria.sensor.entity.SensorData;
import org.dppc.cafeteria.sensor.service.SensorDataService;
import lombok.extern.java.Log;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.logging.Level;

/**
 * @Description 传感器数据
 * @Author lhw
 * @Data 2019/06/03 17:48
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/sensorData")
public class SensorDataRest {

    @Autowired
    private SensorDataService sensorDataService;

    /**
     * @Author lhw
     * @Description 根据条件分页查询传感器数据
     * @Date 2019/06/03 17:48
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, SensorData sensorData) {
        /** 根据条件分页查询集合 **/
        Page<SensorData> page = sensorDataService.findPageList(sensorData,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author lhw
     * @Description 根据id查询传感器数据
     * @Date 2019/06/03 17:48
     **/
    @GetMapping("/{id}")
    public BaseResponse findSensorData(@PathVariable Long id) {
        return ResponseHelper.success(sensorDataService.getOneDataById(id));
    }

    /**
     * @Author lhw
     * @Description 添加传感器数据
     * @Date 2019/06/03 17:48
     **/
    @PostMapping
    public BaseResponse saveSensorData(@Valid @RequestBody SensorData sensorData, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            sensorDataService.save(sensorData);
            return ResponseHelper.success(sensorData);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author lhw
     * @Description 局部更新传感器数据
     * @Date 2019/06/03 17:48
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchSensorData(@PathVariable Long id, @RequestBody SensorData sensorData) {
        try {
            boolean result = sensorDataService.patch(sensorData, id);
            if (result) {
                return ResponseHelper.success(sensorData);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author lhw
     * @Description 根据id删除传感器数据
     * @Date 2019/06/03 17:48
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteSensorData(@PathVariable Long id) {
        try {
            sensorDataService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
