package org.dppc.cafeteria.sensor.component;

import com.jnrsmcu.sdk.netdevice.IDataListener;
import com.jnrsmcu.sdk.netdevice.ParamItem;
import com.jnrsmcu.sdk.netdevice.RSServer;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description 传感器服务控制器类
 * @Author lhw
 * @Data 2019/6/3 14:20
 * @Version 1.0
 **/
@Log
@Component
public class RSServerUtil {
    @Value("${sensor.server_port}")
    private String serverPort;
    /** 启动状态 */
    private Boolean startStatus = false;

    public RSServer rsServer;

    @Autowired
    private IDataListener iDataListener;
    /**
     * @Author lhw
     * @Description 初始化服务
     * @Date 14:28 2019/6/3
     **/
    public void init(){
        if (rsServer == null) {
            rsServer = RSServer.Initiate(Integer.parseInt(serverPort));
            rsServer.addDataListener(iDataListener);
        }
        log.info("[传感器服务端]初始化成功！！！");
    }

    /**
     * @Author lhw
     * @Description 启动服务
     * @Date 14:28 2019/6/3
     **/
    public void start() throws InterruptedException {
        if (rsServer == null) {
            init();
        }
        if (!startStatus) {
            startStatus = true;
            log.info("[传感器服务端]启动成功！！！  服务端口：" + serverPort);
            rsServer.start();
            log.info("[传感器服务端]关闭成功！！！" );
        }
    }

    /**
     * @Author lhw
     * @Description 关闭服务
     * @Date 14:27 2019/6/3
     **/
    public void stop() {
        if (rsServer != null) {
            log.info("关闭[传感器服务端]服务！！！");
            rsServer.stop();
        }
    }

    public void callParamList() throws Exception {
        rsServer.callParamList(30012261);
//        //设置参数
//        List<ParamItem> parameters = new ArrayList<>();
//        parameters.add(ParamItem.New(32, "120"));
//        rsServer.writeParam(30012261, parameters);
    }


    public void writeParam(Integer deviceId, List<ParamItem> parameters) {
        // TODO 需要进行测试，有异常
        rsServer.writeParam(deviceId, parameters);
    }
}
