package org.dppc.cafeteria.sensor.component;

import com.jnrsmcu.sdk.netdevice.*;
import org.dppc.cafeteria.sensor.entity.SensorData;
import org.dppc.cafeteria.sensor.service.SensorDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description 传感器服务端与客户端通讯监听
 * @Author lhw
 * @Data 2019/6/2 19:40
 * @Version 1.0
 **/
@Component
public class IDataListenerImpl implements IDataListener {
    @Autowired
    SensorDataService sensorDataService;
    /**
     * @Author lhw
     * @Description 校时指令应答处理
     * @Date 16:28 2019/6/3
     **/
    @Override
    public void receiveTimmingAck(TimmingAck data) {
        System.out.println("校时应答->设备编号:" + data.getDeviceId()
                + "\t执行结果：" + data.getStatus());
    }

    /**
     * @Author lhw
     * @Description 遥控指令应答处理
     * @Date 16:25 2019/6/3
     **/
    @Override
    public void receiveTelecontrolAck(TelecontrolAck data) {
        System.out.println("遥控应答->设备编号:" + data.getDeviceId()
                + "\t继电器编号:" + data.getRelayId() + "\t执行结果:"
                + data.getStatus());
    }

    /**
     * @Author lhw
     * @Description 已存储数据接收处理
     * @Date 16:25 2019/6/3
     **/
    @Override
    public void receiveStoreData(StoreData data) {

        // 遍历节点数据。数据包括网络设备的数据以及各个节点数据。温湿度数据存放在节点数据中
        for (NodeData nd : data.getNodeList()) {
            SimpleDateFormat sdf = new SimpleDateFormat(
                    "yy-MM-dd HH:mm:ss");
            String str = sdf.format(nd.getRecordTime());
            System.out.println("存储数据->设备地址:" + data.getDeviceId()
                    + "\t节点:" + nd.getNodeId() + "\t温度:" + nd.getTem()
                    + "\t湿度:" + nd.getHum() + "\t存储时间:" + str);
        }

    }

    /**
     * @Author lhw
     * @Description 实时数据接收处理
     * @Date 16:26 2019/6/3
     **/
    @Override
    public void receiveRealtimeData(RealTimeData data) {
        List<SensorData> sensorDataList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat(
                "yy-MM-dd HH:mm:ss");
        // 遍历节点数据。数据包括网络设备的数据以及各个节点数据。温湿度数据存放在节点数据中
        for (NodeData nd : data.getNodeList()) {
            if (nd.getHum() > 0) {
                SensorData sensorData = new SensorData();
                sensorData.setDeviceId(data.getDeviceId());
                sensorData.setNodeId(nd.getNodeId());
                sensorData.setTem(nd.getTem());
                sensorData.setHum(nd.getHum());
                sensorData.setLng(data.getLng());
                sensorData.setLat(data.getLat());
                sensorData.setCoordinateType(data.getCoordinateType());
                sensorData.setRecordTime(new Date());
                sensorDataList.add(sensorData);
            }
           /* System.out.println("实时数据->设备地址:" + data.getDeviceId()
                    + "\t记录时间:" + sdf.format(nd.getRecordTime())
                    + "\t节点:" + nd.getNodeId() + "\t温度:" + nd.getTem()
                    + "\t湿度:" + nd.getHum() + "\t经度:" + data.getLng()
                    + "\t纬度:" + data.getLat() + "\t坐标类型:"
                    + data.getCoordinateType() + "\t继电器状态:"
                    + data.getRelayStatus());*/
        }
        sensorDataService.saveSensorDataList(sensorDataList);

    }

    /**
     * @Author lhw
     * @Description 设备登录处理
     * @Date 16:26 2019/6/3
     **/
    @Override
    public void receiveLoginData(LoginData data) {
        System.out.println("登录->设备地址:" + data.getDeviceId());

    }

    /**
     * @Author lhw
     * @Description 设备参数编号接收处理
     * @Date 16:26 2019/6/3
     **/
    @Override
    public void receiveParamIds(ParamIdsData data) {
        String str = "设备参数编号列表->设备编号：" + data.getDeviceId()
                + "\t参数总数量：" + data.getTotalCount() + "\t本帧参数数量："
                + data.getCount() + "\r\n";
        // 遍历设备中参数id编号
        for (int paramId : data.getPararmIdList()) {
            str += paramId + ",";
        }
        System.out.println(str);

    }

    /**
     * @Author lhw
     * @Description 设备参数接收处理
     * @Date 16:27 2019/6/3
     **/
    @Override
    public void receiveParam(ParamData data) {
        String str = "设备参数->设备编号：" + data.getDeviceId() + "\r\n";

        for (ParamItem pararm : data.getParameterList()) {
            str += "参数编号："
                    + pararm.getParamId()
                    + "\t参数描述："
                    + pararm.getDescription()
                    + "\t参数值："
                    + (pararm.getValueDescription() == null ? pararm
                    .getValue() : pararm.getValueDescription()
                    .get(pararm.getValue())) + "\r\n";
        }
        System.out.println(str);

    }

    /**
     * @Author lhw
     * @Description 下载(设置)参数后设备应答处理
     * @Date 16:27 2019/6/3
     **/
    @Override
    public void receiveWriteParamAck(WriteParamAck data) {
        String str = "设置设备参数->设备编号：" + data.getDeviceId() + "\t参数数量："
                + data.getCount() + "\t"
                + (data.isSuccess() ? "设置成功" : "设置失败");
        System.out.println(str);

    }

    /**
     * @Author lhw
     * @Description 透传数据后设备应答处理
     * @Date 16:27 2019/6/3
     **/
    @Override
    public void receiveTransDataAck(TransDataAck data) {
        String str = "数据透传->设备编号：" + data.getDeviceId() + "\t响应结果："
                + data.getData() + "\r\n字节数：" + data.getTransDataLen();
        System.out.println(str);
    }
}
