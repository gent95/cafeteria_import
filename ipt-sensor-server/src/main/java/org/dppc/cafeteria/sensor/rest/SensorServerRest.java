package org.dppc.cafeteria.sensor.rest;

import lombok.extern.java.Log;
import org.dppc.cafeteria.sensor.component.RSServerUtil;
import org.dppc.cafeteria.sensor.vo.SensorParamVO;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description 客户端服务控制器类
 * @Author lhw
 * @Data 2019/6/3 13:53
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/sensorServer")
public class SensorServerRest {

    @Autowired
    RSServerUtil rsServerUtil;

    @GetMapping("/start")
    public BaseResponse start() throws InterruptedException {
        rsServerUtil.start();
        return ResponseHelper.success("[传感器服务端]启动成功！！！");
    }

    @GetMapping("/stop")
    public BaseResponse stop() {
        rsServerUtil.stop();
        return ResponseHelper.success("[传感器服务端]关闭成功！！！");
    }
    /**
     * @Author lhw
     * @Description 设置传感器参数
     * @Date 11:23 2019/6/4
     **/
    @PatchMapping("/writeParam/{deviceId}")
    public BaseResponse stop(@PathVariable Integer deviceId, @RequestBody SensorParamVO sensorParamVO) {
        rsServerUtil.writeParam(deviceId,sensorParamVO.getParameters());
        return ResponseHelper.success("设置传感器参数！！！");
    }

    @GetMapping("/callParamList")
    public BaseResponse callParamList() throws Exception {
        rsServerUtil.callParamList();
        System.out.printf("获取参数列表成功");
        return ResponseHelper.success("获取参数列表成功！！！");
    }
}
