package org.dppc.cafeteria.sensor.service;

import org.dppc.cafeteria.sensor.entity.SensorData;
import org.dppc.dbexpand.service.BaseService;

import java.util.List;

/**
 * @Description 传感器数据业务层接口
 * @Author lhw
 * @Data 2019/06/03 17:48
 * @Version 1.0
 **/
public interface SensorDataService extends BaseService<SensorData, Long> {
    /**
     * @Author lhw
     * @Description 保存传感器数据集合
     * @Date 18:11 2019/6/3
     **/
    void saveSensorDataList(List<SensorData> sensorDataList);
}
