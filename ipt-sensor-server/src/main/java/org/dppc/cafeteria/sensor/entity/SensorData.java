package org.dppc.cafeteria.sensor.entity;

import java.io.Serializable;
import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

/**
 * @Description 传感器数据
 * @Author lhw
 * @Data 2019/06/03 17:48
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_sensor_data")
public class SensorData implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "data_id")
    private Long dataId;

    /** 设备地址 */
    @Column(name = "device_id")
    private Integer deviceId;

    /** 节点 */
    @Column(name = "node_id")
    private Integer nodeId;

    /** 温度 */
    @Column(name = "tem")
    private Float tem;

    /** 湿度 */
    @Column(name = "hum")
    private Float hum;

    /** 经度 */
    @Column(name = "lng")
    private Float lng;

    /** 纬度 */
    @Column(name = "lat")
    private Float lat;

    /** 坐标类型 */
    @Column(name = "coordinate_type")
    private Short coordinateType;

    /** 记录时间 */
    @Column(name = "record_time")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_PATTERN,timezone = "GMT+8")
    private Date recordTime;

    /** 是否为报警数据 */
    @Column(name = "is_alarm_data")
    private Integer isAlarmData;


}