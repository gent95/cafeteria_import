package org.dppc.cafeteria;

import org.dppc.auth.client.EnableIptAuthClient;
import org.dppc.cafeteria.sensor.component.RSServerUtil;
import org.dppc.dbexpand.repository.BaseRepositoryFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableJpaRepositories(basePackages = {"org.dppc"}, repositoryFactoryBeanClass = BaseRepositoryFactoryBean.class)
@ComponentScan(basePackages = {"org.dppc.cafeteria", "org.dppc.dbexpand"})
@EnableTransactionManagement
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients({"org.dppc.auth.client.feign"})
@EnableIptAuthClient
@EnableHystrix
public class SensorServerApplication {

    public static void main(String[] args) throws InterruptedException {
        ApplicationContext applicationContext = SpringApplication.run(SensorServerApplication.class, args);
        System.out.println("项目初始完毕 >>>");
        System.out.println("启动[传感器服务端]");
        RSServerUtil serverUtil = applicationContext.getBean(RSServerUtil.class);
        serverUtil.start();
    }

}
