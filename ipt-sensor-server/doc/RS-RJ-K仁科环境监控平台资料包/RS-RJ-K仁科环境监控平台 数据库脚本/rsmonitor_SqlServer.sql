USE [master]
GO
/****** Object:  Database [rsmonitor]    Script Date: 05/15/2017 14:06:22 ******/
CREATE DATABASE [rsmonitor] 
GO
USE [rsmonitor]
GO
/****** Object:  Table [dbo].[tbsyslog]    Script Date: 05/15/2017 14:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbsyslog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[Operate] [nvarchar](max) NULL,
	[Details] [nvarchar](max) NULL,
	[Result] [nvarchar](max) NULL,
	[IP] [nvarchar](max) NULL,
	[RecordTime] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbhistory]    Script Date: 05/15/2017 14:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbhistory](
	[ID] [nvarchar](32) PRIMARY KEY,
	[DeviceKey] [nvarchar](32) NULL,
	[DeviceName] [nvarchar](500) NULL,
	[DeviceID] [int] NULL,
	[NodeID] [int] NULL,
	[Tem] [float] NULL,
	[Hum] [float] NULL,
	[Lng] [float] NULL,
	[Lat] [float] NULL,
	[CoordinateType] [smallint] NULL,
	[RecordTime] [datetime] NULL,
	[IsAlarmData] [int] Default (0)
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbalarm]    Script Date: 05/15/2017 14:06:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbalarm](
	[ID] [nvarchar](32) NULL,
	[DeviceKey] [nvarchar](32) NULL,
	[DeviceName] [nvarchar](max) NULL,
	[DeviceID] [int] NULL,
	[NodeID] [int] NULL,
	[Lng] [float] NULL,
	[Lat] [float] NULL,
	[AlarmType] [nvarchar](150) NULL,
	[AlarmMessage] [nvarchar](150) NULL,
	[AlarmRange] [nvarchar](150) NULL,
	[DataValue] [float] NULL,
	[RecordTime] [datetime] NULL,
	[HandingFlag] [int] NULL,
	[HandlingMethod] [nvarchar](max) NULL,
	[HandlingUser] [nvarchar](500) NULL,
	[HandlingTime] [datetime] NULL
) ON [PRIMARY]
GO
