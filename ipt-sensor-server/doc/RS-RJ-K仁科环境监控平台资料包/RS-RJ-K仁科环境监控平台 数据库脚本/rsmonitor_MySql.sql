/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50533
Source Host           : localhost:3306
Source Database       : rsmonitor

Target Server Type    : MYSQL
Target Server Version : 50533
File Encoding         : 65001

Date: 2017-05-15 14:07:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbalarm
-- ----------------------------
DROP TABLE IF EXISTS `tbalarm`;
CREATE TABLE `tbalarm` (
  `ID` varchar(32) NOT NULL,
  `DeviceKey` varchar(32) DEFAULT NULL,
  `DeviceName` varchar(255) DEFAULT NULL,
  `DeviceID` int(11) DEFAULT NULL,
  `NodeID` int(11) DEFAULT NULL,
  `Lng` double(16,6) DEFAULT NULL,
  `Lat` double(16,6) DEFAULT NULL,
  `AlarmType` varchar(50) DEFAULT NULL,
  `AlarmMessage` varchar(500) DEFAULT NULL,
  `AlarmRange` varchar(500) DEFAULT NULL,
  `DataValue` double DEFAULT NULL,
  `RecordTime` datetime DEFAULT NULL,
  `HandingFlag` tinyint(1) DEFAULT '0',
  `HandlingMethod` varchar(1200) DEFAULT NULL,
  `HandlingUser` varchar(255) DEFAULT NULL,
  `HandlingTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbhistory
-- ----------------------------
DROP TABLE IF EXISTS `tbhistory`;
CREATE TABLE `tbhistory` (
  `ID` varchar(32) NOT NULL,
  `DeviceKey` varchar(32) DEFAULT NULL,
  `DeviceName` varchar(500) DEFAULT NULL,
  `DeviceID` int(11) DEFAULT NULL,
  `NodeID` int(11) DEFAULT NULL,
  `Tem` double(16,4) DEFAULT NULL,
  `Hum` double(16,4) DEFAULT NULL,
  `Lng` double(16,6) DEFAULT NULL,
  `Lat` double(16,6) DEFAULT NULL,
  `CoordinateType` int(255) DEFAULT NULL,
  `RecordTime` datetime DEFAULT NULL,
  `IsAlarmData` int(2) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbsyslog
-- ----------------------------
DROP TABLE IF EXISTS `tbsyslog`;
CREATE TABLE `tbsyslog` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(255) DEFAULT NULL,
  `Operate` varchar(255) DEFAULT NULL,
  `Details` varchar(500) DEFAULT NULL,
  `Result` varchar(255) DEFAULT NULL,
  `IP` varchar(255) DEFAULT NULL,
  `RecordTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
