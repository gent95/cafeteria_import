package org.dppc.admin.repository;

import org.dppc.admin.entity.Resource;
import org.dppc.common.enums.MethodEnum;
import org.dppc.dbexpand.util.BeanHelp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;
import java.util.List;
import java.util.Random;

/**
 * @描述 :
 * @作者 :  Zhao Yun
 * @日期 :  2017/10/30
 * @时间 :  13:55
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest
public class ResourceRepositoryTest {

    @Autowired
    private ResourceRepository resourceRepository;

    @Test
    public void testName() throws Exception {
        Resource query = new Resource();
//        query.setMethod(MethodEnum.GET);
//        query.setEnable(1);
        List<Resource> all = resourceRepository.findAll((root, criteriaQuery, criteriaBuilder) -> {
            return BeanHelp.getPredicate(root, query, criteriaBuilder);
        });
        System.out.println(all);
    }

    private LongRange[] longRanges = new LongRange[4];
    private Integer defaultNode = -1;

    public Integer calculate(String columnValue) {
//		columnValue = NumberParseUtil.eliminateQoute(columnValue);
        try {
            long value = Long.parseLong(columnValue);
            Integer rst = null;
            int nodeIndex = 0;
            for (LongRange longRang : this.longRanges) {
                if (value <= longRang.valueEnd && value >= longRang.valueStart) {
                    BigInteger bigNum = new BigInteger(columnValue).abs();
                    int innerIndex = (bigNum.mod(BigInteger.valueOf(longRang.groupSize))).intValue();
                    return nodeIndex + innerIndex;
                } else {
                    nodeIndex += longRang.groupSize;
                }
            }
            //数据超过范围，暂时使用配置的默认节点
            if (rst == null && defaultNode >= 0) {
                return defaultNode;
            }
            return rst;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(new StringBuilder().append("columnValue:").append(columnValue).append(" Please eliminate any quote and non number within it.").toString(), e);
        }
    }

    static class LongRange {
        public final int groupSize;
        public final long valueStart;
        public final long valueEnd;

        public LongRange(int groupSize, long valueStart, long valueEnd) {
            super();
            this.groupSize = groupSize;
            this.valueStart = valueStart;
            this.valueEnd = valueEnd;
        }

    }

    @Test
    public void test1() {
        longRanges[0] = new LongRange(1, 0, 5);
        longRanges[1] = new LongRange(2, 6, 10);
        longRanges[2] = new LongRange(3, 11, 15);
        longRanges[3] = new LongRange(4, 16, 20);
        Integer result = calculate("2");
        Integer result1 = calculate("7");
        Integer result2 = calculate("13");
        System.out.println(result);
        System.out.println(result1);
        System.out.println(result2);
    }

    @Test
    public void test2() {
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            int j = random.nextInt(1000);
            String orderNum = String.format("%08d", j);
            System.out.println(orderNum);
        }


    }

}
