package org.dppc.admin.repository;

import org.dppc.common.constant.UserConstant;
import org.dppc.common.enums.SpecialGoodsEnum;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.math.BigDecimal;

/**
 * Created by yanqi on 2018/4/26.
 */
public class CommenTest {

    public static void generatePassword(String password){
        String passwordEncoder = new BCryptPasswordEncoder(UserConstant.PW_ENCORDER_SALT).encode(password);
        System.out.println(passwordEncoder);
    }

    public static void main(String[] args) {
        //generatePassword("zmhfnmy09230U");

       // System.out.println(formatDouble (6.56999999));
        for (String s: SpecialGoodsEnum.valueof(3).getIndexCode()) {
            System.out.println(s);
        }

    }


    public static Double formatDouble(Double s) {
        BigDecimal b = new BigDecimal(s);
        return b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }
}
