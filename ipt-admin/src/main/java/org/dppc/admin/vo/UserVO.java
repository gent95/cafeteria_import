package org.dppc.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.dppc.admin.entity.Role;
import org.dppc.admin.entity.User;
import org.dppc.common.enums.EnableEnum;
import org.dppc.common.enums.UserTypeEnum;
import org.dppc.common.utils.NullHelper;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/11/30
 * @时间 ：14:39
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserVO extends User {

    /** wyl 2017/11/30 13:52 部门名称 */
    private String departmentName;
    /** wyl 2017/12/1 9:11 禁用、启用显示 */
    private String enableName;
    /** wyl 2017/12/26 14:07 用户类型显示 */
    private String userTypeName;
    /** wyl 2017/12/14 15:16 角色Id列表 */
    private List<Long> roleIdList = new ArrayList<>();
    /** lhw 2018/6/26 17:59  公司编号列表（统一信用代码）  */
//    private List<String> enterpriseIdList = new ArrayList<>();

    /**
     * @描述 :  BeanUtils.copyProperties   只进行浅拷贝，对象和集合只会拷贝其引用；Hibernate中规定禁止两个对象共同引用同一个集合对象
     * @作者 :	lhw
     * @日期 :	2019/2/25
     * @时间 :	17:00
     */
    public static UserVO entityToVO(User user) {
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(user, userVO);
        userVO.setEnableName(NullHelper.isNull(userVO.getEnable()) ? "" : EnableEnum.valueOf(userVO.getEnable()).getName());
        userVO.setUserTypeName(NullHelper.isNull(userVO.getUserType()) ? "" : UserTypeEnum.valueOf(user.getUserType()).getName());
        List<Role> roles = new ArrayList<>();
        user.getRoles().forEach(role -> roles.add(role));
        userVO.setRoles(roles);
        return userVO;
    }
}
