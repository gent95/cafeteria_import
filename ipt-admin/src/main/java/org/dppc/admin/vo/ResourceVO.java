package org.dppc.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.admin.entity.Resource;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/10/26
 * @时间 ：17:26
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResourceVO extends Resource {

    // 子菜单资源
    private List<ResourceVO> childResourceVOList = new ArrayList<>();

    public static ResourceVO entityToVO(Resource resource) {
        ResourceVO resourceVO = new ResourceVO();
        BeanUtils.copyProperties(resource, resourceVO);
        return resourceVO;
    }
}
