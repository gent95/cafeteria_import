package org.dppc.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.dppc.admin.entity.Role;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/11/30
 * @时间 ：16:48
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleVO extends Role{
    /** wyl 2017/12/13 11:06 角色用有用户名显示 逗号分隔 */
    private String userNames;
    /** wyl 2017/12/13 11:06 角色拥有用户姓名显示 逗号分隔 */
    private String userRealNames;
    /** wyl 2017/12/14 10:52 角色拥有资源显示 逗号分隔 */
    private String resourceNames;
    /** wyl 2017/12/19 13:27 选中的所有子资源集合 不包括父级 */
    private List<Long> selectChildList = new ArrayList<>();

    public static RoleVO entityToVO(Role role) {
        RoleVO roleVO = new RoleVO();
        BeanUtils.copyProperties(role, roleVO);
        return roleVO;
    }
}
