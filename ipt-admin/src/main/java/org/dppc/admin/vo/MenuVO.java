package org.dppc.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：菜单
 * @作者 ：wyl
 * @日期 ：2017/11/10
 * @时间 ：9:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuVO implements Serializable {
    /**
     * 资源id
     */
    private Long resourcesId = 0L;
    /**
     * 父资源id
     */
    private Long parentId = 0L;
    /**
     * 图标
     */
    private String icon = "";
    /**
     * 菜单名称
     */
    private String menuName = "";
    /**
     * 菜单跳转页面地址
     */
    private String menuUrl = "";
    /**
     * 子菜单列表
     */
    private List<MenuVO> childrens = new ArrayList<>();
}
