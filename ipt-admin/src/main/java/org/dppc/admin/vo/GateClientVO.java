package org.dppc.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.dppc.admin.entity.GateClient;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/12/27
 * @时间 ：9:40
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GateClientVO extends GateClient {
    /** wyl 2017/12/27 9:48 服务端列表显示 */
    private String clientServiceNames;
    /** wyl 2017/12/28 9:27 客户端列表显示 */
    private String clientNames;
    /** wyl 2017/12/27 9:50 服务端列表 */
    private List<GateClient> clientServiceList = new ArrayList<>();
    /** wyl 2017/12/27 9:51 服务端ID列表 */
    private List<Long> clientServiceIdList = new ArrayList<>();
    /** wyl 2017/12/27 9:50 客户端列表 */
    private List<GateClient> clientList = new ArrayList<>();
    /** wyl 2017/12/27 9:51 客户端ID列表 */
    private List<Long> clientIdList = new ArrayList<>();

    public static GateClientVO entityToVO(GateClient gateClient) {
        GateClientVO gateClientVO = new GateClientVO();
        BeanUtils.copyProperties(gateClient, gateClientVO);
        return gateClientVO;
    }
}