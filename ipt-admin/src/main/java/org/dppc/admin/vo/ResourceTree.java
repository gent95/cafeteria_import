package org.dppc.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.admin.entity.Resource;
import org.dppc.common.utils.NullHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：权限资源树
 * @作者 ：wyl
 * @日期 ：2017/11/30
 * @时间 ：16:48
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResourceTree {
    /** 节点id */
    private Long id = 0L;
    /** 名称 */
    private String label = "";
    /** 叶子节点 */
    private Boolean leaf = true;
    /** 子节点集合 */
    private List<ResourceTree> children = new ArrayList<>();

    public static ResourceTree resourceToMenuTree (Resource resource) {
        ResourceTree resourceTree = new ResourceTree();
        resourceTree.setId(resource.getResourceId() == null ? 0L : resource.getResourceId());
        resourceTree.setLabel(NullHelper.isNullStr(resource.getResourceName()) ? "" : resource.getResourceName());
        return resourceTree;
    }
}
