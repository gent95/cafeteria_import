package org.dppc.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.dppc.admin.entity.LogInfo;
import org.dppc.common.utils.DateHelper;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/11/30
 * @时间 ：16:48
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogInfoVO extends LogInfo {
    @PredicateInfo(queryType = PredicateInfo.QueryType.GREATER_THAN, propName = "createTime")
    @DateTimeFormat(pattern = DateHelper.DATATIMEF_STR)
    @JsonFormat(pattern = DateHelper.DATATIMEF_STR, timezone = "GMT+8")
    private Date createTimeMin;
    @DateTimeFormat(pattern = DateHelper.DATATIMEF_STR)
    @JsonFormat(pattern = DateHelper.DATATIMEF_STR, timezone = "GMT+8")
    @PredicateInfo(queryType = PredicateInfo.QueryType.LESS_THAN, propName = "createTime")
    private Date createTimeMax;
}
