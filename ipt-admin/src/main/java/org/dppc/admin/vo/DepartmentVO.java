package org.dppc.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.admin.entity.Department;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentVO implements Serializable {

    private Long departmentId;

    private Long parentDepartmentId;

    private String departmentName;

    private Integer enable;

    private Integer departmentLevel;

    private String state;

    private List<Department> departments;
}
