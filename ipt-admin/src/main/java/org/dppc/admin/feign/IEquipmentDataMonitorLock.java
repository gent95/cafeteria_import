package org.dppc.admin.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/12/28
 * @时间 ：11:34
 */
@FeignClient("IPT-TRACE-SUPERVISE")
@RequestMapping(value = "/api/equipmentDataMonitorLock")
public interface IEquipmentDataMonitorLock {

    /**
     * @描述 ：
     * @作者 ：wyl
     * @日期 ：2017/12/28
     * @时间 ：11:35
     */
    @GetMapping(value = "/signalTask")
    void signalTask();

}
