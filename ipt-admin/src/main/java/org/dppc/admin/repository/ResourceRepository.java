package org.dppc.admin.repository;

import org.dppc.admin.entity.Resource;
import org.dppc.common.enums.ResourceTypeEnum;
import org.dppc.common.enums.YesOrNoEnum;
import org.dppc.dbexpand.repository.BaseRepository;
import org.springframework.data.domain.Sort;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/17.
 * @时间： 18:04.
 */
public interface ResourceRepository extends BaseRepository<Resource,Long> {

    /**
     * 菜单管理
     * 查找所有可用的权限，
     * @param
     * @return
     */
    List<Resource> findByFilterAndPermissionIsNotNull(YesOrNoEnum filter);


    /**
     * @描述 ：查询父id下子资源列表
     * @作者 ：wyl
     * @日期 ：2017/10/31 
     * @时间 ：11:18
     */
    List<Resource> findByParentId(Long parentId, Sort sort);

    /**
     * @描述 ：根据父资源获取子资源集合
     * @作者 ：wyl
     * @日期 ：2017/10/31 
     * @时间 ：13:59
     */
    List<Resource> findByParentIdAndEnable(Long resourceId, Integer enable);

    /**
     * @描述 ：根据资源父id、资源类型（MENU BUTTON）、禁用启用  用户权限下资源id集合 查询资源列表
     * @作者 ：wyl
     * @日期 ：2017/11/10
     * @时间 ：10:59
     */
    List<Resource> findByPlatformTypeAndParentIdAndTypeAndEnableAndAndFilterAndResourceIdIn(Integer platformType,Long parentId, ResourceTypeEnum type, Integer enable,YesOrNoEnum yesOrNoEnum, Set<Long> resourceIdSet, Sort sequence);

    /**
     * @描述 ：根据PlatformType、资源类型（MENU BUTTON）、禁用启用  用户权限下资源id集合 查询资源列表
     * @作者 ：gaoj
     * @日期 ：2017/11/10
     * @时间 ：10:59
     */
    List<Resource> findByPlatformTypeAndTypeAndEnableAndResourceIdIn(Integer platformType,ResourceTypeEnum type, Integer enable, Set<Long> resourceIdSet, Sort sequence);

    /**
     * @描述 ：根据父资源获取子资源集合
     * @作者 ：wyl
     * @日期 ：2017/11/10 
     * @时间 ：14:42
     */
    List<Resource> findByParentIdAndEnable(Long parentId, Integer enable, Sort sequence);

    /**
     * @描述 ：根据资源id集合查询资源集合
     * @作者 ：wyl
     * @日期 ：2017/11/13
     * @时间 ：9:53
     */
    List<Resource> findByResourceIdIn(Collection<Long> resourcesIds);

    /**
     * @描述 ：根据父级资源id及资源类型查询 排序
     * @作者 ：wyl
     * @日期 ：2017/11/13 
     * @时间 ：14:41
     */
    List<Resource> findByParentIdAndType(Long parentId, ResourceTypeEnum menu, Sort sort);

    /**
     * @描述 ：
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：15:29
     */
    List<Resource> findByParentIdAndEnableOrderBySequenceAsc(Long parentId, Integer enable);
}
