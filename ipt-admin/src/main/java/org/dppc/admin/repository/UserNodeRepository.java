package org.dppc.admin.repository;

import org.dppc.admin.entity.UserNode;
import org.dppc.dbexpand.repository.BaseRepository;

import java.util.List;

/**
 * @描述 :
 * @作者 :	lhw
 * @日期 :	2018/6/26
 * @时间 :	15:42
 */
public interface UserNodeRepository extends BaseRepository<UserNode,Long> {

    List<UserNode> findByUsername(String username);

    List<UserNode> findBySocialCreditCode(String socialCreditCode);
}
