package org.dppc.admin.repository;


import org.dppc.admin.entity.SchoolCafteria;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @描述 :
 * @作者 :	zhumh
 * @日期 :	2019/6/6
 * @时间 :	13:47
 */
public interface SchoolCafteriaRepository extends BaseRepository<SchoolCafteria,Long> {
}
