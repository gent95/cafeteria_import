package org.dppc.admin.repository;

import org.dppc.admin.entity.DataRole;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @描述 :
 * @作者 :	lyf
 * @日期 :	2018/5/9
 * @时间 :	16:40
 */
public interface DataRoleRepository extends BaseRepository<DataRole,Long>{
}
