package org.dppc.admin.repository;

import org.dppc.admin.entity.Role;
import org.dppc.dbexpand.repository.BaseRepository;

import java.util.Collection;
import java.util.List;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/17.
 * @时间： 18:04.
 */
public interface RoleRepository extends BaseRepository<Role, Long> {

    /**
     * @描述 ：根据角色id集合查询角色集合
     * @作者 ：wyl
     * @日期 ：2017/11/10
     * @时间 ：14:09
     */
    List<Role> findByRoleIdIn(Collection<Long> collection);


    List<Role> findByState(Integer state);

}
