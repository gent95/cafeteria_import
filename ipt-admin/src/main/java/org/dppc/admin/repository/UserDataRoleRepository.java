package org.dppc.admin.repository;

import org.dppc.admin.entity.UserDataRole;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @描述 : 用户数据角色表
 * @作者 : Lu Xinwei
 * @日期 : 2018/04/02
 * @时间 : 10:00
 */
public interface UserDataRoleRepository extends BaseRepository<UserDataRole,Long>  {

    UserDataRole findByUserId(Long userId);

}
