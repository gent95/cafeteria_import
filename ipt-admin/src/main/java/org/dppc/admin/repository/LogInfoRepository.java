package org.dppc.admin.repository;

import org.dppc.admin.entity.LogInfo;
import org.dppc.dbexpand.repository.BaseRepository;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/25.
 * @时间： 16:38.
 */
public interface LogInfoRepository extends BaseRepository<LogInfo,Long> {
}
