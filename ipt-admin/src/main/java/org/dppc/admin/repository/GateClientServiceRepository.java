package org.dppc.admin.repository;

import org.dppc.admin.entity.GateClientService;
import org.dppc.dbexpand.repository.BaseRepository;

import java.util.Collection;
import java.util.List;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/12/27
 * @时间 ：9:53
 */
public interface GateClientServiceRepository extends BaseRepository<GateClientService, Long> {

    /**
     * @描述 ：根据服务端id查询
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：10:55
     */
    List<GateClientService> findByServiceId(Long serviceId);

    /**
     * @描述 ：根据客户端id查询
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：13:17
     */
    List<GateClientService> findByClientId(Long clientId);

    /**
     * @描述 ：根据客户端id或者服务端id查询
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：13:56
     */
    List<GateClientService> findByClientIdOrServiceId(Long clientId, Long serviceId);

    /**
     * @描述 ：
     * @作者 ：wyl
     * @日期 ：2017/12/28
     * @时间 ：9:24
     */
    List<GateClientService> findByClientIdInOrServiceIdIn(Collection<Long> clientIdCollection, Collection<Long> serviceIdCollection);
}
