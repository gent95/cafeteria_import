package org.dppc.admin.repository;

import org.dppc.admin.entity.SystemConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SystemConfigRepository extends JpaRepository<SystemConfig, Long>,JpaSpecificationExecutor<Long> {

    /**
     * @描述 ：根据key查询
     * @作者 ：wyl
     * @日期 ：2017/12/13
     * @时间 ：14:06
     */
    SystemConfig findByConfigKey(String configKey);
}
