package org.dppc.admin.repository;

import org.dppc.admin.entity.GateClient;
import org.dppc.dbexpand.repository.BaseRepository;

import java.util.List;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/12/27
 * @时间 ：9:53
 */
public interface GateClientRepository extends BaseRepository<GateClient, Long> {

    /**
     * @描述 ：查询客户服务端列表
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：14:13
     */
    List<GateClient> findByClientIdNot(Long clientId);
}
