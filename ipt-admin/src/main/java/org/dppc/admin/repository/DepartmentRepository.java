package org.dppc.admin.repository;

import org.dppc.admin.entity.Department;
import org.dppc.common.enums.DepartmentLevelEnum;
import org.dppc.dbexpand.repository.BaseRepository;

import java.util.Collection;
import java.util.List;

/**
 * @描述 ：部门信息
 * @作者 ：wyl
 * @日期 ：2017/11/30
 * @时间 ：11:46
 */
public interface DepartmentRepository extends BaseRepository<Department, Long> {

    /**
     * 查询Id所有二级才散
     * @param level
     * @param id
     * @return
     */
    List<Department> findByDepartmentLevelAndParentDepartmentId(Integer level,Long id);

    /**
     * @描述 ：根据部门id集合查询部门集合
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:10
     */
    List<Department> findByDepartmentIdIn(Collection<Long> departmentIdSet);

    /**
     * @描述 ：查询启用的部门列表
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:36
     */
    List<Department> findByEnableAndDepartmentLevelOrderByDepartmentIdAsc(Integer enable, Integer departmentLevel);

    /**
     * @描述 ：所有二级部门
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：9:37
     */
    List<Department> findByDepartmentLevelOrderByDepartmentIdAsc(Integer value);

    /**
     * @描述 ：查询父级部门
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：10:40
     */
    Department findByParentDepartmentIdAndDepartmentLevel(Long parentDepartmentId, Integer value);
}
