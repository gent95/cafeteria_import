package org.dppc.admin.repository;

import org.dppc.admin.entity.User;
import org.dppc.dbexpand.repository.BaseRepository;

import java.util.Collection;
import java.util.List;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/17.
 * @时间： 18:05.
 */
public interface UserRepository extends BaseRepository<User,Long> {

    User findByUsernameAndEnable(String username, Integer enable);

    List<User> findByUserIdIn(Collection<Long> collection);


}
