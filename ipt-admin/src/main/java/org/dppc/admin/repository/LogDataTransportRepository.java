package org.dppc.admin.repository;

import org.dppc.admin.entity.LogDataTransport;
import org.dppc.dbexpand.repository.BaseRepository;

import java.util.List;

/**
 * @描述 :   数据交换异常
 * @作者 :   GAOJ
 * @日期 :   2018/4/16 0027
 * @时间 :   11:43
 */
public interface LogDataTransportRepository extends BaseRepository<LogDataTransport,Long> {

    /**
     * 查询交换数据异常待解决的所有日志
     * @param isSolve
     * @return
     */
    List<LogDataTransport> findByIsSolve(Integer isSolve);

}
