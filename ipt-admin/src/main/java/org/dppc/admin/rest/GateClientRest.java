package org.dppc.admin.rest;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.dppc.admin.entity.GateClient;
import org.dppc.admin.service.GateClientService;
import org.dppc.admin.vo.GateClientVO;
import org.dppc.common.constant.PageConstant;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/12/27
 * @时间 ：9:45
 */
@RestController
@RequestMapping(value = "/gateClient")
public class GateClientRest {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private GateClientService gateClientService;

    /**
     * @描述 ：服务端分页列表
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：9:57
     */
    @GetMapping(value = "/findClientServicePage")
    @ApiOperation(value="服务端分页列表", notes="服务端分页列表")
    public BaseResponse findClientServicePage (@ModelAttribute GateClient query
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE) @ApiParam(value = "当前页数") Integer page
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE_SIZE) @ApiParam(value = "分页大小") Integer size) {
        try {
            return ResponseHelper.success(gateClientService.findClientServicePage(query, new PageRequest(page - 1, size, new Sort(Sort.Direction.DESC, "clientId"))));
        } catch (Exception e) {
            logger.error("查询服务端分页列表异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：客户端分页列表
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：9:57
     */
    @GetMapping(value = "/findClientPage")
    @ApiOperation(value="客户端分页列表", notes="客户端分页列表")
    public BaseResponse findClientPage (@ModelAttribute GateClient query
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE) @ApiParam(value = "当前页数") Integer page
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE_SIZE) @ApiParam(value = "分页大小") Integer size) {
        try {
            return ResponseHelper.success(gateClientService.findClientPage(query, new PageRequest(page - 1, size, new Sort(Sort.Direction.DESC, "clientId"))));
        } catch (Exception e) {
            logger.error("查询客户端分页列表异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：根据id查询
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：11:38
     */
    @GetMapping(value = "/findByClientId/{clientId}")
    @ApiOperation(value="根据id查询", notes="根据id查询")
    public BaseResponse findByClientId (@PathVariable @ApiParam(value = "客户服务端id") Long clientId) {
        try {
            return ResponseHelper.success(gateClientService.findGateClientByClientId(clientId));
        } catch (Exception e) {
            logger.error("查询信息异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：保存修改后的密钥
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：11:44
     */
    @PatchMapping(value = "/modifySecret/{clientId}")
    @ApiOperation(value="保存修改后的密钥", notes="保存修改后的密钥")
    @ApiImplicitParams(value = {@ApiImplicitParam(name = "gateClient", value = "客户端", dataType = "GateClient")})
    public BaseResponse modifySecret (@PathVariable @ApiParam(value = "客户服务端id") Long clientId, @RequestBody GateClient gateClient) {
        try {
            gateClient.setClientId(clientId);
            gateClientService.modifySecret(gateClient);
            return ResponseHelper.success("修改密钥成功");
        } catch (Exception e) {
            logger.error("保存修改后的密钥异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：删除客户服务端
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：11:47
     */
    @DeleteMapping(value = "/deleteClientService/{clientId}")
    @ApiOperation(value="删除客户服务端", notes="根据id删除删除客户服务端")
    public BaseResponse deleteClientService (@PathVariable @ApiParam(value = "客户服务端id") Long clientId) {
        try {
            gateClientService.deleteClientService(clientId);
            return ResponseHelper.success("删除客户服务端成功");
        } catch (Exception e) {
            logger.error("删除客户服务端异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：根据客户服务端id获取其他客户服务端列表
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：13:59
     */
    @GetMapping(value = "/findClientServiceList/{clientId}")
    @ApiOperation(value="根据客户服务端id获取其他客户服务端列表", notes="根据客户服务端id获取其他客户服务端列表")
    public BaseResponse findClientServiceList (@PathVariable @ApiParam(value = "客户端id") Long clientId) {
        try {
            return ResponseHelper.success(gateClientService.findClientServiceList(clientId));
        } catch (Exception e) {
            logger.error("根据客户服务端id获取其他客户服务端列表异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }
    
    /**
     * @描述 ：查询服务端VO
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：14:13
     */
    @GetMapping(value = "/findServiceVOByClientId/{clientId}")
    @ApiOperation(value="查询服务端VO", notes="查询服务端VO")
    public BaseResponse findServiceVOByClientId (@PathVariable @ApiParam(value = "服务端id") Long clientId) {
        try {
            return ResponseHelper.success(gateClientService.findServiceVOByClientId(clientId));
        } catch (Exception e) {
            logger.error("查询服务端VO", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：查询客户端VO
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：14:13
     */
    @GetMapping(value = "/findClientVOByClientId/{clientId}")
    @ApiOperation(value="查询客户端VO", notes="查询客户端VO")
    public BaseResponse findClientVOByClientId (@PathVariable @ApiParam(value = "客户端id") Long clientId) {
        try {
            return ResponseHelper.success(gateClientService.findClientVOByClientId(clientId));
        } catch (Exception e) {
            logger.error("查询客户端VO", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：修改服务端具有的客户端列表
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：15:07
     */
    @PatchMapping(value = "/modifyServiceClientList/{clientId}")
    @ApiOperation(value="修改服务端具有的客户端列表", notes="修改服务端具有的客户端列表")
    @ApiImplicitParams(value = {@ApiImplicitParam(name = "gateClientVO", value = "服务端VO", dataType = "GateClientVO")})
    public BaseResponse modifyServiceClientList (@PathVariable @ApiParam(value = "服务端id") Long clientId, @RequestBody GateClientVO gateClientVO) {
        try {
            gateClientVO.setClientId(clientId);
            gateClientService.modifyServiceClientList(gateClientVO);
            return ResponseHelper.success("修改服务端具有的客户端列表成功");
        }catch (Exception e) {
            logger.error("修改服务端具有的客户端列表异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：修改客户端具有的服务端列表
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：15:07
     */
    @PatchMapping(value = "/modifyClientServiceList/{clientId}")
    @ApiOperation(value="修改客户端具有的服务端列表", notes="修改客户端具有的服务端列表")
    @ApiImplicitParams(value = {@ApiImplicitParam(name = "gateClientVO", value = "客户端VO", dataType = "GateClientVO")})
    public BaseResponse modifyClientServiceList (@PathVariable @ApiParam(value = "客户端id") Long clientId, @RequestBody GateClientVO gateClientVO) {
        try {
            gateClientVO.setClientId(clientId);
            gateClientService.modifyClientServiceList(gateClientVO);
            return ResponseHelper.success("修改客户端具有的服务端列表成功");
        }catch (Exception e) {
            logger.error("修改客户端具有的服务端列表异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：批量删除客户服务端
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：17:23
     */
    @DeleteMapping(value = "/batchDeleteClient")
    @ApiOperation(value="批量删除客户服务端", notes="批量删除客户服务端")
    @ApiImplicitParams(value = {@ApiImplicitParam(name = "checkAllBoxList", value = "批量选择的客户服务端id集合", dataType = "List<Long>")})
    public BaseResponse batchDeleteClient (@RequestBody List<Long> checkAllBoxList) {
        try {
            gateClientService.deleteClientServiceList(checkAllBoxList);
            return ResponseHelper.success("批量删除客户服务端成功");
        } catch (Exception e) {
            logger.error("批量删除客户服务端异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }
}
