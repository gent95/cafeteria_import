package org.dppc.admin.rest;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.dppc.admin.entity.LogInterface;
import org.dppc.admin.service.LogInterfaceService;
import org.dppc.admin.vo.LogInterfaceVO;
import org.dppc.common.enums.ForwardEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.DateHelper;
import org.dppc.common.utils.ResponseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.dppc.common.constant.PageConstant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping(value = "sta")
public class StatisticalController {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private LogInterfaceService logInterface;

    @GetMapping(value = "/quertResponseTime")
    @ApiOperation(value = "查询7天接口访问数据", notes = "")
    @ResponseBody
    public BaseResponse quertResponseTime(Integer days) {
        if (days == null || days >= 0) {
            days = -6;
        }
        Map<String, Object> resultMap = new HashMap();
        List<Double> list = logInterface.quertResponseTime(days);
        List<String> dateList = DateHelper.getDateArray(days);
        resultMap.put("values", list);
        resultMap.put("xAxis", dateList);
        return ResponseHelper.success(resultMap);
    }

    @GetMapping(value = "/quertMonthCount")
    @ApiOperation(value = "进一个月请求数", notes = "")
    @ResponseBody
    public BaseResponse quertMonthCount(String state, Integer days) {
        Map<String, Object> resultMap = new HashMap();
        List<String> bizTypeList = new ArrayList<>();
        List<Integer> countList = new ArrayList<>();
        List<Map> list = logInterface.quertMonthCount(state, days);
        for (Map<String, Object> stringObjectMap : list) {
            bizTypeList.add(ForwardEnum.valueOf(String.valueOf(stringObjectMap.get("bizType"))).getDescription());
            countList.add(Integer.valueOf(String.valueOf(stringObjectMap.get("count"))));
        }
        resultMap.put("bizTypes", bizTypeList);
        resultMap.put("counts", countList);
        return ResponseHelper.success(resultMap);
    }

    @GetMapping(value = "/quertErrorRate")
    @ApiOperation(value = "错误率", notes = "")
    @ResponseBody
    public BaseResponse quertErrorRate(Integer days) {
        if (days == null || days >= 0) {
            days = -6;
        }
        Map<String, Object> resultMap = new HashMap();
        List<Double> list = logInterface.quertErrorRate(days);
        List<String> dateList = DateHelper.getDateArray(days);
        resultMap.put("values", list);
        resultMap.put("xAxis", dateList);
        return ResponseHelper.success(resultMap);
    }

    @ApiOperation(value = "接口日志操作列表", notes = "接口日志操作列表")
    @GetMapping(value = "/quertMonitorList")
    @ResponseBody
    public BaseResponse quertMonitorList(@ModelAttribute LogInterfaceVO query
            , @RequestParam(defaultValue =PageConstant.DEFAULT_PAGE) @ApiParam(value = "当前页数") Integer page
            , @RequestParam(defaultValue =PageConstant.DEFAULT_PAGE_SIZE) @ApiParam(value = "分页大小") Integer size) {
        try {
            return ResponseHelper.success(logInterface.findPageList(query,new PageRequest(page - 1,size,new Sort(Sort.Direction.DESC,"logInterfaceId"))));
        } catch (Exception e) {
            logger.error("查询操作日志分页列表异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }


    @ApiOperation(value = "每日必填项", notes = "")
    @GetMapping(value = "/required")
    @ResponseBody
    public BaseResponse getRequired(String state, Integer days, Integer index) {
        Map<String, Object> resultMap = new HashMap();
        List<String> bizTypeList = new ArrayList<>();
        List<Integer> countList = new ArrayList<>();
        List<Map> list = logInterface.requiredList(state, days);
        for (Map<String, Object> stringObjectMap : list) {
            if (index == 1) {//必填项
                ForwardEnum bizType = ForwardEnum.valueOf(String.valueOf(stringObjectMap.get("bizType")));
                List<ForwardEnum> required = ForwardEnum.getRequired();
                boolean contains = required.contains(bizType);
                if (contains) {
                    if (bizTypeList.size() < 10) {
                        bizTypeList.add(bizType.getDescription());
                        countList.add(Integer.valueOf(String.valueOf(stringObjectMap.get("count"))));
                    }
                }
            } else if (index == 2) {//非必填项
                ForwardEnum bizType = ForwardEnum.valueOf(String.valueOf(stringObjectMap.get("bizType")));
                List<ForwardEnum> required = ForwardEnum.getNoRequiredlist();
                boolean contains = required.contains(bizType);
                if (contains) {
                    if (bizTypeList.size() < 10) {
                        bizTypeList.add(bizType.getDescription());
                        countList.add(Integer.valueOf(String.valueOf(stringObjectMap.get("count"))));
                    }
                }
            }
        }
        resultMap.put("bizTypes", bizTypeList);
        resultMap.put("counts", countList);
        return ResponseHelper.success(resultMap);
    }
}
