package org.dppc.admin.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.dppc.admin.entity.Resource;
import org.dppc.admin.service.ResourceService;
import org.dppc.admin.service.RoleService;
import org.dppc.common.constant.PageConstant;
import org.dppc.common.enums.EnableEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

/**
 * @描述 ：资源
 * @作者 ：wyl
 * @日期 ：2017/10/25
 * @时间 ：15:50
 */
@RestController
@RequestMapping(value = "/resource")
public class ResourceRest {

    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private ResourceService resourceService;
    @Autowired
    private RoleService roleService;

    /**
     * @描述 ：资源分页数据列表
     * @作者 ：wyl
     * @日期 ：2017/10/25 
     * @时间 ：15:53
     */
    @GetMapping(value = "/findPage")
    @ApiOperation(value="资源分页数据列表", notes="条件查询资源分页数据 父资源下包含子资源列表")
    public BaseResponse findPage (@ModelAttribute Resource resource
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE) @ApiParam(value = "当前页数") Integer page
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE_SIZE) @ApiParam(value = "分页大小") Integer size) {
        try {
            return ResponseHelper.success(resourceService.findResourcePage(resource, new PageRequest(page - 1, size, new Sort(Sort.Direction.ASC, "sequence"))).getContent());
        } catch (Exception e) {
            logger.error("查询资源分页数据异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }
    
    /**
     * @描述 ：所有资源 层级包含
     * @作者 ：wyl
     * @日期 ：2017/11/13 
     * @时间 ：14:35
     */
    @GetMapping(value = "/findResourceVOList")
    @ApiOperation(value="所有资源", notes="所有资源 层级包含子资源")
    public BaseResponse findResourceVOList () {
        try {
            return ResponseHelper.success(resourceService.findResourceVOList());
        } catch (Exception e) {
            logger.error("查询所有资源层级关系列表异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }
    
    /**
     * @描述 ：启用资源 若为菜单资源 级联启用所有未启用的子资源
     * @作者 ：wyl
     * @日期 ：2017/10/27 
     * @时间 ：9:01
     */
    @PatchMapping(value = "/enableResource/{resourceId}")
    @ApiOperation(value="启用资源", notes="启用资源 若为菜单资源 级联启用所有未启用的子资源 url/{resourceId}")
    public BaseResponse enableResource (@PathVariable @ApiParam(value = "资源id") Long resourceId) {
        try {
            resourceService.enableOrDisableResource(resourceId, EnableEnum.ENABLE);
            return ResponseHelper.success("启用资源成功");
        } catch (Exception e) {
            logger.error("启用资源异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }
    
    /**
     * @描述 ：禁用资源 若为菜单 则级联禁用所有启用的子资源
     * @作者 ：wyl
     * @日期 ：2017/10/27 
     * @时间 ：9:10
     */
    @PatchMapping(value = "/disableResource/{resourceId}")
    @ApiOperation(value="禁用资源", notes="禁用资源 若为菜单 则级联禁用所有启用的子资源 url/{resourceId}")
    public BaseResponse disableResource (@PathVariable @ApiParam(value = "资源id") Long resourceId) {
        try {
            resourceService.enableOrDisableResource(resourceId, EnableEnum.DISABLE);
            return ResponseHelper.success("禁用资源成功");
        } catch (Exception e) {
            logger.error("禁用资源异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：所有启用的资源列表
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：15:10
     */
    @GetMapping(value = "/getResourceTreeList")
    @ApiOperation(value="角色对应的所有资源列表", notes="角色对应的所有资源列表")
    public BaseResponse getResourceTreeList () {
        try {
            return ResponseHelper.success(resourceService.getResourceTreeList());
        } catch (Exception e) {
            logger.error("查询角色对应的所有资源列表异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：根据角色id查询角色VO
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：16:06
     */
    @GetMapping(value = "/findRoleVOById/{roleId}")
    public BaseResponse findRoleVOById (@PathVariable @ApiParam(value = "角色id")  Long roleId) {
        try {
            return ResponseHelper.success(roleService.findRoleVOByRoleId(roleId));
        } catch (Exception e) {
            logger.error("根据角色id查询角色VO异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    @PatchMapping(value = "/update/{resourceId}")
    @ApiOperation(value = "根绝Id修改资源名称", notes = "根绝Id修改资源名称")
    public BaseResponse update (@RequestBody Resource resource,@PathVariable Long resourceId){
        Resource res = resourceService.getOneDataById(resourceId);
        res.setResourceName(resource.getResourceName());
        return ResponseHelper.success(resourceService.update(res));
    }
}
