package org.dppc.admin.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.dppc.admin.service.LogInfoService;
import org.dppc.admin.vo.LogInfoVO;
import org.dppc.common.constant.PageConstant;
import org.dppc.common.enums.LogOptEnum;
import org.dppc.common.enums.MethodEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @描述 ：日志管理
 * @作者 ：wyl
 * @日期 ：2017/11/30
 * @时间 ：16:48
 */
@RestController
@RequestMapping(value = "/logInfo")
public class LogInfoRest {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private LogInfoService logInfoService;

    /**
     * @描述 ：用户操作日志分页列表
     * @作者 ：wyl
     * @日期 ：2017/12/20
     * @时间 ：10:20
     */
    @GetMapping(value = "/findUserOptLogPage")
    @ApiOperation(value="用户操作日志分页列表", notes="用户操作日志分页列表")
    public BaseResponse findUserOptLogPage(@ModelAttribute LogInfoVO query
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE) @ApiParam(value = "当前页数") Integer page
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE_SIZE) @ApiParam(value = "分页大小") Integer size) {
        try {
            return ResponseHelper.success(logInfoService.finUserOptLogPage(query, new PageRequest(page - 1, size, new Sort(Sort.Direction.DESC, "createTime"))));
        } catch (Exception e) {
            logger.error("查询用户操作日志分页列表异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：系统运行日志分页列表
     * @作者 ：wyl
     * @日期 ：2017/12/20
     * @时间 ：10:32
     */
    @GetMapping(value = "/findSystemRunLogPage")
    @ApiOperation(value="系统运行日志分页列表", notes="系统运行日志分页列表")
    public BaseResponse findSystemRunLogPage(@ModelAttribute LogInfoVO query
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE) @ApiParam(value = "当前页数") Integer page
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE_SIZE) @ApiParam(value = "分页大小") Integer size) {
        try {
            return ResponseHelper.success(logInfoService.finSystemRunLogPage(query, new PageRequest(page - 1, size, new Sort(Sort.Direction.DESC, "createTime"))));
        } catch (Exception e) {
            logger.error("查询系统运行日志分页列表异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：页面条件查询枚举列表
     * @作者 ：wyl
     * @日期 ：2017/12/20
     * @时间 ：11:44
     */
    @GetMapping(value = "/getPageQueryEnumList")
    @ApiOperation(value="页面条件查询枚举列表", notes="页面条件查询枚举列表")
    public BaseResponse getPageQueryEnumList () {
        Map<String, Object> map = new HashMap<>();
        map.put("optEnumList", LogOptEnum.getQueryList());
        map.put("methodEnumList", MethodEnum.getQueryList());
        return ResponseHelper.success(map);
    }
}
