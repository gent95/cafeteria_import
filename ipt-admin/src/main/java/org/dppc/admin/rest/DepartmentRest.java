package org.dppc.admin.rest;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.dppc.admin.entity.Department;
import org.dppc.admin.service.DepartmentService;
import org.dppc.admin.vo.DepartmentVO;
import org.dppc.common.constant.PageConstant;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
/**
 * @描述 ：部门管理
 * @作者 ：wyl
 * @日期 ：2017/11/30
 * @时间 ：15:24
 */
@RestController
@RequestMapping(value = "/department")
public class DepartmentRest {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DepartmentService departmentService;

    @GetMapping(value = "/findDepartmentPage")
    @ApiOperation(value="分页查询所有部门", notes="分页查询所有部门")
    public BaseResponse findDepartmentPage(@ModelAttribute Department query
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE) @ApiParam(value = "当前页数") Integer page
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE_SIZE) @ApiParam(value = "分页大小") Integer size) {
        try {
            return ResponseHelper.success(departmentService.findDepartmentPage(query, page - 1, size));
        } catch (Exception e) {
            logger.error("查询部门分页数据异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }
    /**
     * @描述 根据id删除部门
     * @作者 ：lyf
     * @日期 ：2018/04/08
     * @时间 ：18:00
     */
    @DeleteMapping(value = "/delDepartment/{departmentId}")
    @ApiOperation(value = "删除部门", notes = "删除部门")
    public BaseResponse delDepartment(@PathVariable @ApiParam(value = "部门id") Long departmentId) {
        try {
            departmentService.delDepartment(departmentId);
            return ResponseHelper.success("删除部门成功");
        } catch (Exception e) {
            logger.error("删除部门失败", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：用户部门列表 启用的二级部门
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:26
     */
    @GetMapping(value = "/getUserDepartmentList")
    @ApiOperation(value = "用户部门列表", notes = "用户部门列表 启用的二级部门")
    public BaseResponse getUserDepartmentList() {
        try {
            return ResponseHelper.success(departmentService.getUserDepartmentList());
        } catch (Exception e) {
            logger.error("查询部门列表异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：查询父级部门
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：10:31
     */
    @GetMapping(value = "/findParentDepartment/{parentDepartmentId}")
    @ApiOperation(value = "查询父级部门", notes = "查询父级部门")
    public BaseResponse findParentDepartment (@PathVariable @ApiParam(value = "父级部门id") Long parentDepartmentId) {
        try {
            return ResponseHelper.success(departmentService.findParentDepartment(parentDepartmentId));
        } catch (Exception e) {
            logger.error("查询父级部门异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：查询所有二级部门
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：9:34
     */
    @GetMapping(value = "/getAllDepartment")
    @ApiOperation(value = "查询所有二级部门", notes = "查询所有二级部门")
    public BaseResponse getAllDepartment() {
        try {
            return ResponseHelper.success(departmentService.getAllDepartment());
        } catch (Exception e) {
            logger.error("查询所有部门二级异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：根据部门id查询部门信息
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:38
     */
    @GetMapping(value = "/findByDepartmentId/{departmentId}")
    @ApiOperation(value = "根据部门id查询部门信息", notes = "根据部门id查询部门信息")
    public BaseResponse findByDepartmentId(@PathVariable @ApiParam(value = "部门id") Long departmentId) {
        try {
            return ResponseHelper.success(departmentService.findDepartmentById(departmentId));
        } catch (Exception e) {
            logger.error("根据id查询部门信息异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：禁用部门
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:45
     */
    @PatchMapping(value = "/disableDepartment/{departmentId}")
    @ApiOperation(value = "禁用部门", notes = "根据部门id禁用部门")
    public BaseResponse enableUser(@PathVariable @ApiParam(value = "部门id") Long departmentId) {
        try {
            departmentService.disableDepartment(departmentId);
            return ResponseHelper.success("禁用部门成功");
        } catch (Exception e) {
            logger.error("禁用部门异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：启用部门
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:45
     */
    @PatchMapping(value = "/enableDepartment/{departmentId}")
    @ApiOperation(value = "启用部门", notes = "根据部门id启用部门")
    public BaseResponse enableDepartment(@PathVariable @ApiParam(value = "部门id") Long departmentId) {
        try {
            departmentService.enableDepartment(departmentId);
            return ResponseHelper.success("启用部门成功");
        } catch (Exception e) {
            logger.error("启用部门异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：部门名称验证
     * @作者 ：wyl
     * @日期 ：2017/12/1
     * @时间 ：13:43
     */
    @GetMapping(value = "/departmentNameValidate")
    @ApiOperation(value = "验证用户名称是否已存在", notes = "验证用户名称是否已存在")
    public BaseResponse departmentNameValidate(@ModelAttribute Department department) {
        try {
            if (departmentService.departmentNameValidate(department) < 1L) {
                return ResponseHelper.success();
            }
            return ResponseHelper.error("部门名称已存在");
        } catch (Exception e) {
            logger.error("部门名称验证异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：保存修改后的部门信息
     * @作者 ：wyl
     * @日期 ：2017/12/1
     * @时间 ：13:55
     */
    @PatchMapping(value = "/modifyDepartment/{departmentId}")
    @ApiOperation(value = "保存修改后的部门信息", notes = "保存修改后的部门信息")
    @ApiImplicitParams(value = {@ApiImplicitParam(name = "department", value = "保存修改后的部门信息", dataType = "Department")})
    public BaseResponse modifyDepartment(@PathVariable @ApiParam(value = "部门id") Long departmentId, @RequestBody Department department) {
        try {
            department.setDepartmentId(departmentId);
            departmentService.modifyDepartment(department);
            return ResponseHelper.success("修改部门信息成功");
        } catch (Exception e) {
            logger.error("修改部门信息异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：添加部门
     * @作者 ：lyf
     * @日期 ：2018/05/05
     * @时间 ：11:35
     */
    @PostMapping(value = "/addDepartment")
    @ApiOperation(value = "添加部门", notes = "添加部门")
    public BaseResponse addDepartment(@RequestBody DepartmentVO departmentVO) {
        try {
            return ResponseHelper.success(departmentService.addDepartment(departmentVO));
        } catch (Exception e) {
            logger.error("添加子部门异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：查询所有一级部门
     * @作者 ：lxw
     * @日期 ：2018/4/17
     * @时间 ：13:50
     */
    @GetMapping(value = "/getAllDepartmentOne")
    @ApiOperation(value = "查询所有一级部门", notes = "查询所有一级部门")
    public BaseResponse getAllDepartmentOne() {
        try {
            return ResponseHelper.success(departmentService.getAllDepartmentOne());
        } catch (Exception e) {
            logger.error("查询所有部门一级异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }
}
