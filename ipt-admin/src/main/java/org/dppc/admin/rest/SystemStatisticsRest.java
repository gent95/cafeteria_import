package org.dppc.admin.rest;

import io.swagger.annotations.ApiOperation;
import org.dppc.admin.service.SystemStatisticsService;
import org.dppc.common.enums.ForwardEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.DateHelper;
import org.dppc.common.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/system")
public class SystemStatisticsRest {

    @Autowired
    private SystemStatisticsService systemStatisticsService;

    @ApiOperation(value = "系统统计", notes = "")
    @GetMapping(value = "/required")
    @ResponseBody
    public BaseResponse getRequired(String state) {
        Map<String, Object> resultMap = new HashMap();
        List<String> bizTypeList = new ArrayList<>();
        List<Integer> countList = new ArrayList<>();
        List<Map> list = systemStatisticsService.requiredList(state);
        for (Map<String, Object> stringObjectMap : list) {
            String type = String.valueOf(stringObjectMap.get("type"));
            bizTypeList.add(type);
            String count = String.valueOf(stringObjectMap.get("count"));
            countList.add(Integer.parseInt(count));
        }
        resultMap.put("types",bizTypeList);
        resultMap.put("counts",countList);
        return ResponseHelper.success(resultMap);
    }


    @GetMapping(value = "/quertResponseTime")
    @ApiOperation(value = "查询7天接口平均响应时间", notes = "")
    @ResponseBody
    public BaseResponse quertResponseTime(Integer days) {
        if (days == null || days >= 0) {
            days = -6;
        }
        Map<String, Object> resultMap = new HashMap();
        List<Double> list = systemStatisticsService.quertResponseTime(days);
        List<String> dateList = DateHelper.getDateArray(days);
        resultMap.put("values", list);
        resultMap.put("xAxis", dateList);
        return ResponseHelper.success(resultMap);
    }
}
