package org.dppc.admin.rest;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.dppc.admin.entity.User;
import org.dppc.admin.service.UserService;
import org.dppc.admin.vo.UserVO;
import org.dppc.common.constant.PageConstant;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.enums.EnableEnum;
import org.dppc.common.enums.UserTypeEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @描述 ：用户管理
 * @作者 ：wyl
 * @日期 ：2017/10/25
 * @时间 ：15:54
 */
@RestController
@RequestMapping(value = "/user")
public class UserRest {

    private Logger logger = LoggerFactory.getLogger(UserRest.class.getName());
    @Autowired
    private UserService userService;

    /**
     * 根据用户名查询用户
     */
    @GetMapping(value = "/findUserByUsername")
    @ApiOperation(value = "根据用户名查询用户", notes = "根据用户名查询用户信息")
    public User findUserByUsername(@ApiParam(value = "用户名") String username) {
        logger.info("当前登录名为：{};编号：{}；姓名：{}",
                BaseContextHandler.getUsername(), BaseContextHandler.getUserID(), BaseContextHandler.getName());
        return userService.findByUsername(username);
    }

    /**
     * @描述 ：添加用户
     * @作者 ：wyl
     * @日期 ：2017/10/25
     * @时间 ：15:56
     */
    @PostMapping(value = "/addUser")
    @ApiOperation(value = "添加用户", notes = "添加用户 角色id集处理为字符串 逗号拼接")
    @ApiImplicitParams(value = {@ApiImplicitParam(name = "user", value = "添加用户", dataType = "User")})
    public BaseResponse addUser(@RequestBody UserVO userVO) {
        try {
            userService.saveUser(userVO);
            return ResponseHelper.success("添加用户成功");
        } catch (Exception e) {
            logger.error("添加用户异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：用户数据分页列表
     * @作者 ：wyl
     * @日期 ：2017/10/25
     * @时间 ：15:55
     */
    @GetMapping(value = "/findPage")
    @ApiOperation(value = "用户数据分页查询", notes = "条件查询用户信息分页数据 当前页数page默认1 size分页大小默认5")
    public BaseResponse findPage(@ModelAttribute User user
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE) @ApiParam(value = "当前页数") Integer page
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE_SIZE) @ApiParam(value = "分页大小") Integer size) {
        try {
            return ResponseHelper.success(userService.findUserPage(user, new PageRequest(page - 1, size, new Sort(Sort.Direction.DESC, "userId"))));
        } catch (Exception e) {
            logger.error("查询用户分页数据异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：禁用用户
     * @作者 ：wyl
     * @日期 ：2017/11/2
     * @时间 ：16:40
     */
    @PatchMapping(value = "/disableUser/{userId}")
    @ApiOperation(value = "禁用用户", notes = "根据用户主键id禁用用户 url/{userId}")
    public BaseResponse disableUser(@PathVariable @ApiParam(value = "用户id") Long userId) {
        try {
            userService.disableUser(userId);
            return ResponseHelper.success("禁用用户成功");
        } catch (Exception e) {
            logger.error("禁用用户异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：启用用户
     * @作者 ：wyl
     * @日期 ：2017/10/25
     * @时间 ：15:56
     */
    @PatchMapping(value = "/enableUser/{userId}")
    @ApiOperation(value = "启用用户", notes = "根据用户主键id启用用户 url/{userId}")
    public BaseResponse enableUser(@PathVariable @ApiParam(value = "用户id") Long userId) {
        try {
            userService.enableUser(userId);
            return ResponseHelper.success("启用用户成功");
        } catch (Exception e) {
            logger.error("启用用户异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：根据用户id查询用户
     * @作者 ：wyl
     * @日期 ：2017/10/26
     * @时间 ：14:23
     */
    @GetMapping(value = "/findUserById/{userId}")
    @ApiOperation(value = "根据用户id查询用户", notes = "根据用户id查询用户")
    public BaseResponse findUserById(@PathVariable @ApiParam(value = "用户id") Long userId) {
        try {
            return ResponseHelper.success(userService.findUserByUserId(userId));
        } catch (Exception e) {
            logger.error("查询用户信息异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：根据用户id查询VO
     * @作者 ：wyl
     * @日期 ：2017/12/28
     * @时间 ：9:14
     */
    @GetMapping(value = "/findUserVOById/{userId}")
    @ApiOperation(value = "根据用户id查询VO", notes = "根据用户id查询VO")
    public BaseResponse findUserVOById(@PathVariable @ApiParam(value = "用户id") Long userId) {
        try {
            return ResponseHelper.success(userService.findUserVOByUserId(userId));
        } catch (Exception e) {
            logger.error("查询用户VO信息异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }


    /**
     * @描述 ：保存修改后的用户信息
     * @作者 ：wyl
     * @日期 ：2017/10/25
     * @时间 ：15:57
     */
    @PatchMapping(value = "/modifyUser/{userId}")
    @ApiOperation(value = "保存修改后的用户信息", notes = "保存修改后的用户信息 角色id集处理为字符串 逗号拼接")
    @ApiImplicitParams(value = {@ApiImplicitParam(name = "user", value = "保存修改后的用户信息", dataType = "User")})
    public BaseResponse modifyUser(@PathVariable @ApiParam(value = "用户id") Long userId, @RequestBody UserVO userVO) {
        try {
            userVO.setUserId(userId);
            userService.modifyUser(userVO);
            return ResponseHelper.success("修改用户信息成功");
        } catch (Exception e) {
            logger.error("修改用户信息异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：验证用户名称是否已存在
     * @作者 ：wyl
     * @日期 ：2017/10/26
     * @时间 ：13:47
     */
    @GetMapping(value = "/usernameValidate")
    @ApiOperation(value = "验证用户名称是否已存在", notes = "验证用户名称是否已存在")
    public BaseResponse usernameValidate(@ModelAttribute User user) {
        try {
            if (userService.usernameValidate(user) < 1L) {
                return ResponseHelper.success("用户名称可以使用");
            }
            return ResponseHelper.error("用户名称已存在");
        } catch (Exception e) {
            logger.error("用户名验证异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：重置密码
     * @作者 ：wyl
     * @日期 ：2017/12/14
     * @时间 ：18:37
     */
    @PatchMapping(value = "/resetPassword/{userId}/{passWord}")
    @ApiOperation(value = "重置密码", notes = "重置密码")
    public BaseResponse resetPassword(@PathVariable Long userId, @PathVariable String passWord) {
        try {
            userService.resetPassword(userId, passWord);
            return ResponseHelper.success("重置用户密码成功");
        } catch (Exception e) {
            logger.error("重置用户密码异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：获取当前登录用户
     * @作者 ：wyl
     * @日期 ：2017/12/20
     * @时间 ：14:01
     */
    @GetMapping(value = "/getLoginUser")
    @ApiOperation(value = "获取当前登录用户", notes = "获取当前登录用户")
    public BaseResponse getLoginUser() {
        try {
            return ResponseHelper.success(userService.findByUsername(BaseContextHandler.getUsername()));
        } catch (Exception e) {
            logger.error("获取当前登录用户异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：用户类型枚举
     * @作者 ：wyl
     * @日期 ：2017/12/26
     * @时间 ：14:36
     */
    @GetMapping(value = "/getUserTypeEnum")
    @ApiOperation(value = "获取当用户类型枚举前登录用户", notes = "用户类型枚举")
    public BaseResponse getUserTypeEnum() {
        Map<String, Object> map = new HashMap<>();
        map.put("userTypeList", UserTypeEnum.getEnumList());//用户类型枚举
        map.put("PROVINCE_USER", UserTypeEnum.PROVINCE_USER.getEnumTypeVO());
        map.put("INTERFACE_USER", UserTypeEnum.INTERFACE_USER.getEnumTypeVO());
        return ResponseHelper.success(map);
    }

    /**
     * @描述 ：获取菜单状态枚举（禁用、启用）
     * @作者 ：wyl
     * @日期 ：2017/12/5
     * @时间 ：16:35
     */
    @GetMapping(value = "/getMenuEnableValues")
    @ApiOperation(value = "获取菜单状态枚举（禁用、启用）", notes = "获取菜单状态枚举（禁用、启用）")
    public BaseResponse getMenuEnableValues() {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("enableList", EnableEnum.getEnumList());
            map.put("ENABLE", EnableEnum.ENABLE.getEnumTypeVO());
            map.put("DISABLE", EnableEnum.DISABLE.getEnumTypeVO());
            return ResponseHelper.success(map);
        } catch (Exception e) {
            logger.error("获取条件查询列表异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * 测试
     *
     * @return
     */
    @PostMapping(value = "/test")
    public BaseResponse test() {
        return ResponseHelper.success("成功");
    }

}
