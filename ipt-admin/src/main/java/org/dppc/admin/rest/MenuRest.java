package org.dppc.admin.rest;

import io.swagger.annotations.ApiOperation;
import org.dppc.admin.service.MenuService;
import org.dppc.admin.vo.MenuVO;
import org.dppc.common.enums.EnableEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @描述 ：菜单
 * @作者 ：wyl
 * @日期 ：2017/11/10
 * @时间 ：9:45
 */
@RestController
@RequestMapping(value = "/menu")
public class MenuRest {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MenuService menuService;

    /**
     * @描述 ：获取当前登录用户权限下的菜单
     * @作者 ：wyl
     * @日期 ：2017/11/10 
     * @时间 ：10:04
     */
    @PostMapping(value = "/getUserMenuList")
    @ApiOperation(value = "获取当前登录用户权限下的菜单", notes = "获取当前登录用户权限下的菜单")
    public BaseResponse getUserMenuList () {
        try {
            List<MenuVO> menuList =menuService.getUserMenuList();
            return ResponseHelper.success(menuList);
        } catch (Exception e) {
            logger.error("获取当前登录用户权限下的菜单异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }
}
