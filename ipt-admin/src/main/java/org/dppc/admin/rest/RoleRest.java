package org.dppc.admin.rest;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.dppc.admin.entity.Role;
import org.dppc.admin.service.RoleService;
import org.dppc.admin.vo.RoleVO;
import org.dppc.common.constant.PageConstant;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

/**
 * @描述 ：角色
 * @作者 ：wyl
 * @日期 ：2017/10/25
 * @时间 ：15:34
 */
@RestController
@RequestMapping(value = "/role")
public class RoleRest {

    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private RoleService roleService;

    /**
     * @描述 ：添加角色
     * @作者 ：wyl
     * @日期 ：2017/10/25
     * @时间 ：15:41
     */
    @PostMapping(value = "/addRole")
    @ApiOperation(value="添加角色", notes="添加角色")
    @ApiImplicitParams(value = {@ApiImplicitParam(name = "role", value = "添加角色", dataType = "Role")})
    public BaseResponse addRole(@RequestBody Role role) {
        try {
            role.setState(1);
            roleService.addRole(role);
            return ResponseHelper.success("添加角色成功");
        } catch (Exception e) {
            logger.error("添加角色异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：角色管理分页列表
     * @作者 ：wyl
     * @日期 ：2017/10/25
     * @时间 ：15:41
     */
    @GetMapping(value = "/findRoleManagePage")
    @ApiOperation(value="角色管理分页列表", notes="角色管理分页列表")
    public BaseResponse findRoleManagePage(@ModelAttribute Role role
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE) @ApiParam(value = "当前页数") Integer page
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE_SIZE) @ApiParam(value = "分页大小") Integer size) {
        try {
            return ResponseHelper.success(roleService.findRolePage(role, page - 1, size));
        } catch (Exception e) {
            logger.error("查询角色分页数据异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：权限管理分页列表
     * @作者 ：wyl
     * @日期 ：2017/12/14
     * @时间 ：10:38
     */
    @GetMapping(value = "/findPermissionManagePage")
    @ApiOperation(value="权限管理分页列表", notes="权限管理分页列表")
    public BaseResponse findPermissionManagePage(@ModelAttribute Role role
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE) @ApiParam(value = "当前页数") Integer page
            , @RequestParam(defaultValue = PageConstant.DEFAULT_PAGE_SIZE) @ApiParam(value = "分页大小") Integer size) {
        try {

            return ResponseHelper.success(roleService.findPermissionManagePage(role, new PageRequest(page - 1, size, new Sort(Sort.Direction.DESC, "roleId"))));
        } catch (Exception e) {
            logger.error("查询角色分页数据异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：根据角色主键id删除角色
     * @作者 ：wyl
     * @日期 ：2017/10/25
     * @时间 ：15:42
     */
    @DeleteMapping(value = "/deleteRole/{roleId}")
    @ApiOperation(value="删除角色", notes="根据角色主键id删除角色 url/{roleId}")
    public BaseResponse deleteRole(@PathVariable @ApiParam(value = "角色id") Long roleId) {
        try {
            roleService.deleteRole(roleId);
            return ResponseHelper.success("删除角色成功");
        } catch (Exception e) {
            logger.error("删除角色信息异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：根据角色id查询角色信息
     * @作者 ：wyl
     * @日期 ：2017/10/26
     * @时间 ：14:28
     */
    @GetMapping(value = "/findRoleById/{roleId}")
    @ApiOperation(value="查询角色", notes="根据角色id查询角色信息")
    public BaseResponse findRoleByRoleId(@PathVariable @ApiParam(value = "角色id") Long roleId) {
        try {
            return ResponseHelper.success(roleService.findRoleByRoleId(roleId));
        } catch (Exception e) {
            logger.error("查询修改的角色信息异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：修改角色信息
     * @作者 ：wyl
     * @日期 ：2017/10/25
     * @时间 ：15:44
     */
    @PatchMapping(value = "/modifyRole/{roleId}")
    @ApiOperation(value="修改角色信息", notes="保存修改后的角色信息")
    @ApiImplicitParams(value = {@ApiImplicitParam(name = "role", value = "保存修改后的角色信息", dataType = "Role")})
    public BaseResponse modifyRole(@PathVariable @ApiParam(value = "角色id") Long roleId, @RequestBody Role role) {
        try {
            role.setRoleId(roleId);
            roleService.modifyRole(role);
            return ResponseHelper.success("修改角色信息成功");
        } catch (Exception e) {
            logger.error("修改角色信息异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：修改角色权限
     * @作者 ：wyl
     * @日期 ：2017/12/19
     * @时间 ：11:40
     */
    @PatchMapping(value = "/modifyRolePermission/{roleId}")
    @ApiOperation(value="修改角色权限", notes="修改角色权限")
    @ApiImplicitParams(value = {@ApiImplicitParam(name = "role", value = "修改角色权限", dataType = "Role")})
    public BaseResponse modifyRolePermission (@PathVariable @ApiParam(value = "角色id") Long roleId, @RequestBody RoleVO roleVO) {
        try {
            roleVO.setRoleId(roleId);
            roleService.modifyRolePermission(roleVO);
            return ResponseHelper.success("修改角色权限成功");
        } catch (Exception e) {
            logger.error("修改角色权限异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }

    /**
     * @描述 ：角色名验证
     * @作者 ：wyl
     * @日期 ：2017/10/26
     * @时间 ：14:01
     */
    @GetMapping(value = "/roleNameValidate")
    @ApiOperation(value="角色名验证", notes="角色名验证")
    public BaseResponse roleNameValidate(@ModelAttribute Role role) {
        try {
            if (roleService.roleNameValidate(role) < 1L) {
                return ResponseHelper.success("角色名称可以使用");
            }
            return ResponseHelper.error("角色名称已存在");
        } catch (Exception e) {
            logger.error("角色名验证异常", e.getMessage(), e);
            return ResponseHelper.error(e.getMessage());
        }
    }
    
    /**
     * @描述 ：根据state查询角色
     * @作者 ：wyl
     * @日期 ：2017/11/2 
     * @时间 ：14:06
     */
    @GetMapping(value = "/findStateList")
    @ApiOperation(value="所有角色信息列表", notes="所有角色信息列表")
    public BaseResponse findStateList () {
        try {
            return ResponseHelper.success(roleService.findByStateList(1));
        } catch (Exception e) {
            logger.error("查询所有角色信息列表异常");
            return ResponseHelper.error(e.getMessage());
        }
    }

}
