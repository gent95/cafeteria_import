package org.dppc.admin.config;

import feign.Logger;
import feign.RequestInterceptor;
import org.dppc.auth.client.interceptor.RequestHeadersInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @描述 :  内部接口配置
 * @作者 :  Zhao Yun
 * @日期 :  2017/12/19
 * @时间 :  12:47
 */
@Configuration
public class FeignConfig {

    @Bean
    RequestInterceptor attachHeadersForFeign(){
        return new RequestHeadersInterceptor();
    }

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }
}
