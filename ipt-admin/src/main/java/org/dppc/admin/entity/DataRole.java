package org.dppc.admin.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/7/27.
 * @时间： 11:41.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "SYS_DATA_ROLE")
@Entity
public class DataRole implements Serializable {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "DATA_ROLE_ID")
    @PredicateInfo
    @ApiModelProperty(value = "主键id")
    private Long roleId;
    /**
     * 角色名
     */
    @Column(name = "DATA_ROLE_NAME", length = 20)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    @ApiModelProperty(value = "角色名")
    private String roleName;
    /**
     * 角色描述
     */
    @Column(name = "DESCRIPTION", length = 100)
    @ApiModelProperty(value = "角色描述")
    private String description;

    /**
     * 默认数据Id
     */
    @Column(name = "DEFAULT_USER_ID", length = 100)
    @ApiModelProperty(value = "角色描述")
    private Long defaultUserId;
}
