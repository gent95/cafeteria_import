package org.dppc.admin.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;
import java.io.Serializable;

/**
  * @描述 ：部门信息
  * @作者 ：wyl
  * @日期 ：2017/11/30
  * @时间 ：11:21
  */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "SYS_DEPARTMENT")
@Entity
public class Department implements Serializable {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "DEPARTMENT_ID", length = 19)
    @PredicateInfo
    @ApiModelProperty(value = "主键id")
    private Long departmentId;
    /**
     * 部门名称
     */
    @Column(name = "DEPARTMENT_NAME", length = 20)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    @ApiModelProperty(value = "部门名称")
    private String departmentName;
    /**
     * 父部门id
     */
    @Column(name = "PARENT_DEPARTMENT_ID", length = 19)
    @ApiModelProperty(value = "父部门id")
    @PredicateInfo
    private Long parentDepartmentId;
    /**
     * 1：启用 0：禁用
     */
    @Column(name = "ENABLE", length = 1)
    @ApiModelProperty(value = "1：启用 0：禁用")
    @PredicateInfo
    private Integer enable;
    /**
     * 部门级别 1：一级 2：二级
     */
    @Column(name = "DEPARTMENT_LEVEL", length = 1)
    @ApiModelProperty(value = "1：一级 2：二级")
    @PredicateInfo
    private Integer departmentLevel;
}
