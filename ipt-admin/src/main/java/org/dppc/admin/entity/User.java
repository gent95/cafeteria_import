package org.dppc.admin.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @描述：
 * @作者： 朱明华
 * @日期： 2017/7/27.
 * @时间： 11:36.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "SYS_USER")
@Entity
public class User implements Serializable {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    @PredicateInfo
    @ApiModelProperty(value = "主键id")
    private Long userId;
    /**
     * 用户名
     */
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    @Column(name = "USERNAME", length = 20, unique = true)
    @ApiModelProperty(value = "用户名")
    private String username;
    /**
     * 密码 需加密
     */
    @Column(name = "PASSWORD", length = 200)
    @ApiModelProperty(value = "密码 加密")
    private String password;
    /**
     * 电子邮件
     */
    @Column(name = "EMAIL", length = 50)
    @ApiModelProperty(value = "电子邮件")
    private String email;
    /**
     * 用户姓名
     */
    @Column(name = "NAME", length = 20)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    @ApiModelProperty(value = "用户姓名")
    private String name;
    /**
     * 用户图片地址
     */
    @Column(name = "IMAGE_URL")
    @ApiModelProperty(value = "用户图片地址")
    private String imageUrl;
    /**
     * 是否启用 1：启用  0：禁用
     */
    @Column(name = "ENABLE")
    @PredicateInfo
    @ApiModelProperty(value = "是否启用 1：启用  0：禁用")
    private Integer enable;
    /**
     * 用户类型 1：云平台用户;2;监管平台用户;3:学校平台用户(一个食堂)
     */
    @Column(name = "USER_TYPE")
    @PredicateInfo
    @ApiModelProperty(value = "1：云平台用户;2;监管平台用户;3:学校平台用户")
    private Integer userType;

    /**
     * 部门id
     */
    @Column(name = "DEPARTMENT_ID")
    @PredicateInfo
    @ApiModelProperty(value = "部门id")
    private Long departmentId;
    /**
     * 联系电话
     */
    @Column(name = "PHONE")
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    @ApiModelProperty(value = "联系电话")
    private String phone;
   /* *//**
     * 企业ID
     *//*
    @Column(name = "ENTERPRISE_ID")
    @PredicateInfo
    @ApiModelProperty(value = "企业ID")
    private Long enterpriseId;

    @Column(name = "CITY_CODE")
    @ApiModelProperty(value = "城市编码")
    private String cityCode;*/

    /**
     * 用户-角色  多对多关系映射
     */
   //    @JsonIgnore
    @ApiModelProperty(value = "用户-角色  多对多关系映射")
    @ManyToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinTable(name = "SYS_USER_ROLE", joinColumns = {@JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")},
            inverseJoinColumns = {@JoinColumn(name = "ROLE_ID", referencedColumnName = "ROLE_ID")})
    private List<Role> roles;

    /**
     * 用户-学校食堂  一对多手动关联关系
     * 食堂添加是（只有一个学校食堂数据）
     * 监管（）
     */
//    //    @JsonIgnore
//    @ApiModelProperty(value = "用户-学校食堂 一对多手动关联关系")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name="sys_user_school_cafteria",joinColumns={@JoinColumn(name="user_id")}
            ,inverseJoinColumns={@JoinColumn(name="school_cafteria_id")})
    private Set<SchoolCafteria> schoolCafterias;
}
