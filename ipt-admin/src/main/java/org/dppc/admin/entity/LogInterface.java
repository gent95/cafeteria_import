package org.dppc.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.common.utils.DateHelper;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * @描述 ： 日志
 * @作者 ： 颜齐
 * @日期 ： 2017/10/25.
 * @时间 ： 16:23.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "SYS_LOG_INTERFACE")
@Entity
public class LogInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "log_interface_id")
    @ApiModelProperty(value = "主键")
    private Long logInterfaceId;

    @Column(name = "biz_type")
    @ApiModelProperty(value = "业务类型")
    private String bizType;


    @Column(name = "biz_type_name")
    @ApiModelProperty(value = "业务类型中文")
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String bizTypeName;

    @Column(name = "url")
    @ApiModelProperty(value = "url")
    private String url;

    @Column(name = "request_method")
    @ApiModelProperty(value = "请求方式")
    private String requestMethod;

    @Column(name = "request_time")
    @DateTimeFormat(pattern = DateHelper.DATATIMEF_STR)
    @JsonFormat(pattern = DateHelper.DATATIMEF_STR, timezone = "GMT+8")
    @ApiModelProperty(value = "请求时间")
    private Date requestTime;

    @Column(name = "request_params")
    @ApiModelProperty(value = "请求参数")
    private String requestParams;

    @Column(name = "upload_count")
    @ApiModelProperty(value = "上传个数")
    private int uploadCount;

    @Column(name = "process_time")
    @ApiModelProperty(value = "处理时间（毫秒）")
    private int processTime;

    @Column(name = "response_time")
    @DateTimeFormat(pattern = DateHelper.DATATIMEF_STR)
    @JsonFormat(pattern = DateHelper.DATATIMEF_STR, timezone = "GMT+8")
    @ApiModelProperty(value = "响应时间")
    private Date responseTime;

    @Column(name = "response_body")
    @ApiModelProperty(value = "响应体")
    private String responseBody;

    @Column(name = "response_status")
    @ApiModelProperty(value = "响应状态")
    private String responseStatus;

    @Column(name = "interface_type")
    @ApiModelProperty(value = "接口类型(0:默认)")
    private int interfaceType;

    @Column(name = "host")
    @ApiModelProperty(value = "主机")
    private String host;

    @Column(name = "username")
    @ApiModelProperty(value = "用户名")
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String username;

    @Column(name = "create_time")
    @DateTimeFormat(pattern = DateHelper.DATATIMEF_STR)
    @JsonFormat(pattern = DateHelper.DATATIMEF_STR, timezone = "GMT+8")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @Column(name = "req_id")
    @ApiModelProperty(value = "请求序列号")
    private String reqId;

    @Column(name="resp_id")
    @ApiModelProperty(value = "返回序列号")
    private String respId;

    @Column(name="error_msg")
    @ApiModelProperty(value="错误日志")
    private String errorMsg;



}
