package org.dppc.admin.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/7/27.
 * @时间： 11:41.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "SYS_ROLE")
@Entity
public class Role implements Serializable {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)

    @Column(name = "ROLE_ID")
    @PredicateInfo
    @ApiModelProperty(value = "主键id")
    private Long roleId;
    /**
     * 角色名
     */
    @Column(name = "ROLE_NAME", length = 20)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    @ApiModelProperty(value = "角色名")
    private String roleName;
    /**
     * 角色描述
     */
    @Column(name = "DESCRIPTION", length = 100)
    @ApiModelProperty(value = "角色描述")
    private String description;

    @Column(name = "STATE",length = 2)
    @ApiModelProperty(value = "2")
    private Integer state;
    /**
     * 多对多关系映射   角色对应的资源列表
     */
    @JsonIgnore
    @ApiModelProperty(value = "多对多关系映射   角色对应的资源列表")
    @ManyToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinTable(name = "SYS_ROLE_RESOURCE", joinColumns = {@JoinColumn(name = "ROLE_ID", referencedColumnName = "ROLE_ID")},
            inverseJoinColumns = {@JoinColumn(name = "RESOURCE_ID", referencedColumnName = "RESOURCE_ID")})
    private List<Resource> resources;

  /* cms废除 *//**
     * 多对多关系映射   角色对应的栏目列表
     *//*
    @JsonIgnore
    @ApiModelProperty(value = "多对多关系映射   角色对应的栏目列表")
    @ManyToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinTable(name = "SYS_ROLE_COLUMNS", joinColumns = {@JoinColumn(name = "ROLE_ID", referencedColumnName = "ROLE_ID")},
            inverseJoinColumns = {@JoinColumn(name = "COLUMNS_ID", referencedColumnName = "COLUMNS_ID")})
    private Set<Columns> columns;*/
}
