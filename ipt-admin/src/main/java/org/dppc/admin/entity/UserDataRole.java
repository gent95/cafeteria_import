package org.dppc.admin.entity;

import java.io.Serializable;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

/**
 * @描述 : 用户数据角色表
 * @作者 : Lu Xinwei
 * @日期 : 2018/04/02
 * @时间 : 10:00
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "sys_user_data_role")
public class UserDataRole implements Serializable {

    @Id
    @Column(name = "user_id")
    @ApiModelProperty(value = "userId")
    /** Lu Xinwei 2018/04/02  */
    private Long userId;
    @Column(name = "data_role_id")
    @ApiModelProperty(value = "dataRoleId")
    /** Lu Xinwei 2018/04/02  */
    private Long dataRoleId;

}