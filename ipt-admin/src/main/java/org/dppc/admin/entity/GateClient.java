package org.dppc.admin.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/12/27
 * @时间 ：9:40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "SYS_GATE_CLIENT")
public class GateClient implements Serializable{
    /**
     * 主键ID
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)

    @Column(name = "CLIENT_ID")
    @ApiModelProperty(value = "主键ID")
    private Long clientId;
    /**
     * 节点
     */
    @Column(name = "CODE",length = 30)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    @ApiModelProperty(value = "节点")
    private String code;
    /**
     * 密钥
     */
    @Column(name = "SECRET",length = 50)
    @ApiModelProperty(value = "密钥")
    private String secret;
    /**
     * 名称
     */
    @Column(name = "NAME",length = 30)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    @ApiModelProperty(value = "名称")
    private String name;
    /**
     * 
     */
    @Column(name = "LOCKED",length = 30)
    private String locked = "0";
    /**
     * 描述
     */
    @Column(name = "DESCRIPTION",length = 30)
    private String description;

    @Column(name = "CRT_TIME")
    private Date crtTime;

    @Column(name = "CRT_USER",length = 30)
    private String crtUser;

    @Column(name = "CRT_NAME",length = 30)
    private String crtName;

    @Column(name = "CRT_HOST",length = 30)
    private String crtHost;

    @Column(name = "UPD_TIME")
    private Date updTime;

    @Column(name = "UPD_USER",length = 30)
    private String updUser;

    @Column(name = "UPD_NAME",length = 30)
    private String updName;

    @Column(name = "UPD_HOST",length = 30)
    private String updHost;

}