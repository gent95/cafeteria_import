
package org.dppc.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.common.enums.LogTypeEnum;
import org.dppc.common.enums.MethodEnum;
import org.dppc.common.utils.DateHelper;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * @描述 ： 日志
 * @作者 ： 颜齐
 * @日期 ： 2017/10/25.
 * @时间 ： 16:23.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "SYS_LOG_INFO")
@Entity
public class LogInfo {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)

    @Column(name = "LOG_INFO_ID", length = 19)
    private Long logInfoId;
    /**
     * 操作类型 添加 修改 删除 登录 退出 其他
     */
    @PredicateInfo
    @Column(name = "OPT", length = 30)
    private String opt;
    /**
     * 日志内容
     */
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    @Column(name = "MSG", length = 500)
    private String msg;
    /**
     * 访问路径
     */
    @Column(name = "URL", length = 500)
    private String url;
    /**
     * 操作用户
     */
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    @Column(name = "USERNAME", length = 100)
    private String username;
    /**
     * 创建时间 yyyy-MM-dd HH:mm:ss
     */
    @DateTimeFormat(pattern = DateHelper.DATATIMEF_STR)
    @JsonFormat(pattern = DateHelper.DATATIMEF_STR)
    @Column(name = "CREATE_TIME", length = 7)
    private Date createTime;
    /**
     * 主机（访问者）ip
     */
    @Column(name = "HOST", length = 200)
    private String host;
    /**
     * 日志类型 用户操作 系统日志
     */
    @PredicateInfo
    @Enumerated(value = EnumType.STRING)
    @Column(name = "LOG_TYPE", length = 30)
    private LogTypeEnum type;
    /**
     * 方法类型 GET POST PUT PATCH DELETE
     */
    @PredicateInfo
    @Enumerated(value = EnumType.STRING)
    @Column(name = "LOG_METHOD", length = 30)
    private MethodEnum method;
    /**
     * 模块
     */
    @Column(name = "MODULE", length = 100)
    private String module;

    /**
     * 处理时间
     */
    @Column(name = "PROCESS_TIME")
    private Long processTime;

}
