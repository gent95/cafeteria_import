package org.dppc.admin.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/12/27
 * @时间 ：9:40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "SYS_GATE_CLIENT_SERVICE")
public class GateClientService implements Serializable{
    /**
     * 主键ID
     */
    @Id

    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;
    /**
     * 服务端ID
     */
    @Column(name="SERVICE_ID")
    private Long serviceId;
    /**
     * 客户端ID
     */
    @Column(name="CLIENT_ID")
    private Long clientId;
    /**
     * 描述
     */
    @Column(name="DESCRIPTION")
    private String description;

}
