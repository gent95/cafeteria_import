package org.dppc.admin.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @描述 :  用户角色关联信息（一对多）
 * @作者 :	lhw
 * @日期 :	2018/6/26
 * @时间 :	15:29
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "sys_user_node")
public class UserNode implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "编号")
    private Long id;
    /**
     * 用户名   与sys_user相关联
     */
    @Column(name = "username")
    @ApiModelProperty(value = "用户名")
    private String username;
    /**
     * 统一社会信用代码    与trace_base_node相关联
     */
    @Column(name = "social_credit_code", length = 18)
    @ApiModelProperty(value = "统一社会信用代码")
    private String socialCreditCode;

 /*   @OneToMany(fetch=FetchType.EAGER)
    @JoinColumn(name="username",referencedColumnName="username")
    private List<User> users;*/

    public UserNode(String username, String socialCreditCode) {
        this.username = username;
        this.socialCreditCode = socialCreditCode;
    }

}
