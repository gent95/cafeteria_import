package org.dppc.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.common.utils.DateHelper;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @描述 :   数据传输异常记录表
 * @作者 :   GAOJ
 * @日期 :   2018/4/16 0027
 * @时间 :   10:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "SYS_LOG_DATA_TRANSPORT")
@Entity
public class LogDataTransport implements Serializable {

    /**
     * 异常ID
     */
    @ApiModelProperty(value = "异常ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "EXCEPTION_ID")
    private Long exceptionId;
    @DateTimeFormat(pattern = DateHelper.DATATIMEF_STR)
    @JsonFormat(pattern = DateHelper.DATATIMEF_STR)
    @Column(name = "CREATE_TIME")
    private Date createTime;
    /**
     * 异常类型 DataTransportExceptionTypeEnum
     */
    @ApiModelProperty(value = "异常类型")
    @Column(name = "TYPE")
    @PredicateInfo
    private Integer type;

    /**
     * 异常表 TraceTableEnum
     */
    @ApiModelProperty(value = "异常表")
    @Column(name = "TRACE_TABLE_NAME", length = 50)
    @PredicateInfo
    private String traceTableName;

    /**
     * 异常ids(异常类型为 0号则此字段值无意义，对应nx_ipt_interface_dev库的接口数据id)
     */
    @ApiModelProperty(value = "异常ids")
    @Column(name = "IDS", length = 100)
    @PredicateInfo
    private String ids;

    /**
     * 异常数据
     */
    @ApiModelProperty(value = "异常数据")
    @Column(name = "DATA")
    private String data;

    /**
     * 报错信息
     */
    @ApiModelProperty(value = "报错信息")
    @Column(name = "EXCEPTION_INFO")
    private String exceptionInfo;

    /**
     * 错误是否解决(枚举) YesOrNoEnum
     */
    @ApiModelProperty(value = "异常是否解决")
    @Column(name = "IS_SOLVE")
    @PredicateInfo
    private Integer isSolve;

}
