package org.dppc.admin.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @描述 ： 系统配置信息
 * @作者 ：wyl
 * @日期 ：2017/11/30
 * @时间 ：16:48
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "SYS_SYSTEM_CONFIG")
@Entity
public class SystemConfig implements Serializable {

    /**
     * 主键ID
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)

    @Column(name = "CONFIG_ID", length = 10)
    private Long configId;
    /**
     * key
     */
    @Column(name = "CONFIG_KEY", length = 100)
    private String configKey;
    /**
     * value
     */
    @Column(name = "CONFIG_VALUE", length = 100)
    private String configValue;
    /**
     * 描述
     */
    @Column(name = "CONFIG_DESCRIPTION", length = 200)
    private String configDescription;
}
