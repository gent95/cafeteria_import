package org.dppc.admin.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @描述 :
 * @作者 :	zhumh
 * @日期 :	2019/6/2
 * @时间 :	16:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "sys_school_cafteria")
@Entity
public class SchoolCafteria implements Serializable{

    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "school_cafteria_id")
    @PredicateInfo
    @ApiModelProperty(value = "主键id")
    private Long schoolCafteriaId;

    /**
     * 用户id
     */
  /*  @Id
    @Column(name = "USER_ID")
    @PredicateInfo
    @ApiModelProperty(value = "用户id")
    private Long userId;*/

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    private String schoolCode;

    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;

    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;
    /**
     * 学校名称
     */
    @Column(name = "school_name", length = 50)
    private String schoolName;
    /**
     * 省级编码
     */
    @Column(name = "province_code", length = 50)
    private String provinceCode;
//    /**
//     * 省级名称
//     */
//    @Column(name = "province_name", length = 50)
//    private String provinceName;
    /**
     * 市级编码
     */
    @Column(name = "city_code", length = 50)
    private String cityCode;
//    /**
//     * 市级名称
//     */
//    @Column(name = "city_name", length = 50)
//    private String cityName;
    /**
     * 县级编码
     */
    @Column(name = "area_code", length = 50)
    private String areaCode;
//    /**
//     * 县级名称
//     */
//    @Column(name = "area_name", length = 50)
//    private String areaName;
    /**
     * 关系（用户执行所有操作）
     */
//    @ManyToOne(cascade = {CascadeType.REFRESH,CascadeType.PERSIST}, fetch = FetchType.LAZY, optional=false)
//    @JoinColumn(name="user_id")
//    @JsonIgnore
//    private User user;
}
