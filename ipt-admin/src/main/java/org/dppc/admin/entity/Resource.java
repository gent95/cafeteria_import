package org.dppc.admin.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.common.enums.MethodEnum;
import org.dppc.common.enums.ResourceTypeEnum;
import org.dppc.common.enums.YesOrNoEnum;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/7/27.
 * @时间： 11:41.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "SYS_RESOURCE")
@Entity
public class Resource implements Serializable {
    /**
     * 主键id
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)

    @Column(name = "RESOURCE_ID", length = 19)
    @PredicateInfo
    @ApiModelProperty(value = "主键id")
    private Long resourceId;
    /**
     * 父资源id
     */
    @Column(name = "PARENT_ID", length = 19)
    @PredicateInfo
    @ApiModelProperty(value = "父资源id")
    private Long parentId;
    /**
     * 资源名称
     */
    @Column(name = "RESOURCE_NAME", length = 100)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    @ApiModelProperty(value = "资源名称")
    private String resourceName;
    /**
     * 访问路径
     */
    @Column(name = "RESOURCE_URL", length = 200)
    @ApiModelProperty(value = "访问路径")
    private String resourceUrl;
    /**
     * 菜单类型  菜单：MENU   按钮：BUTTON
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "TYPE", length = 10)
    @PredicateInfo
    @ApiModelProperty(value = "菜单类型 MENU  BUTTON")
    private ResourceTypeEnum type;
    /**
     * 允许访问路径 多个路径 逗号分隔
     */
    @Column(name = "PERMISSION", length = 2000)
    @ApiModelProperty(value = "允许访问路径")
    private String permission;
    /**
     * 方法 GET POST
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "METHOD", length = 10)
    @PredicateInfo
    @ApiModelProperty(value = "方法 GET POST")
    private MethodEnum method;

    /**
     * 排序 菜单资源升序排列
     */
    @Column(name = "SEQUENCE", length = 10)
    @ApiModelProperty(value = "排序 菜单资源升序排列")
    private Integer sequence;

    /**
     * 是否启用 1：启用   0：禁用
     */
    @Column(name = "ENABLE", length = 1)
    @PredicateInfo
    @ApiModelProperty(value = "是否启用 1：启用   0：禁用")
    private Integer enable;
    /**
     * 是否开启权限过滤 YES NO
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "FILTER", length = 10)
    @PredicateInfo
    @ApiModelProperty(value = "是否开启权限过滤 YES NO")
    private YesOrNoEnum filter;
    /**
     * 图标
     */
    @Column(name = "ICON", length = 50)
    @ApiModelProperty(value = "图标")
    private String icon;

    /**
     * 0平台资源，1门户后台管理资源
     */
    @Column(name = "PLATFORM_TYPE", length = 1)
    @PredicateInfo
    @ApiModelProperty(value = "0平台资源，1门户后台管理资源")
    private Integer platformType;

}
