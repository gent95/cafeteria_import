package org.dppc.admin.service.impl;

import org.dppc.admin.entity.DataRole;
import org.dppc.admin.repository.DataRoleRepository;
import org.dppc.admin.service.DateRoleService;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @描述 :
 * @作者 :	lyf
 * @日期 :	2018/5/9
 * @时间 :	16:42
 */
@Service
@Transactional
public class DateRoleServiceImpl extends BaseServiceImpl<DataRole, Long> implements DateRoleService {

    @Autowired
    private DataRoleRepository dataRoleRepository;

    @Override
    public DataRole addDataRole(DataRole dataRole) {
        return dataRoleRepository.save(dataRole);
    }
}
