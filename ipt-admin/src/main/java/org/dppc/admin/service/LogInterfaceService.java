package org.dppc.admin.service;

import org.dppc.admin.entity.LogInterface;
import org.dppc.admin.vo.LogInterfaceVO;
import org.dppc.dbexpand.service.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/25.
 * @时间： 16:45.
 */
public interface LogInterfaceService extends BaseService<LogInterface,Long>{

    List<Double> quertResponseTime(Integer days);

    List<Map> quertMonthCount(String state, Integer days);

    List<Double> quertErrorRate(Integer days);

    Page<LogInterface> findPageList(LogInterfaceVO query, Pageable pageable);

    List<Map> requiredList(String state, Integer days);
}
