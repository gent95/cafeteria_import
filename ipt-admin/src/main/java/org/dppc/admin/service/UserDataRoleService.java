package org.dppc.admin.service;

import org.dppc.admin.entity.UserDataRole;

public interface UserDataRoleService {


    UserDataRole findOneByUserId(Long userId);

    UserDataRole addUserDataRole(UserDataRole userDataRole);
}
