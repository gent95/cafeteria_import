package org.dppc.admin.service;

import org.dppc.admin.entity.GateClient;
import org.dppc.admin.vo.GateClientVO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/12/27
 * @时间 ：9:54
 */
public interface GateClientService {

    /**
     * @描述 ：服务端分页列表
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：10:02
     */
    Page<GateClientVO> findClientServicePage(GateClient query, Pageable pageable);

    /**
     * @描述 ：根据客户端id集合查询集合
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：10:56
     */
    List<GateClient> findByClientIdCollection(Collection<Long> collection);

    /**
     * @描述 ：根据id查询客户端
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：11:41
     */
    GateClient findGateClientByClientId(Long clientId);

    /**
     * @描述 ：修改密钥
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：11:46
     */
    GateClient modifySecret(GateClient gateClient);

    /**
     * @描述 ：根据id删除客户端
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：11:50
     */
    GateClient deleteClientService(Long clientId);

    /**
     * @描述 ：根据客户服务端id获取其他客户服务端列表
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：14:09
     */
    List<GateClient> findClientServiceList(Long clientId);
    
    /**
     * @描述 ：查询服务端VO
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：14:17
     */
    GateClientVO findServiceVOByClientId(Long clientId);

    /**
     * @描述 ：修改服务端具有的客户端列表
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：15:11
     */
    GateClient modifyServiceClientList(GateClientVO gateClientVO);

    /**
     * @描述 ：批量删除客户端
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：17:26
     */
    List<GateClient> deleteClientServiceList(List<Long> clientIdList);

    /**
     * @描述 ：客户端分页列表
     * @作者 ：wyl
     * @日期 ：2017/12/28
     * @时间 ：9:42
     */
    Page<GateClientVO> findClientPage(GateClient query, Pageable pageable);

    /**
     * @描述 ：查询客户端VO
     * @作者 ：wyl
     * @日期 ：2017/12/28
     * @时间 ：9:56
     */
    GateClientVO findClientVOByClientId(Long clientId);

    /**
     * @描述 ：修改客户端具有的服务端列表
     * @作者 ：wyl
     * @日期 ：2017/12/28
     * @时间 ：10:06
     */
    GateClient modifyClientServiceList(GateClientVO gateClientVO);
}
