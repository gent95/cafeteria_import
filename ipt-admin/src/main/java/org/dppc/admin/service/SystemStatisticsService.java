package org.dppc.admin.service;

import java.util.List;
import java.util.Map;

public interface SystemStatisticsService {
    List<Map> requiredList(String state);

    List<Double> quertResponseTime(Integer days);
}
