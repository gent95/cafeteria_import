package org.dppc.admin.service.impl;

import org.dppc.admin.entity.GateClientService;
import org.dppc.admin.repository.GateClientServiceRepository;
import org.dppc.admin.service.GateClientServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/11/30
 * @时间 ：16:48
 */
@Service
@Transactional
public class GateClientServiceServiceImpl implements GateClientServiceService {

    @Autowired
    private GateClientServiceRepository gateClientServiceRepository;

    /**
     * @描述 ：根据服务端id查询客户端id集合
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：10:54
     */
    @Override
    public List<Long> findClientIdListByServiceId(Long serviceId) {
        List<GateClientService> gateClientServiceList = this.findServiceListByServiceId(serviceId);
        List<Long> clientIdList = new ArrayList<>();
        for (GateClientService gateClientService : gateClientServiceList) {
            clientIdList.add(gateClientService.getClientId());
        }
        return clientIdList;
    }

    /**
     * @描述 ：根据服务端id查询
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：11:07
     */
    @Override
    public List<GateClientService> findServiceListByServiceId(Long serviceId) {
        return gateClientServiceRepository.findByServiceId(serviceId);
    }

    /**
     * @描述 ：根据客户端id查询
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：13:16
     */
    @Override
    public List<GateClientService> findServiceListByClientId(Long clientId) {
        return gateClientServiceRepository.findByClientId(clientId);
    }

    /**
     * @描述 ：根据客户端id查询服务端id集合
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：13:16
     */
    @Override
    public List<Long> findServiceIdListByClientId(Long clientId) {
        List<GateClientService> gateClientServiceList = this.findServiceListByClientId(clientId);
        List<Long> serviceIdList = new ArrayList<>();
        for (GateClientService gateClientService : gateClientServiceList) {
            serviceIdList.add(gateClientService.getServiceId());
        }
        return serviceIdList;
    }

    /**
     * @描述 ：删除客户端-服务端关系
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：13:04
     */
    @Override
    public List<GateClientService> deleteGateClientService(Long clientId) {
        List<GateClientService> gateClientServiceList = gateClientServiceRepository.findByClientIdOrServiceId(clientId, clientId);
        gateClientServiceRepository.delete(gateClientServiceList);
        return gateClientServiceList;
    }

    /**
     * @描述 ：保存客户服务端集合
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：16:39
     */
    @Override
    public List<GateClientService> saveClientServiceList(List<GateClientService> clientServiceList) {
        return gateClientServiceRepository.save(clientServiceList);
    }

    /**
     * @描述 ：批量删除
     * @作者 ：wyl
     * @日期 ：2017/12/28
     * @时间 ：9:22
     */
    @Override
    public List<GateClientService> batchDeleteGateClientService(Collection<Long> collection) {
        List<GateClientService> gateClientServiceList = gateClientServiceRepository.findByClientIdInOrServiceIdIn(collection, collection);
        gateClientServiceRepository.delete(gateClientServiceList);
        return gateClientServiceList;
    }

}
