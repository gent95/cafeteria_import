package org.dppc.admin.service.impl;

import org.dppc.admin.entity.LogDataTransport;
import org.dppc.admin.repository.LogDataTransportRepository;
import org.dppc.admin.service.LogDataTransportService;
import org.dppc.common.enums.YesOrNoEnum;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @描述 :   数据交换异常
 * @作者 :   GAOJ
 * @日期 :   2018/4/16 0027
 * @时间 :   11:43
 */
@Service
@Transactional
public class LogDataTransportServiceImpl extends BaseServiceImpl<LogDataTransport, Long> implements LogDataTransportService {

    @Autowired
    private LogDataTransportRepository logDataTransportRepository;


    @Override
    public List<LogDataTransport> getLogDataTransportAllBySolve() {
        return logDataTransportRepository.findByIsSolve(YesOrNoEnum.NO.getValue());
    }
}
