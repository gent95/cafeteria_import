package org.dppc.admin.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.dppc.admin.entity.*;
import org.dppc.admin.redis.ResourceRedis;
import org.dppc.admin.redis.UserRedis;
import org.dppc.admin.repository.SchoolCafteriaRepository;
import org.dppc.admin.repository.UserNodeRepository;
import org.dppc.admin.repository.UserRepository;
import org.dppc.admin.service.*;
import org.dppc.admin.vo.UserVO;
import org.dppc.api.cmsHome.RegisterVO;
import org.dppc.common.constant.UserConstant;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.enums.EnableEnum;
import org.dppc.common.enums.UserTypeEnum;
import org.dppc.common.exception.MessageException;
import org.dppc.common.utils.NullHelper;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.dppc.dbexpand.annotation.support.PredicateInfoCover;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.dppc.dbexpand.util.BeanHelp;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.*;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/18.
 * @时间： 13:29.
 */
@Service
@Transactional
public class UserServiceImpl extends BaseServiceImpl<User, Long> implements UserService {
    private static final String keyHead = "admin:user:";
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRedis userRedis;
    @Autowired
    private ResourceRedis resourceRedis;
    @Autowired
    private RoleService roleService;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private DateRoleService dateRoleService;
    @Autowired
    private UserDataRoleService userDataRoleService;
    @Autowired
    private UserNodeRepository userNodeRepository;
    @Autowired
    private SchoolCafteriaRepository schoolCafteriaRepository;

    /**
     * 根据用户名称查询用户
     */
//    @Cacheable(value = keyHead, keyGenerator = "simpleKey")
    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsernameAndEnable(username, EnableEnum.ENABLE.getValue());
    }

    @Override
    public List<Resource> findPermissionByUsername(String username) {
        List<Resource> resourceList = null;
        try {
            if (resourceList != null)
                return resourceList;
            User user = findByUsername(username);
            List<Role> roles = user.getRoles();
            if (roles == null || roles.size() == 0) {
                return null;
            }
            Map<String, Resource> resourceMap = new HashMap<>();
            String key;
            for (Role role : roles) {
                if (role.getResources() == null || role.getResources().size() == 0)
                    continue;
                for (Resource resource : role.getResources()) {
                    key = resource.getPermission() + "|" + resource.getMethod();
                    if (StringUtils.isEmpty(resource.getPermission())
                            || resourceMap.containsKey(key))
                        continue;
                    resourceMap.put(key, resource);
                }
            }
            if (resourceMap.isEmpty())
                return null;
            resourceList = new ArrayList<>();
            resourceList.addAll(resourceMap.values());
            resourceRedis.add(keyHead + "findPermissionByUsername:" + username, 30L, resourceList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resourceList;
    }

    /**
     * 保存新添加的用户
     */
//    @CachePut(value = keyHead, keyGenerator = "objectId")
    @Override
    public User saveUser(UserVO userVO) {
        User user = new User();
        BeanUtils.copyProperties(userVO, user);
        List<String> enterpriseIdList;
        List<Long> roleIdList = new ArrayList<>();
        List<Role> roleList = new ArrayList<>();
        Role role = null;
        for(Long roleId : userVO.getRoleIdList()){ // 存放角色
            role = new Role();
            role.setRoleId(roleId);
            roleList.add(role);
        }
        user.setRoles(roleList);
        user.setDepartmentId(userVO.getDepartmentId());
        /*if (UserTypeEnum.INTERFACE_USER.getValue() == userVO.getUserType()) { //云平台用户
            user.setSchoolCafterias(userVO.getSchoolCafterias());
        }*/
        User validate = new User();
        validate.setUsername(userVO.getUsername());
        if (this.usernameValidate(validate) > 0L) {
            throw new MessageException(MessageFormat.format("用户名[{0}]已存在", validate.getUsername()));
        }
        String password = new BCryptPasswordEncoder(UserConstant.PW_ENCORDER_SALT).encode(user.getPassword().trim());
        user.setPassword(password);
        user.setEnable(EnableEnum.ENABLE.getValue());
        //------------------添加数据角色-------------------
        User ue = userRepository.save(user);
        DataRole dataRole = new DataRole();
        dataRole.setRoleName(userVO.getUsername());
        dataRole.setDescription(userVO.getName());
        dataRole.setDefaultUserId(ue.getUserId());
        DataRole dr = dateRoleService.save(dataRole);
        UserDataRole userDataRole = new UserDataRole();
        userDataRole.setUserId(ue.getUserId());
        userDataRole.setDataRoleId(dr.getRoleId());
        userDataRoleService.addUserDataRole(userDataRole);
        return ue;
    }

    /**
     * @描述 ：查询用户分页列表
     * @作者 ：wyl
     * @日期 ：2017/10/24
     * @时间 ：13:35
     */
    @Override
    public Page<UserVO> findUserPage(User query, Pageable pageable) {
        Page<User> page = userRepository.findAll((root, criteriaQuery, criteriaBuilder) -> BeanHelp.getPredicate(root, query, criteriaBuilder), pageable);
        return this.pageEntityToVO(page, pageable, query);
    }

    /**
     * @描述 ：page<entity> -》 page<VO>
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：14:54
     */
    private Page<UserVO> pageEntityToVO(Page<User> page, Pageable pageable, User query) {
        List<UserVO> userVOList = new ArrayList<>();
        List<User> userList = page.getContent();
        Set<Long> departmentIdSet = new HashSet<>();
        for (User user : userList) {
            if (!NullHelper.isNull(user.getDepartmentId())) departmentIdSet.add(user.getDepartmentId());
        }
        Map<Long, Department> departmentMap = departmentIdSet.size() > 0 ? departmentService.findDepartmentMap(departmentIdSet) : new HashMap<>();
        for (User user : userList) {
            UserVO userVO = UserVO.entityToVO(user);
            Department department = departmentMap.get(userVO.getDepartmentId());
            userVO.setDepartmentName(department != null ? department.getDepartmentName() : "");
            userVOList.add(userVO);
        }
        return new PageImpl<>(userVOList, pageable, page.getTotalElements());
    }

    /**
     * @描述 ：保存修改后的用户
     * @作者 ：wyl
     * @日期 ：2017/10/25
     * @时间 ：13:48
     */
//    @CachePut(value = keyHead, keyGenerator = "objectId")
    @Override
    public User modifyUser(UserVO userVO) {
        User oldUser = userRepository.findOne(userVO.getUserId());
        if (oldUser == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", userVO.getUserId()));
        }
        User newUser = new User();
        List<Role> roleList = new ArrayList<>();
        BeanUtils.copyProperties(userVO, newUser);
        Role role = null;
        for(Long roleId : userVO.getRoleIdList()){ // 存放角色
            role = new Role();
            role.setRoleId(roleId);
            roleList.add(role);
        }
        newUser.setRoles(roleList);
        /** lhw 2018/6/27 15:15  修改用户绑定的节点集合  先删除之前绑定的数据，再重新绑定  */
        //删除之前数据
        userNodeRepository.delete(userNodeRepository.findByUsername(userVO.getUsername()));
        List<UserNode> list = new ArrayList<>();
      /*  if (userVO.getEnterpriseIdList() != null) {
            for (String str :
                    userVO.getEnterpriseIdList()) {
                UserNode userNode = new UserNode(userVO.getUsername(),str);
                list.add(userNode);
            }
        }*/
        //进行封装保存
        userNodeRepository.save(list);
        schoolCafteriaRepository.delete(oldUser.getSchoolCafterias());
        User u  = BeanHelp.updateEntityExceptEmptyProps(oldUser, newUser);
        return userRepository.saveAndFlush(u);
    }

    /**
     * @描述 ：禁用用户
     * @作者 ：wyl
     * @日期 ：2017/10/25
     * @时间 ：13:55
     */
//    @CacheEvict(value = keyHead, keyGenerator = "simpleKey")
    @Override
    public User disableUser(Long userId) {
        User user = userRepository.findOne(userId);
        if (user == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", userId));
        }
        user.setEnable(EnableEnum.DISABLE.getValue());
        return userRepository.saveAndFlush(user);
    }

    /**
     * @描述 ：用户名验证
     * @作者 ：wyl
     * @日期 ：2017/10/26
     * @时间 ：13:54
     */
    @Override
    public Long usernameValidate(User user) {
        return userRepository.count((root, criteriaQuery, criteriaBuilder) ->
                BeanHelp.getPredicate(root, user, criteriaBuilder
                        , new PredicateInfoCover("userId", PredicateInfo.QueryType.NOT_EQUAL)
                        , new PredicateInfoCover("username", PredicateInfo.QueryType.BASIC)));
    }

    /**
     * @描述 ：根据用户id查询用户信息
     * @作者 ：wyl
     * @日期 ：2017/10/26
     * @时间 ：14:27
     */
    @Override
    public User findUserByUserId(Long userId) {
        User user = userRepository.findOne(userId);
        if (user == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", userId));
        }
        return user;
    }

    /**
     * @描述 ：启用用户
     * @作者 ：wyl
     * @日期 ：2017/11/2
     * @时间 ：16:55
     */
    @Override
    public User enableUser(Long userId) {
        User user = userRepository.findOne(userId);
        if (user == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", userId));
        }
        user.setEnable(EnableEnum.ENABLE.getValue());
        return userRepository.saveAndFlush(user);
    }

    /**
     * @描述 ：通过用户ID查询集合
     * @作者 ：luxinwei
     * @日期 ：2017/11/13
     * @时间 ：15:00
     */
    @Override
    public Map<Long, User> findUserMap(Collection<Long> collection) {
        Map<Long, User> map = new HashMap<>();
        for (User user : userRepository.findByUserIdIn(collection)) {
            map.put(user.getUserId(), user);
        }
        return map;
    }

    /**
     * @描述 ：根据用户id集合查询用户
     * @作者 ：wyl
     * @日期 ：2017/12/13
     * @时间 ：11:47
     */
    @Override
    public List<User> findByUserIdCollect(Collection<Long> collection) {
        return userRepository.findByUserIdIn(collection);
    }

    /**
     * @描述 ：重置密码
     * @作者 ：wyl
     * @日期 ：2017/12/14
     * @时间 ：18:39
     */
    @Override
    public User resetPassword(Long userId,String passWord) {
        User user = this.findUserByUserId(userId);
        String password = new BCryptPasswordEncoder(UserConstant.PW_ENCORDER_SALT).encode(passWord.trim());
        user.setPassword(password);
        return userRepository.saveAndFlush(user);
    }

    /**
     * @描述 ：根据用户id查询VO
     * @作者 ：wyl
     * @日期 ：2017/12/28
     * @时间 ：9:16
     */
    @Override
    public UserVO findUserVOByUserId(Long userId) {
        User user = this.findUserByUserId(userId);
        UserVO userVO = UserVO.entityToVO(user);
        List<Role> roleList = user.getRoles();
        List<Long> roleIdList = new ArrayList<>();
        for (Role role : roleList) {
            if (!NullHelper.isNull(role.getRoleId()) && !roleIdList.contains(role.getRoleId())) {
                roleIdList.add(role.getRoleId());
            }
        }
        userVO.setRoleIdList(roleIdList);
        //查询用户的公司信息列表
        List<String> list = new ArrayList<>();
        List<UserNode> userNodes = userNodeRepository.findByUsername(userVO.getUsername());
        if(userNodes != null){
            for (UserNode userNode :
                    userNodes) {
                list.add(userNode.getSocialCreditCode());
            }
        }
//        userVO.setEnterpriseIdList(list);
        return userVO;
    }

    /**
     * @描述 ：门户 企业用户注册
     * @作者 ：gaoj
     * @日期 ：2018/5/25
     * @时间 ：9:53
     */
    @Override
    public String register(RegisterVO register) {
        try {
            User user = new User();
            user.setUsername(register.getUsername());
            user.setPassword(new BCryptPasswordEncoder(UserConstant.PW_ENCORDER_SALT).encode(register.getPassword()));
            user.setName(register.getCompName());
            user.setEnable(EnableEnum.ENABLE.getValue());
            user.setUserType(UserTypeEnum.INTERFACE_USER.getValue());
            user.setPhone(register.getPhone());
//            user.setEnterpriseId(register.getEnterpriseId());
            Role role = new Role();
            role.setRoleId(5l);
            user.setRoles(Collections.singletonList(role));
            userRepository.save(user);
            return "1";
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }
    }
}
