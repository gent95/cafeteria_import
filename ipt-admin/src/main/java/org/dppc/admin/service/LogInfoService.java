package org.dppc.admin.service;

import org.dppc.admin.entity.LogInfo;
import org.dppc.admin.vo.LogInfoVO;
import org.dppc.dbexpand.service.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/25.
 * @时间： 16:45.
 */
public interface LogInfoService extends BaseService<LogInfo,Long>{

    /**
     * @描述 ：用户操作日志分页列表
     * @作者 ：wyl
     * @日期 ：2017/12/20
     * @时间 ：10:23
     */
    Page<LogInfo> finUserOptLogPage(LogInfoVO query, Pageable pageable);

    /**
     * @描述 ：系统运行日志分页列表
     * @作者 ：wyl
     * @日期 ：2017/12/20
     * @时间 ：10:33
     */
    Page<LogInfo> finSystemRunLogPage(LogInfoVO query, Pageable pageable);
}
