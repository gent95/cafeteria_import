package org.dppc.admin.service;

import org.dppc.admin.entity.Role;
import org.dppc.admin.vo.RoleVO;
import org.dppc.dbexpand.service.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/18.
 * @时间： 13:24.
 */
public interface RoleService extends BaseService<Role, Long> {

    /**
     * @描述 ：查询角色分页列表
     * @作者 ：wyl
     * @日期 ：2017/10/24
     * @时间 ：14:11
     */
    Page<RoleVO> findRolePage(Role query, int pageNum, int pageSize);

    /**
     * @描述 ：权限管理分页列表
     * @作者 ：wyl
     * @日期 ：2017/12/14
     * @时间 ：10:51
     */
    Page<RoleVO> findPermissionManagePage(Role query, Pageable pageable);

    /**
     * @描述 ：添加角色 级联添加角色-资源关系
     * @作者 ：wyl
     * @日期 ：2017/10/24
     * @时间 ：14:14
     */
    Role addRole(Role role);

    /**
     * @描述 ：根据角色id删除角色（物理删除） 级联删除角色-资源关系
     * @作者 ：wyl
     * @日期 ：2017/10/24
     * @时间 ：14:24
     */
    void deleteRole(Long roleId);

    /**
     * @描述 ：修改角色信息
     * @作者 ：wyl
     * @日期 ：2017/10/24
     * @时间 ：14:35
     */
    Role modifyRole(Role role);

    /**
     * @描述 ：角色名称验证
     * @作者 ：wyl
     * @日期 ：2017/10/26
     * @时间 ：14:15
     */
    Long roleNameValidate(Role role);

    /**
     * @描述 ：根据角色id查询角色信息
     * @作者 ：wyl
     * @日期 ：2017/10/26
     * @时间 ：14:31
     */
    Role findRoleByRoleId(Long roleId);

    /**
     * @描述 ：根据state查询角色
     * @作者 ：wyl
     * @日期 ：2017/11/2
     * @时间 ：14:15
     */
    List<Role> findByStateList(Integer state);

    /**
     * @描述 ：根据角色id集合查询角色集合
     * @作者 ：wyl
     * @日期 ：2017/11/10
     * @时间 ：14:08
     */
    List<Role> findByRoleIdIn(Collection<Long> collection);

    /**
     * @描述 ：根据id查询VO
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：16:10
     */
    RoleVO findRoleVOByRoleId(Long roleId);

    /**
     * @描述 ：修改角色权限
     * @作者 ：wyl
     * @日期 ：2017/12/19
     * @时间 ：11:46
     */
    Role modifyRolePermission(RoleVO roleVO);


    Role findByState(Integer state);
}
