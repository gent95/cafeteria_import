package org.dppc.admin.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.dppc.admin.entity.LogInterface;
import org.dppc.admin.repository.LogInterfaceRepository;
import org.dppc.admin.service.LogInterfaceService;
import org.dppc.admin.vo.LogInterfaceVO;
import org.dppc.common.utils.DateHelper;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.dppc.dbexpand.util.BeanHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/25.
 * @时间： 16:46.
 */
@Service
@Transactional
public class LogInterfaceServiceImpl extends BaseServiceImpl<LogInterface, Long> implements LogInterfaceService {
    @Autowired
    public LogInterfaceRepository logInterfaceRepository;

    @Override
    public List<Double> quertResponseTime(Integer days) {
        String timeStart = DateHelper.date2Str(DateHelper.getThatDay(days), DateHelper.DATAFORMAT_STR);
        String timeEnd = DateHelper.getNowTime(DateHelper.DATAFORMAT_STR);
        List<Double> mlist = new ArrayList<>();
        List<String> listTime = collectLocalDates(timeStart, timeEnd);
        for (String item : listTime) {
            String sql = "SELECT cast(sum(t.process_time)/count(t.process_time) as DECIMAL(20,2)) as line FROM `sys_log_interface` t where t.create_time like '" + item + "%'";
            Map<String, Object> map = logInterfaceRepository.findOneMapByNativeSQL(sql);
            BigDecimal order = (BigDecimal) map.get("line");
            if (order != null) {
                mlist.add(order.doubleValue());
            } else {
                mlist.add(0D);
            }
        }
        return mlist;
    }

    @Override
    public List<Map> quertMonthCount(String state, Integer days) {
        if (days == null || days >= 0) {
            days = -29;
        }
        if (StringUtils.isEmpty(state)) {
            state = "1";
        }
        String timeStart = DateHelper.date2Str(DateHelper.getThatDay(days), DateHelper.DATAFORMAT_STR);
        List<Map> list;
        if (state.equals("1")) {
            String sqlUp = "SELECT t.biz_type as bizType,count(t.biz_type) as count FROM `sys_log_interface` t where t.create_time >= '" + timeStart + "' GROUP BY t.biz_type ";
            list = logInterfaceRepository.findMapListByNativeSQL(sqlUp);
        } else {
            String sqlDown = "SELECT t.biz_type as bizType,count(t.biz_type) as count FROM `sys_log_interface` t where t.create_time >= '" + timeStart + "' GROUP BY t.biz_type ";
            list = logInterfaceRepository.findMapListByNativeSQL(sqlDown);
        }
        return list;
    }

    @Override
    public List<Double> quertErrorRate(Integer days) {
        String timeStart = DateHelper.date2Str(DateHelper.getThatDay(days), DateHelper.DATAFORMAT_STR);
        String timeEnd = DateHelper.getNowTime(DateHelper.DATAFORMAT_STR);
        List<Double> list = new ArrayList<>();
        List<String> listTime = collectLocalDates(timeStart, timeEnd);
        for (String item : listTime) {
            String sql = "SELECT count(*) from `sys_log_interface` t where t.create_time like '" + item + "%'";
            Double total = Double.parseDouble(logInterfaceRepository.findOneByNativeSQL(sql).toString());
            sql += "and t.response_status!=200";
            Double error = Double.parseDouble(logInterfaceRepository.findOneByNativeSQL(sql).toString());
            if (total > 0 && error > 0) {
                list.add(formatDouble(error * 100 / total));
            } else {
                list.add(0.00D);
            }
        }
        return list;
    }

    @Override
    public Page<LogInterface> findPageList(LogInterfaceVO query, Pageable pageable) {
        return logInterfaceRepository.findAll((root, criteriaQuery, criteriaBuilder) -> {
            return BeanHelp.getPredicate(root, query, criteriaBuilder);
        }, pageable);
    }


    public static List<String> collectLocalDates(String timeStart, String timeEnd) {
        return collectLocalDates(LocalDate.parse(timeStart), LocalDate.parse(timeEnd));
    }

    public static List<String> collectLocalDates(LocalDate start, LocalDate end) {
        return Stream.iterate(start, localDate -> localDate.plusDays(1))
                .limit(ChronoUnit.DAYS.between(start, end) + 1)
                .map(LocalDate::toString)
                .collect(Collectors.toList());
    }

    public Double formatDouble(Double s) {
        BigDecimal b = new BigDecimal(s);
        return b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }


    @Override
    public List<Map> requiredList(String state, Integer days) {
        if(state==null){
            state="1";
        }
        String dataformatStr=null;
        if (state.equals("2")) {//统计每月的
            dataformatStr = DateHelper.YYYY_MM_DATAFORMAT_STR;
        }else if (state.equals("3")){//统计每个季度的
            String timeStart = DateHelper.date2Str(new Date(), DateHelper.DATAFORMAT_STR);
            String[] split = timeStart.split("-");
            String sqlUp=null;
           if(Integer.parseInt(split[1])>0&&Integer.parseInt(split[1])<4){
               sqlUp = "SELECT t.biz_type as bizType,count(t.biz_type) as count FROM `sys_log_interface` t where t.create_time BETWEEN '"+split[0]+"-01-01' AND '"+timeStart+"' GROUP BY t.biz_type ORDER BY count(t.biz_type) DESC;";
           }else if(Integer.parseInt(split[1])>3&&Integer.parseInt(split[1])<7){
               sqlUp = "SELECT t.biz_type as bizType,count(t.biz_type) as count FROM `sys_log_interface` t where t.create_time BETWEEN '"+split[0]+"-04-01' AND '"+timeStart+"' GROUP BY t.biz_type ORDER BY count(t.biz_type) DESC;";
           }else if(Integer.parseInt(split[1])>6&&Integer.parseInt(split[1])<10){
               sqlUp = "SELECT t.biz_type as bizType,count(t.biz_type) as count FROM `sys_log_interface` t where t.create_time BETWEEN '"+split[0]+"-07-01' AND '"+timeStart+"' GROUP BY t.biz_type ORDER BY count(t.biz_type) DESC;";
           }else{
               sqlUp = "SELECT t.biz_type as bizType,count(t.biz_type) as count FROM `sys_log_interface` t where t.create_time BETWEEN '"+split[0]+"-10-01' AND '"+timeStart+"' GROUP BY t.biz_type ORDER BY count(t.biz_type) DESC;";
           }
            List<Map> list = logInterfaceRepository.findMapListByNativeSQL(sqlUp);
            return list;
        }else if(state.equals("4")){//统计每年的.
           dataformatStr = "yyyy";
        }else {//统计每日的.
           dataformatStr = DateHelper.DATAFORMAT_STR;
        }
        String timeStart = DateHelper.date2Str(new Date(), dataformatStr);

        String sqlUp = "SELECT t.biz_type as bizType,count(t.biz_type) as count FROM `sys_log_interface` t where t.create_time like '" + timeStart + "%' GROUP BY t.biz_type ORDER BY count(t.biz_type) DESC";
        List<Map> list = logInterfaceRepository.findMapListByNativeSQL(sqlUp);

        return list;
    }
}
