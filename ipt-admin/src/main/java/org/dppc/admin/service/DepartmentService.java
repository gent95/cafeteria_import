package org.dppc.admin.service;

import org.dppc.admin.entity.Department;
import org.dppc.admin.entity.Role;
import org.dppc.admin.vo.DepartmentVO;
import org.dppc.admin.vo.RoleVO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
  * @描述 ：
  * @作者 ：wyl
  * @日期 ：2017/11/30
  * @时间 ：15:07
  */

public interface DepartmentService {


    /**
     * @描述 ：分页查询部门信息
     * @作者 ：lyf
     * @日期 ：2018/04/08
     * @时间 ：17:36
     */
    Page<DepartmentVO> findDepartmentPage(Department query, int pageNum, int pageSize);
    /**
     * @描述 ：根据id删除部门信息
     * @作者 ：lyf
     * @日期 ：2018/04/08
     * @时间 ：17:36
     */
    void delDepartment(Long departmentId);

    /**
     * @描述 ：根据部门id集合查询部门map
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:07
     */
    Map<Long,Department> findDepartmentMap(Collection<Long> departmentIdSet);

    /**
     * @描述 ：查询部门列表 启用状态的
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:31
     */
    List<Department> getUserDepartmentList();

    /**
     * @描述 ：根据部门id查询部门信息
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:43
     */
    Department findDepartmentById(Long departmentId);

    /**
     * @描述 ：禁用部门
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:46
     */
    Department disableDepartment(Long departmentId);

    /**
     * @描述 ：启用部门
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:52
     */
    Department enableDepartment(Long departmentId);

    /**
     * @描述 ：部门名称验证
     * @作者 ：wyl
     * @日期 ：2017/12/1
     * @时间 ：13:47
     */
    Long departmentNameValidate(Department department);

    /**
     * @描述 ：添加子部门
     * @作者 ：wyl
     * @日期 ：2017/12/1
     * @时间 ：13:49
     */
    Department addDepartment(DepartmentVO departmentVO);

    /**
     * @描述 ：修改部门信息
     * @作者 ：wyl
     * @日期 ：2017/12/1
     * @时间 ：14:00
     */
    Department modifyDepartment(Department department);

    /**
     * @描述 ：查询所有二级部门
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：9:35
     */
    List<Department> getAllDepartment();

    /**
     * @描述 ：查询父级部门
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：10:38
     */
    Department findParentDepartment(Long parentDepartmentId);

    /**
    * @描述 ：查询所有一级部门
    * @作者 ：lxw
    * @日期 ：2018/4/17
    * @时间 ：13:54
    */
    List<Department> getAllDepartmentOne();
}
