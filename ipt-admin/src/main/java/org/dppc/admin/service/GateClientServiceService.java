package org.dppc.admin.service;

import org.dppc.admin.entity.GateClientService;

import java.util.Collection;
import java.util.List;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/12/27
 * @时间 ：9:54
 */
public interface GateClientServiceService {

    /**
     * @描述 ：根据服务端id查询客户端id集合
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：10:54
     */
    List<Long> findClientIdListByServiceId(Long serviceId);

    /**
     * @描述 ：根据服务端id查询
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：11:05
     */
    List<GateClientService> findServiceListByServiceId (Long serviceId);

    /**
     * @描述 ：根据客户端id查询服务端id集合
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：13:14
     */
    List<Long> findServiceIdListByClientId(Long clientId);

    /**
     * @描述 ：根据客户端id查询
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：13:15
     */
    List<GateClientService> findServiceListByClientId (Long clientId);

    /**
     * @描述 ：删除客户端-服务端关系
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：13:03
     */
    List<GateClientService> deleteGateClientService(Long clientId);

    /**
     * @描述 ：保存服务端集合
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：16:39
     */
    List<GateClientService> saveClientServiceList(List<GateClientService> clientServiceList);

    /**
     * @描述 ：批量删除
     * @作者 ：wyl
     * @日期 ：2017/12/28
     * @时间 ：9:22
     */
    List<GateClientService> batchDeleteGateClientService(Collection<Long> collection);
}
