package org.dppc.admin.service;


import org.dppc.admin.entity.SystemConfig;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/12/13
 * @时间 ：14:17
 */
public interface SystemConfigService {

    /**
     * @描述 ：添加配置
     * @作者 ：wyl
     * @日期 ：2017/12/13
     * @时间 ：14:00
     */
    SystemConfig addSystemConfig(SystemConfig systemConfig);

    /**
     * @描述 ：根据key查询
     * @作者 ：wyl
     * @日期 ：2017/12/13
     * @时间 ：14:05
     */
    SystemConfig findByConfigKey(String configKey);

    /**
     * @描述 ：根据id查询
     * @作者 ：wyl
     * @日期 ：2017/12/13
     * @时间 ：14:05
     */
    SystemConfig findByConfigId(Long configId);

    /**
     * @描述 ：保存修改后的配置 主键ID必须有值
     * @作者 ：wyl
     * @日期 ：2017/12/13
     * @时间 ：14:06
     */
    SystemConfig modifyConfigKeyById(SystemConfig systemConfig);

    /**
     * @描述 ：保存修改后的配置 key必须有值
     * @作者 ：wyl
     * @日期 ：2017/12/13
     * @时间 ：14:06
     */
    SystemConfig updateConfigKeyByKey(SystemConfig systemConfig);
}
