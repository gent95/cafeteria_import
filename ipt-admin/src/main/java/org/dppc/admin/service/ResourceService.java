package org.dppc.admin.service;

import org.dppc.admin.entity.Resource;
import org.dppc.admin.vo.ResourceTree;
import org.dppc.admin.vo.MenuVO;
import org.dppc.admin.vo.ResourceVO;
import org.dppc.common.enums.EnableEnum;
import org.dppc.common.enums.YesOrNoEnum;
import org.dppc.dbexpand.service.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/18.
 * @时间： 13:24.
 */
public interface ResourceService extends BaseService<Resource, Long> {

    /**
     * 查找过滤的资源
     *
     * @return
     */
    List<Resource> findFilterPermission();

    /**
     * 查找不过滤的资源
     *
     * @return
     */
    List<Resource> findNoFilterPermission();

    /**
     * @描述 ：资源数据分页列表
     * @作者 ：wyl
     * @日期 ：2017/10/26
     * @时间 ：13:24
     */
    Page<ResourceVO> findResourcePage(Resource resource, Pageable pageable);

    /**
     * @描述 ：启用资源
     * @作者 ：wyl
     * @日期 ：2017/10/27
     * @时间 ：9:19
     */
    Resource enableOrDisableResource(Long resourceId, EnableEnum enableEnum);

    /**
     * @描述 ：根据参数查询资源集合
     * @作者 ：wyl
     * @日期 ：2017/10/27
     * @时间 ：9:34
     */
    List<Resource> findResourceList(Resource resource);

    /**
     * @描述 ：查询父id下子资源列表
     * @作者 ：wyl
     * @日期 ：2017/10/31
     * @时间 ：11:16
     */
    List<Resource> findByParentId(Long parentId);

    /**
     * @描述 ：根据父资源获取子资源集合
     * @作者 ：wyl
     * @日期 ：2017/10/31
     * @时间 ：13:58
     */
    List<Resource> findChildResourceList(Resource parentResource);

    /**
     * @描述 ：根据资源父id、资源类型（MENU BUTTON）、禁用启用  用户权限下资源id集合 查询资源列表
     * @作者 ：wyl
     * @日期 ：2017/11/10
     * @时间 ：10:45
     */
    List<MenuVO> findByParentIdAndResourcesIdIn(Integer platformType, Long parentId, Set<Long> resourceIdSet);

    /**
     * @描述 ：根据资源id集合查询资源集合
     * @作者 ：wyl
     * @日期 ：2017/11/13
     * @时间 ：9:19
     */
    List<Resource> findByResourceIds(Collection<Long> resourcesIds);

    /**
     * @描述 ：所有资源 层级包含子资源
     * @作者 ：wyl
     * @日期 ：2017/11/13
     * @时间 ：14:38
     */
    List<ResourceVO> findResourceVOList();

    /**
     * @描述 ：角色对应的资源列表
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：15:14
     */
    List<ResourceTree> getResourceTreeList();
}
