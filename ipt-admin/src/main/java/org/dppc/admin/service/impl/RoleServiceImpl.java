package org.dppc.admin.service.impl;

import org.dppc.admin.entity.Resource;
import org.dppc.admin.entity.Role;
import org.dppc.admin.entity.User;
import org.dppc.admin.repository.RoleRepository;
import org.dppc.admin.service.ResourceService;
import org.dppc.admin.service.RoleService;
import org.dppc.admin.service.UserService;
import org.dppc.admin.vo.RoleVO;
import org.dppc.common.exception.MessageException;
import org.dppc.common.utils.NullHelper;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.dppc.dbexpand.annotation.support.PredicateInfoCover;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.dppc.dbexpand.util.BeanHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.*;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/18.
 * @时间： 13:28.
 */
@SuppressWarnings({"StringBufferMayBeStringBuilder", "unchecked", "MismatchedQueryAndUpdateOfStringBuilder", "StringConcatenationInLoop"})
@Service
@Transactional
public class RoleServiceImpl extends BaseServiceImpl<Role, Long> implements RoleService {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private ResourceService resourceService;
    @Autowired
    private UserService userService;

    public static final String keyHead = "admin:role:";

    /**
     * @描述 ：查询角色分页列表
     * @作者 ：wyl
     * @日期 ：2017/10/24
     * @时间 ：14:11
     */
    @Override
    public Page<RoleVO> findRolePage(Role query, int pageNum, int pageSize) {
        /** wyl 2017/12/13 11:53 角色分页列表 */
        StringBuffer listSql = new StringBuffer("SELECT ROLE_ID,ROLE_NAME,DESCRIPTION FROM sys_role WHERE 1=1");
        if (!NullHelper.isNull(query.getRoleName())) {
            listSql.append(" AND ROLE_NAME LIKE '%").append(query.getRoleName()).append("%'");
        }
        if (!NullHelper.isNull(query.getDescription())) {
            listSql.append(" AND DESCRIPTION LIKE '%").append(query.getDescription()).append("%'");
        }
        listSql.append(" ORDER BY ROLE_ID DESC LIMIT ").append((pageNum) * pageSize).append(",").append(pageSize);
        List<Object[]> objectArrayList = (List<Object[]>) roleRepository.findByNativeSQL(listSql.toString());
        List<RoleVO> roleVOList = new ArrayList<>();
        Set<Long> roleIdSet = new HashSet<>();
        for (Object[] objectArray : objectArrayList) {
            RoleVO roleVO = new RoleVO();
            if (!NullHelper.isNull(objectArray[0])) {
                roleVO.setRoleId(Long.parseLong(objectArray[0].toString()));
                roleIdSet.add(roleVO.getRoleId());
            }
            roleVO.setRoleName(NullHelper.isNull(objectArray[1]) ? "" : objectArray[1].toString());
            roleVO.setDescription(NullHelper.isNull(objectArray[2]) ? "" : objectArray[2].toString());
            roleVOList.add(roleVO);
        }
        /** wyl 2017/12/13 11:53 角色列表总记录数 */
        StringBuilder countSql = new StringBuilder("SELECT COUNT(1) FROM (SELECT ROLE_ID, ROLE_NAME, DESCRIPTION FROM sys_role WHERE 1=1");
        if (!NullHelper.isNull(query.getRoleName())) {
            listSql.append(" AND ROLE_NAME LIKE '%").append(query.getRoleName()).append("%'");
        }
        if (!NullHelper.isNull(query.getDescription())) {
            listSql.append(" AND DESCRIPTION LIKE '%").append(query.getDescription()).append("%'");
        }
        countSql.append(") A");
        Object object = roleRepository.findOneByNativeSQL(countSql.toString());
        Long totalElements = object == null ? 0L : Long.parseLong(object.toString());
        /** wyl 2017/12/13 12:16 角色拥有用户集 */
        Map<Long, List<User>> map = new HashMap<>();
        for (Long roleId : roleIdSet) {
            StringBuffer sql = new StringBuffer("SELECT USER_ID FROM sys_user_role WHERE 1=1");
            sql.append(" AND ROLE_ID = ").append(roleId);
            List<Object> objectList = roleRepository.findByNativeSQL(sql.toString());
            Set<Long> userIdSet = new HashSet<>();
            for (Object obj : objectList) {
                if (!NullHelper.isNull(obj)) userIdSet.add(Long.parseLong(obj.toString()));
            }
            List<User> userList = userIdSet.size() > 0 ? userService.findByUserIdCollect(userIdSet) : new ArrayList<>();
            map.put(roleId, userList);
        }
        for (RoleVO roleVO : roleVOList) {
            List<User> userList = map.get(roleVO.getRoleId());
            if (userList != null && userList.size() > 0) {
                String userNames = "";
                String userRealNames = "";
                for (User user : userList) {
                    userNames += user.getUsername() + ",";
                    userRealNames += user.getName() + ",";
                }
                if (!NullHelper.isNull(userNames)) {
                    userNames = userNames.substring(0, userNames.lastIndexOf(","));
                }
                if (!NullHelper.isNull(userRealNames)) {
                    userRealNames = userRealNames.substring(0, userRealNames.lastIndexOf(","));
                }
                roleVO.setUserNames(userNames);
                roleVO.setUserRealNames(userRealNames);
            }
        }
        return new PageImpl<>(roleVOList, new PageRequest(pageNum, pageSize), totalElements);
    }

    /**
     * @描述 ：权限管理分页列表
     * @作者 ：wyl
     * @日期 ：2017/12/14
     * @时间 ：10:51
     */
    @Override
    public Page<RoleVO> findPermissionManagePage(Role query, Pageable pageable) {
        Page<Role> page = roleRepository.findAll((root, criteriaQuery, criteriaBuilder) -> BeanHelp.getPredicate(root, query, criteriaBuilder), pageable);
        return this.pageEntityToVO(page, pageable, query);
    }

    /**
     * @描述 ：page<Entity> -> page<VO>
     * @作者 ：wyl
     * @日期 ：2017/12/14
     * @时间 ：10:56
     */
    private Page<RoleVO> pageEntityToVO(Page<Role> page, Pageable pageable, Role query) {
        List<RoleVO> roleVOList = new ArrayList<>();
        List<Role> roleList = page.getContent();
        for (Role role : roleList) {
            RoleVO roleVO = RoleVO.entityToVO(role);
            String resourceNames = "";
            for (Resource resource : roleVO.getResources()) {
                resourceNames += resource.getResourceName() + ",";
            }
            if (!NullHelper.isNull(resourceNames)) {
                resourceNames = resourceNames.substring(0, resourceNames.lastIndexOf(","));
            }
            roleVO.setResourceNames(resourceNames);
            roleVOList.add(roleVO);
        }
        return new PageImpl<>(roleVOList, pageable, page.getTotalElements());
    }

    /**
     * @描述 ：添加角色
     * @作者 ：wyl
     * @日期 ：2017/10/24
     * @时间 ：14:20
     */
//    @CachePut(value = keyHead,keyGenerator = "objectId")
    @Override
    public Role addRole(Role role) {
        Role validate = new Role();
        validate.setRoleName(role.getRoleName());
        if (this.roleNameValidate(validate) > 0L) {
            throw new MessageException(MessageFormat.format("角色名[{0}]已存在", validate.getRoleName()));
        }
        return roleRepository.save(role);
    }

    /**
     * @描述 ：根据角色id删除角色（物理删除） 级联删除 角色-资源 角色-用户 关系
     * @作者 ：wyl
     * @日期 ：2017/10/24
     * @时间 ：14:26
     */
//    @CacheEvict(value = keyHead,keyGenerator = "simpleKey")
    @Override
    public void deleteRole(Long roleId) {
        Role role = roleRepository.findOne(roleId);
        if (role == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", roleId));
        }
        roleRepository.delete(roleId);
    }

    /**
     * @描述 ：修改角色信息
     * @作者 ：wyl
     * @日期 ：2017/10/24
     * @时间 ：14:37
     */
//    @CachePut(value = keyHead,keyGenerator = "objectId")
    @Override
    public Role modifyRole(Role role) {
        Role info = roleRepository.findOne(role.getRoleId());
        if (info == null) {
            throw new MessageException(MessageFormat.format("未找到id未[{0}]的数据", role.getRoleId()));
        }
        Role validate = new Role();
        validate.setRoleId(role.getRoleId());
        validate.setRoleName(role.getRoleName());
        if (this.roleNameValidate(validate) > 0L) {
            throw new MessageException(MessageFormat.format("角色名称[{0}]已存在", validate.getRoleName()));
        }
        return roleRepository.saveAndFlush(BeanHelp.updateEntityExceptEmptyProps(info, role));
    }

    /**
     * @描述 ：角色名称验证
     * @作者 ：wyl
     * @日期 ：2017/10/26
     * @时间 ：14:15
     */
    @Override
    public Long roleNameValidate(Role role) {
        return roleRepository.count((root, criteriaQuery, criteriaBuilder) -> BeanHelp.getPredicate(root, role, criteriaBuilder
                , new PredicateInfoCover("roleId", PredicateInfo.QueryType.NOT_EQUAL)
                , new PredicateInfoCover("roleName", PredicateInfo.QueryType.BASIC)));
    }

    /**
     * @描述 ：根据角色id查询角色信息
     * @作者 ：wyl
     * @日期 ：2017/10/26
     * @时间 ：14:31
     */
//    @Cacheable(value = keyHead,keyGenerator = "simpleKey")
    @Override
    public Role findRoleByRoleId(Long roleId) {
        Role role = roleRepository.findOne(roleId);
        if (role == null) {
            throw new MessageException(MessageFormat.format("未找到id未[{0}]的数据", roleId));
        }
        return role;
    }

    /**
     * @描述 ：根据state查询角色
     * @作者 ：wyl
     * @日期 ：2017/11/2
     * @时间 ：14:19
     */
    @Override
    public List<Role> findByStateList(Integer state) {
        return roleRepository.findByState(state);
    }

    /**
     * @描述 ：根据角色id集合查询角色集合
     * @作者 ：wyl
     * @日期 ：2017/11/10
     * @时间 ：14:09
     */
    @Override
    public List<Role> findByRoleIdIn(Collection<Long> collection) {
        return roleRepository.findByRoleIdIn(collection);
    }

    /**
     * @描述 ：根据id查询VO
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：16:10
     */
    @Override
    public RoleVO findRoleVOByRoleId(Long roleId) {
        Role role = this.findRoleByRoleId(roleId);
        RoleVO roleVO = RoleVO.entityToVO(role);
        List<Resource> resourceList = role.getResources();
        List<Long> selectChildList = new ArrayList<>();
        for (Resource resource : resourceList) {
            List<Resource> resources = resourceService.findByParentId(resource.getResourceId());
            if (resources == null || resources.size() <= 0) {
                selectChildList.add(resource.getResourceId());
            }
        }
        roleVO.setSelectChildList(selectChildList);
        return roleVO;
    }

    /**
     * @描述 ：修改角色权限
     * @作者 ：wyl
     * @日期 ：2017/12/19
     * @时间 ：11:46
     */
    @Override
    public Role modifyRolePermission(RoleVO roleVO) {
        Role role = this.findRoleByRoleId(roleVO.getRoleId());
        List<Long> resourceIdList = roleVO.getSelectChildList();
        /** wyl 2017/12/19 14:02 查询所有的父级id */
        List<Resource> resourceList = new ArrayList<>();
        if (resourceIdList != null && resourceIdList.size() > 0) {
            resourceList = resourceService.findByResourceIds(resourceIdList);
            if (resourceList != null && resourceList.size() > 0) {
                /** wyl 2017/12/19 14:10 递归父级资源 */
                this.findAllParentResources(resourceList, resourceList);
            }
        }
        List<Resource> list = new ArrayList<>();
        if (resourceList != null && resourceList.size() > 0) {
            Set<Long> resourceIdSet = new HashSet<>();
            for (Resource resource : resourceList) {
                resourceIdSet.add(resource.getResourceId());
            }
            if (resourceIdSet.size() > 0) {
                list = resourceService.findByResourceIds(resourceIdSet);
            }
        }
        role.setResources(list);
        return roleRepository.saveAndFlush(role);
    }

    @Override
    public Role findByState(Integer state) {
        if (roleRepository.findByState(state).size() < 0) {
            throw new MessageException(MessageFormat.format("未找到state未[{0}]的数据", state));
        }
        return roleRepository.findByState(state).get(0);
    }

    private void findAllParentResources(List<Resource> resourceList, List<Resource> parentList) {
        List<Long> parentIdList = new ArrayList<>();
        for (Resource resource : parentList) {
            parentIdList.add(resource.getParentId());
        }
        if (parentIdList.size() > 0) {
            List<Resource> parentResourceList = resourceService.findByResourceIds(parentIdList);
            if (parentResourceList != null && parentResourceList.size() > 0) {
                resourceList.addAll(parentResourceList);
                this.findAllParentResources(resourceList, parentResourceList);
            }
        }
    }
}
