package org.dppc.admin.service.impl;

import org.dppc.admin.entity.LogInfo;
import org.dppc.admin.repository.LogInfoRepository;
import org.dppc.admin.service.LogInfoService;
import org.dppc.admin.vo.LogInfoVO;
import org.dppc.common.enums.LogTypeEnum;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.dppc.dbexpand.util.BeanHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/25.
 * @时间： 16:46.
 */
@Service
@Transactional
public class LogInfoServiceImpl extends BaseServiceImpl<LogInfo,Long> implements LogInfoService{
    @Autowired
    private LogInfoRepository logInfoRepository;

    /**
     * @描述 ：用户操作日志分页列表
     * @作者 ：wyl
     * @日期 ：2017/12/20
     * @时间 ：10:24
     */
    @Override
    public Page<LogInfo> finUserOptLogPage(LogInfoVO query, Pageable pageable) {
        query.setType(LogTypeEnum.USER);
        return logInfoRepository.findAll((root, criteriaQuery, criteriaBuilder) -> BeanHelp.getPredicate(root, query, criteriaBuilder), pageable);
    }

    /**
     * @描述 ：系统运行日志分页列表
     * @作者 ：wyl
     * @日期 ：2017/12/20
     * @时间 ：10:33
     */
    @Override
    public Page<LogInfo> finSystemRunLogPage(LogInfoVO query, Pageable pageable) {
        query.setType(LogTypeEnum.SYSTEM);
        return logInfoRepository.findAll((root, criteriaQuery, criteriaBuilder) -> BeanHelp.getPredicate(root, query, criteriaBuilder), pageable);
    }
}
