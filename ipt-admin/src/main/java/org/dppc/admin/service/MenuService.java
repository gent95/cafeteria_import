package org.dppc.admin.service;

import org.dppc.admin.entity.Resource;
import org.dppc.admin.vo.MenuVO;

import java.util.List;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/11/10
 * @时间 ：10:01
 */
public interface MenuService {
    /**
     * @描述 ：获取当前登录用户权限下的菜单
     * @作者 ：wyl
     * @日期 ：2017/11/10 
     * @时间 ：10:12
     */
    List<MenuVO> getUserMenuList();


    /**
     * @描述 ：当前用户 获取资源
     * @作者 ：gaoj
     * @日期 ：2018/3/30
     * @时间 ：15:14
     */
    List<Resource> getUserResource(String username);

}
