package org.dppc.admin.service.impl;

import org.dppc.admin.repository.LogInfoRepository;
import org.dppc.admin.service.SystemStatisticsService;
import org.dppc.common.utils.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class SystemStatisticsServiceImpl implements SystemStatisticsService {
    @Autowired
    private LogInfoRepository logInfoRepository;

    @Override
    public List<Map> requiredList(String state) {
        if(state==null){
            state="1";
        }
        String dataformatStr=null;
        if (state.equals("2")) {//统计每月的
            dataformatStr = DateHelper.YYYY_MM_DATAFORMAT_STR;
        }else if (state.equals("3")){//统计每个季度的
            String timeStart = DateHelper.date2Str(new Date(), DateHelper.DATAFORMAT_STR);
            String[] split = timeStart.split("-");
            String sqlUp=null;
            if(Integer.parseInt(split[1])>0&&Integer.parseInt(split[1])<4){
                sqlUp = "select count(s.msg) as count, s.msg as type from sys_log_info s WHERE s.create_time BETWEEN '"+split[0]+"-01-01' AND '"+timeStart+"' GROUP BY s.msg;";
            }else if(Integer.parseInt(split[1])>3&&Integer.parseInt(split[1])<7){
                sqlUp = "select count(s.msg) as count, s.msg as type from sys_log_info s WHERE s.create_time BETWEEN '"+split[0]+"-04-01' AND '"+timeStart+"' GROUP BY s.msg;";
            }else if(Integer.parseInt(split[1])>6&&Integer.parseInt(split[1])<10){
                sqlUp = "select count(s.msg) as count, s.msg as type from sys_log_info s WHERE s.create_time BETWEEN '"+split[0]+"-07-01' AND '"+timeStart+"' GROUP BY s.msg;";
            }else{
                sqlUp = "select count(s.msg) as count, s.msg as type from sys_log_info s WHERE s.create_time BETWEEN '"+split[0]+"-10-01' AND '"+timeStart+"' GROUP BY s.msg;";
            }
            List<Map> list = logInfoRepository.findMapListByNativeSQL(sqlUp);
            return list;
        }else if(state.equals("4")){//统计每年的.
            dataformatStr = "yyyy";
        }else {//统计每日的.
            dataformatStr = DateHelper.DATAFORMAT_STR;
        }
        String timeStart = DateHelper.date2Str(new Date(), dataformatStr);
        String sqlUp = "select count(s.msg) as count, s.msg as type from sys_log_info s WHERE s.create_time LIKE '"+timeStart+"%' GROUP BY s.msg";
        List<Map> list = logInfoRepository.findMapListByNativeSQL(sqlUp);
        return list;
    }

    @Override
    public List<Double> quertResponseTime(Integer days) {
        String timeStart = DateHelper.date2Str(DateHelper.getThatDay(days), DateHelper.DATAFORMAT_STR);
        String timeEnd = DateHelper.getNowTime(DateHelper.DATAFORMAT_STR);
        List<Double> mlist = new ArrayList<>();
        List<String> listTime = collectLocalDates(timeStart, timeEnd);
        for (String item : listTime) {
            String sql = "SELECT cast(sum(t.process_time)/count(t.process_time) as DECIMAL(20,2)) as line FROM `sys_log_info` t where t.create_time like '"+item+"%'";
            Map<String, Object> map = logInfoRepository.findOneMapByNativeSQL(sql);
            BigDecimal order = (BigDecimal) map.get("line");
            if (order != null) {
                mlist.add(order.doubleValue());
            } else {
                mlist.add(0D);
            }
        }
        return mlist;
    }

    public static List<String> collectLocalDates(String timeStart, String timeEnd) {
        return collectLocalDates(LocalDate.parse(timeStart), LocalDate.parse(timeEnd));
    }

    public static List<String> collectLocalDates(LocalDate start, LocalDate end) {
        return Stream.iterate(start, localDate -> localDate.plusDays(1))
                .limit(ChronoUnit.DAYS.between(start, end) + 1)
                .map(LocalDate::toString)
                .collect(Collectors.toList());
    }
}
