package org.dppc.admin.service;

import org.dppc.admin.entity.LogDataTransport;
import org.dppc.dbexpand.service.BaseService;

import java.util.List;

/**
 * @描述 :   数据交换异常
 * @作者 :   GAOJ
 * @日期 :   2018/4/16 0027
 * @时间 :   11:43
 */
public interface LogDataTransportService extends BaseService<LogDataTransport, Long> {

    /**
     * 获取所有数据交换异常状态为未解决
     * @return
     */
    List<LogDataTransport> getLogDataTransportAllBySolve();


}
