package org.dppc.admin.service.impl;

import org.dppc.admin.entity.GateClient;
import org.dppc.admin.repository.GateClientRepository;
import org.dppc.admin.service.GateClientService;
import org.dppc.admin.service.GateClientServiceService;
import org.dppc.admin.vo.GateClientVO;
import org.dppc.common.exception.MessageException;
import org.dppc.common.utils.NullHelper;
import org.dppc.dbexpand.util.BeanHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/11/30
 * @时间 ：16:48
 */
@Service
@Transactional
public class GateClientServiceImpl implements GateClientService {

    @Autowired
    private GateClientRepository gateClientRepository;
    @Autowired
    private GateClientServiceService gateClientServiceService;

    /**
     * @描述 ：服务端分页列表
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：10:02
     */
    @Override
    public Page<GateClientVO> findClientServicePage(GateClient query, Pageable pageable) {
        Page<GateClient> page = gateClientRepository.findAll((root, criteriaQuery, criteriaBuilder) -> BeanHelp.getPredicate(root, query, criteriaBuilder), pageable);
        return this.pageServiceEntityToVO(page, pageable, query);
    }

    /**
     * @描述 ：根据客户端id集合查询集合
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：10:58
     */
    @Override
    public List<GateClient> findByClientIdCollection(Collection<Long> collection) {
        return gateClientRepository.findAll(collection);
    }

    /**
     * @描述 ：根据id查询客户端
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：11:41
     */
    @Override
    public GateClient findGateClientByClientId(Long clientId) {
        GateClient gateClient = gateClientRepository.findOne(clientId);
        if (gateClient == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", clientId));
        }
        return gateClient;
    }

    /**
     * @描述 ：修改密钥
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：11:46
     */
    @Override
    public GateClient modifySecret(GateClient gateClient) {
        GateClient info = this.findGateClientByClientId(gateClient.getClientId());
        info.setSecret(gateClient.getSecret());
        return gateClientRepository.saveAndFlush(info);
    }

    /**
     * @描述 ：根据id删除客户端
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：11:50
     */
    @Override
    public GateClient deleteClientService(Long clientId) {
        GateClient gateClient = this.findGateClientByClientId(clientId);
        gateClientRepository.delete(gateClient);
        /** wyl 2017/12/27 13:25 删除客户端-服务端关系 */
        gateClientServiceService.deleteGateClientService(clientId);
        return gateClient;
    }

    /**
     * @描述 ：根据客户服务端id获取其他客户服务端列表
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：14:09
     */
    @Override
    public List<GateClient> findClientServiceList(Long clientId) {
        return gateClientRepository.findByClientIdNot(clientId);
    }

    /**
     * @描述 ：查询服务端VO
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：14:17
     */
    @Override
    public GateClientVO findServiceVOByClientId(Long clientId) {
        GateClient service = this.findGateClientByClientId(clientId);
        GateClientVO serviceVO = GateClientVO.entityToVO(service);
        List<Long> clientIdList = gateClientServiceService.findClientIdListByServiceId(serviceVO.getClientId());
        if (clientIdList != null && clientIdList.size() > 0) {
            List<GateClient> clientList = this.findByClientIdCollection(clientIdList);
            if (clientList != null && clientList.size() > 0) {
                serviceVO.setClientList(clientList);
            }
            serviceVO.setClientIdList(clientIdList);
        }
        return serviceVO;
    }

    /**
     * @描述 ：修改服务端具有的客户端列表
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：15:11
     */
    @Override
    public GateClient modifyServiceClientList(GateClientVO gateClientVO) {
        GateClient gateClient = this.findGateClientByClientId(gateClientVO.getClientId());
        List<Long> clientIdList = gateClientVO.getClientIdList();
        gateClientServiceService.deleteGateClientService(gateClientVO.getClientId());
        List<org.dppc.admin.entity.GateClientService> clientServiceList = new ArrayList<>();
        for (Long clientId: clientIdList) {
            org.dppc.admin.entity.GateClientService clientService = new org.dppc.admin.entity.GateClientService();
            clientService.setClientId(clientId);
            clientService.setServiceId(gateClientVO.getClientId());
            clientServiceList.add(clientService);
        }
        gateClientServiceService.saveClientServiceList(clientServiceList);
        return gateClient;
    }

    /**
     * @描述 ：批量删除客户端
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：17:26
     */
    @Override
    public List<GateClient> deleteClientServiceList(List<Long> clientIdList) {
        List<GateClient> gateClientList = this.findByClientIdCollection(clientIdList);
        gateClientRepository.delete(gateClientList);
        gateClientServiceService.batchDeleteGateClientService(clientIdList);
        return gateClientList;
    }

    /**
     * @描述 ：客户端分页列表
     * @作者 ：wyl
     * @日期 ：2017/12/28
     * @时间 ：9:42
     */
    @Override
    public Page<GateClientVO> findClientPage(GateClient query, Pageable pageable) {
        Page<GateClient> page = gateClientRepository.findAll((root, criteriaQuery, criteriaBuilder) -> BeanHelp.getPredicate(root, query, criteriaBuilder), pageable);
        return this.pageClientEntityToVO(page, pageable, query);
    }

    /**
     * @描述 ：查询客户端VO
     * @作者 ：wyl
     * @日期 ：2017/12/28
     * @时间 ：9:56
     */
    @Override
    public GateClientVO findClientVOByClientId(Long clientId) {
        GateClient client = this.findGateClientByClientId(clientId);
        GateClientVO clientVO = GateClientVO.entityToVO(client);
        List<Long> serviceIdList = gateClientServiceService.findServiceIdListByClientId(clientVO.getClientId());
        if (serviceIdList != null && serviceIdList.size() > 0) {
            List<GateClient> serviceList = this.findByClientIdCollection(serviceIdList);
            if (serviceList != null && serviceList.size() > 0) {
                clientVO.setClientServiceList(serviceList);
            }
            clientVO.setClientServiceIdList(serviceIdList);
        }
        return clientVO;
    }

    /**
     * @描述 ：修改客户端具有的服务端列表
     * @作者 ：wyl
     * @日期 ：2017/12/28
     * @时间 ：10:06
     */
    @Override
    public GateClient modifyClientServiceList(GateClientVO gateClientVO) {
        GateClient service = this.findGateClientByClientId(gateClientVO.getClientId());
        List<Long> serviceIdList = gateClientVO.getClientIdList();
        gateClientServiceService.deleteGateClientService(gateClientVO.getClientId());
        List<org.dppc.admin.entity.GateClientService> clientServiceList = new ArrayList<>();
        for (Long serviceId: serviceIdList) {
            org.dppc.admin.entity.GateClientService clientService = new org.dppc.admin.entity.GateClientService();
            clientService.setClientId(gateClientVO.getClientId());
            clientService.setServiceId(serviceId);
            clientServiceList.add(clientService);
        }
        gateClientServiceService.saveClientServiceList(clientServiceList);
        return service;
    }

    /**
     * @描述 ：服务端分页列表 Page<Entity> -> Page<VO>
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：10:33
     */
    private Page<GateClientVO> pageServiceEntityToVO(Page<GateClient> page, Pageable pageable, GateClient query) {
        List<GateClientVO> clientServiceVOList = new ArrayList<>();
        List<GateClient> clientServiceList = page.getContent();
        for (GateClient clientService : clientServiceList) {
            GateClientVO clientServiceVO = GateClientVO.entityToVO(clientService);
            List<Long> clientIdList = gateClientServiceService.findClientIdListByServiceId(clientServiceVO.getClientId());
            List<GateClient> clientList = clientIdList != null && clientIdList.size() > 0 ? this.findByClientIdCollection(clientIdList) : new ArrayList<>();
            String clientNames = "";
            for (GateClient client : clientList) {
                clientNames += client.getName() + ",";
            }
            if (!NullHelper.isNull(clientNames)) {
                clientNames = clientNames.substring(0, clientNames.lastIndexOf(","));
            }
            clientServiceVO.setClientNames(clientNames);
            clientServiceVOList.add(clientServiceVO);
        }
        return new PageImpl<>(clientServiceVOList, pageable, page.getTotalElements());
    }

    /**
     * @描述 ：客户端分页列表 Page<Entity> -> Page<VO>
     * @作者 ：wyl
     * @日期 ：2017/12/27
     * @时间 ：10:33
     */
    private Page<GateClientVO> pageClientEntityToVO(Page<GateClient> page, Pageable pageable, GateClient query) {
        List<GateClientVO> clientVOList = new ArrayList<>();
        List<GateClient> clientList = page.getContent();
        for (GateClient client : clientList) {
            GateClientVO clientVO = GateClientVO.entityToVO(client);
            List<Long> clientServiceIdList = gateClientServiceService.findServiceIdListByClientId(clientVO.getClientId());
            List<GateClient> clientServiceList = clientServiceIdList != null && clientServiceIdList.size() > 0 ? this.findByClientIdCollection(clientServiceIdList) : new ArrayList<>();
            String clientServiceNames = "";
            for (GateClient service : clientServiceList) {
                clientServiceNames += service.getName() + ",";
            }
            if (!NullHelper.isNull(clientServiceNames)) {
                clientServiceNames = clientServiceNames.substring(0, clientServiceNames.lastIndexOf(","));
            }
            clientVO.setClientServiceNames(clientServiceNames);
            clientVOList.add(clientVO);
        }
        return new PageImpl<>(clientVOList, pageable, page.getTotalElements());
    }
}
