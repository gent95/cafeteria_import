package org.dppc.admin.service;

import org.dppc.admin.entity.DataRole;
import org.dppc.dbexpand.service.BaseService;

/**
 * @描述 :
 * @作者 :	lyf
 * @日期 :	2018/5/9
 * @时间 :	16:42
 */
public interface DateRoleService extends BaseService<DataRole,Long> {

    DataRole addDataRole(DataRole dataRole);
}
