package org.dppc.admin.service.impl;

import org.dppc.admin.entity.Department;
import org.dppc.admin.repository.DepartmentRepository;
import org.dppc.admin.service.DepartmentService;
import org.dppc.admin.vo.DepartmentVO;
import org.dppc.common.enums.DepartmentLevelEnum;
import org.dppc.common.enums.EnableEnum;
import org.dppc.common.exception.MessageException;
import org.dppc.common.utils.NullHelper;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.dppc.dbexpand.annotation.support.PredicateInfoCover;
import org.dppc.dbexpand.util.BeanHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.*;

@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public Page<DepartmentVO> findDepartmentPage(Department query, int pageNum, int pageSize) {
        StringBuffer sql = new StringBuffer("SELECT DEPARTMENT_ID,DEPARTMENT_NAME,ENABLE,PARENT_DEPARTMENT_ID,DEPARTMENT_LEVEL FROM sys_department WHERE DEPARTMENT_LEVEL=1");
        StringBuilder count = new StringBuilder("SELECT COUNT(1) FROM sys_department WHERE DEPARTMENT_LEVEL=1");
        if (!NullHelper.isNull(query.getDepartmentName())) {
            sql.append(" AND DEPARTMENT_NAME LIKE '%").append(query.getDepartmentName()).append("%'");
            count.append(" AND DEPARTMENT_NAME LIKE '%").append(query.getDepartmentName()).append("%'");
        }
        if (!NullHelper.isNull(query.getDepartmentLevel())) {
            sql.append(" AND DEPARTMENT_LEVEL LIKE '%").append(query.getDepartmentLevel()).append("%'");
            count.append(" AND DEPARTMENT_LEVEL LIKE '%").append(query.getDepartmentLevel()).append("%'");
        }
        sql.append(" ORDER BY DEPARTMENT_ID ASC LIMIT ").append((pageNum) * pageSize).append(",").append(pageSize);
        Object object = departmentRepository.findOneByNativeSQL(count.toString());
        Long total = object == null ? 0L : Long.parseLong(object.toString());
        List<Map> maps = departmentRepository.findMapListByNativeSQL(sql.toString());
        List<DepartmentVO> departmentVOS = new ArrayList<>();
        maps.forEach((map) -> {
            DepartmentVO departmentVO = new DepartmentVO();
            List<Department> two = departmentRepository.findByDepartmentLevelAndParentDepartmentId(
                    DepartmentLevelEnum.TWO_LEVEL_DEPARTMENT.getValue(), Long.valueOf(String.valueOf(map.get("DEPARTMENT_ID"))));
            departmentVO.setDepartmentId(Long.valueOf(String.valueOf(map.get("DEPARTMENT_ID"))));
            departmentVO.setEnable((Integer) map.get("ENABLE"));
            departmentVO.setDepartmentName(map.get("DEPARTMENT_NAME").toString());
            departmentVO.setDepartmentLevel((Integer) map.get("DEPARTMENT_LEVEL"));
            departmentVO.setDepartments(two);
            if (two.size() > 0)
                departmentVO.setState("1");  //有下级菜单
            departmentVOS.add(departmentVO);
        });
        return new PageImpl<>(departmentVOS, new PageRequest(pageNum, pageSize), total);
    }

    @Override
    public void delDepartment(Long departmentId) {
        Department department = departmentRepository.findOne(departmentId);
        if (department == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", department));
        }
        departmentRepository.delete(departmentId);
    }

    /**
     * @描述 ：根据部门id集合查询部门map
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:08
     */
    @Override
    public Map<Long, Department> findDepartmentMap(Collection<Long> departmentIdSet) {
        Map<Long, Department> map = new HashMap<>();
        List<Department> departmentList = departmentRepository.findByDepartmentIdIn(departmentIdSet);
        for (Department department : departmentList) map.put(department.getDepartmentId(), department);
        return map;
    }

    /**
     * @描述 ：查询部门列表 二级 启用状态的
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:32
     */
    @Override
    public List<Department> getUserDepartmentList() {
        return departmentRepository.findByEnableAndDepartmentLevelOrderByDepartmentIdAsc(EnableEnum.ENABLE.getValue(), DepartmentLevelEnum.TWO_LEVEL_DEPARTMENT.getValue());
    }

    /**
     * @描述 ：根据部门id查询部门信息
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:44
     */
    @Override
    public Department findDepartmentById(Long departmentId) {
        return departmentRepository.findOne(departmentId);
    }

    /**
     * @描述 ：禁用部门
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:47
     */
    @Override
    public Department disableDepartment(Long departmentId) {
        Department department = departmentRepository.findOne(departmentId);
        if (department == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", departmentId));
        }
        department.setEnable(EnableEnum.DISABLE.getValue());
        return departmentRepository.saveAndFlush(department);
    }

    /**
     * @描述 ：启用部门
     * @作者 ：wyl
     * @日期 ：2017/11/30
     * @时间 ：15:52
     */
    @Override
    public Department enableDepartment(Long departmentId) {
        Department department = departmentRepository.findOne(departmentId);
        if (department == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", departmentId));
        }
        department.setEnable(EnableEnum.ENABLE.getValue());
        return departmentRepository.saveAndFlush(department);
    }

    /**
     * @描述 ：部门名称验证
     * @作者 ：wyl
     * @日期 ：2017/12/1
     * @时间 ：13:47
     */
    @Override
    public Long departmentNameValidate(Department department) {
        return departmentRepository.count((root, criteriaQuery, criteriaBuilder) ->
                BeanHelp.getPredicate(root, department, criteriaBuilder
                        , new PredicateInfoCover("departmentId", PredicateInfo.QueryType.NOT_EQUAL)
                        , new PredicateInfoCover("departmentName", PredicateInfo.QueryType.BASIC)));
    }

    /**
     * @描述 ：添加部门
     * @作者 ：wyl
     * @日期 ：2017/12/1
     * @时间 ：13:49
     */
    @Override
    public Department addDepartment(DepartmentVO departmentVO) {
        Department department = new Department();
        department.setDepartmentName(departmentVO.getDepartmentName());
        department.setEnable(EnableEnum.ENABLE.getValue());
        if (this.departmentNameValidate(department) > 0L) {
            throw new MessageException(MessageFormat.format("部门名称[{0}]已存在", department.getDepartmentName()));
        }
        if (departmentVO.getState().equals("1")) {  //添加父类部门
            department.setParentDepartmentId(0L);
            department.setDepartmentLevel(DepartmentLevelEnum.ONE_LEVEL_DEPARTMENT.getValue());
            return departmentRepository.save(department);
        } else { //子类部门
            department.setParentDepartmentId(departmentVO.getParentDepartmentId());
            department.setDepartmentLevel(DepartmentLevelEnum.TWO_LEVEL_DEPARTMENT.getValue());
            return departmentRepository.save(department);
        }
    }

    /**
     * @描述 ：修改部门信息
     * @作者 ：wyl
     * @日期 ：2017/12/1
     * @时间 ：14:01
     */
    @Override
    public Department modifyDepartment(Department department) {
        Department info = departmentRepository.findOne(department.getDepartmentId());
        if (info == null) {
            throw new MessageException(MessageFormat.format("未找到id未[{0}]的数据", department.getDepartmentId()));
        }
        Department validate = new Department();
        validate.setDepartmentId(department.getDepartmentId());
        validate.setDepartmentName(department.getDepartmentName());
        if (this.departmentNameValidate(validate) > 0L) {
            throw new MessageException(MessageFormat.format("部门名称[{0}]已存在", validate.getDepartmentName()));
        }
        return departmentRepository.saveAndFlush(BeanHelp.updateEntityExceptEmptyProps(info, department));
    }

    /**
     * @描述 ：查询所有二级部门
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：9:35
     */
    @Override
    public List<Department> getAllDepartment() {
        return departmentRepository.findByDepartmentLevelOrderByDepartmentIdAsc(DepartmentLevelEnum.TWO_LEVEL_DEPARTMENT.getValue());
    }

    /**
     * @描述 ：查询父级部门
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：10:38
     */
    @Override
    public Department findParentDepartment(Long parentDepartmentId) {
        return departmentRepository.findByParentDepartmentIdAndDepartmentLevel(parentDepartmentId, DepartmentLevelEnum.ONE_LEVEL_DEPARTMENT.getValue());
    }

    /**
     * @描述 ：查询所有一级部门
     * @作者 ：lxw
     * @日期 ：2018/4/17
     * @时间 ：13:53
     */
    @Override
    public List<Department> getAllDepartmentOne() {
        return departmentRepository.findByDepartmentLevelOrderByDepartmentIdAsc(DepartmentLevelEnum.ONE_LEVEL_DEPARTMENT.getValue());
    }
}
