package org.dppc.admin.service.impl;

import org.dppc.admin.entity.Resource;
import org.dppc.admin.entity.Role;
import org.dppc.admin.entity.User;
import org.dppc.admin.repository.ResourceRepository;
import org.dppc.admin.service.MenuService;
import org.dppc.admin.service.ResourceService;
import org.dppc.admin.service.UserService;
import org.dppc.admin.vo.MenuVO;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.enums.ColumnsShowsEnum;
import org.dppc.common.enums.EnableEnum;
import org.dppc.common.enums.PlatformTypeEnum;
import org.dppc.common.enums.ResourceTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/11/10
 * @时间 ：10:01
 */
@Service
@Transactional
public class MenuServiceImpl implements MenuService {

    @Autowired
    private UserService userService;
    @Autowired
    private ResourceService resourceService;
    @Autowired
    private ResourceRepository resourceRepository;

    /**
     * @描述 ：获取当前登录用户权限下的菜单
     * @作者 ：wyl
     * @日期 ：2017/11/10
     * @时间 ：10:12
     */
    @Override
    public List<MenuVO> getUserMenuList() {
        User user = userService.findByUsername(BaseContextHandler.getUsername());
        if (user == null) return new ArrayList<>();
        List<Role> roles = user.getRoles();
        if (roles == null || roles.size() == 0) return new ArrayList<>();
        Set<Long> resourceIdSet = new HashSet<>();
        for (Role role : roles) {
            List<Resource> resources = role.getResources();
            if (resources == null || resources.size() == 0) continue;
            resourceIdSet.addAll(resources.stream().map(Resource::getResourceId).collect(Collectors.toList()));
        }
        List<MenuVO> menuVOList = resourceService.findByParentIdAndResourcesIdIn(PlatformTypeEnum.TRACE.getValue(),0L, resourceIdSet);
        if (menuVOList == null || menuVOList.size() == 0) return new ArrayList<>();
        /** wyl 2017/11/10 11:02 递归查询菜单下子菜单 */
        this.findChildrenList(menuVOList, resourceIdSet);
        return menuVOList;
    }

    /**
     * @描述 ：递归查询菜单下子菜单
     * @作者 ：wyl
     * @日期 ：2017/11/10
     * @时间 ：11:03
     */
    private void findChildrenList(List<MenuVO> menuVOList, Set<Long> resourceIdSet) {
        for (MenuVO menuVO : menuVOList) {
            List<MenuVO> childrenList = resourceService.findByParentIdAndResourcesIdIn(PlatformTypeEnum.TRACE.getValue(),menuVO.getResourcesId(), resourceIdSet);
            if (childrenList != null && childrenList.size() > 0) {
                menuVO.setChildrens(childrenList);
                this.findChildrenList(childrenList, resourceIdSet);
            }
        }
    }

    /**
     * @描述 ：获取当前CMS登录用户权限下的菜单
     * @作者 ：gaoj
     * @日期 ：2018/4/10
     * @时间 ：10:12
     */
    @Override
    public List<Resource> getUserResource(String username) {
//        String username = BaseContextHandler.getUsername();
        User user = userService.findByUsername(username);
        if (user == null) return new ArrayList<>();
        List<Role> roles = user.getRoles();
        if (roles == null || roles.size() == 0) return new ArrayList<>();
        Set<Long> resourceIdSet = new HashSet<>();
        for (Role role : roles) {
            List<Resource> resources = role.getResources();
            if (resources == null || resources.size() == 0) continue;
            resourceIdSet.addAll(resources.stream().map(Resource::getResourceId).collect(Collectors.toList()));
        }
        List<Resource> resourceList = resourceRepository.findByPlatformTypeAndTypeAndEnableAndResourceIdIn
                (PlatformTypeEnum.CMS.getValue(),ResourceTypeEnum.MENU, EnableEnum.ENABLE.getValue(),
                        resourceIdSet, new Sort(Sort.Direction.ASC, "sequence"));
        return resourceList;
    }

}
