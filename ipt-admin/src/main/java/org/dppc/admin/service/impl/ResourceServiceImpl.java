package org.dppc.admin.service.impl;

import org.dppc.admin.entity.Resource;
import org.dppc.admin.repository.ResourceRepository;
import org.dppc.admin.service.ResourceService;
import org.dppc.admin.service.RoleService;
import org.dppc.admin.vo.ResourceTree;
import org.dppc.admin.vo.MenuVO;
import org.dppc.admin.vo.ResourceVO;
import org.dppc.common.enums.EnableEnum;
import org.dppc.common.enums.ResourceTypeEnum;
import org.dppc.common.enums.YesOrNoEnum;
import org.dppc.common.exception.MessageException;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.dppc.dbexpand.util.BeanHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/18.
 * @时间： 13:28.
 */
@SuppressWarnings("Duplicates")
@Service
@Transactional
public class ResourceServiceImpl extends BaseServiceImpl<Resource, Long> implements ResourceService {

    @Autowired
    private ResourceRepository resourceRepository;
    @Autowired
    private RoleService roleService;
    public static final String keyHead = "admin:resource:";


    /**
     * 查找过滤的资源
     * @return
     */
    @Override
    public List<Resource> findFilterPermission() {
        return resourceRepository.findByFilterAndPermissionIsNotNull(YesOrNoEnum.YES);
    }

    /**
     * 查找不过滤的资源
     * @return
     */
    @Override
    public List<Resource> findNoFilterPermission() {
        return resourceRepository.findByFilterAndPermissionIsNotNull(YesOrNoEnum.YES);
    }

    /**
     * @描述 ：资源数据分页列表
     * @作者 ：wyl
     * @日期 ：2017/10/26
     * @时间 ：13:25
     */
    @Override
    public Page<ResourceVO> findResourcePage(Resource query, Pageable pageable) {
        query.setParentId(0L);
        query.setType(ResourceTypeEnum.MENU);
        Page<Resource> page = resourceRepository.findAll((root, criteriaQuery, criteriaBuilder) -> BeanHelp.getPredicate(root, query, criteriaBuilder), pageable);
        return this.pageToVO(page, pageable);
    }

    /**
     * @描述 ：page：实体->VO转换
     * @作者 ：wyl
     * @日期 ：2017/10/31
     * @时间 ：11:08
     */
    private Page<ResourceVO> pageToVO(Page<Resource> page, Pageable pageable) {
        List<Resource> resourceList = page.getContent();
        List<ResourceVO> resourceVOList = new ArrayList<>();
        for (Resource resource : resourceList) {
            ResourceVO resourceVO = ResourceVO.entityToVO(resource);
            /** wyl 2017/11/10 12:59 resourceVO 下子资源集合  */
            List<ResourceVO> list = this.findVOList(resourceVO);
            /** wyl 2017/11/10 12:59 递归获取子资源下所有资源  */
            this.getChildResourceList(list, resourceVO);
            resourceVOList.add(resourceVO);
        }
        return new PageImpl<>(resourceVOList, pageable, page.getTotalElements());
    }

    /**
     * @描述 ：获取资源下子资源
     * @作者 ：wyl
     * @日期 ：2017/10/31
     * @时间 ：11:11
     */
    private void getChildResourceList(List<ResourceVO> childResourceList, ResourceVO resourceVO) {
        if (childResourceList == null || childResourceList.size() <= 0) {
            return;
        }
        /** wyl 2017/11/10 12:58 获取子资源  */
        List<ResourceVO> resourceVOList = this.findVOList(resourceVO);
        for (ResourceVO vo : resourceVOList) {
            /** wyl 2017/11/10 12:59 递归  */
            this.getChildResourceList(resourceVOList, vo);
        }
        resourceVO.setChildResourceVOList(resourceVOList);
    }

    /**
     * @描述 ：根据VO获取VO集合
     * @作者 ：wyl
     * @日期 ：2017/10/31
     * @时间 ：11:14
     */
    private List<ResourceVO> findVOList(ResourceVO resourceVO) {
        List<Resource> resourceList = this.findByParentId(resourceVO.getResourceId());
        List<ResourceVO> resourceVOList = new ArrayList<>();
        for (Resource resource : resourceList) {
            ResourceVO vo = ResourceVO.entityToVO(resource);
            resourceVOList.add(vo);
        }
        return resourceVOList;
    }

    /**
     * @描述 ：启用资源
     * @作者 ：wyl
     * @日期 ：2017/10/27
     * @时间 ：9:20
     */
    @Override
    public Resource enableOrDisableResource(Long resourceId, EnableEnum enableEnum) {
        Resource info = resourceRepository.findOne(resourceId);
        if (info == null) {
            throw new MessageException(MessageFormat.format("未找到id为[{0}]的数据", resourceId));
        }
        return this.enableOrDisableAllChildResource(info, enableEnum);
    }

    /**
     * @描述 ：禁用/启用 所有子资源
     * @作者 ：wyl
     * @日期 ：2017/10/27
     * @时间 ：10:11
     */
    private Resource enableOrDisableAllChildResource(Resource resource, EnableEnum enableEnum) {
        resource.setEnable(enableEnum.getValue());
        /** wyl 2017/11/10 12:58 查看资源级别 菜单  */
        if (ResourceTypeEnum.MENU.getName().equals(resource.getType().getName())) {
            /** wyl 2017/11/10 12:58 菜单 禁用/启用 所有未禁用/未启用的子资源  */
            Resource parentResource = new Resource();
            parentResource.setResourceId(resource.getResourceId());
            parentResource.setEnable(enableEnum.getValue() == EnableEnum.ENABLE.getValue() ? EnableEnum.DISABLE.getValue() : EnableEnum.ENABLE.getValue());
            /** wyl 2017/11/10 12:58 查询菜单下所有 未禁用/未启用 的子资源列表  */
            List<Resource> childResourceList = this.findChildResourceList(parentResource);
            /** wyl 2017/11/10 12:58 禁用/启用 所有子资源  */
            for (Resource childResource : childResourceList) {
                this.enableOrDisableAllChildResource(childResource, enableEnum);
            }
        }
        return resourceRepository.saveAndFlush(resource);
    }

    /**
     * @描述 ：根据参数查询资源集合
     * @作者 ：wyl
     * @日期 ：2017/10/27
     * @时间 ：9:38
     */
    @Override
    public List<Resource> findResourceList(Resource query) {
        return resourceRepository.findAll((root, criteriaQuery, criteriaBuilder) ->
                BeanHelp.getPredicate(root, query, criteriaBuilder));
    }

    /**
     * @描述 ：查询父id下子资源列表
     * @作者 ：wyl
     * @日期 ：2017/10/31
     * @时间 ：11:16
     */
    @Override
    public List<Resource> findByParentId(Long parentId) {
        return resourceRepository.findByParentId(parentId, new Sort(Sort.Direction.ASC, "sequence"));
    }

    /**
     * @描述 ：根据父资源获取子资源集合
     * @作者 ：wyl
     * @日期 ：2017/10/31
     * @时间 ：14:31
     */
    @Override
    public List<Resource> findChildResourceList(Resource parentResource) {
        return resourceRepository.findByParentIdAndEnable(parentResource.getResourceId(), parentResource.getEnable());
    }

    /**
     * @描述 ：根据资源父id、资源类型（MENU BUTTON）、禁用启用  用户权限下资源id集合 查询资源列表
     * @作者 ：wyl
     * @日期 ：2017/11/10
     * @时间 ：10:45
     */
    @Override
    public List<MenuVO> findByParentIdAndResourcesIdIn(Integer platformType, Long parentId, Set<Long> resourceIdSet) {
        List<Resource> resourceList = resourceRepository.findByPlatformTypeAndParentIdAndTypeAndEnableAndAndFilterAndResourceIdIn(
                platformType, parentId, ResourceTypeEnum.MENU, EnableEnum.ENABLE.getValue(), YesOrNoEnum.NO, resourceIdSet,
                new Sort(Sort.Direction.ASC, "sequence"));
        List<MenuVO> menuVOList = new ArrayList<>();
        for (Resource info : resourceList) {
            MenuVO menuVO = new MenuVO();
            menuVO.setIcon(info.getIcon() == null ? "" : info.getIcon());
            menuVO.setMenuName(info.getResourceName() == null ? "" : info.getResourceName());
            menuVO.setMenuUrl(info.getResourceUrl() == null ? "" : info.getResourceUrl());
            menuVO.setResourcesId(info.getResourceId());
            menuVO.setParentId(info.getParentId());
            menuVOList.add(menuVO);
        }
        return menuVOList;
    }


    /**
     * @描述 ：根据资源id集合查询资源集合
     * @作者 ：wyl
     * @日期 ：2017/11/13
     * @时间 ：9:40
     */
    @Override
    public List<Resource> findByResourceIds(Collection<Long> resourcesIds) {
        return resourceRepository.findByResourceIdIn(resourcesIds);
    }

    /**
     * @描述 ：所有资源 层级包含子资源
     * @作者 ：wyl
     * @日期 ：2017/11/13
     * @时间 ：14:39
     */
    @Override
    public List<ResourceVO> findResourceVOList() {
        List<Resource> resourceList = resourceRepository.findByParentIdAndType(0L, ResourceTypeEnum.MENU, new Sort(Sort.Direction.ASC, "sequence"));
        List<ResourceVO> resourceVOList = new ArrayList<>();
        for (Resource resource : resourceList) {
            ResourceVO resourceVO = ResourceVO.entityToVO(resource);
            /** wyl 2017/11/10 12:59 resourceVO 下子资源集合  */
            List<ResourceVO> list = this.findVOList(resourceVO);
            /** wyl 2017/11/10 12:59 递归获取子资源下所有资源  */
            this.getChildResourceList(list, resourceVO);
            resourceVOList.add(resourceVO);
        }
        return resourceVOList;
    }

    /**
     * @描述 ：角色对应的资源列表
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：15:14
     */
    @Override
    public List<ResourceTree> getResourceTreeList() {
        List<ResourceTree> resourceTreeList = new ArrayList<>();
        List<Resource> parentResourcesList = resourceRepository.findByParentIdAndEnableOrderBySequenceAsc(0L, EnableEnum.ENABLE.getValue());
        return this.findAllMenuTreeList(resourceTreeList, parentResourcesList);
    }

    /**
     * @描述 ：递归树结构
     * @作者 ：wyl
     * @日期 ：2017/12/18
     * @时间 ：15:14
     */
    private List<ResourceTree> findAllMenuTreeList(List<ResourceTree> resourceTreeList, List<Resource> parentResourceList) {
        for (Resource resource : parentResourceList) {
            ResourceTree resourceTree = ResourceTree.resourceToMenuTree(resource);
            List<Resource> childResourceList = resourceRepository.findByParentIdAndEnableOrderBySequenceAsc(resource.getResourceId(), EnableEnum.ENABLE.getValue());
            if (childResourceList != null && childResourceList.size() > 0) {
                List<ResourceTree> childResourceTreeList = this.findAllMenuTreeList(new ArrayList<>(), childResourceList);
                resourceTree.setChildren(childResourceTreeList);
            }
            resourceTree.setLeaf(false);
            resourceTreeList.add(resourceTree);
        }
        return resourceTreeList;
    }

}
