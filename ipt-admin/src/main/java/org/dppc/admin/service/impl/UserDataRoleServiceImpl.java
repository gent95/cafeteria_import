package org.dppc.admin.service.impl;

import org.dppc.admin.entity.UserDataRole;
import org.dppc.admin.repository.UserDataRoleRepository;
import org.dppc.admin.service.UserDataRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class UserDataRoleServiceImpl implements UserDataRoleService {

    @Resource
    private UserDataRoleRepository userDataRoleRepository;

    @Override
    public UserDataRole findOneByUserId(Long userId) {
        UserDataRole userDataRole = userDataRoleRepository.findByUserId(userId);
        return userDataRole;
    }

    @Override
    public UserDataRole addUserDataRole(UserDataRole userDataRole) {
        return userDataRoleRepository.save(userDataRole);
    }
}
