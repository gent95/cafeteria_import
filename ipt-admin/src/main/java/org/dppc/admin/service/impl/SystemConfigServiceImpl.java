package org.dppc.admin.service.impl;

import org.dppc.admin.entity.SystemConfig;
import org.dppc.admin.repository.SystemConfigRepository;
import org.dppc.admin.service.SystemConfigService;
import org.dppc.common.exception.MessageException;
import org.dppc.common.utils.NullHelper;
import org.dppc.dbexpand.util.BeanHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/11/30
 * @时间 ：16:48
 */
@Service
@Transactional
public class SystemConfigServiceImpl implements SystemConfigService {

    @Autowired
    private SystemConfigRepository systemConfigRepository;

    /**
     * @描述 ：添加配置
     * @作者 ：wyl
     * @日期 ：2017/12/13
     * @时间 ：14:01
     */
    @Override
    public SystemConfig addSystemConfig(SystemConfig systemConfig) {
        return systemConfigRepository.save(systemConfig);
    }

    /**
     * @描述 ：根据key查询
     * @作者 ：wyl
     * @日期 ：2017/12/13
     * @时间 ：14:05
     */
    @Override
    public SystemConfig findByConfigKey(String configKey) {
        return systemConfigRepository.findByConfigKey(configKey);
    }

    /**
     * @描述 ：根据id查询
     * @作者 ：wyl
     * @日期 ：2017/12/13
     * @时间 ：14:08
     */
    @Override
    public SystemConfig findByConfigId(Long configId) {
        return systemConfigRepository.findOne(configId);
    }

    /**
     * @描述 ：保存修改后的配置 主键ID必须有值
     * @作者 ：wyl
     * @日期 ：2017/12/13
     * @时间 ：14:07
     */
    @Override
    public SystemConfig modifyConfigKeyById(SystemConfig systemConfig) {
        if (NullHelper.isNull(systemConfig.getConfigId())) {
            throw new MessageException("主键configId值不能为空");
        }
        return systemConfigRepository.saveAndFlush(systemConfig);
    }

    /**
     * @描述 ：保存修改后的配置 key必须有值
     * @作者 ：wyl
     * @日期 ：2017/12/13
     * @时间 ：14:10
     */
    @Override
    public SystemConfig updateConfigKeyByKey(SystemConfig systemConfig) {
        if (NullHelper.isNull(systemConfig.getConfigKey())) {
            throw new MessageException("configKey值不能为空");
        }
        SystemConfig oldConfig = this.findByConfigKey(systemConfig.getConfigKey());
        if (oldConfig == null) {
            throw new MessageException(MessageFormat.format("未找到configKey为[{0}]", systemConfig.getConfigKey()));
        }
        return systemConfigRepository.saveAndFlush(BeanHelp.updateEntityExceptEmptyProps(oldConfig, systemConfig));
    }


}
