package org.dppc.admin.service;

import org.dppc.admin.entity.Resource;
import org.dppc.admin.entity.User;
import org.dppc.admin.vo.UserVO;
import org.dppc.api.cmsHome.RegisterVO;
import org.dppc.dbexpand.service.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/18.
 * @时间： 13:24.
 */
public interface UserService extends BaseService<User,Long> {

    /**
     * 保存新添加的用户 级联添加用户-角色
     */
    User saveUser(UserVO userVO);

    /**
     * 根据用户名称查询用户
     */
    User findByUsername(String username);

    /**
     * 根据用户名查找用户具有的权限
     * @param username
     * @return
     */
    List<Resource> findPermissionByUsername(String username);

    /**
     * @描述 ：查询用户分页列表
     * @作者 ：wyl
     * @日期 ：2017/10/24
     * @时间 ：13:26
     */
    Page<UserVO> findUserPage(User user, Pageable pageable);

    /**
     * @描述 ：保存修改后的用户信息 密码加密
     * @作者 ：wyl
     * @日期 ：2017/10/25
     * @时间 ：13:48
     */
    User modifyUser(UserVO userVO);

    /**
     * @描述 ：禁用用户
     * @作者 ：wyl
     * @日期 ：2017/10/25
     * @时间 ：13:54
     */
    User disableUser(Long userId);

    /**
     * @描述 ：用户名验证
     * @作者 ：wyl
     * @日期 ：2017/10/26
     * @时间 ：13:53
     */
    Long usernameValidate(User user);

    /**
     * @描述 ：根据用户id查询用户信息
     * @作者 ：wyl
     * @日期 ：2017/10/26
     * @时间 ：14:27
     */
    User findUserByUserId(Long userId);

    /**
     * @描述 ：启用用户
     * @作者 ：wyl
     * @日期 ：2017/11/2
     * @时间 ：16:55
     */
    User enableUser(Long userId);

    /**
     * @描述 ：通过用户ID查询集合
     * @作者 ：luxinwei
     * @日期 ：2017/11/13
     * @时间 ：14:51
     */
    Map<Long,User> findUserMap(Collection<Long> collection);

    /**
     * @描述 ：根据用户id集合查询用户
     * @作者 ：wyl
     * @日期 ：2017/12/13
     * @时间 ：11:47
     */
    List<User> findByUserIdCollect(Collection<Long> collection);

    /**
     * @描述 ：重置密码
     * @作者 ：wyl
     * @日期 ：2017/12/14
     * @时间 ：18:39
     */
    User resetPassword(Long userId,String passWord);

    /**
     * @描述 ：根据用户id查询VO
     * @作者 ：wyl
     * @日期 ：2017/12/28
     * @时间 ：9:15
     */
    UserVO findUserVOByUserId(Long userId);

    /**
     * @描述 ：门户 企业用户注册
     * @作者 ：gaoj
     * @日期 ：2018/5/25
     * @时间 ：9:53
     */
    String register(RegisterVO register);
}
