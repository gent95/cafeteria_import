package org.dppc.admin.redis;

import org.dppc.admin.entity.Resource;
import org.dppc.admin.entity.User;
import org.dppc.dbexpand.redis.BaseRedis;
import org.springframework.stereotype.Repository;

/**
 * @描述： user缓存操作
 * @作者： 颜齐
 * @日期： 2017/10/27.
 * @时间： 15:05.
 */
@Repository
public class ResourceRedis extends BaseRedis<Resource>{

}
