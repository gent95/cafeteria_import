package org.dppc.admin.redis;

import org.dppc.admin.entity.Resource;
import org.dppc.admin.entity.User;
import org.dppc.common.utils.JsonHelper;
import org.dppc.dbexpand.redis.BaseRedis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @描述： user缓存操作
 * @作者： 颜齐
 * @日期： 2017/10/27.
 * @时间： 15:05.
 */
@Repository
public class UserRedis extends BaseRedis<User>{
}
