package org.dppc.admin.rpc;

import org.dppc.admin.entity.Resource;
import org.dppc.admin.service.MenuService;
import org.dppc.admin.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/23.
 * @时间： 17:20.
 */
@RestController
@RequestMapping(value = "api/resource")
public class ResourceApi {

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private MenuService menuService;

    /**
     * 返回需要权限才可以查看的菜单
     * @return
     */
    @GetMapping(value = "/findFilterPermission")
    public List<Resource> findFilterPermission(){
        return resourceService.findFilterPermission();
    }

    /**
     * 返回不需要权限就可以查看的菜单
     * @return
     */
    @GetMapping(value = "/findNoFilterPermission")
    public List<Resource> findNoFilterPermission(){
        return resourceService.findNoFilterPermission();
    }

    /**
     * cms-admin 获取资源菜单
     * @param username
     * @return
     */
    @GetMapping(value = "/getUserResource/{username}")
    public List<Resource> getUserResource(@PathVariable("username") String username){
        return menuService.getUserResource(username);
    }
}
