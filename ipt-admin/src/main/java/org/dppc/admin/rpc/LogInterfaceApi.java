package org.dppc.admin.rpc;

import org.dppc.admin.entity.LogInterface;
import org.dppc.admin.service.LogInterfaceService;
import org.dppc.auth.client.annotation.IgnoreClientToken;
import org.dppc.auth.client.annotation.IgnoreUserToken;
import org.dppc.common.enums.ForwardEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/25.
 * @时间： 16:48.
 */
@RestController
@RequestMapping("api/logInterface")
@IgnoreClientToken
public class LogInterfaceApi {

    @Autowired
    private LogInterfaceService logInterfaceService;

    /**
     * 保存日志
     *
     * @param logInterface
     */
    @PostMapping(value = "/save")
    @IgnoreUserToken
    public void save(@RequestBody LogInterface logInterface) {
        if (logInterface != null) {
            logInterface.setBizTypeName(ForwardEnum.valueOf(logInterface.getBizType()).getDescription());
            logInterfaceService.save(logInterface);
        }
    }
}
