package org.dppc.admin.rpc;


import org.dppc.admin.entity.User;
import org.dppc.admin.entity.UserNode;
import org.dppc.admin.repository.UserNodeRepository;
import org.dppc.admin.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @描述： 用户-节点（用户上传数据权限）Api
 * @作者： GAOJ
 * @日期： 2018/7/2
 * @时间： 10:19
 */

@RestController
@RequestMapping("api/userNode")
public class UserNodeApi {

    @Resource
    private UserNodeRepository userNodeRepository;

    @Resource
    private UserService userService;

    @GetMapping(value = "/getNodes/{username}")
    public List<String> getUserInterfaceUploadPermissions(@PathVariable("username") String username) {
        List<UserNode> userNode = userNodeRepository.findByUsername(username);
        List<String> list = new ArrayList<>();
        for (UserNode u : userNode) {
            list.add(u.getSocialCreditCode());
        }
        return list;
    }

    @GetMapping(value = "/getUsersBySocialCreditCode/{socialCreditCode}")
    public List<User> getUsersBySocialCreditCode(@PathVariable("socialCreditCode") String socialCreditCode) {
        List<UserNode> userNode = userNodeRepository.findBySocialCreditCode(socialCreditCode);
        List<User> l = new ArrayList<>();
        User u = userService.findByUsername(userNode.get(0).getUsername());
        l.add(u);
        return l;
    }
}
