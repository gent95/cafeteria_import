package org.dppc.admin.rpc;

import org.dppc.admin.entity.LogInfo;
import org.dppc.admin.service.LogInfoService;
import org.dppc.auth.client.annotation.IgnoreClientToken;
import org.dppc.auth.client.annotation.IgnoreUserToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/25.
 * @时间： 16:48.
 */
@RestController
@RequestMapping("api/logInfo")
@IgnoreClientToken
public class LogInfoApi {

    @Autowired
    private LogInfoService logInfoService;

    /**
     * 保存日志
     *
     * @param logInfo
     */
    @IgnoreUserToken
    @PutMapping(value = "/save")
    public void save(@RequestBody LogInfo logInfo) {
        if (logInfo != null) {
            logInfoService.save(logInfo);
        }
    }
}
