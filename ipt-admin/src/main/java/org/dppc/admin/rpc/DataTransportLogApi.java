package org.dppc.admin.rpc;

import org.dppc.admin.entity.LogDataTransport;
import org.dppc.admin.service.LogDataTransportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @描述 ：数据转换异常
 * @作者 ：gaoj
 * @日期 ：2018/4/16
 * @时间 ：16:49
 */
@RestController
@RequestMapping(value = "/api/logDataTransport")
public class DataTransportLogApi {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private LogDataTransportService logDataTransportService;

    /**
     * @描述 ：添加数据转换异常
     * @作者 ：gaoj
     * @日期 ：2018/4/16
     * @时间 ：11:28
     */
    @PostMapping(value = "/add")
    public void add(@RequestBody LogDataTransport logDataTransport) {
        try {
            logDataTransportService.save(logDataTransport);
        } catch (Exception e) {
            logger.error("添加数据转换异常数据时出现异常{}", logDataTransport, e);
        }
    }

    /**
     * @描述 ：获取所有交换数据异常未解决数据
     * @作者 ：zhumh
     * @日期 ：2018/4/16
     * @时间 ：11:28
     */
    @GetMapping(value = "/logDataTransportAllBySolve")
    public List<LogDataTransport> logDataTransportAllBySolve() {
        return logDataTransportService.getLogDataTransportAllBySolve();
    }



}
