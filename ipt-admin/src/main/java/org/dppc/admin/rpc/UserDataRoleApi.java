package org.dppc.admin.rpc;


import org.dppc.admin.entity.UserDataRole;
import org.dppc.admin.service.UserDataRoleService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @描述： 用户数据角色表Api
 * @作者： Lu Xinwei
 * @日期： 2018/4/2
 * @时间： 10:19
 */

@RestController
@RequestMapping("api/userDataRole")
public class UserDataRoleApi {

    @Resource
    private UserDataRoleService userDataRoleService;

    /**
     * @描述： 通过userId查询userDataRoleId
     * @作者： Lu Xinwei
     * @日期： 2018/4/2
     * @时间： 11:03
     */
    @GetMapping(value = "/findUserDataRoleId/{userId}")
    public Long findUserDataRoleId(@PathVariable("userId") Long userId) {

        UserDataRole userDataRole = userDataRoleService.findOneByUserId(userId);
        if (userDataRole == null)
            return -1L;
        return userDataRole.getDataRoleId();
    }
}
