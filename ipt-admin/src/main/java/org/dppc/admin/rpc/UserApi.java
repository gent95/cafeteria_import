package org.dppc.admin.rpc;

import org.dppc.admin.entity.Resource;
import org.dppc.admin.entity.User;
import org.dppc.admin.service.UserService;
import org.dppc.api.cmsHome.RegisterVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/19.
 * @时间： 14:44.
 */
@RestController
@RequestMapping("api/user")
public class UserApi {

    @Autowired
    private UserService userService;

    /**
     * 根据名称查询用户
     *
     * @param username
     * @return
     */
    @RequestMapping(value = "/getUser/{username}", method = RequestMethod.GET)
    public User getUserByUsername(@PathVariable("username") String username) {
        return userService.findByUsername(username);
    }

    @RequestMapping(value = "/getPermissions/{username}", method = RequestMethod.GET)
    public List<Resource> getPermissionByUsername(@PathVariable("username") String username) {
        return userService.findPermissionByUsername(username);
    }


    /**
     * @描述 ：通过用户ID查询用户集合
     * @作者 ：luxinwei
     * @日期 ：2017/11/13
     * @时间 ：9:53
     */
    @PostMapping(value = "/findUserMap")
    public Map<Long, User> findUserMap(@RequestBody Collection<Long> collection) {
        return userService.findUserMap(collection);
    }

    @GetMapping(value = "/findUserByUserId/{userId}")
    public User findUserByUserId(@PathVariable("userId") Long userId) {
        return userService.findUserByUserId(userId);
    }


    @RequestMapping(value = "/updateUserPassword", method = RequestMethod.POST)
    public User updateUserPassword(@RequestBody User user) {
        User update = userService.update(user);
        return update;
    }

    /**
     * @描述 ：门户 企业用户注册
     * @作者 ：gaoj
     * @日期 ：2018/5/25
     * @时间 ：9:53
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@RequestBody RegisterVO register) {
        return userService.register(register);
    }
}
