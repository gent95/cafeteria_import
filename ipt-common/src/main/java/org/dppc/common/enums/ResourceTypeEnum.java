package org.dppc.common.enums;

/**
 * @描述： 资源类型
 * @作者： 颜齐
 * @日期： 2017/10/17.
 * @时间： 11:45.
 */
public enum ResourceTypeEnum {
    BUTTON(1,"BUTTON"),
    MENU(2,"MENU");
    private int value;
    private String name;

    ResourceTypeEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        ResourceTypeEnum.

        this.name = name;
    }
}
