package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

/**
 * @描述 : 健康状况(1健康、2体弱多病、3长期慢性病、4患有大病、5残疾人)
 * @作者 :	zhumh
 * @日期 :	2018/5/22
 * @时间 :	9:34
 */
public enum HealthConditionEnum {
    HEALTH("健康",1),
    INFIRM("体弱多病",2),
    CHRONICDISEASE("长期慢性病",3),
    ILLNESS("患有大病",4),
    DISABLED("残疾人",5);

    private String name;
    private int value;

    HealthConditionEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public Map<String,String> getMap(){
        Map<String,String> m = new HashMap<>();
        m.put("name",getName());
        m.put("value",String.valueOf(getValue()));
        return m;
    }

    public static HealthConditionEnum valueOf(int value) {
        HealthConditionEnum[] enums = values();
        for (HealthConditionEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + HealthConditionEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }
}
