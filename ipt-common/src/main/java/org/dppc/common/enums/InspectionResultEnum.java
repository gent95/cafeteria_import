package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 :  检测结果：0为不合格，1为合格)
 * @作者 :  GAO JING
 * @日期 :  2018/5/9
 * @时间 :  10:33
 */
public enum InspectionResultEnum {

    NOT_PASS("不合格", 0)
    ,PASS("合格", 1)
    ;

    private String name;
    private Integer value;


    InspectionResultEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public static String stringOf(String value) {
        int i = Integer.parseInt(value);
        InspectionResultEnum[] enums = values();
        for (InspectionResultEnum type : enums) {
            if (i == type.value) {
                return type.getName();
            }
        }
        return "无";
    }

    public static InspectionResultEnum valueof(int value) {
        InspectionResultEnum[] enums = values();
        for (InspectionResultEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + InspectionResultEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList(){
        List<EnumTypeVO> list = new ArrayList<>();
        for (InspectionResultEnum type : values()){
            list.add(new EnumTypeVO(type.getName(), String.valueOf(type.getValue())));
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
