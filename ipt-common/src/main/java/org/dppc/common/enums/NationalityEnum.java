package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @描述 :  民族
 * @作者 :	zhumh
 * @日期 :	2018/5/21
 * @时间 :	15:50
 */
public enum NationalityEnum {
    HANCLAN("汉族",1),
    MONGOLIACLAN("蒙古族",2),
    RETURNCLAN("回族",3),
    SPROUTCLAN("苗族",4),
    TAINLEICLAN("傣族",5);

    private String name;
    private int value;

    NationalityEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    NationalityEnum() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public Map<String,String> getNationalityEnumMap(){
        Map<String,String> m = new HashMap<>();
        m.put("name",getName());
        m.put("value",String.valueOf(getValue()));
        return m;
    }

    public static NationalityEnum valueOf(int value) {
        NationalityEnum[] enums = values();
        for (NationalityEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + NationalityEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }
}
