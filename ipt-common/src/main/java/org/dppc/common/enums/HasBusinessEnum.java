package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @描述 :  下游是否有商户(0无,1有)
 * @作者 :  Zhao Yun
 * @日期 :  2017/12/4
 * @时间 :  14:26
 */
public enum HasBusinessEnum {

    NO_BUSINESS("无", 0)
    , HAS_BUSINESS("有", 1)
    ;

    private String name;
    private Integer value;


    HasBusinessEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public static HasBusinessEnum valueOf(int value) {
        HasBusinessEnum[] enums = values();
        for (HasBusinessEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + HasBusinessEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
