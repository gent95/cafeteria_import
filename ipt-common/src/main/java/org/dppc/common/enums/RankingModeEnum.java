package org.dppc.common.enums;

/**
 * @描述 单期考核详情 (1:按企业类型排名; 2: 按流通节点排名; 3: 按考核指标排名)
 * @作者 Guo Ze Qiang
 * @日期 2017/12/15
 * @时间 16:13
 */
public enum RankingModeEnum {

    ENTERPRISE("按企业排名", "1"),
    CURRENT_NODE_TYPE("按流通节点类型排名", "2"),
    CHECK_INDEX("按考核指标排名", "3");


    private String name;
    private String value;

    RankingModeEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    RankingModeEnum() {
    }

    public static RankingModeEnum valueof(String value) {
        RankingModeEnum[] rankingModeEnas = values();
        for (RankingModeEnum rankingModeEnum : rankingModeEnas) {
            if (rankingModeEnum.getValue().equals(value)) {
                return rankingModeEnum;
            }
        }
        throw new IllegalArgumentException("未定义" + RankingModeEnum.class.getName() + "，value=" + value + "的数据");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "RankingModeEnum{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
