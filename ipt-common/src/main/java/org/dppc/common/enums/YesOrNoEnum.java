package org.dppc.common.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/4/11.
 * @时间： 19:36.
 */
public enum YesOrNoEnum {
    NO(0,"否"),
    YES(1,"是");

    private int value;
    private String name;

    YesOrNoEnum(int value, String name){
        this.value = value;
        this.name = name;
    }

    public static YesOrNoEnum valueOf(int value) {
        YesOrNoEnum[] yesOrNoEnums = values();
        for (YesOrNoEnum type : yesOrNoEnums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("YesOrNoEnum，value=" + value + "的数据");
    }
    public static Map<Integer, String> fromatToMap() {
        Map<Integer, String> map = new HashMap<>();
        YesOrNoEnum[] values = values();
        for (YesOrNoEnum returnType : values) {
            map.put( returnType.value,returnType.getName());
        }
        return map;
    }
    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
