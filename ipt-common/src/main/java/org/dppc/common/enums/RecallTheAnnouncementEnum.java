package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 : 召回公布情况(0 未公布、1 已公布)
 * @作者 : lhw
 * @时间 : 2018/7/19 11:52
 */
public enum RecallTheAnnouncementEnum {
    NON_PUBLISH("未公布", 0),
    publish("已公布", 1);


    RecallTheAnnouncementEnum(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    private String name;
    private Integer value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }


    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    public static RecallTheAnnouncementEnum valueOf(Integer value) {
        RecallTheAnnouncementEnum[] enums = values();
        for (RecallTheAnnouncementEnum type : enums) {
            if (type.getValue().equals(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + RecallTheAnnouncementEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        RecallTheAnnouncementEnum[] values = RecallTheAnnouncementEnum.values();
        for (RecallTheAnnouncementEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    public static RecallTheAnnouncementEnum valueOf(int value) {
        RecallTheAnnouncementEnum[] enums = values();
        for (RecallTheAnnouncementEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + RecallTheAnnouncementEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
