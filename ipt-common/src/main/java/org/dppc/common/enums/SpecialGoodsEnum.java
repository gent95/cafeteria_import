package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created by yanqi on 2018/4/21.
 */
public enum SpecialGoodsEnum {
    //1枸杞、2葡萄酒、3瓜菜、4牛羊肉、5粮油、6乳制品、7其他
    //后加：8中药9西药10原肉菜平台
    //在统计数据时，中药与原肉菜平台计入“其它”统计
    MEDLAR(1, "0614217", "枸杞", new String[]{"0614217"}),
    WINE(2, "2421", "葡萄酒", new String[]{"2421"}),
    MELON_VEGETABLE(3, "012,013", "瓜菜", new String[]{"012", "013"}),
    BEEF_AND_MUTTON(4, "211192,211190,21117,21116,21115,21111", "牛羊肉", new String[]{"211192", "211190", "21117", "21116", "21115", "21111"}),
    GRAIN_OIL(5, "216,011", "粮油", new String[]{"216", "011"}),
    DAIRY(6, "22", "乳制品", new String[]{"22"}),
    OTHER(7, "", "其他", new String[]{}),
    CHINESE_MEDICAL(8, "06", "中药", new String[]{"06"}),
    OLD_ROUCAI(10, "211,01", "原肉菜平台", new String[]{"211", "01"});


    SpecialGoodsEnum(Integer value, String code, String name, String[] indexCode) {
        this.value = value;
        this.code = code;
        this.name = name;
        this.indexCode = indexCode;
    }

    /**
     * code  品种编码(GB/T 7635.1-2002《全国主要产品分类与代码 第一部分：可运输产品》
     * 中六大品类的品类编码的前n位
     */
    private String code;
    /**
     * 编码
     */
    private Integer value;
    /**
     * 名称
     */
    private String name;
    /**
     * 起始值，部分商品的起始值是有多个的，比如， 粮油， 粮食是一个，油是一个
     */
    private String[] indexCode;


    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), getValue().toString(), getCode());
    }

    public static SpecialGoodsEnum valueof(Integer value) {
        SpecialGoodsEnum[] specialGoodsEnum = values();
        for (SpecialGoodsEnum type : specialGoodsEnum) {
            if (type.getValue().equals(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException("nodeTypeEnum，value=" + value + "的数据");
    }

    public static SpecialGoodsEnum[] getNXSpecials() {
        return new SpecialGoodsEnum[]{
                SpecialGoodsEnum.MEDLAR,
                SpecialGoodsEnum.WINE,
                SpecialGoodsEnum.MELON_VEGETABLE,
                SpecialGoodsEnum.BEEF_AND_MUTTON,
                SpecialGoodsEnum.GRAIN_OIL,
                SpecialGoodsEnum.DAIRY,
        };
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String[] getIndexCode() {
        return indexCode;
    }

    public void setIndexCode(String[] indexCode) {
        this.indexCode = indexCode;
    }
}
