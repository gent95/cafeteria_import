package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：
 * @日期 ：2017/8/22
 * @时间 ：17:06
 */
public enum LogTypeEnum {
    USER("用户操作", 1), SYSTEM("系统日志", 2);
    private String name;
    private int value;

    LogTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public static LogTypeEnum valueOf(int value) {
        LogTypeEnum[] enums = values();
        for (LogTypeEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + LogTypeEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        LogTypeEnum[] values = LogTypeEnum.values();
        for (LogTypeEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
