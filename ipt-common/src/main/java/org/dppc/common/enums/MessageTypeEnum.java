package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @描述 :   留言类型枚举
 * @作者 :   GAOJ
 * @日期 :   2017/11/2 0002
 * @时间 :   15:11
 */
public enum MessageTypeEnum {

    CONSULTATION(1, "咨询类"),
    PROPOSAL(2, "建议类"),
    COMPLAINT(3, "投诉类"),
    PRAISE(4, "褒奖类");

    private int value;
    private String name;

    MessageTypeEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
