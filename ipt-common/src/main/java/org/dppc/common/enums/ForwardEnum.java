package org.dppc.common.enums;

import java.util.ArrayList;
import java.util.List;

public enum ForwardEnum {
    POOR_HOUSEHOLDS_INFO("/node/poorHouseholdsInfo", "贫困户备案", new ValidRuleEnum[]{ValidRuleEnum.ID_CARD_NO1}),
    TRACE_BASE_NODE("/node/traceBaseNode", "企业备案", new ValidRuleEnum[]{ValidRuleEnum.SOCIAL_CREDIT_CODE1}),
    TRACE_HUMAN_NODE("/node/traceHumanNode", "自然人责任主体", new ValidRuleEnum[]{ValidRuleEnum.ID_CARD_NO2}),
    TRACE_VARIETY_TYPE("/node/traceVarietyType", "追溯品种类型", new ValidRuleEnum[]{ValidRuleEnum.SOCIAL_CREDIT_CODE}),
    TRACE_SEED("/plant/traceSeed", "种子采购", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_PESTICIDE("/plant/tracePesticide", "农药采购", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_CHEMICAL_FERTILIZER("/plant/traceChemicalFertilizer", "肥料采购", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_PLANT_INPUTS("/plant/tracePlantInputs", "种植其他投入品采购", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_PLANT_GROUND("/plant/tracePlantGround", "种植基地", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_WEATHER_DETAL("/plant/traceWeatherDetail", "种植基地实时气象", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_PLANT_AREA("/plant/tracePlantArea", "种植地块", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_PLANT("/plant/tracePlant", "种植信息", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_PLANT_PROCESS("/plant/tracePlantProcess", "种植过程", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_PLANT_WATER("/plant/tracePlantWater", "种植灌溉", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_PLANT_PERTICIDE("/plant/tracePlantPerticide", "种植施药", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_HARVEST("/plant/traceHarvest", "种植收获", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_PLANT_TRAN("/plant/tracePlantTran", "种植交易", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE, ValidRuleEnum.TRACE_CODE_OUT, ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT, ValidRuleEnum.AMOUNT}),
    TRACE_BREED_SOURCE("/breed/traceBreedSource", "幼崽采购", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_FEED("/breed/traceFeed", "饲料采购", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_REGISTRATION("/breed/traceRegistration", "兽药采购", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_BREED_INPUTS("/breed/traceBreedInputs", "养殖其他投入品采购", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_FEED_SITE("/breed/traceFeedSite", "养殖基地", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_COLONY_HOUSE("/breed/traceColonyHouse", "养殖圈舍", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_BREED_PROCESS("/breed/traceBreedProcess", "养殖过程", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_BREED("/breed/traceBreed", "养殖基础", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_BREED_HARVEST("/breed/traceBreedHarvest", "养殖收获", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_BREED_TRAN("/breed/traceBreedTran", "养殖交易", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE, ValidRuleEnum.TRACE_CODE_OUT, ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT, ValidRuleEnum.AMOUNT}),
    TRACE_PRODUCTION_MATERIALS("/production/traceProductionMaterials", "生产加工原料采购", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE, ValidRuleEnum.UP_TRACE_CODE,ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT,ValidRuleEnum.PURCHASE_AMOUNT}),
    TRACE_PRODUCTION_INPUTS("/production/traceProductionInputs", "生产加工投料采购", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_PRODUCTION_PROCESS("/production/traceProductionProcess", "生产加工过程", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_PRODUCTION("/production/traceProduction", "生产加工基础", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_DETECTION("/production/traceDetection", "生产企业检验", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_PACKAGE_CODING("/production/tracePackageCoding", "分包赋码", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE, ValidRuleEnum.TRACE_CODE_PACK, ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT, ValidRuleEnum.AMOUNT}),
    TRACE_PRODUCTION_TRAN("/production/traceProductionTran", "生产加工交易", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE, ValidRuleEnum.TRACE_CODE_OUT, ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT, ValidRuleEnum.AMOUNT}),
    TRACE_LOGISTICS("/logistics/traceLogistics", "物流", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE, ValidRuleEnum.TRACE_CODE_OUT, ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT, ValidRuleEnum.AMOUNT}),
    TRACE_ANIMAL_IN("/animal/traceAnimalIn", "屠宰厂牲畜进场", new ValidRuleEnum[]{ValidRuleEnum.BUTCHER_FAC_ID, ValidRuleEnum.QUARANTINE_ID, ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT, ValidRuleEnum.AMOUNT}),
    TRACE_QUARANTINE("/animal/traceQuarantine", "屠宰厂检疫检验", new ValidRuleEnum[]{ValidRuleEnum.BUTCHER_FAC_ID}),
    TRACE_PRODUCT_PROCESS("/animal/traceProductProcess", "屠宰过程", new ValidRuleEnum[]{ValidRuleEnum.BUTCHER_FAC_ID}),
    TRACE_MEAT_OUT("/animal/traceMeatOut", "屠宰厂肉品交易", new ValidRuleEnum[]{ValidRuleEnum.BUTCHER_FAC_ID, ValidRuleEnum.TRACE_CODE_OUT, ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT, ValidRuleEnum.AMOUNT}),
    TRACE_MARKET_IN("/market/traceMarketIn", "批发市场进场", new ValidRuleEnum[]{ValidRuleEnum.MARKET_ID, ValidRuleEnum.UP_TRACE_CODE, ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT, ValidRuleEnum.AMOUNT}),
    TRACE_MARKET_DETECTION("/market/traceMarketDetection", "批发市场检验", new ValidRuleEnum[]{ValidRuleEnum.MARKET_ID}),
    TRACE_MARKET_TRAN("/market/traceMarketTran", "批发市场交易", new ValidRuleEnum[]{ValidRuleEnum.MARKET_ID, ValidRuleEnum.TRACE_CODE_OUT, ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT, ValidRuleEnum.AMOUNT}),
    TRACE_RETAIL_MARKET_IN("/retailMarket/traceRetailMarketIn", "零售市场进场", new ValidRuleEnum[]{ValidRuleEnum.RETAIL_ID, ValidRuleEnum.UP_TRACE_CODE, ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT, ValidRuleEnum.AMOUNT}),
    TRACE_RETAIL_MARKET_TRAN("/retailMarket/traceRetailMarketTran", "零售市场销售", new ValidRuleEnum[]{ValidRuleEnum.RETAIL_ID, ValidRuleEnum.TRACE_CODE_OUT, ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT, ValidRuleEnum.AMOUNT}),
    TRACE_SUPER_MARKET_IN("/superMarket/traceSuperMarketIn", "超市进场", new ValidRuleEnum[]{ValidRuleEnum.SUPERMARKET_ID, ValidRuleEnum.UP_TRACE_CODE, ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT, ValidRuleEnum.PURCHASE_AMOUNT}),
    TRACE_TEAM_BUY_ACCEPTANCE("/team/traceTeamBuyAcceptance", "团体消费", new ValidRuleEnum[]{ValidRuleEnum.TEAM_CONSUME_ID, ValidRuleEnum.UP_TRACE_CODE, ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT, ValidRuleEnum.PURCHASE_AMOUNT}),
    TRACE_EBUSINESS_IN("/ebusiness/traceEbusinessIn", "电商进场", new ValidRuleEnum[]{ValidRuleEnum.EBUSINESS_ID, ValidRuleEnum.UP_TRACE_CODE, ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT, ValidRuleEnum.PURCHASE_AMOUNT}),
    TRACE_EBUSINESS_TRAN("/ebusiness/traceEbusinessTran", "电商销售", new ValidRuleEnum[]{ValidRuleEnum.EBUSINESS_ID, ValidRuleEnum.TRACE_CODE_OUT, ValidRuleEnum.SPECIFICATION, ValidRuleEnum.UNIT, ValidRuleEnum.AMOUNT}),
    TRACE_RECALL("/recall/traceRecall", "召回", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_DEALING("/recall/traceDealing", "处置", new ValidRuleEnum[]{ValidRuleEnum.ENTERPRISE_CODE}),
    TRACE_CODE("/traceCode/requestCode", "追溯码下发", new ValidRuleEnum[]{});

    private String url;
    private String description;
    private ValidRuleEnum[] validRuleEnum;

    ForwardEnum(String url, String description, ValidRuleEnum[] validRuleEnum) {
        this.url = url;
        this.description = description;
        this.validRuleEnum = validRuleEnum;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ValidRuleEnum[] getValidRuleEnum() {
        return validRuleEnum;
    }

    public void setValidRuleEnum(ValidRuleEnum[] validRuleEnum) {
        this.validRuleEnum = validRuleEnum;
    }

    public static List<ForwardEnum> getRequired() {
        List<ForwardEnum> requiredlist = new ArrayList<>();
        requiredlist.add(ForwardEnum.POOR_HOUSEHOLDS_INFO);
        requiredlist.add(ForwardEnum.TRACE_BASE_NODE);
        requiredlist.add(ForwardEnum.TRACE_HUMAN_NODE);
        requiredlist.add(ForwardEnum.TRACE_PLANT_GROUND);
        requiredlist.add(ForwardEnum.TRACE_PLANT);
        requiredlist.add(ForwardEnum.TRACE_PLANT_WATER);
        requiredlist.add(ForwardEnum.TRACE_PLANT_PERTICIDE);
        requiredlist.add(ForwardEnum.TRACE_HARVEST);
        requiredlist.add(ForwardEnum.TRACE_PLANT_TRAN);
        requiredlist.add(ForwardEnum.TRACE_BREED_SOURCE);
        requiredlist.add(ForwardEnum.TRACE_FEED_SITE);
        requiredlist.add(ForwardEnum.TRACE_COLONY_HOUSE);
        requiredlist.add(ForwardEnum.TRACE_BREED);
        requiredlist.add(ForwardEnum.TRACE_BREED_HARVEST);
        requiredlist.add(ForwardEnum.TRACE_BREED_TRAN);
        requiredlist.add(ForwardEnum.TRACE_PRODUCTION_MATERIALS);
        requiredlist.add(ForwardEnum.TRACE_PRODUCTION);
        requiredlist.add(ForwardEnum.TRACE_DETECTION);
        requiredlist.add(ForwardEnum.TRACE_PRODUCTION_TRAN);
        requiredlist.add(ForwardEnum.TRACE_LOGISTICS);
        requiredlist.add(ForwardEnum.TRACE_ANIMAL_IN);
        requiredlist.add(ForwardEnum.TRACE_QUARANTINE);
        requiredlist.add(ForwardEnum.TRACE_MEAT_OUT);
        requiredlist.add(ForwardEnum.TRACE_MARKET_IN);
        requiredlist.add(ForwardEnum.TRACE_MARKET_DETECTION);
        requiredlist.add(ForwardEnum.TRACE_MARKET_TRAN);
        requiredlist.add(ForwardEnum.TRACE_RETAIL_MARKET_IN);
        requiredlist.add(ForwardEnum.TRACE_RETAIL_MARKET_TRAN);
        requiredlist.add(ForwardEnum.TRACE_SUPER_MARKET_IN);
        requiredlist.add(ForwardEnum.TRACE_TEAM_BUY_ACCEPTANCE);
        requiredlist.add(ForwardEnum.TRACE_EBUSINESS_IN);
        requiredlist.add(ForwardEnum.TRACE_EBUSINESS_TRAN);
        requiredlist.add(ForwardEnum.TRACE_RECALL);
        requiredlist.add(ForwardEnum.TRACE_DEALING);
        return requiredlist;
    }

    public static List<ForwardEnum> getNoRequiredlist() {
        List<ForwardEnum> noRequiredlist = new ArrayList<>();
        noRequiredlist.add(ForwardEnum.TRACE_VARIETY_TYPE);
        noRequiredlist.add(ForwardEnum.TRACE_SEED);
        noRequiredlist.add(ForwardEnum.TRACE_PESTICIDE);
        noRequiredlist.add(ForwardEnum.TRACE_CHEMICAL_FERTILIZER);
        noRequiredlist.add(ForwardEnum.TRACE_PLANT_INPUTS);
        noRequiredlist.add(ForwardEnum.TRACE_WEATHER_DETAL);
        noRequiredlist.add(ForwardEnum.TRACE_PLANT_AREA);
        noRequiredlist.add(ForwardEnum.TRACE_PLANT_PROCESS);
        noRequiredlist.add(ForwardEnum.TRACE_FEED);
        noRequiredlist.add(ForwardEnum.TRACE_REGISTRATION);
        noRequiredlist.add(ForwardEnum.TRACE_BREED_INPUTS);
        noRequiredlist.add(ForwardEnum.TRACE_BREED_PROCESS);
        noRequiredlist.add(ForwardEnum.TRACE_PRODUCTION_INPUTS);
        noRequiredlist.add(ForwardEnum.TRACE_PRODUCTION_PROCESS);
        noRequiredlist.add(ForwardEnum.TRACE_PACKAGE_CODING);
        noRequiredlist.add(ForwardEnum.TRACE_PRODUCT_PROCESS);
        return noRequiredlist;
    }

}
