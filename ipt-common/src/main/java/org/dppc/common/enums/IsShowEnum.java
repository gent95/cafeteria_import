package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 :   新闻是否展示
 * @作者 :   GAOJ
 * @日期 :   2017/11/2 0002
 * @时间 :   15:11
 */
public enum IsShowEnum {

    HIDE(0,"下架"),
    SHOW(1,"展示");

    private int value;
    private String name;

    IsShowEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public static IsShowEnum valueOf(int value) {
        IsShowEnum[] enums = values();
        for (IsShowEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + IsShowEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList(){
        List<EnumTypeVO> list = new ArrayList<>();
        for (IsShowEnum type : values()){
            list.add(new EnumTypeVO(type.getName(), String.valueOf(type.getValue())));
        }
        return list;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
