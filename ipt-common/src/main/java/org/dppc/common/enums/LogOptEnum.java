package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：
 * @作者 ：wyl
 * @日期 ：2017/8/22
 * @时间 ：17:06
 */
public enum LogOptEnum {

    ADD("添加", 1),
    UPDATE("修改", 2),
    DELETE("删除", 3),
    LOGIN("登录",4),
    LOGOUT("退出",5),
    OTHER("其他",6),
    INTERFACE("接口",7);

    private String name;
    private int value;

    LogOptEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public static LogOptEnum valueOf(int value) {
        LogOptEnum[] enums = values();
        for (LogOptEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + LogOptEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static LogOptEnum nameOf(String name) {
        LogOptEnum[] enums = values();
        for (LogOptEnum type : enums) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + LogOptEnum.class.getSimpleName() + "，name=" + name + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        LogOptEnum[] values = LogOptEnum.values();
        for (LogOptEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }
    public static List<EnumTypeVO> getQueryList() {
        List<EnumTypeVO> list = new ArrayList<>();
        LogOptEnum[] values = LogOptEnum.values();
        for (LogOptEnum v : values) {
            list.add(new EnumTypeVO(v.getName(), v.name()));
        }
        return list;
    }

    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "LogOptEnum{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
