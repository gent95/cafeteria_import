package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：部门级别
 * @作者 ：wyl
 * @日期 ：2017/12/11
 * @时间 ：14:45
 */
public enum DepartmentLevelEnum {


    ONE_LEVEL_DEPARTMENT(1, "一级部门"),
    TWO_LEVEL_DEPARTMENT(2, "二级部门");
    private String name;
    private int value;

    DepartmentLevelEnum(int value, String name) {
        this.name = name;
        this.value = value;
    }

    public static DepartmentLevelEnum valueOf(int value) {
        DepartmentLevelEnum[] enums = values();
        for (DepartmentLevelEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + DepartmentLevelEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        DepartmentLevelEnum[] values = DepartmentLevelEnum.values();
        for (DepartmentLevelEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
