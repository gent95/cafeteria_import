package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ： 用户类型
 * @作者 ： 颜齐
 * @日期 ： 2017/12/25.
 * @时间 ： 19:42
 */
public enum UserTypeEnum {

    SYS_USER(1,"云平台用户"),//暂时未用到
    INTERFACE_USER(2,"监管平台用户"),//也是企业账户，可以在门户登陆
    PROVINCE_USER(3,"学校平台用户");
//    CITY_USER(4,"市级账户"),
//    INTERFACE_CITY_USER(5,"市级接口用户");

    private int value;
    private String name;

    UserTypeEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }
    public static UserTypeEnum valueOf(int value) {
        UserTypeEnum[] enums = values();
        for (UserTypeEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + UserTypeEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        UserTypeEnum[] values = UserTypeEnum.values();
        for (UserTypeEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
