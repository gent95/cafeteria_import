package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @描述 :  是否首页导航：是、否
 * @作者 :  Zhao Yun
 * @日期 :  2018/2/7
 * @时间 :  11:40
 */
public enum IsMenuEnum {

    YES("是", 0),
    NO("否", 1)
    ;

    private String name;
    private Integer value;


    IsMenuEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public static IsMenuEnum valueOf(int value) {
        IsMenuEnum[] enums = values();
        for (IsMenuEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + IsMenuEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
