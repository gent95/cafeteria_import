package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 :  检验标准(选择：1 国家标准、2 企业标准、3 地方标准)
 * @作者 :  GAO JING
 * @日期 :  2018/5/9
 * @时间 :  10:33
 */
public enum InspectionStandardEnum {

    COUNTRY("国家标准", 1)
    ,ENTERPRISE("企业标准", 2)
    ,LOCAL("企业标准", 3)
    ;

    private String name;
    private Integer value;


    InspectionStandardEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public static String stringOf(String value) {
        int i = Integer.parseInt(value);
        InspectionStandardEnum[] enums = values();
        for (InspectionStandardEnum type : enums) {
            if (i == type.value) {
                return type.getName();
            }
        }
        return "无";
    }

    public static InspectionStandardEnum valueof(int value) {
        InspectionStandardEnum[] enums = values();
        for (InspectionStandardEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + InspectionStandardEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList(){
        List<EnumTypeVO> list = new ArrayList<>();
        for (InspectionStandardEnum type : values()){
            list.add(new EnumTypeVO(type.getName(), String.valueOf(type.getValue())));
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
