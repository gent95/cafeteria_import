package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：婚姻状况
 * @作者 ：GAOJ
 * @日期 ：2017/8/4
 * @时间 ：7:55
 */
public enum MaritalStatusEnum {

    NOT(1, "未婚"),
    HAVE(2, "已婚");
    private String name;
    private int value;

    MaritalStatusEnum(int value, String name) {
        this.name = name;
        this.value = value;
    }

    public static MaritalStatusEnum valueOf(int value) {
        MaritalStatusEnum[] enums = values();
        for (MaritalStatusEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + MaritalStatusEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        MaritalStatusEnum[] values = MaritalStatusEnum.values();
        for (MaritalStatusEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
