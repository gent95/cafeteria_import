package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述： 标识菜单启用或禁用状态，或其他表启用禁用状态
 * @作者： 颜齐
 * @日期： 2017/6/5.
 * @时间： 14:11.
 */
public enum EnableEnum {

    ENABLE(1, "启用"),
    DISABLE(0, "禁用");

    private int value;
    private String name;

    EnableEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public static EnableEnum valueOf(int value) {
        EnableEnum[] enums = values();
        for (EnableEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + EnableEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        EnableEnum[] values = EnableEnum.values();
        for (EnableEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
