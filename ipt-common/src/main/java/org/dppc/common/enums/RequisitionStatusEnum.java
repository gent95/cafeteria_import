package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：领用单状态、1 已填单、2 已出库
 * @作者 ：GAOJ
 * @日期 ：2017/8/4
 * @时间 ：7:55
 */
public enum RequisitionStatusEnum {

    RECORD(1, "已填单"),
    OUT_BASE(2, "已出库");
    private String name;
    private int value;

    RequisitionStatusEnum(int value, String name) {
        this.name = name;
        this.value = value;
    }

    public static RequisitionStatusEnum valueOf(int value) {
        RequisitionStatusEnum[] enums = values();
        for (RequisitionStatusEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + RequisitionStatusEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        RequisitionStatusEnum[] values = RequisitionStatusEnum.values();
        for (RequisitionStatusEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
