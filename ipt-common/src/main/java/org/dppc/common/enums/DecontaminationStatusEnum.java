package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：清洗状态、0 未清洗、1 已清洗
 * @作者 ：GAOJ
 * @日期 ：2017/8/4
 * @时间 ：7:55
 */
public enum DecontaminationStatusEnum {

    NOT(0, "未清洗"),
    ALREADY(1, "已清洗");

    private String name;
    private int value;

    DecontaminationStatusEnum(int value, String name) {
        this.name = name;
        this.value = value;
    }

    public static DecontaminationStatusEnum valueOf(int value) {
        DecontaminationStatusEnum[] enums = values();
        for (DecontaminationStatusEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + DecontaminationStatusEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        DecontaminationStatusEnum[] values = DecontaminationStatusEnum.values();
        for (DecontaminationStatusEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
