package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 :  审核状态
 * @作者 :	liutian
 * @日期 :	2017/12/11
 * @时间 :	9:49
 */
public enum AuditStatusEnum {

    NOT_AUDIT(0, "未审核"),
    THROUGH(1,"审核通过"),
    NOT_THROUGH(2, "审核未通过");

    private int value;
    private String name;

    AuditStatusEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public static AuditStatusEnum valueOf(int value) {
        AuditStatusEnum[] enums = values();
        for (AuditStatusEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + AuditStatusEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        AuditStatusEnum[] values = AuditStatusEnum.values();
        for (AuditStatusEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
