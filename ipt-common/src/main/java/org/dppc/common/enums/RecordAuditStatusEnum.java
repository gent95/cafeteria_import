package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @描述 :  审核状态(0:备案未审核；1：备案通过；2：备案未通过 )
 * @作者 :  Zhao Yun
 * @日期 :  2017/11/13
 * @时间 :  9:44
 */
public enum RecordAuditStatusEnum {

    NOT_AUDIT("待审核", 0)
    , AUDIT_PASSED("审核通过", 1)
    , AUDIT_NOT_PASSED("审核未通过", 2)
    ;

    private String name;
    private Integer value;


    RecordAuditStatusEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public static RecordAuditStatusEnum valueOf(int value) {
        RecordAuditStatusEnum[] enums = values();
        for (RecordAuditStatusEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + RecordAuditStatusEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
