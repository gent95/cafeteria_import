package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：学校类型
 * @作者 ：GAOJ
 * @日期 ：2017/8/4
 * @时间 ：7:55
 */
public enum SchoolTypeEnum {

    SCHOOL(1, "学校")
    ,HOTEL(2, "饭店")
    /*KINDERGARTEN(1, "幼儿园")
    ,CHILDCARE(2, "托幼机构")
    ,PRIMARY_SCHOOL(3, "小学")
    ,JUNIOR_MIDDLE_SCHOOL(4, "初中")
    ,HIGH_SCHOOL(5, "高中")
    ,UNIVERSITY(6, "大学")
    ,VOCATIONAL_EDUCATION(7, "职业教育学校")
    ,SPECIAL_SCHOOLS(8, "特殊学校")*/
    ;
    private String name;
    private int value;

    SchoolTypeEnum(int value, String name) {
        this.name = name;
        this.value = value;
    }

    public static SchoolTypeEnum valueOf(int value) {
        SchoolTypeEnum[] enums = values();
        for (SchoolTypeEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + SchoolTypeEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        SchoolTypeEnum[] values = SchoolTypeEnum.values();
        for (SchoolTypeEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
