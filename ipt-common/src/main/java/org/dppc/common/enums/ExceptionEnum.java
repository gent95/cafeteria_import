package org.dppc.common.enums;

/**
 * @描述： 错误异常
 * @作者： 颜齐
 * @日期： 2017/10/19.
 * @时间： 17:20.
 */
public enum ExceptionEnum {
    OK(200, "ok", "操作成功"),
    EX_GRAMMAR_ERROR(400, "grammar_error", "服务器不理解请求的语法"),
    EX_PARAM_ERROR(40001, "param_error", "参数错误"),
    EX_NOT_FOUND(404, "not_found", "目标未发现"),
    EX_REQUEST_EXPIRE(408, "request_expire", "（请求超时） 服务器等候请求时发生超时"),
    EX_REQUEST_DATE_ERROR(40801, "request_date_error", "请求时间与服务器时间相差过大"),
    EX_LOGIN_ERROR(40109, "login_error", "登陆错误"),
    EX_TOKEN_NULL(40100, "token_null", "token为空"),
    EX_TOKEN_ERROR(40101, "token_error", "token错误"),
    EX_USER_INVALID(40102, "user_invalid", "用户token无效"),
    EX_USER_NOT_ALLOW(40103, "user_not_allow", "用户无权限访问该路径"),
    EX_TOKEN_EXPIRE(40104, "token_expire", "token过期"),
    EX_TOKEN_ILLEGAL(40105, "token_illegal", "token不合法"),
    EX_CLIENT_INVALID(40131, "client_invalid", "客户端token无效"),
    EX_CLIENT_FORBIDDEN(40331, "client_forbidden", "token被禁止"),
    EX_VERIFICATION_FAIL(40441,"verification_fail","数据验证出错"),
    EX_METHOD_NOT_ALLOWED(405, "method_not_allowed", "请求方法对指定的资源不适用"),
    EX_OTHER_ERROR(500, "other_error", "服务器遇到了意料不到的错误"),
    EX_GATEWAY_TIME_OUT(501, "gateway_time_out", "网关无法从原始服务器上获取应答"),
    EX_GATEWAY_FORBIDDEN(50101, "gateway_forbidden", "网关无法转发未授权的客户端，可能是权限不允许"),

    EX_STOCK_NUMBER(50201, "stock_number_err", "库存数量异常"),
    EX_REPEAT_OPERATE(50301, "repeat_operate_err", "重复操作异常"),

    EX_USER_NO_BIND_CAFETERIA(50401, "user_no_bind_cafeteria_err", "用户未绑定食堂");

    private int status;
    private String type;
    private String msg;

    ExceptionEnum(int status, String type, String msg) {
        this.status = status;
        this.type = type;
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
