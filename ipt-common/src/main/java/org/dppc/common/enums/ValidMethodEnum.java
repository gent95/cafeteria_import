package org.dppc.common.enums;

public enum ValidMethodEnum {

    cityCode("城市编码非空且存在枚举中"),
    creditCode("统一社会信用代码非空且在企业备案信息中唯一"),
    humanId("自然人身份证号非空且在自然人责任主体信息中唯一"),
    idNumber("贫困户身份证号非空且在贫困户信息中唯一"),
    baseNode("是否有权限上传该企业数据"),
    batchCode("批次码非空"),
    traceCodeOut("追溯码验证唯一"),
    traceCodePack("加工分包追溯码验证唯一"),
    upTraceCode("上游追溯码验证，允许为空，但如果不为空，则需要执行唯一验证"),
    unit("KG单位验证"),
    specification("规格大于零，空或小于等于零则返回1"),
    numerical("非空且为数字大于零");

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    ValidMethodEnum() {
    }

    ValidMethodEnum(String description) {
        this.description = description;
    }
}
