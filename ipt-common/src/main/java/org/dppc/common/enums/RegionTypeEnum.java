package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @描述 地区类型   省市区县 (1:省; 2:市; 3:市（市中市）、区、县)
 * @作者 Guo Ze Qiang
 * @日期 2017/12/14
 * @时间 14:39
 */
public enum RegionTypeEnum {

    PROVINCE("省", 1),
    CITY("市", 2),
    DISTRICT_COUNTY("区县", 3);

    private String name;
    private int value;

    RegionTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public static RegionTypeEnum valueof(int value) {
        RegionTypeEnum[] regionTypeEnums = values();
        for (RegionTypeEnum regionTypeEnum : regionTypeEnums) {
            if (regionTypeEnum.getValue() == value) {
                return regionTypeEnum;
            }
        }
        throw new IllegalArgumentException("未定义" + RegionTypeEnum.class.getName() + "，value=" + value + "的数据");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }
}
