package org.dppc.common.enums;

/**
 * @描述： 标识摸个节点是否包含进出场
 * @作者： yanqi
 * @日期： 2018/5/28
 */
public enum NodeTypeIncludeEnum {

    ALL(1, "具有进场和出场"),
    JUST_IN(2, "只有进场"),
    JUST_OUT(3, "只有出场"),
    NOT_CERTAIN(0,"不确定");
    private int value;
    private String message;

    NodeTypeIncludeEnum(int value, String message) {
        this.value = value;
        this.message = message;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
