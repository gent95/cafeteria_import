package org.dppc.common.enums;

public enum ValidRuleEnum {

    ID_CARD_NO1(ValidColumnEnum.ID_NUMBER, ValidMethodEnum.idNumber)
    , ID_CARD_NO2(ValidColumnEnum.HUMAN_ID, ValidMethodEnum.humanId)
    , SOCIAL_CREDIT_CODE1(ValidColumnEnum.SOCIAL_CREDIT_CODE, ValidMethodEnum.creditCode)
    , SOCIAL_CREDIT_CODE(ValidColumnEnum.SOCIAL_CREDIT_CODE, ValidMethodEnum.baseNode)
    , ENTERPRISE_CODE(ValidColumnEnum.ENTERPRISE_CODE, ValidMethodEnum.baseNode)
    , BUTCHER_FAC_ID(ValidColumnEnum.BUTCHER_FAC_ID, ValidMethodEnum.baseNode)
    , MARKET_ID(ValidColumnEnum.MARKET_ID, ValidMethodEnum.baseNode)
    , RETAIL_ID(ValidColumnEnum.RETAIL_ID, ValidMethodEnum.baseNode)
    , SUPERMARKET_ID(ValidColumnEnum.SUPERMARKET_ID, ValidMethodEnum.baseNode)
    , TEAM_CONSUME_ID(ValidColumnEnum.TEAM_CONSUME_ID, ValidMethodEnum.baseNode)
    , EBUSINESS_ID(ValidColumnEnum.EBUSINESS_ID, ValidMethodEnum.baseNode)
    , TRACE_CODE_OUT(ValidColumnEnum.TRACE_CODE, ValidMethodEnum.traceCodeOut)
    , TRACE_CODE_PACK(ValidColumnEnum.TRACE_CODE, ValidMethodEnum.traceCodePack)
    , UP_TRACE_CODE(ValidColumnEnum.UP_TRACE_CODE, ValidMethodEnum.upTraceCode)
    , QUARANTINE_ID(ValidColumnEnum.QUARANTINE_ID, ValidMethodEnum.upTraceCode)
    , SPECIFICATION(ValidColumnEnum.SPECIFICATION, ValidMethodEnum.specification)
    , UNIT(ValidColumnEnum.UNIT, ValidMethodEnum.unit)
    , AMOUNT(ValidColumnEnum.AMOUNT, ValidMethodEnum.numerical)
    , PURCHASE_AMOUNT(ValidColumnEnum.PURCHASE_AMOUNT, ValidMethodEnum.numerical)
    , PRICE(ValidColumnEnum.PRICE, ValidMethodEnum.numerical)
    ;

    private ValidColumnEnum validColumnEnum;
    private ValidMethodEnum validMethodEnum;

    ValidRuleEnum(ValidColumnEnum validColumnEnum, ValidMethodEnum validMethodEnum) {
        this.validColumnEnum = validColumnEnum;
        this.validMethodEnum = validMethodEnum;
    }

    public ValidColumnEnum getValidColumnEnum() {
        return validColumnEnum;
    }

    public void setValidColumnEnum(ValidColumnEnum validColumnEnum) {
        this.validColumnEnum = validColumnEnum;
    }

    public ValidMethodEnum getValidMethodEnum() {
        return validMethodEnum;
    }

    public void setValidMethodEnum(ValidMethodEnum validMethodEnum) {
        this.validMethodEnum = validMethodEnum;
    }
}
