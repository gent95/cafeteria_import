package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：学校性质
 * @作者 ：GAOJ
 * @日期 ：2017/8/4
 * @时间 ：7:55
 */
public enum SchoolNatureEnum {

    PUBLIC(1, "公立"), PRIVATE(2, "私立"), OTHER(3, "其它");
    private String name;
    private int value;

    SchoolNatureEnum(int value, String name) {
        this.name = name;
        this.value = value;
    }

    public static SchoolNatureEnum valueOf(int value) {
        SchoolNatureEnum[] enums = values();
        for (SchoolNatureEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + SchoolNatureEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        SchoolNatureEnum[] values = SchoolNatureEnum.values();
        for (SchoolNatureEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
