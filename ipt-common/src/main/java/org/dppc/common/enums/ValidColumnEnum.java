package org.dppc.common.enums;

public enum ValidColumnEnum {
    ID_NUMBER
    ,HUMAN_ID
    ,SOCIAL_CREDIT_CODE
    ,ENTERPRISE_CODE
    ,BUTCHER_FAC_ID
    ,MARKET_ID
    ,RETAIL_ID
    ,SUPERMARKET_ID
    ,TEAM_CONSUME_ID
    ,EBUSINESS_ID
    ,QUARANTINE_ID
    ,TRACE_CODE
    ,UP_TRACE_CODE
    ,SPECIFICATION
    ,UNIT
    ,AMOUNT
    ,PURCHASE_AMOUNT
    ,QUARANTINE_NUM
    ,PRICE
    ;

    ValidColumnEnum() {
    }


}
