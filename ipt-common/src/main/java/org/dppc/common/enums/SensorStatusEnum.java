package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.List;

/**
 * @描述 :  审核状态
 * @作者 :	liutian
 * @日期 :	2017/12/11
 * @时间 :	9:49
 */
public enum SensorStatusEnum {

    NORMAL("正常", 0),
    EARLY_WARNING("预警", 1),
    STOP("离线", 2);

    private int value;
    private String name;

    SensorStatusEnum(String name, int value) {
        this.value = value;
        this.name = name;
    }

    public static SensorStatusEnum valueOf(int value) {
        SensorStatusEnum[] enums = values();
        for (SensorStatusEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + SensorStatusEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        SensorStatusEnum[] values = SensorStatusEnum.values();
        for (SensorStatusEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
