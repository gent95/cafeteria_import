package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：餐次类型、1 早餐、2 午餐、3 晚餐
 * @作者 ：GAOJ
 * @日期 ：2017/8/4
 * @时间 ：7:55
 */
public enum MealTimesEnum {

    BREAKFAST(1, "早餐"),
    LUNCH(2, "午餐"),
    DINNER(3, "晚餐");

    private String name;
    private int value;

    MealTimesEnum(int value, String name) {
        this.name = name;
        this.value = value;
    }

    public static MealTimesEnum valueOf(int value) {
        MealTimesEnum[] enums = values();
        for (MealTimesEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + MealTimesEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        MealTimesEnum[] values = MealTimesEnum.values();
        for (MealTimesEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
