package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @描述 :   节点编码枚举 （
 * @作者 :   GAOJ
 * @日期 :   2018/4/2 0002
 * @时间 :   15:11
 */
public enum NodeTypeEnum {
    ALL("0", "所有环节"),
    PLANTATION_CULTIVATION("10", "种养殖", 3),
    PLANTATION("11", "种植", 3),
    CULTIVATION("12", "养殖", 3),
    PRODUCTION_PROCESSING("20", "生产加工", 1),
    WHOLESALE("30", "批发", 1),
    ABATTOIR("31", "屠宰", 1),
    RETAIL("40", "零售", 2),
    TEAM_BUY("41", "团体消费单位", 2),
    SUPER_MARKET("42", "超市", 2),
    FARMERS("43", "农贸", 1),
    E_BUSINESS("50", "电商"),
    LOGISTICS("60", "物流"),
    OTHER("90", "其它");

    /**
     * 节点类型编码
     */
    private String value;
    /**
     * 节点名称
     */
    private String name;

    private int includeInOrOut;//使用nodeTypeIncludeEnum标识

    NodeTypeEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    NodeTypeEnum(String value, String name, int includeInOrOut) {
        this.value = value;
        this.name = name;
        this.includeInOrOut = includeInOrOut;
    }

    public static NodeTypeEnum valueof(String value) {
        NodeTypeEnum[] nodeTypeEnum = values();
        for (NodeTypeEnum type : nodeTypeEnum) {
            if (type.getValue().equals(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException("nodeTypeEnum，value=" + value + "的数据");
    }

    /**
     * 单一环节使用
     * @return
     */
    public static NodeTypeEnum[] getNodes() {
        return new NodeTypeEnum[]{
                NodeTypeEnum.PLANTATION,
                NodeTypeEnum.CULTIVATION,
                NodeTypeEnum.PRODUCTION_PROCESSING,
                NodeTypeEnum.WHOLESALE,
                NodeTypeEnum.ABATTOIR,
                NodeTypeEnum.RETAIL,
                NodeTypeEnum.TEAM_BUY,
                NodeTypeEnum.SUPER_MARKET,
                NodeTypeEnum.FARMERS,
                NodeTypeEnum.E_BUSINESS,
                NodeTypeEnum.LOGISTICS,
                NodeTypeEnum.OTHER
        };
    }

    /**
     * 工作考核使用
     * @return
     */
    public static NodeTypeEnum[] getExamineNodes() {
        return new NodeTypeEnum[]{
                NodeTypeEnum.PLANTATION,
                NodeTypeEnum.CULTIVATION,
                NodeTypeEnum.PRODUCTION_PROCESSING,
                NodeTypeEnum.WHOLESALE,
                NodeTypeEnum.ABATTOIR,
                NodeTypeEnum.RETAIL,
                NodeTypeEnum.TEAM_BUY,
                NodeTypeEnum.SUPER_MARKET,
                NodeTypeEnum.E_BUSINESS,
                NodeTypeEnum.LOGISTICS
        };
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), getValue());
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIncludeInOrOut() {
        return includeInOrOut;
    }

    public void setIncludeInOrOut(int includeInOrOut) {
        this.includeInOrOut = includeInOrOut;
    }
}
