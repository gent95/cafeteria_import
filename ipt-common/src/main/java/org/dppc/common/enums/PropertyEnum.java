package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @描述 :  经营者性质(0011 种植者,0012 养殖者,0013 渔业从业者,0021 农产品经纪人,0022 农产品贩运人)
 * @作者 :  Zhao Yun
 * @日期 :  2017/12/5
 * @时间 :  19:37
 */
public enum PropertyEnum {

    PLANT("种植者", "0011")
    , FEED("养殖者", "0012")
    , FISH("渔业从业者", "0013")
    , MANGER("农产品经纪人", "0021")
    , TRANSPORT("农产品贩运人", "0022")
    ;

    private String name;
    private String value;


    PropertyEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public static PropertyEnum valueof(String value) {
        PropertyEnum[] enums = values();
        for (PropertyEnum type : enums) {
            if (type.getValue().equals(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + PropertyEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), getValue());
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
