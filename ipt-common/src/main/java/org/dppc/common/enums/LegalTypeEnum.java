package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @描述 :  法人责任主体类型(1企业法人,2个体工商户)
 * @作者 :  Zhao Yun
 * @日期 :  2017/12/5
 * @时间 :  14:59
 */
public enum LegalTypeEnum {

    ENTERPRISE("企业法人", 1)
    , PERSONAL("个体工商户", 2)
    , OTHER("其他",3)
    ;

    private String name;
    private Integer value;


    LegalTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public static LegalTypeEnum valueOf(int value) {
        LegalTypeEnum[] enums = values();
        for (LegalTypeEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + LegalTypeEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
