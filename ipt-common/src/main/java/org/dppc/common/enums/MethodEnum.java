package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述：
 * @作者： 颜齐
 * @日期： 2017/10/26.
 * @时间： 13:14.
 */
public enum MethodEnum {
    GET(1, "GET"),
    POST(2, "POST"),
    PUT(3, "PUT"),
    PATCH(4, "PATCH"),
    DELETE(5, "DELETE"),
    ALL(6,"ALL");

    private int value;
    private String name;

    MethodEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public static MethodEnum nameOf(String name) {
        MethodEnum[] enums = values();
        for (MethodEnum method : enums) {
            if (method.getName().equals(name)) {
                return method;
            }
        }
        throw new IllegalArgumentException("未定义" + LogOptEnum.class.getSimpleName() + "，name=" + name + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        MethodEnum[] values = MethodEnum.values();
        for (MethodEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }
    public static List<EnumTypeVO> getQueryList() {
        List<EnumTypeVO> list = new ArrayList<>();
        MethodEnum[] values = MethodEnum.values();
        for (MethodEnum v : values) {
            list.add(new EnumTypeVO(v.getName(), v.name()));
        }
        return list;
    }

    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
