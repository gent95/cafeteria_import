package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 :  栏目显示 0否1是
 * @作者 :  GAO JING
 * @日期 :  2018/2/27
 * @时间 :  10:33
 */
public enum ColumnsShowsEnum {

    NO("否", 0)
    ,YES("是", 1)
    ;

    private String name;
    private Integer value;


    ColumnsShowsEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public static ColumnsShowsEnum valueOf(int value) {
        ColumnsShowsEnum[] enums = values();
        for (ColumnsShowsEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + ColumnsShowsEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList(){
        List<EnumTypeVO> list = new ArrayList<>();
        for (ColumnsShowsEnum type : values()){
            list.add(new EnumTypeVO(type.getName(), String.valueOf(type.getValue())));
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
