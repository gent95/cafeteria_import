package org.dppc.common.enums.stock;

import com.fasterxml.jackson.annotation.JsonValue;
import org.dppc.common.enums.EnumTypeVO;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：收货状态枚举
 * @作者 ：wyl
 * @日期 ：2017/8/4
 * @时间 ：7:55
 */
public enum InStatusEnum {
    TAKE_OVER(1, "已收货"),
    STAY_TAKE_OVER(0, "待收货");
    private String name;
    private int value;

    InStatusEnum(int value, String name) {
        this.name = name;
        this.value = value;
    }

    public static InStatusEnum valueOf(int value) {
        InStatusEnum[] enums = values();
        for (InStatusEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + InStatusEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        InStatusEnum[] values = InStatusEnum.values();
        for (InStatusEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
