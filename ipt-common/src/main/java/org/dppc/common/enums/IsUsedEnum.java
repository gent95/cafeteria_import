package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：是否使用枚举、0 未使用、1 已使用
 * @作者 ：gaoj
 * @日期 ：2019/5/22
 * @时间 ：7:55
 */
public enum IsUsedEnum {

    NOT_USED(0, "未使用"),
    USED(1, "已使用");

    private String name;
    private int value;

    IsUsedEnum(int value, String name) {
        this.name = name;
        this.value = value;
    }

    public static IsUsedEnum valueOf(int value) {
        IsUsedEnum[] enums = values();
        for (IsUsedEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + IsUsedEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        IsUsedEnum[] values = IsUsedEnum.values();
        for (IsUsedEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
