package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 :  检车报告类型：1自检，2第三方检测机构)
 * @作者 :  YJJ
 * @日期 :  2018/6/5
 * @时间 :  10:33
 */
public enum InspectionReoprtTypeEnum {

    SELF_INSPECTION("自检", 1)
    ,THIRD_PARTY_INSPECTION("第三方检测机构", 2)
    ;

    private String name;
    private Integer value;


    InspectionReoprtTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public static String stringOf(String value) {
        int i = Integer.parseInt(value);
        InspectionReoprtTypeEnum[] enums = values();
        for (InspectionReoprtTypeEnum type : enums) {
            if (i == type.value) {
                return type.getName();
            }
        }
        return "无";
    }

    public static InspectionReoprtTypeEnum valueof(int value) {
        InspectionReoprtTypeEnum[] enums = values();
        for (InspectionReoprtTypeEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + InspectionReoprtTypeEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList(){
        List<EnumTypeVO> list = new ArrayList<>();
        for (InspectionReoprtTypeEnum type : values()){
            list.add(new EnumTypeVO(type.getName(), String.valueOf(type.getValue())));
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
