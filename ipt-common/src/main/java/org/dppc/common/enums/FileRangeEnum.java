package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 :  Files类与image类共用枚举  文件上传来源  1web网站2用户上传3追溯系统9其他
 * @作者 :  GAO JING
 * @日期 :  2018/3/15
 * @时间 :  10:33
 */
public enum FileRangeEnum {

    WEB("web网站", 1)
    ,USER("用户上传", 2)
    ,TRACE("追溯系统", 3)
    ,EDITOR("文本编辑器", 4)
    ,OTHER("其它", 9)
    ;

    private String name;
    private Integer value;


    FileRangeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public static FileRangeEnum valueOf(int value) {
        FileRangeEnum[] enums = values();
        for (FileRangeEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + FileRangeEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList(){
        List<EnumTypeVO> list = new ArrayList<>();
        for (FileRangeEnum type : values()){
            list.add(new EnumTypeVO(type.getName(), String.valueOf(type.getValue())));
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
