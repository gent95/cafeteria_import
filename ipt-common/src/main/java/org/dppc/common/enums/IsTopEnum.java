package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @描述 :   新闻是否置顶
 * @作者 :   GAOJ
 * @日期 :   2017/11/2 0002
 * @时间 :   15:11
 */
public enum IsTopEnum {

    NO(0,"否"),
    YES(1,"是");

    private int value;
    private String name;

    IsTopEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
