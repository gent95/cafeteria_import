package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 : 召回实施情况(0 未实施、1 正在处理、2 处理完毕)
 * @作者 : lhw
 * @时间 : 2018/7/19 11:52
 */
public enum RecallSituationEnum {
    NON_USE("未实施", 0),
    PROCESSING("正在处理", 1),
    DISPOSED("处理完毕", 2);


    RecallSituationEnum(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    private String name;
    private Integer value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }


    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    public static RecallSituationEnum valueOf(Integer value) {
        RecallSituationEnum[] enums = values();
        for (RecallSituationEnum type : enums) {
            if (type.getValue().equals(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + RecallSituationEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        RecallSituationEnum[] values = RecallSituationEnum.values();
        for (RecallSituationEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    public static RecallSituationEnum valueOf(int value) {
        RecallSituationEnum[] enums = values();
        for (RecallSituationEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + RecallSituationEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
