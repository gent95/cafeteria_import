package org.dppc.common.enums;

import java.util.ArrayList;
import java.util.List;

public enum TraceTableEnum {

    POOR_HOUSEHOLDS_INFO(false, "贫困户备案", "90", ""),
    TRACE_BASE_NODE(false, "企业备案", "90", ""),
    TRACE_HUMAN_NODE(false, "自然人责任主体", "90", ""),
    TRACE_VARIETY_TYPE(false, "追溯品种类型", "90", ""),
    TRACE_SEED(false, "种子采购", "11", ""),
    TRACE_PESTICIDE(false, "农药采购", "11", ""),
    TRACE_CHEMICAL_FERTILIZER(false, "肥料采购", "11", ""),
    TRACE_PLANT_INPUTS(false, "种植其他投入品采购", "11", ""),
    TRACE_PLANT_GROUND(false, "种植基地", "11", ""),
    TRACE_WEATHER_DETAL(false, "种植基地实时气象", "11", ""),
    TRACE_PLANT_AREA(false, "种植地块", "11", ""),
    TRACE_PLANT(false, "种植信息", "11", ""),
    TRACE_PLANT_PROCESS(false, "种植过程", "11", ""),
    TRACE_PLANT_WATER(false, "种植灌溉", "11", ""),
    TRACE_PLANT_PERTICIDE(false, "种植施药", "11", ""),
    TRACE_HARVEST(false, "种植收获", "11", ""),
    TRACE_PLANT_TRAN(true, "种植交易", "11", "trace_plant_tran"),
    TRACE_BREED_SOURCE(false, "幼崽采购", "12", ""),
    TRACE_FEED(false, "饲料采购", "12", ""),
    TRACE_REGISTRATION(false, "兽药采购", "12", ""),
    TRACE_BREED_INPUTS(false, "养殖其他投入品采购", "12", ""),
    TRACE_FEED_SITE(false, "养殖基地", "12", ""),
    TRACE_COLONY_HOUSE(false, "养殖圈舍", "12", ""),
    TRACE_BREED_PROCESS(false, "养殖过程", "12", ""),
    TRACE_BREED(false, "养殖基础", "12", ""),
    TRACE_BREED_HARVEST(false, "养殖收获", "12", ""),
    TRACE_BREED_TRAN(true, "养殖交易", "12", "trace_breed_tran"),
    TRACE_PRODUCTION_MATERIALS(true, "生产加工原料采购", "20", "trace_production_materials"),
    TRACE_PRODUCTION_INPUTS(false, "生产加工投料采购", "20", ""),
    TRACE_PRODUCTION_PROCESS(false, "生产加工过程", "20", ""),
    TRACE_PRODUCTION(false, "生产加工基础", "20", ""),
    TRACE_DETECTION(false, "生产企业检验", "20", ""),
    TRACE_PACKAGE_CODING(true, "分包赋码", "20", "trace_package_coding"),
    TRACE_PRODUCTION_TRAN(true, "生产加工交易", "20", "trace_production_tran"),
    TRACE_LOGISTICS(true, "物流", "60", "trace_logistics"),
    TRACE_ANIMAL_IN(true, "屠宰厂牲畜进场", "31", "trace_animal_in"),
    TRACE_QUARANTINE(false, "屠宰厂检疫检验", "31", ""),
    TRACE_PRODUCT_PROCESS(false, "屠宰过程", "31", ""),
    TRACE_MEAT_OUT(true, "屠宰厂肉品交易", "31", "trace_meat_out"),
    TRACE_MARKET_IN(true, "批发市场进场", "30", "trace_market_in"),
    TRACE_MARKET_DETECTION(false, "批发市场检验", "30", ""),
    TRACE_MARKET_TRAN(true, "批发市场交易", "30", "trace_market_tran"),
    TRACE_RETAIL_MARKET_IN(true, "零售市场进场", "43", "trace_retail_market_in"),
    TRACE_RETAIL_MARKET_TRAN(true, "零售市场销售", "43", "trace_retail_market_tran"),
    TRACE_SUPER_MARKET_IN(true, "超市进场", "43", "trace_super_market_in"),
    TRACE_TEAM_BUY_ACCEPTANCE(true, "团体消费", "41", "trace_team_buy_acceptance"),
    TRACE_EBUSINESS_IN(true, "电商进场", "50", "trace_ebusiness_in"),
    TRACE_EBUSINESS_TRAN(true, "电商销售", "50", "trace_ebusiness_tran"),
    TRACE_RECALL(false, "召回", "90", ""),
    TRACE_DEALING(false, "处置", "90", "");

    /**
     * GAOJ 2018/4/12 是否进入中间表
     */
    private boolean middleFlag;

    private String description;

    private String nodeType;

    private String tableName;

    TraceTableEnum() {
    }

    TraceTableEnum(boolean middleFlag, String description, String nodeType) {
        this.middleFlag = middleFlag;
        this.description = description;
        this.nodeType = nodeType;
    }

    TraceTableEnum(boolean middleFlag, String description, String nodeType, String tableName) {
        this.middleFlag = middleFlag;
        this.description = description;
        this.nodeType = nodeType;
        this.tableName = tableName;
    }

    /**
     * 获取节点下面的内部环节
     *
     * @param nodeTypeEnum
     * @return
     */
    public static List<TraceTableEnum> getNodeTypeTraceTable(NodeTypeEnum nodeTypeEnum) {
        List<TraceTableEnum> result = new ArrayList<>();
        TraceTableEnum[] enums = values();
        for (TraceTableEnum table : enums) {
            if (table.getNodeType() == nodeTypeEnum.getValue()) {
                result.add(table);
            }
        }
        return result;
    }

    public boolean isMiddleFlag() {
        return middleFlag;
    }

    public void setMiddleFlag(boolean middleFlag) {
        this.middleFlag = middleFlag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public String toString() {
        return this.name();
    }
}
