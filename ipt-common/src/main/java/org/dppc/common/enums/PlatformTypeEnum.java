package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 :  0追溯平台资源，1门户后台管理资源
 * @作者 :  GAO JING
 * @日期 :  2018/4/2
 * @时间 :  10:33
 */
public enum PlatformTypeEnum {

    TRACE("追溯平台", 0)
    ,CMS("CMS后台管理", 1)
    ;

    private String name;
    private Integer value;


    PlatformTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public static PlatformTypeEnum valueOf(int value) {
        PlatformTypeEnum[] enums = values();
        for (PlatformTypeEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + PlatformTypeEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList(){
        List<EnumTypeVO> list = new ArrayList<>();
        for (PlatformTypeEnum type : values()){
            list.add(new EnumTypeVO(type.getName(), String.valueOf(type.getValue())));
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
