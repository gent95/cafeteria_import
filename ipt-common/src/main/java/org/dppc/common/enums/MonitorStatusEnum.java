package org.dppc.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 ：设备监控状态枚举
 * @作者 ：wyl
 * @日期 ：2017/11/9
 * @时间 ：16:24
 */
public enum MonitorStatusEnum {


    NORMAL(1, "正常"),
    STOP_USE(2, "停用");
//    REPAIRING(3, "维修");
    private String name;
    private int value;

    MonitorStatusEnum(int value, String name) {
        this.name = name;
        this.value = value;
    }

    public static MonitorStatusEnum valueOf(int value) {
        MonitorStatusEnum[] enums = values();
        for (MonitorStatusEnum type : enums) {
            if (type.getValue() == value) {
                return type;
            }
        }
        throw new IllegalArgumentException("未定义" + MonitorStatusEnum.class.getSimpleName() + "，value=" + value + "的数据");
    }

    public static List<EnumTypeVO> getEnumList() {
        List<EnumTypeVO> list = new ArrayList<>();
        MonitorStatusEnum[] values = MonitorStatusEnum.values();
        for (MonitorStatusEnum v : values) {
            list.add(v.getEnumTypeVO());
        }
        return list;
    }

    @JsonValue
    public EnumTypeVO getEnumTypeVO() {
        return new EnumTypeVO(getName(), String.valueOf(getValue()));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
