package org.dppc.common.context;

import org.dppc.api.admin.dto.SchoolCafteria;
import org.dppc.common.constant.CommonConstants;
import org.dppc.common.utils.StringHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 */
public class BaseContextHandler {
    public static ThreadLocal<Map<String, Object>> threadLocal = new ThreadLocal<Map<String, Object>>();

    public static void set(String key, Object value) {
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            map = new HashMap<String, Object>();
            threadLocal.set(map);
        }
        map.put(key, value);
    }

    public static Object get(String key){
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            map = new HashMap<String, Object>();
            threadLocal.set(map);
        }
        return map.get(key);
    }

    public static String getUserID(){
        Object value = get(CommonConstants.CONTEXT_KEY_USER_ID);
        return returnObjectValue(value);
    }

    public static String getUsername(){
        Object value = get(CommonConstants.CONTEXT_KEY_USERNAME);
        return returnObjectValue(value);
    }


    public static String getName(){
        Object value = get(CommonConstants.CONTEXT_KEY_USER_NAME);
        return StringHelper.getObjectValue(value);
    }

    public static String getToken(){
        Object value = get(CommonConstants.CONTEXT_KEY_USER_TOKEN);
        return StringHelper.getObjectValue(value);
    }

    public static String getCityCode(){
        Object value = get(CommonConstants.CONTEXT_KEY_USER_CITY_CODE);
        return StringHelper.getObjectValue(value);
    }

    public static String getUserType(){
        Object value = get(CommonConstants.CONTEXT_KEY_USER_TYPE);
        return StringHelper.getObjectValue(value);
    }

    public static Set<SchoolCafteria> getSchoolCafterias(){
        Set<SchoolCafteria> value = (Set<SchoolCafteria>)get(CommonConstants.SCHOOL_CAFTERIAS_KEY);
        return value;
    }

    public static void setToken(String token){set(CommonConstants.CONTEXT_KEY_USER_TOKEN,token);}

    public static void setName(String name){set(CommonConstants.CONTEXT_KEY_USER_NAME,name);}

    public static void setUserID(String userID){
        set(CommonConstants.CONTEXT_KEY_USER_ID,userID);
    }

    public static void setUsername(String username){
        set(CommonConstants.CONTEXT_KEY_USERNAME,username);
    }

    public static void setCityCode(String cityCode){
        set(CommonConstants.CONTEXT_KEY_USER_CITY_CODE,cityCode);
    }

    public static void setUserType(Integer userType){
        set(CommonConstants.CONTEXT_KEY_USER_TYPE,userType);
    }

    private static String returnObjectValue(Object value) {
        return value==null?null:value.toString();
    }

    public static void remove(){
        threadLocal.remove();
    }

    public static void setSchoolCafterias(Set<SchoolCafteria> schoolCafterias){
        set(CommonConstants.SCHOOL_CAFTERIAS_KEY,schoolCafterias);
    }


}
