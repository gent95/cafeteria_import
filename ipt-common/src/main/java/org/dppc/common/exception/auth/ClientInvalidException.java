package org.dppc.common.exception.auth;


import org.dppc.common.constant.CommonConstants;
import org.dppc.common.enums.ExceptionEnum;
import org.dppc.common.exception.BaseException;

/**
 * @描述： 客户端无效异常
 * @作者： 颜齐
 * @日期： 2017/10/19.
 * @时间： 15:13.
 */
public class ClientInvalidException extends BaseException {
    public ClientInvalidException() {
        super(ExceptionEnum.EX_CLIENT_INVALID.getStatus(),ExceptionEnum.EX_CLIENT_FORBIDDEN.getMsg());
    }
}
