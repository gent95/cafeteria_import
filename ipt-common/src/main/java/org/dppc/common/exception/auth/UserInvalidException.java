package org.dppc.common.exception.auth;


import org.dppc.common.enums.ExceptionEnum;
import org.dppc.common.exception.BaseException;

/**
 * @描述： 用户无效异常
 * @作者： 颜齐
 * @日期： 2017/10/19.
 * @时间： 15:13.
 */
public class UserInvalidException extends BaseException {
    public UserInvalidException() {
        super(ExceptionEnum.EX_USER_INVALID.getStatus(),ExceptionEnum.EX_USER_INVALID.getMsg());
    }
}
