package org.dppc.common.exception.auth;

import org.dppc.common.enums.ExceptionEnum;
import org.dppc.common.exception.BaseException;
import org.dppc.common.msg.BaseResponse;

/**
 * Created by yanqi on 2018/4/13.
 * 公共错误，免声明
 */
public class CommonErrorException extends BaseException {

    public CommonErrorException(ExceptionEnum exceptionEnum){
        super(exceptionEnum.getStatus(),exceptionEnum.getMsg());
    }
}
