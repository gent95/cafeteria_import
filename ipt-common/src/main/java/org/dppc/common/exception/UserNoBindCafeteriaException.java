package org.dppc.common.exception;

import org.dppc.common.enums.ExceptionEnum;

/**
 * @Description 食堂用户未判断食堂
 * @Author lhw
 * @Data 2019/6/8 15:22
 * @Version 1.0
 **/
public class UserNoBindCafeteriaException extends BaseException {
    public UserNoBindCafeteriaException() {
        super(ExceptionEnum.EX_USER_NO_BIND_CAFETERIA.getStatus(), ExceptionEnum.EX_USER_NO_BIND_CAFETERIA.getMsg());
    }
}
