package org.dppc.common.exception;

import org.dppc.common.enums.ExceptionEnum;

/**
 * @Description 重复操作异常
 * @Author lhw
 * @Data 2019/5/13 15:52
 * @Version 1.0
 **/
public class RepeatOperateException extends BaseException {
    public RepeatOperateException() {
        super(ExceptionEnum.EX_REPEAT_OPERATE.getStatus(), ExceptionEnum.EX_REPEAT_OPERATE.getMsg());
    }
}
