package org.dppc.common.exception;

import org.dppc.common.enums.ExceptionEnum;

/**
 * 错误异常基础类
 */
public class BaseException extends RuntimeException {
    private int status = 200;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }



    public BaseException() {
    }

    public BaseException( int status,String message) {
       super(message);
        this.status = status;
    }

    public BaseException(ExceptionEnum ex){
       super(ex.getMsg());
        this.status = ex.getStatus();
    }


    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(Throwable cause) {
        super(cause);
    }

    public BaseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }


}
