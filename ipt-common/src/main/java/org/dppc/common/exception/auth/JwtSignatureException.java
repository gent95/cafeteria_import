package org.dppc.common.exception.auth;

import org.dppc.common.enums.ExceptionEnum;
import org.dppc.common.exception.BaseException;

/**
 * TOKEN签名错误
 */
public class JwtSignatureException extends BaseException {
    public JwtSignatureException() {
        super(ExceptionEnum.EX_TOKEN_ILLEGAL);
    }
}
