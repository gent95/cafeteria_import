package org.dppc.common.exception;

import org.dppc.common.enums.ExceptionEnum;

/**
 * @Description 库存异常
 * @Author lhw
 * @Data 2019/5/13 15:52
 * @Version 1.0
 **/
public class StockNumberException extends BaseException {
    private String batchCode;
    private String materialsName;
    private Double surplusNum;
    private String customMessage;

    public StockNumberException() {
        super(ExceptionEnum.EX_STOCK_NUMBER.getStatus(),ExceptionEnum.EX_STOCK_NUMBER.getMsg());
    }

    public StockNumberException(String materialsName) {
        super(ExceptionEnum.EX_STOCK_NUMBER.getStatus(),ExceptionEnum.EX_STOCK_NUMBER.getMsg());
        this.customMessage = "{"+materialsName+ "}无库存";
    }

    public StockNumberException(String batchCode, String materialsName, Double surplusNum) {
        super(ExceptionEnum.EX_STOCK_NUMBER.getStatus(),ExceptionEnum.EX_STOCK_NUMBER.getMsg());
        this.batchCode = batchCode;
        this.materialsName = materialsName;
        this.surplusNum = surplusNum;
        this.customMessage = "";
        if(batchCode != null){
            this.customMessage = "{" + this.batchCode + "}批次中的";
        }else {
            this.customMessage = "请核对库存信息，总库存中";
        }
        this.customMessage = this.customMessage + "{" + this.materialsName + "}的库存不足，剩余数量为{" + this.surplusNum + "}";
    }

    public String getCustomMessage() {
        return customMessage;
    }

    public void setCustomMessage(String customMessage) {
        this.customMessage = customMessage;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public String getMaterialsName() {
        return materialsName;
    }

    public void setMaterialsName(String materialsName) {
        this.materialsName = materialsName;
    }

    public Double getSurplusNum() {
        return surplusNum;
    }

    public void setSurplusNum(Double surplusNum) {
        this.surplusNum = surplusNum;
    }
}
