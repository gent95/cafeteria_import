package org.dppc.common.exception.auth;


import org.dppc.common.enums.ExceptionEnum;
import org.dppc.common.exception.BaseException;

/**
 * @描述： token错误异常
 * @作者： 颜齐
 * @日期： 2017/10/19.
 * @时间： 15:13.
 */
public class TokenErrorException extends BaseException {
    public TokenErrorException() {
        super(ExceptionEnum.EX_TOKEN_ERROR.getStatus(),ExceptionEnum.EX_TOKEN_ERROR.getMsg());
    }
}
