package org.dppc.common.exception.auth;


import org.dppc.common.constant.CommonConstants;
import org.dppc.common.enums.ExceptionEnum;
import org.dppc.common.exception.BaseException;

/**
 * @描述： 客户端禁止访问异常
 * @作者： 颜齐
 * @日期： 2017/10/19.
 * @时间： 15:13.
 */
public class ClientForbiddenException extends BaseException {
    public ClientForbiddenException() {
        super(ExceptionEnum.EX_CLIENT_FORBIDDEN.getStatus(),ExceptionEnum.EX_CLIENT_FORBIDDEN.getMsg());
    }

}
