package org.dppc.common.exception.auth;

import org.dppc.common.enums.ExceptionEnum;
import org.dppc.common.exception.BaseException;

/**
 * token错误
 */
public class JwtIllegalArgumentException extends BaseException {
    public JwtIllegalArgumentException() {
        super(ExceptionEnum.EX_TOKEN_ERROR);
    }
}
