package org.dppc.common.exception.auth;

import org.dppc.common.enums.ExceptionEnum;
import org.dppc.common.exception.BaseException;

/**
 * TOKEN过期异常
 */
public class JwtTokenExpiredException extends BaseException {

    public JwtTokenExpiredException(){
        super(ExceptionEnum.EX_TOKEN_EXPIRE);
    }


}
