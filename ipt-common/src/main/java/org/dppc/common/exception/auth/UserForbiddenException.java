package org.dppc.common.exception.auth;

import org.dppc.common.enums.ExceptionEnum;
import org.dppc.common.exception.BaseException;
import org.dppc.common.msg.BaseResponse;

/**
 * Created by yanqi on 2018/4/13.
 * 用户权限禁止
 */
public class UserForbiddenException extends BaseException {

    public UserForbiddenException() {
        super(ExceptionEnum.EX_USER_NOT_ALLOW.getStatus(), "用户没有访问权限");
    }
}
