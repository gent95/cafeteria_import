package org.dppc.common.utils;

/**
 * 时间和日期的工具类
 */

import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("all")
public class DateHelper {
    public static final String YYYYMMDD = "yyyyMMdd";
    public static final String HHMMSS = "HHmmss";
    public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    public static final String YYYYMMDDHHMMSSFFF = "yyyyMMddHHmmssSSS";
    public static final String SSMMHHDDMMYY = "ssmmHHddMMyy";
    // 默认显示日期的格式
    public static final String DATAFORMAT_STR = "yyyy-MM-dd";
    // 默认显示日期的格式
    public static final String YYYY_MM_DATAFORMAT_STR = "yyyy-MM";
    public static final String MMdd = "MM.dd";
    // 默认显示日期时间的格式
    public static final String DATATIMEF_STR = "yyyy-MM-dd HH:mm:ss";
    public static final String DATATIME_STR = "yyyy-MM-dd HH:mm";
    // 默认显示简体中文日期的格式
    public static final String ZHCN_DATA_FORMAT = "yyyy年MM月dd日";
    // 默认显示简体中文日期时间的格式
    public static final String ZHCN_DATATIME_FORMAT = "yyyy年MM月dd日HH时mm分ss秒";
    // 默认显示简体中文日期时间的格式
    public static final String ZHCN_DATATIME_FORMAT_4yMMddHHmm = "yyyy年MM月dd日HH时mm分";
    // 默认显示简体中文星期格式
    public static final String ZHCN_EEEE = "EEEE";
    // 时
    public static final Integer HOUR_RADIX = 24;
    // 分
    public static final Integer MINUTE_RADIX = 60;
    // 秒
    public static final Integer SECOND_RADIX = 60;
    // 毫秒
    public static final Integer MILLISECOND_RADIX = 1000;
    private static Logger logger = LoggerFactory.getLogger(DateHelper.class);
    // 默认日期格式
    private static String datePattern = DATAFORMAT_STR;
    // 默认时间格式
    private static String timePattern = DATATIMEF_STR;

    private final static SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private final static SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * 获取当前时间（yyyy-MM-dd HH:mm:ss）
     */
    public static String getNowTime() {
        return date2Str(new Date(), timePattern);
    }

    /**
     * 获取当前时间（yyyy-MM-dd HH:mm:ss）
     */
    public static String getNowTime(String pattern) {
        return date2Str(new Date(), pattern);
    }

    /**
     * 字符串转字符串（指定格式）
     */
    public static final String str2Str(String strDate, String oldPattern, String newPattern) {
        return date2Str(str2Date(strDate, oldPattern), newPattern);
    }

    /**
     * 时间转字符串（日期格式）
     */
    public static final String getDateStr(Date date) {
        return date2Str(date, DATAFORMAT_STR);
    }

    /**
     * 时间转字符串（时间格式）
     */
    public static final String getTimeStr(Date date) {
        return date2Str(date, DATATIMEF_STR);
    }

    /**
     * 时间转字符串（指定格式）
     */
    public static final String date2Str(Date date, String pattern) {
        if (date == null) {
            return "";
        }
        return new SimpleDateFormat(pattern).format(date);
    }

    /**
     * 字符串转日期（指定格式）
     */
    public static final Date str2Date(String strDate, String pattern) {
        try {
            return new SimpleDateFormat(pattern).parse(strDate);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * 获取距离今天days天的日期
     *
     * @param days 距离今天的天数，如：-1代表昨天，1代表明天
     * @return
     */
    public static Date getThatDay(int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, days);
        return calendar.getTime();
    }

    /**
     * @param startDate
     * @param second
     * @return
     * @描述 : 与startDate时间相聚second秒的时间
     * @作者 : yanggang
     * @日期 : 2017/4/11
     * @时间 : 12:49
     */
    public static Date getAfterDate(Date startDate, Long second) {
        return new Date(startDate.getTime() + second * 1000);
    }

    /**
     * @param startDate
     * @param second
     * @return
     * @描述 : 与startDate时间相聚second秒的时间
     * @作者 : GAOJ
     * @日期 : 2017/4/18
     * @时间 : 12:49
     */
    public static Date getbeforeDate(Date startDate, Long second) {
        return new Date(startDate.getTime() - second * 1000);
    }

    /**
     * 与startDate时间相聚second秒的时间
     */
    public static Date getAfterDate(String startDate, String startDatePattern, Long second) {
        return new Date(str2Date(startDate, startDatePattern).getTime() + second * 1000);
    }

    /**
     * 与startDate时间相聚second秒的时间
     */
    public static String getAfterDate(String startDate, String startDatePattern, Long second, String returnPattern) {
        return date2Str(new Date(str2Date(startDate, startDatePattern).getTime() + second * 1000), returnPattern);
    }

    /**
     * 获取距离今天days天的日期
     *
     * @param days 距离今天的天数，如：-1代表昨天，1代表明天
     * @return
     */
    public static String getThatDay(int days, String pattern) {
        return date2Str(getThatDay(days), pattern);
    }

    /**
     * 获取距离某天days天的日期
     *
     * @param days 距离某天的天数，如：-1代表某天的昨天，1代表某天的明天
     * @return
     */
    public static Date getThatDay(Date date, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, days);
        return calendar.getTime();
    }

    /**
     * 获取距离某天days天的日期
     *
     * @param days 距离某天的天数，如：-1代表某天的昨天，1代表某天的明天
     * @return
     */
    public static String getThatDay(Date date, int days, String thatPattern) {
        return date2Str(getThatDay(date, days), thatPattern);
    }

    /**
     * 获取距离某天days天的日期
     *
     * @param days 距离某天的天数，如：-1代表某天的昨天，1代表某天的明天
     * @return
     */
    public static Date getThatDay(String strDate, String pattern, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(str2Date(strDate, pattern));
        calendar.add(Calendar.DAY_OF_MONTH, days);
        return calendar.getTime();
    }

    /**
     * 获取距离某天days天的日期
     *
     * @param days 距离某天的天数，如：-1代表某天的昨天，1代表某天的明天
     * @return
     */
    public static String getThatDay(String strDate, String pattern, int days, String thatPattern) {
        return date2Str(getThatDay(strDate, pattern, days), thatPattern);
    }

    /**
     * date1 在 date2 前返回 1
     * date1 在 date2 后返回 -1
     * date1 等于 date2 返回 0
     *
     * @param date1
     * @param date2
     * @return
     */
    public static int compareDate(Date date1, Date date2) {
        if (date1.getTime() > date2.getTime()) {
            return -1;
        } else if (date1.getTime() < date2.getTime()) {
            return 1;
        } else {
            return 0;
        }
    }

    public static int compareDate(String date1, String date1Pattern, String date2, String date2Pattern) {
        Date date11 = str2Date(date1, date1Pattern);
        Date date22 = str2Date(date2, date2Pattern);
        return compareDate(date11, date22);
    }

    /**
     * 获取两个日期之间的时间差字符串
     *
     * @param
     * @return day+"天"+hour+"小时"+minute+"分"+second+"秒"
     */
    public static String dvalue(Date begin, Date end) {
        long between;
        if (end.getTime() > begin.getTime()) {
            between = (end.getTime() - begin.getTime()) / 1000;//除以1000是为了转换成秒
        } else if (end.getTime() < begin.getTime()) {
            between = (begin.getTime() - end.getTime()) / 1000;//除以1000是为了转换成秒
        } else {
            return "0秒";
        }
        return secondToTimeStr(between);
    }

    /**
     * 秒值变耗时字符串(非毫秒值)
     *
     * @param
     * @return day+"天"+hour+"小时"+minute+"分"+second+"秒"
     */
    public static String secondToTimeStr(Long between) {

        long day = between / (24 * 3600);
        long hour = between % (24 * 3600) / 3600;
        long minute = between % 3600 / 60;
        long second = between % 60 / 60;
        StringBuffer sb = new StringBuffer("");
        if (day != 0) {
            sb.append(day + "天");
        }
        if (hour != 0) {
            sb.append(hour + "小时");
        }
        if (minute != 0) {
            sb.append(minute + "分");
        }
        if (second != 0) {
            sb.append(second + "秒");
        }
        return sb.toString();
    }

    /**
     * 获取两个日期之间的时间差字符串
     *
     * @param
     * @return between
     */
    public static long dvalues(Date begin, Date end) {
        if (begin == null || end == null) {
            logger.error("开始时间[{}]，结束时间[{}]", begin, end);
        }
        long between;
        if (end.getTime() > begin.getTime()) {
            between = (end.getTime() - begin.getTime()) / 1000;//除以1000是为了转换成秒
        } else if (end.getTime() < begin.getTime()) {
            between = (begin.getTime() - end.getTime()) / 1000;//除以1000是为了转换成秒
        } else {
            return 0;
        }
        return between;
    }

    /**
     * 秒值变耗时字符串(非毫秒值)
     * GUOZQ
     *
     * @param between
     * @return day+"天"+hour+"小时"+minute+"分"
     */
    public static String secondToTimeStrs(Long between) {

        long day = between / (24 * 3600);
        long hour = between % (24 * 3600) / 3600;
        long minute = between % 3600 / 60;
        StringBuffer sb = new StringBuffer("");
        if (day >= 0) {
            sb.append(day + "天");
        }
        if (hour >= 0) {
            sb.append(hour + "小时");
        }
        if (minute >= 0) {
            sb.append(minute + "分");
        }
        return sb.toString();
    }

    public static String secondToTimeStrs(String effectiveDemandTime, Date date) {
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.DATE, -Integer.valueOf(effectiveDemandTime));
        Long time = date.getTime() - instance.getTime().getTime();
        StringBuilder sb = new StringBuilder();
        if (time > 0L) {
            long day = TimeUnit.DAYS.convert(time, TimeUnit.MILLISECONDS);
            sb.append(day).append("天");
            long sourceDuration = time - TimeUnit.MILLISECONDS.convert(day, TimeUnit.DAYS);
            long hours = TimeUnit.HOURS.convert(sourceDuration, TimeUnit.MILLISECONDS);
            sb.append(hours).append("小时");
            sb.append(TimeUnit.MINUTES.convert(sourceDuration - TimeUnit.MILLISECONDS.convert(hours, TimeUnit.HOURS), TimeUnit.MILLISECONDS)).append("分");
            return sb.toString();
        } else {
            return "";
        }
    }

    /**
     * @描述 :  获取指定时间的零点值
     * @作者 :	lhw
     * @日期 :	2017/5/23
     * @时间 :	17:05
     */
    public static Date getZeroTime(Date date) {
        return DateHelper.str2Date(DateHelper.date2Str(date, DateHelper.DATAFORMAT_STR), DateHelper.DATAFORMAT_STR);
    }

    /**
     * 获取当前时间指定分钟差值的时间
     * 获取当前时间前3分钟     DateHelp.getCurrentTime(-3);
     * 获取当前时间后3分钟     DateHelp.getCurrentTime(3);
     *
     * @param stuff
     * @return
     */
    public static Date getCurrentTime(int stuff) {
        Calendar beforeTime = Calendar.getInstance();
        beforeTime.add(Calendar.MINUTE, stuff);// 3分钟之前的时间
        Date beforeD = beforeTime.getTime();
        return beforeD;
    }

    /**
     * 获取指定时间的年差值的时间
     *
     * @param stuff
     * @return
     */
    public static Date getStuffYear(int stuff) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.YEAR, stuff);
        Date y = c.getTime();
        return y;
    }

    /**
     * @描述 : 获取日期间的天数(不足一天的、记为一天)
     * @作者 : yanggang
     * @日期 : 2017/5/12
     * @时间 : 13:38
     */
    public static Long getDays(Date startTime, Date endTime) {
        return (long) Math.ceil(dvalues(startTime, endTime) / (60.0 * 60 * 24));
    }

    /**
     * @描述 : 获取指定日期当月的最后一天的日期
     * @作者 :	lhw
     * @日期 :	2017/5/31
     * @时间 :	15:16
     */
    public static Date gainMonthLastDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        date = calendar.getTime();
        return date;
    }

    /**
     * @描述 : 比较两个时间是否是同一天
     * @作者 : yanggang
     * @日期 : 2017/6/16
     * @时间 : 13:55
     */
    public static boolean isSameDay(Date date1, Date date2) {
        return date2Str(date1, DATAFORMAT_STR).equals(date2Str(date2, DATAFORMAT_STR));
    }

    /**
     * @param date    日期 ，为空则为当前时间
     * @param pattern 日期格式
     * @return 日期字符串
     * @function 将日期转换成指定格式的字符串
     */
    public static String formatDate(Date date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        if (date == null) {
            date = new Date();
        }
        return sdf.format(date);
    }

    /**
     * @param date 传递的时间，可以为null，为null时默认是当前时间
     * @param days 距离某天的天数，如：-1代表某天的前年，1代表某天的明年
     * @描述 获取距离某年days年的日期
     * @作者 Guo Ze Qiang
     */
    public static Date getThatYear(Date date, int days) {
        Calendar calendar = Calendar.getInstance();
        if (date != null) {
            calendar.setTime(date);
        }
        calendar.add(Calendar.YEAR, days);
        return calendar.getTime();

    }

    /**
     * @param arbitrarilyTime 任意的时间；参数可为null，为空时默认当前时间
     * @return 返回 map集合；key = 1 为开始，key = 2 为最后
     * @描述 获取任意日期时间的月份的前一月的时间范围(前一月的开始和最后, 使用时自行格式化时间)
     * @作者 Guo Ze Qiang
     */
    public static Map<Integer, Date> getBeforeMonthDateRange(Date arbitrarilyTime) {
        Calendar monthStartCalendar = Calendar.getInstance();
        Calendar monthEndCalendar = Calendar.getInstance();
        Map<Integer, Date> dateMap = new HashMap<>();
        if (arbitrarilyTime != null) {
            monthStartCalendar.setTime(arbitrarilyTime);
            monthEndCalendar.setTime(arbitrarilyTime);
        }
        // 开始时间
        monthStartCalendar.add(Calendar.MONTH, -1);
        monthStartCalendar.set(Calendar.DAY_OF_MONTH, 1);
        dateMap.put(1, monthStartCalendar.getTime());

        // 最后时间
        monthEndCalendar.add(Calendar.MONTH, 0);
        monthEndCalendar.set(Calendar.DAY_OF_MONTH, 0);
        dateMap.put(2, monthEndCalendar.getTime());
        return dateMap;

    }

    /**
     * @param arbitrarilyTime 任意的时间；参数可为null，为空时默认当前时间
     * @return 返回 map集合；key = 1 为开始，key = 2 为最后
     * @描述 获取任意日期时间的季度的前一季度的时间范围(前季度的开始和最后, 使用时自行格式化时间)
     * @作者 Guo Ze Qiang
     */
    public static Map<Integer, Date> getBeforeQuarterDateRange(Date arbitrarilyTime) {
        Calendar quarterStartCalendar = Calendar.getInstance();
        Calendar quarterEndCalendar = Calendar.getInstance();
        Map<Integer, Date> dateMap = new HashMap<>();
        if (arbitrarilyTime != null) {
            quarterStartCalendar.setTime(arbitrarilyTime);
            quarterEndCalendar.setTime(arbitrarilyTime);
        }
        // 开始时间
        quarterStartCalendar.set(Calendar.MONTH, (quarterStartCalendar.get(Calendar.MONTH) / 3 - 1) * 3);
        quarterStartCalendar.set(Calendar.DAY_OF_MONTH, 1);
        dateMap.put(1, quarterStartCalendar.getTime());

        // 最后时间
        quarterEndCalendar.set(Calendar.MONTH, (quarterEndCalendar.get(Calendar.MONTH) / 3 - 1) * 3 + 2);
        quarterEndCalendar.set(Calendar.DAY_OF_MONTH, quarterEndCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        dateMap.put(2, quarterEndCalendar.getTime());
        return dateMap;
    }

    /**
     * @param arbitrarilyTime 任意的时间；参数可为null，为空时默认当前时间
     * @return 返回 map集合；key = 1 为开始，key = 2 为最后
     * @描述 获取任意日期时间的年的前一年的时间范围(前一年的开始和最后, 使用时自行格式化时间)
     * @作者 Guo Ze Qiang
     */
    public static Map<Integer, Date> getBeforeYearDateRange(Date arbitrarilyTime) {
        Calendar yearStartCalendar = Calendar.getInstance();
        Calendar yearEndCalendar = Calendar.getInstance();
        Map<Integer, Date> dateMap = new HashMap<>();
        if (arbitrarilyTime != null) {
            yearStartCalendar.setTime(arbitrarilyTime);
            yearEndCalendar.setTime(arbitrarilyTime);
        }
        // 开始时间
        yearStartCalendar.add(Calendar.YEAR, -1);
        yearStartCalendar.set(Calendar.MARCH, 0);
        yearStartCalendar.set(Calendar.DATE, 1);
        dateMap.put(1, yearStartCalendar.getTime());

        // 最后时间
        yearEndCalendar.add(Calendar.YEAR, 0);
        yearEndCalendar.set(Calendar.MARCH, 0);
        yearEndCalendar.set(Calendar.DATE, 0);
        dateMap.put(2, yearEndCalendar.getTime());
        return dateMap;
    }

    /**
     * @param arbitrarilyTime 任意的时间；参数可为null，为空时默认当前时间
     * @return 返回 map集合；key = 1 为开始，key = 2 为最后
     * @描述 获取任意日期时间的星期的前一星期的时间范围(前一星期的开始和最后, 使用时自行格式化时间)
     * @作者 Guo Ze Qiang
     */
    public static Map<Integer, Date> getBeforeWeekDateRange(Date arbitrarilyTime) {
        return getFirstAndLastDayOfWeek(arbitrarilyTime, getWeekOfYear(arbitrarilyTime) - 1);
    }

    /**
     * @param date 需要得到这第几周的时间 参数可为null
     * @return 参数时间在参数时间年的第几周
     * @描述 获取参数时间是所在年的第几周 默认为当前时间
     * @日期 2018-5-9
     */
    public static int getWeekOfYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        if (date != null) {
            calendar.setTime(date);
        }
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setMinimalDaysInFirstWeek(7);
        return calendar.get(Calendar.WEEK_OF_YEAR);
    }

    /**
     * @param date 得到这周的时间 参数可为null
     * @param week 星期 week大于0小于8 超出范围默认为周一 例：1 为星期一、 2为星期二 ...、7为星期日
     * @return 需要的星期日期时间
     * @描述 获取参数时间周一至周日直接任意星期的日期时间 默认为当前时间
     * @日期 2018-5-9
     */
    public static Date getDayOfWeek(Date date, int week) {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        if (date != null) {
            calendar.setTime(date);
        }
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek() + (week > 0 && week < 8 ? (week - 1) : 0));
        return calendar.getTime();
    }

    /**
     * @param date 年时间
     * @param week 年的周数
     * @return 返回 map集合；key = 1 为开始，key = 2 为最后
     * @描述 获取某年的第几周的开始日期和结束日期
     * @日期 2018-5-9
     */
    public static Map<Integer, Date> getFirstAndLastDayOfWeek(Date date, int week) {
        Calendar calendar = Calendar.getInstance();
        Map<Integer, Date> dateMap = new LinkedHashMap<>();
        if (date != null) {
            calendar.setTime(date);
        }
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
        calendar.set(Calendar.MONTH, Calendar.JANUARY);
        calendar.set(Calendar.DATE, 1);
        Calendar cal = (Calendar) calendar.clone();
        Calendar cale = (Calendar) calendar.clone();
        cal.add(Calendar.DATE, 7 * (week - 1));
        cale.add(Calendar.DATE, 7 * week - 1);
        dateMap.put(1, cal.getTime());
        dateMap.put(2, cale.getTime());
        return dateMap;
    }

    public static XMLGregorianCalendar xmlToDate(Date date) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        XMLGregorianCalendar gc = null;
        try {
            gc = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gc;
    }

    public static Date DateToXML(XMLGregorianCalendar gc) {
        GregorianCalendar ca = gc.toGregorianCalendar();
        return ca.getTime();
    }

    /**
     * 获取当前日期Date yyyy-MM-dd
     */
    public static Date getNowDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static List<String> getDateArray(Integer days) {
        List<String> dateArray = new ArrayList<>();
        Date startDate = getThatDay(days);
        Date endDate = getNowDate();
        for (; !date2Str(startDate, DATAFORMAT_STR).equals(date2Str(endDate, DATAFORMAT_STR)); ) {
            dateArray.add(date2Str(startDate, MMdd));
            startDate = getThatDay(startDate, 1);
        }
        dateArray.add(date2Str(endDate, MMdd));
        return dateArray;
    }


    public static List<String> getDateArrays(Integer days) {
        List<String> dateArray = new ArrayList<>();
        Date startDate = getThatDay(days);
        Date endDate = getNowDate();
        for (; !date2Str(startDate, DATAFORMAT_STR).equals(date2Str(endDate, DATAFORMAT_STR)); ) {
            dateArray.add(date2Str(startDate, DATAFORMAT_STR));
            startDate = getThatDay(startDate, 1);
        }
        dateArray.add(date2Str(endDate, DATAFORMAT_STR));
        return dateArray;
    }

    /**
     * @param randomTime 任意时间
     * @param hh         时
     * @param mi         分
     * @param ss         秒
     * @return dateTime
     * @描述 获取任意时间的月份分的最后一天的时间
     * @作者 Guo Ze Qiang
     */
    public static Date getFinallyTime(Date randomTime, int hh, int mi, int ss) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(randomTime);
        //得到一个月最后一天日期(31/30/29/28)
        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        //按要求设置时间
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), maxDay, hh, mi, ss);
        Date dateTime = calendar.getTime();
        return dateTime;
    }

    /**
     * 描述: 获取任意月的前12个月，当前月
     *
     * @param date 当前时间
     * @return
     */
    public static String[] getLast12Months(Date date) {


        String[] last12Months = new String[12];
        Calendar cal = Calendar.getInstance();
        //设置输入条件时间
        cal.setTime(date);

        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)); //要先+1,才能把本月的算进去
        for (int i = 0; i < 12; i++) {
            cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - 1); //逐次往前推1个月
            last12Months[11 - i] = cal.get(Calendar.YEAR) + "-" + addZeroForNum(String.valueOf(cal.get(Calendar.MONTH) + 1), 2);
        }

        return last12Months;
    }

    public static String addZeroForNum(String str, int strLength) {
        int strLen = str.length();
        if (strLen < strLength) {
            while (strLen < strLength) {
                StringBuffer sb = new StringBuffer();
                sb.append("0").append(str);// 左补0
                // sb.append(str).append("0");//右补0
                str = sb.toString();
                strLen = str.length();
            }
        }
        return str;
    }

    /**
     * 当前季度的开始时间
     *
     * @return
     */
    public static Date getCurrentQuarterStartTime() {
        Calendar c = Calendar.getInstance();
        int currentMonth = c.get(Calendar.MONTH) + 1;
        Date now = null;
        try {
            if (currentMonth >= 1 && currentMonth <= 3)
                c.set(Calendar.MONTH, 0);
            else if (currentMonth >= 4 && currentMonth <= 6)
                c.set(Calendar.MONTH, 3);
            else if (currentMonth >= 7 && currentMonth <= 9)
                c.set(Calendar.MONTH, 4);
            else if (currentMonth >= 10 && currentMonth <= 12)
                c.set(Calendar.MONTH, 9);
            c.set(Calendar.DATE, 1);
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    /**
     * 当前季度的结束时间
     *
     * @return
     */
    public static Date getCurrentQuarterEndTime() {
        Calendar c = Calendar.getInstance();
        int currentMonth = c.get(Calendar.MONTH) + 1;
        Date now = null;
        try {
            if (currentMonth >= 1 && currentMonth <= 3) {
                c.set(Calendar.MONTH, 2);
                c.set(Calendar.DATE, 31);
            } else if (currentMonth >= 4 && currentMonth <= 6) {
                c.set(Calendar.MONTH, 5);
                c.set(Calendar.DATE, 30);
            } else if (currentMonth >= 7 && currentMonth <= 9) {
                c.set(Calendar.MONTH, 8);
                c.set(Calendar.DATE, 30);
            } else if (currentMonth >= 10 && currentMonth <= 12) {
                c.set(Calendar.MONTH, 11);
                c.set(Calendar.DATE, 31);
            }
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    /**
     * 本月的结束时间
     *
     * @return
     */
    public static Date getCurrentMonthEndTime() {
        Calendar c = Calendar.getInstance();
        Date now = null;
        try {
            c.set(Calendar.DATE, 1);
            c.add(Calendar.MONTH, 1);
            c.add(Calendar.DATE, -1);
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    /**
     * date2比date1多的天数
     *
     * @param date1
     * @param date2
     * @return
     */
    public static int differentDays(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        int day1 = cal1.get(Calendar.DAY_OF_YEAR);
        int day2 = cal2.get(Calendar.DAY_OF_YEAR);

        int year1 = cal1.get(Calendar.YEAR);
        int year2 = cal2.get(Calendar.YEAR);
        if (year1 != year2)   //同一年
        {
            int timeDistance = 0;
            for (int i = year1; i < year2; i++) {
                if (i % 4 == 0 && i % 100 != 0 || i % 400 == 0)    //闰年
                {
                    timeDistance += 366;
                } else    //不是闰年
                {
                    timeDistance += 365;
                }
            }

            return timeDistance + (day2 - day1);
        } else    //不同年
        {
            return day2 - day1;
        }
    }

    /**
     * 获取时间所在月份最大天数
     *
     * @param date
     * @return
     */
    public static int getDaysOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    /**
     * 获取年份
     *
     * @return
     */
    public static String getCurrentYear() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        Date date = new Date();
        return sdf.format(date);
    }

    /**
     * 获取今年多少天
     *
     * @param year
     * @return
     */
    public static int getYearDays(int year) {
        int days;//某年(year)的天数
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {//闰年的判断规则
            days = 366;
        } else {
            days = 365;
        }
        return days;
    }

    /**
     * 取得季度月
     *
     * @param date
     * @return
     */
    public static Date[] getSeasonDate(Date date) {
        Date[] season = new Date[3];
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int nSeason = getSeason(date);
        if (nSeason == 1) {// 第一季度
            c.set(Calendar.MONTH, Calendar.JANUARY);
            season[0] = c.getTime();
            c.set(Calendar.MONTH, Calendar.FEBRUARY);
            season[1] = c.getTime();
            c.set(Calendar.MONTH, Calendar.MARCH);
            season[2] = c.getTime();
        } else if (nSeason == 2) {// 第二季度
            c.set(Calendar.MONTH, Calendar.APRIL);
            season[0] = c.getTime();
            c.set(Calendar.MONTH, Calendar.MAY);
            season[1] = c.getTime();
            c.set(Calendar.MONTH, Calendar.JUNE);
            season[2] = c.getTime();
        } else if (nSeason == 3) {// 第三季度
            c.set(Calendar.MONTH, Calendar.JULY);
            season[0] = c.getTime();
            c.set(Calendar.MONTH, Calendar.AUGUST);
            season[1] = c.getTime();
            c.set(Calendar.MONTH, Calendar.SEPTEMBER);
            season[2] = c.getTime();
        } else if (nSeason == 4) {// 第四季度
            c.set(Calendar.MONTH, Calendar.OCTOBER);
            season[0] = c.getTime();
            c.set(Calendar.MONTH, Calendar.NOVEMBER);
            season[1] = c.getTime();
            c.set(Calendar.MONTH, Calendar.DECEMBER);
            season[2] = c.getTime();
        }
        return season;
    }

    /**
     * 1 第一季度 2 第二季度 3 第三季度 4 第四季度
     *
     * @param date
     * @return
     */
    public static int getSeason(Date date) {
        int season = 0;
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int month = c.get(Calendar.MONTH);
        switch (month) {
            case Calendar.JANUARY:
            case Calendar.FEBRUARY:
            case Calendar.MARCH:
                season = 1;
                break;
            case Calendar.APRIL:
            case Calendar.MAY:
            case Calendar.JUNE:
                season = 2;
                break;
            case Calendar.JULY:
            case Calendar.AUGUST:
            case Calendar.SEPTEMBER:
                season = 3;
                break;
            case Calendar.OCTOBER:
            case Calendar.NOVEMBER:
            case Calendar.DECEMBER:
                season = 4;
                break;
            default:
                break;
        }
        return season;
    }

    /**
     * 获取两个时间相差月份
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public static int getMonth(String startTime, String endTime) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime start = formatter.parseDateTime(startTime);
        DateTime end = formatter.parseDateTime(endTime);
        return Months.monthsBetween(start, end).getMonths();
    }


    /**
     * 描述:获取下一个月.
     * 获取上一个月
     * 如：  -1 代表上一个月
     *
     * @return
     */
    public static String getPreOrLastMonth(int day) {
        Calendar cal = Calendar.getInstance();
        cal.add(cal.MONTH, day);
        SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM");
        String preMonth = dft.format(cal.getTime());
        return preMonth;
    }

    public static Date getCurrentYearStartTime() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.YEAR));
        return cal.getTime();
    }
}
