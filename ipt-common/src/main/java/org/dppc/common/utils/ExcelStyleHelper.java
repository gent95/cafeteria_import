package org.dppc.common.utils;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;

/**
 * @Description poi样式工具
 * @Author GAOJ
 * @Data 2019/05/14 19:11
 * @Version 1.0
 **/
public class ExcelStyleHelper {

    /**
     * 创建字体 fontName字体名 fontHeightInPoints字号
     */
    public static HSSFFont getFont(HSSFWorkbook wb, String fontName, short fontHeightInPoints) {
        HSSFFont font = wb.createFont();
        font.setFontName(fontName);
        font.setFontHeightInPoints(fontHeightInPoints);
        return font;
    }

    /**
     * 创建单元格样式 horizontalAlignment水平对齐方式 verticalAlignment纵向对齐方式
     */
    public static HSSFCellStyle getCellStyle(HSSFWorkbook wb, HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment, HSSFFont font) {
        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(horizontalAlignment);
        style.setVerticalAlignment(verticalAlignment);
        style.setFont(font);
        return style;
    }

    /**
     * 设置边框 left左边框 right右 top上 bottom下
     */
    public static void setBorder(HSSFCellStyle style, BorderStyle left, BorderStyle right, BorderStyle top, BorderStyle bottom) {
        style.setBorderBottom(bottom);
        style.setBorderLeft(left);
        style.setBorderTop(top);
        style.setBorderRight(right);
    }

    /**
     * 设置单元格信息
     */
    public static void setCell(HSSFRow row, int column, double cellValue, HSSFCellStyle style) {
        HSSFCell cell = row.createCell(column);
        cell.setCellValue(cellValue);
        cell.setCellStyle(style);
    }
    /**
     * 设置单元格信息
     */
    public static void setCell(HSSFRow row, int column, String cellValue, HSSFCellStyle style) {
        HSSFCell cell = row.createCell(column);
        cell.setCellValue(cellValue);
        cell.setCellStyle(style);
    }
}
