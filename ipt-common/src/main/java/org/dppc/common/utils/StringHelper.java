package org.dppc.common.utils;

/**
 * Created by ace on 2017/9/10.
 */
public class StringHelper {
    public static String getObjectValue(Object obj){
        return obj==null?"":obj.toString();
    }

    /**
     * 判断是否是以传入的访问路径开的的
     * @param requestUri
     * @param startWith
     * @return
     */
    public static boolean isStartWith(String requestUri,String[] startWith) {
        boolean flag = false;
        for (String s : startWith) {
            if (requestUri.startsWith(s))
                return true;
        }
        return flag;
    }


    public static boolean equalArray(String method,String[] methodArray) {
        boolean flag = false;
        for (String s : methodArray) {
            if (method.equals(s))
                return true;
        }
        return flag;
    }
}
