package org.dppc.common.utils;


import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * @描述：发送邮件工具类
 */
public class MailUtil {

    private static final String USERNAME = "m13323580724@163.com";

    private static final String PASSWORD = "NingXia123456";

    private static final String NICKNAME = "宁夏重要产品追溯平台";


    /**
     * 发送邮件
     * @param to   给谁发
     * @param text 发送内容
     */
    public static void send_mail(String to, String text) throws MessagingException {
        send(to, "验证码邮件邮件", text);
    }

    /**
     * 发送自定义标题邮件
     *
     * @param to    给谁发
     * @param title 发送标题
     * @param text  发送内容
     * @throws MessagingException
     */
    public static void send_mail(String to, String title, String text) throws MessagingException {
        send(to, title, text);
    }

    /**
     * 执行发送功能
     *
     * @param to    给谁发
     * @param title 发送标题
     * @param text  发送内容
     */
    private static void send(String to, String title, String text) throws MessagingException {
        //创建连接对象 连接到邮件服务器
        Properties properties = new Properties();
        //设置发送邮件的基本参数
        //发送邮件服务器
//        properties.put("mail.smtp.host", "smtp.163.com");
        properties.put("mail.smtp.host", "smtp.163.com");
        //发送端口
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.port", "25");
        properties.put("mail.smtp.auth", "true");
        //设置发送邮件的账号和密码
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                //两个参数分别是发送邮件的账户和密码
                return new PasswordAuthentication(USERNAME, PASSWORD);
            }
        });
        String nick="";
        try {
            nick=javax.mail.internet.MimeUtility.encodeText(NICKNAME);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //创建邮件对象
        Message message = new MimeMessage(session);
        //设置发件人
        message.setFrom(new InternetAddress(nick+" <"+USERNAME+">"));
        //设置收件人
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
        //设置主题
        message.setSubject(title);
        //设置邮件正文  第二个参数是邮件发送的类型
        message.setContent(text, "text/html;charset=UTF-8");
        //发送一封邮件
        Transport.send(message);
    }

}
