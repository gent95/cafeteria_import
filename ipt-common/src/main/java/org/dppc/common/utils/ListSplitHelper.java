package org.dppc.common.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @描述 :
 * @作者 :   GAOJ
 * @日期 :   2017/12/19 0019
 * @时间 :   15:03
 */
public class ListSplitHelper {

    /**  将targe 按size分割为子 list   */
    public static <T> List<List<T>> createList(List<T> targe, int size) {
        List<List<T>> listArr = new ArrayList<>();
        int arrSize = targe.size() % size == 0 ? targe.size() / size : targe.size() / size + 1;
        for (int i = 0; i < arrSize; i++) {
            List<T> sub = new ArrayList();
            for (int j = i * size; j <= size * (i + 1) - 1; j++) {
                if (j <= targe.size() - 1) {
                    sub.add(targe.get(j));
                }
            }
            listArr.add(sub);
        }
        return listArr;
    }
}
