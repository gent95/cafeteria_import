package org.dppc.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * 描述:此类用于取得当前日期相对应的月初，月末，季初，季末，年初，年末，返回值均为String字符串
 *      1、得到当前日期         today()
 *      2、得到当前月份月初      thisMonth()
 *      3、得到当前月份月底      thisMonthEnd()
 *      4、得到当前季度季初      thisSeason()
 *      5、得到当前季度季末      thisSeasonEnd()
 *      6、得到当前年份年初      thisYear()
 *      7、得到当前年份年底      thisYearEnd()
 *      8、判断输入年份是否为闰年 leapYear
 *
 * 注意事项:  日期格式为：xxxx-yy-zz (eg: 2007-12-05)
 *
 * @author lyf
 */
@SuppressWarnings("all")
public class DateThis {
    private int x;                  // 日期属性：年
    private int y;                  // 日期属性：月
    private int z;                  // 日期属性：日
    private Calendar localTime;     // 当前日期
    public DateThis() {
        localTime = Calendar.getInstance();
    }
    /**
     * 功能：得到当前日期 格式为：xxxx-yy-zz (eg: 2007-12-05)<br> 
     * @return String
     * @author lyf
     */
    public String today() {
        String strY = null;
        String strZ = null;
        x = localTime.get(Calendar.YEAR);
        y = localTime.get(Calendar.MONTH) + 1;
        z = localTime.get(Calendar.DATE);
        strY = y >= 10 ? String.valueOf(y) : ("0" + y);
        strZ = z >= 10 ? String.valueOf(z) : ("0" + z);
        return x + "-" + strY + "-" + strZ;
    }
    /**
     * 功能：得到当前月份月初 格式为：xxxx-yy-zz (eg: 2007-12-01)<br> 
     * @return String
     * @author lyf
     */
    public String thisMonth() {
        String strY = null;
        x = localTime.get(Calendar.YEAR);
        y = localTime.get(Calendar.MONTH) + 1;
        strY = y >= 10 ? String.valueOf(y) : ("0" + y);
        return x + "-" + strY + "-01";
    }
    /**
     * 功能：得到当前月的 上一个月份月初 格式为：xxxx-yy-zz (eg: 2007-12-01)<br>
     * @return String
     * @author yjj
     */
    public String thisUpMonth() {
        String strY = null;
        x = localTime.get(Calendar.YEAR);
        y = localTime.get(Calendar.MONTH);
        strY = y >= 10 ? String.valueOf(y-1) : ("0" + y);
        return x + "-" + strY + "-01";
    }
    /**
     * 功能：得到当前月份月底 格式为：xxxx-yy-zz (eg: 2007-12-31)<br> 
     * @return String
     * @author lyf
     */
    public String thisMonthEnd() {
        String strY = null;
        String strZ = null;
        boolean leap = false;
        x = localTime.get(Calendar.YEAR);
        y = localTime.get(Calendar.MONTH) + 1;
        if (y == 1 || y == 3 || y == 5 || y == 7 || y == 8 || y == 10 || y == 12) {
            strZ = "31";
        }
        if (y == 4 || y == 6 || y == 9 || y == 11) {
            strZ = "30";
        }
        if (y == 2) {
            leap = leapYear(x);
            if (leap) {
                strZ = "29";
            }
            else {
                strZ = "28";
            }
        }
        strY = y >= 10 ? String.valueOf(y) : ("0" + y);
        return x + "-" + strY + "-" + strZ;
    }
    /**
     * 功能：得到当前月份的 上一个月的 月底 格式为：xxxx-yy-zz (eg: 2007-12-31)<br>
     * @return String
     * @author yjj
     */
    public String thisUpMonthEnd() {
        String strY = null;
        String strZ = null;
        boolean leap = false;
        x = localTime.get(Calendar.YEAR);
        y = localTime.get(Calendar.MONTH);
        if (y == 1 || y == 3 || y == 5 || y == 7 || y == 8 || y == 10 || y == 12) {
            strZ = "31";
        }
        if (y == 4 || y == 6 || y == 9 || y == 11) {
            strZ = "30";
        }
        if (y == 2) {
            leap = leapYear(x);
            if (leap) {
                strZ = "29";
            }
            else {
                strZ = "28";
            }
        }
        strY = y >= 10 ? String.valueOf(y-1) : ("0" + y);
        return x + "-" + strY + "-" + strZ;
    }
    /**
     * 功能：得到当前季度季初 格式为：xxxx-yy-zz (eg: 2007-10-01)<br> 
     * @return String
     * @author lyf
     */
    public String thisSeason() {
        String dateString = "";
        x = localTime.get(Calendar.YEAR);
        y = localTime.get(Calendar.MONTH) + 1;
        if (y >= 1 && y <= 3) {
            dateString = x + "-" + "01" + "-" + "01";
        }
        if (y >= 4 && y <= 6) {
            dateString = x + "-" + "04" + "-" + "01";
        }
        if (y >= 7 && y <= 9) {
            dateString = x + "-" + "07" + "-" + "01";
        }
        if (y >= 10 && y <= 12) {
            dateString = x + "-" + "10" + "-" + "01";
        }
        return dateString;
    }
    /**
     * 功能：得到当前季度季末 格式为：xxxx-yy-zz (eg: 2007-12-31)<br> 
     * @return String
     * @author lyf
     */
    public String thisSeasonEnd() {
        String dateString = "";
        x = localTime.get(Calendar.YEAR);
        y = localTime.get(Calendar.MONTH) + 1;
        if (y >= 1 && y <= 3) {
            dateString = x + "-" + "03" + "-" + "31";
        }
        if (y >= 4 && y <= 6) {
            dateString = x + "-" + "06" + "-" + "30";
        }
        if (y >= 7 && y <= 9) {
            dateString = x + "-" + "09" + "-" + "30";
        }
        if (y >= 10 && y <= 12) {
            dateString = x + "-" + "12" + "-" + "31";
        }
        return dateString;
    }
    /**
     * 功能：得到当前年份年初 格式为：xxxx-yy-zz (eg: 2007-01-01)<br> 
     * @return String
     * @author lyf
     */
    public String thisYear() {
        x = localTime.get(Calendar.YEAR);
        return x + "-01" + "-01";
    }
    /**
     * 功能：得到当前年份年底 格式为：xxxx-yy-zz (eg: 2007-12-31)<br> 
     * @return String
     * @author lyf
     */
    public String thisYearEnd() {
        x = localTime.get(Calendar.YEAR);
        return x + "-12" + "-31";
    }
    /**
     * 功能：判断输入年份是否为闰年<br>
     *
     * @param year
     * @return 是：true  否：false
     * @author lyf
     */
    public boolean leapYear(int year) {
        boolean leap;
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) leap = true;
                else leap = false;
            }
            else leap = true;
        }
        else leap = false;
        return leap;
    }

    /**
     * 功能：获取两个时间相差天数
     * @param startTime <String>
     * @param endTime <String>
     * @return int
     * @throws ParseException
     * @author lyf
     */
    public int daysBetween(String startTime,String endTime) throws ParseException{
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(startTime));
        long time1 = cal.getTimeInMillis();
        cal.setTime(sdf.parse(endTime));
        long time2 = cal.getTimeInMillis();
        long between_days=(time2-time1)/(1000*3600*24);
        return Integer.parseInt(String.valueOf(between_days));
    }
    /*   public static void main(String[] args) throws ParseException {
        DateThis dateThis = new DateThis();
        System.out.println(dateThis.daysBetween("2000-02-01","2000-02-29"));
        System.out.println(dateThis.thisMonth());
        System.out.println(dateThis.thisMonthEnd());
    }*/
}