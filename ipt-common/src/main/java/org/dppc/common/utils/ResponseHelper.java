package org.dppc.common.utils;

import org.dppc.common.entity.EditorImage;
import org.dppc.common.enums.ExceptionEnum;
import org.dppc.common.exception.BaseException;
import org.dppc.common.msg.*;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @描述： 返回数据工具类
 * @作者： 颜齐
 * @日期： 2017/10/27.
 * @时间： 17:13.
 */
public class ResponseHelper {

    private static BindingResult result;

    /**
     * 成功返回
     *
     * @return
     */
    public static BaseResponse success() {
        return success(ExceptionEnum.OK.getMsg());
    }

    /**
     * 返回自定义消息
     *
     * @param msg
     * @return
     */
    public static BaseResponse success(String msg) {
        BaseResponse response = new BaseResponse();
        response.setStatus(ExceptionEnum.OK.getStatus());
        response.setMsg(msg);
        return response;
    }

    /**
     * 返回单个对象
     *
     * @param object
     * @return
     */
    public static BaseResponse success(Object object) {
        ObjectRestResponse response = new ObjectRestResponse();
        response.setStatus(ExceptionEnum.OK.getStatus());
        response.setMsg(ExceptionEnum.OK.getMsg());
        response.setData(object);
        return response;
    }

    /**
     * 返回列表对象
     *
     * @param list
     * @return
     */
    public static <T> BaseResponse success(List<T> list) {
        ListRestResponse response = new ListRestResponse();
        response.setStatus(ExceptionEnum.OK.getStatus());
        response.setMsg(ExceptionEnum.OK.getMsg());
        if (list != null) {
            response.setData(list);
        }
        return response;
    }

    /**
     * @描述 :  返回分页响应
     * @作者 :  Zhao Yun
     * @日期 :  2017/11/15
     * @时间 :  9:36
     */
    public static <T> BaseResponse success(Page<T> page) {
        PageResponse<T> response = new PageResponse<>();
        response.setStatus(ExceptionEnum.OK.getStatus());
        response.setMsg(ExceptionEnum.OK.getMsg());
        if (page != null) {
            response.setData(page.getContent());
            response.setCurrentPage(page.getNumber() + 1);
            response.setTotalRecords(page.getTotalElements());
            response.setTotalPages(page.getTotalPages());
        }
        return response;
    }

    /**
     * @描述 :  返回分页响应
     * @作者 :  Zhao Yun
     * @日期 :  2017/11/15
     * @时间 :  9:36
     */
    public static BaseResponse success(Page page, Map map) {
        ObjectRestResponse response = new ObjectRestResponse<>();
        response.setStatus(ExceptionEnum.OK.getStatus());
        response.setMsg(ExceptionEnum.OK.getMsg());
        if (page != null) {
            Map map1 = new HashMap(4);
            map1.put("data", page.getContent());
            map1.put("currentPage", page.getNumber() + 1);
            map1.put("totalRecords", page.getTotalElements());
            map1.put("TotalPages", page.getTotalPages());
            map.put("page", map1);
        }
        response.setData(map);
        return response;
    }

    /**
     * @描述 :  返回分页响应
     * @作者 :  Zhao Yun
     * @日期 :  2017/11/15
     * @时间 :  9:36
     */
    public static <T> BaseResponse success(org.dppc.common.entity.Page<T> page) {
        PageResponse<T> response = new PageResponse<>();
        response.setStatus(ExceptionEnum.OK.getStatus());
        response.setMsg(ExceptionEnum.OK.getMsg());
        if (page != null) {
            response.setData(page.getData());
            response.setCurrentPage(page.getPage());
            response.setTotalRecords(page.getTotal());
            response.setTotalPages(page.getTotalPages().intValue());
        }
        return response;
    }

    /**
     * @描述 :  返回layui-table分页响应
     * @作者 :  Gao Jing
     * @日期 :  2017/11/15
     * @时间 :  9:36
     */
    public static <T> BaseResponse successLayui(Page<T> page) {
        LayuiTableResponse<T> response = new LayuiTableResponse<>();
        response.setStatus(ExceptionEnum.OK.getStatus());
        response.setMsg(ExceptionEnum.OK.getMsg());
        if (page != null) {
            response.setCode(LayuiTableResponse.CodeType.SUCCESS.getValue());
            response.setCount(page.getTotalElements());
            response.setData(page.getContent());
        }
        return response;
    }

    /**
     * @描述 :  返回layui-table分页响应
     * @作者 :  Gao Jing
     * @日期 :  2017/3/1
     * @时间 :  9:36
     */
    public static <T> BaseResponse success(List<T> voList, long totalElements) {
        LayuiTableResponse<T> response = new LayuiTableResponse<>();
        response.setStatus(ExceptionEnum.OK.getStatus());
        response.setMsg(ExceptionEnum.OK.getMsg());
        response.setCode(LayuiTableResponse.CodeType.SUCCESS.getValue());
        response.setCount(totalElements);
        response.setData(voList);
        return response;
    }

    /**
     * @描述 :  返回layui-layedit响应
     * @作者 :  Gao Jing
     * @日期 :  2018/3/15
     * @时间 :  9:36
     */
    public static <T> BaseResponse successEditor(EditorImage image) {
        LayuiEditorResponse response = new LayuiEditorResponse();
        response.setStatus(ExceptionEnum.OK.getStatus());
        response.setMsg(ExceptionEnum.OK.getMsg());
        response.setCode(LayuiEditorResponse.CodeType.SUCCESS.getValue());
        response.setData(image);
        return response;
    }

    /**
     * 错误返回
     *
     * @return
     */
    public static BaseResponse error() {
        BaseResponse response = new BaseResponse();
        response.setStatus(ExceptionEnum.EX_OTHER_ERROR.getStatus());
        response.setMsg(ExceptionEnum.EX_OTHER_ERROR.getMsg());
        return response;
    }

    /**
     * @描述 :  返回BindingResult的错误消息
     * @作者 :  Zhao Yun
     * @日期 :  2017/11/8
     * @时间 :  13:28
     */
    public static BaseResponse error(BindingResult result) {
        BaseResponse response = new BaseResponse();
        response.setStatus(ExceptionEnum.EX_OTHER_ERROR.getStatus());
        List<ObjectError> errors = result.getAllErrors();
        List<String> msgList = new ArrayList<>();
        for (ObjectError error : errors) {
            msgList.add(error.getDefaultMessage());
        }
        response.setMsg(JsonHelper.toJSon(msgList));
        return response;
    }

    /**
     * 指定类型的错误返回
     *
     * @param ex
     * @return
     */
    public static BaseResponse error(ExceptionEnum ex) {
        BaseResponse response = new BaseResponse();
        response.setStatus(ex.getStatus());
        response.setMsg(ex.getMsg());
        return response;
    }

    /**
     * 根据异常返回错误
     * @param be
     * @return
     */
    public static BaseResponse error(BaseException be) {
        BaseResponse response = new BaseResponse();
        response.setStatus(be.getStatus());
        response.setMsg(be.getMessage());
        return response;
    }

    /**
     * 指定明确的验证错误
     *
     * @param result
     * @return
     */
    public static BaseResponse verifyError(BindingResult result) {
        BaseResponse response = new BaseResponse();
        response.setStatus(ExceptionEnum.EX_PARAM_ERROR.getStatus());
        response.setMsg(ValidateHelp.toErrJsonString(result));
        return response;
    }


    /**
     * 指定明确错误的返回
     *
     * @param msg
     * @return
     */
    public static BaseResponse error(String msg) {
        BaseResponse response = new BaseResponse();
        response.setStatus(ExceptionEnum.EX_OTHER_ERROR.getStatus());
        response.setMsg(msg);
        return response;
    }

    /**
     * 指定明确错误的返回
     *
     * @param msg
     * @return
     */
    public static BaseResponse error(ExceptionEnum exp, String msg) {
        BaseResponse response = new BaseResponse();
        response.setStatus(exp.getStatus());
        response.setMsg(msg);
        return response;
    }


}
