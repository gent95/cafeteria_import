package org.dppc.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.dppc.common.enums.SpecialGoodsEnum;

/**
 * Created by yanqi on 2018/4/24.
 */
public class VarietyHelper {
    /**
     * 012 菜开头
     * 013 瓜开头
     * 06 中药
     * 21111 牛肉相关
     * 21113 猪肉
     * 21114 冷猪肉
     * 21115 羊肉
     * 21116 羊肉
     * 21117 羊肉
     * 211190 牛内脏
     * 211192 羊内脏
     * 22 乳制品
     * 2421 葡萄酒、
     * 011 谷物 （粮）
     * 216 动、植物油脂
     * 0614217 中草药中的枸杞分类
     * 根据商品编码获取商品属于那种类型
     */
    public static SpecialGoodsEnum getSpecialGoodsByVarityCode(String varityCode) {
        //计算顺序按照先计算长字符串，在计算端字符串,并且肉菜平台放在最后
        //枸杞
        if (StringHelper.isStartWith(varityCode, SpecialGoodsEnum.MEDLAR.getIndexCode()))
            return SpecialGoodsEnum.MEDLAR;
            //牛羊肉
        else if (StringHelper.isStartWith(varityCode, SpecialGoodsEnum.BEEF_AND_MUTTON.getIndexCode()))
            return SpecialGoodsEnum.BEEF_AND_MUTTON;
            //葡萄酒
        else if (StringHelper.isStartWith(varityCode, SpecialGoodsEnum.WINE.getIndexCode()))
            return SpecialGoodsEnum.WINE;
            //瓜菜
        else if (StringHelper.isStartWith(varityCode, SpecialGoodsEnum.MELON_VEGETABLE.getIndexCode()))
            return SpecialGoodsEnum.MELON_VEGETABLE;
            //粮油
        else if (StringHelper.isStartWith(varityCode, SpecialGoodsEnum.GRAIN_OIL.getIndexCode()))
            return SpecialGoodsEnum.GRAIN_OIL;
            //乳制品
        else if (StringHelper.isStartWith(varityCode, SpecialGoodsEnum.DAIRY.getIndexCode()))
            return SpecialGoodsEnum.DAIRY;
            //中草药
        else if (StringHelper.isStartWith(varityCode, SpecialGoodsEnum.CHINESE_MEDICAL.getIndexCode()))
            return SpecialGoodsEnum.CHINESE_MEDICAL;
            //老肉菜
        else if (StringHelper.isStartWith(varityCode, SpecialGoodsEnum.OLD_ROUCAI.getIndexCode()))
            return SpecialGoodsEnum.OLD_ROUCAI;
            //其他
        else
            return SpecialGoodsEnum.OTHER;

    }

    /**
     * 获取8位长度的申请商品编码，因为追溯码合成中只预留了8位
     *
     * @param varityCode
     * @return
     */
    public static String getStandartVarityCode(String varityCode) {
        String standartVarityCode = null;
        if (StringUtils.isEmpty(varityCode)) {
            standartVarityCode = "00000000";
        } else if (varityCode.length() == 8)
            standartVarityCode = varityCode;
        else if (varityCode.length() < 8)
            standartVarityCode = String.format("%08d", Integer.valueOf(varityCode));
        else if (varityCode.length() > 8)
            standartVarityCode = varityCode.substring(0, 8);
        return standartVarityCode;
    }


}
