package org.dppc.common.utils;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
  * @描述 : 生成Excel 方法ExcelHelper.generateExcel()  参数为List集合，文件名（可null），标题（可null）
  * @作者 : Lxw
  * @日期 : 2018/5/18
  * @时间 : 14:09
  */
public class ExcelHelper {

    /** Lu Xinwei 2018/5/18 14:03   一个工作表sheet的行数 */
    private static int total =1000000;

    private XSSFWorkbook wb = null;//得到Excel工作簿对象
    private XSSFSheet sheet = null;//得到Excel工作表对象

    public ExcelHelper(XSSFWorkbook wb,XSSFSheet sheet){
        this.wb=wb;
        this.sheet = sheet;
    }
    /**
    * @描述: 导出 Excel
     * tests 导出列表数据
     * fileName 导出 Excel 路径
     * worksheetTitle 导出Excel标题
     * excelHeader Excel 表头
     * excelName Excel导出名
    * @作者: YJJ
    * @日期: 2018/6/7
    * @时间: 13:28
    **/
    public static <T> XSSFWorkbook generateExcel(List<T> tests, String worksheetTitle,List<String> excelHeader) {
        //List<String> l = new ArrayList<>();
        List<Map<String,T>> list=new ArrayList<Map<String,T>>();
        for (int j=0; j<tests.size(); j++) {
            Map<String,T> map=new HashMap<String,T>();//用map循环接收数据
            Field[] fields = tests.get(j).getClass().getDeclaredFields();
            for (int i = 0, len = fields.length; i < len; i++) {
                try {
                    boolean accessFlag = fields[i].isAccessible();
                    // 修改访问控制权限
                    fields[i].setAccessible(true);
                    /*if(j==0){
                        l.add(fields[i].getName());
                    }*/
                    map.put(excelHeader.get(i), (T) fields[i].get(tests.get(j)));//写入数据
                    fields[i].setAccessible(accessFlag);
                } catch (IllegalArgumentException ex) {
                    ex.printStackTrace();
                } catch (IllegalAccessException ex) {
                    ex.printStackTrace();
                }
            }
            list.add(map);
        }
        /*SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
        Date date=new Date();
        excelName=excelName+sdf.format(date);
        if(fileName==null||"".equals(fileName)) {
            //当前系统桌面 获得服务器端桌面
            File desktopDir = FileSystemView.getFileSystemView().getHomeDirectory();
            fileName = desktopDir.getPath() + "\\"+excelName+".xlsx";
            *//*String desktopPath = desktopDir.getAbsolutePath();
            fileName = System.getProperty("user.dir") + "\\"+excelName+".xlsx";//定义导出路径 当前项目目录*//*
            System.out.println(fileName);
        }else{
            fileName = fileName+"\\"+excelName+".xlsx";
        }*/
        if(worksheetTitle==null||"".equals(worksheetTitle)) {
            worksheetTitle = "Excel导出信息";//sheet名
        }
        List<Map<String,Object>> list1 = new ArrayList<>();

        XSSFWorkbook wb = new XSSFWorkbook();
        ExcelHelper exportExcel =null;
        List<XSSFSheet> sheets = new ArrayList<>();
        for(int i=0; i<(int)Math.ceil((double)list.size()/total); i++){
            String sheetName="导出数据"+i+1;

            XSSFSheet sheet = wb.createSheet(sheetName);//定义sheet名
            exportExcel = new ExcelHelper(wb,sheet);//调用Excel工具类
            /*try {
                fileName = new String(fileName.getBytes("GBK"), "GBK");//修改编码格式
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }*/

            /** Lu Xinwei 2018/5/18 10:55  创建报表头部  */
            exportExcel.createNormalHead(worksheetTitle, excelHeader.size()-1);
            /** Lu Xinwei 2018/5/18 10:55   定义第一行 */
            exportExcel.createNormalTwoRow(excelHeader, 0);
            /** Lu Xinwei 2018/5/18 10:55   导入数据 */
            exportExcel.createColumHeader(list.subList(total*i,(i+1)*total>list.size()?list.size():(i+1)*total),excelHeader);
            /** Lu Xinwei 2018/5/18 10:55   输出文件流，把相应的Excel工作簿 输出到本地 */
//        exportExcel.outputExcel(fileName);
            sheets.add(sheet);
        }
        //输出文件
        //exportExcel.outputExcel(fileName);
        return wb;
    }

    /**
     * 创建通用的Excel头
     *
     * @param  headString  头部显示的字符
     *  @param colSum  该报表的列数
     */
    public void createNormalHead(String headString, int colSum){
        // 设置第一行
        XSSFRow row = sheet.createRow(0);//创建Excel工作表的行
        XSSFCell cell = row.createCell(0);//创建Excel工作表指定行的单元格
        row.setHeight((short) 1000);//设置高度
        // 定义单元格为字符串类型
        cell.setCellType(CellType.STRING);// 中文处理
        cell.setCellValue(new XSSFRichTextString(headString));

        // 指定合并区域
        sheet.addMergedRegion(new CellRangeAddress(0, (short) 0, 0, (short) colSum));

        // 定义单元格格式，添加单元格表样式，并添加到工作簿
        XSSFCellStyle cellStyle = wb.createCellStyle();
        // 设置单元格水平对齐类型
        cellStyle.setAlignment(HorizontalAlignment.CENTER); // 指定单元格居中对齐
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);// 指定单元格垂直居中对齐
        cellStyle.setWrapText(true);// 指定单元格自动换行
        // 设置单元格字体
        XSSFFont font = wb.createFont();
        font.setBold(true);
        font.setFontName("微软雅黑");//字体
        font.setFontHeightInPoints((short)16);//设置字体
        cellStyle.setFont(font);
        cell.setCellStyle(cellStyle);
    }

    /**
     * 创建报表第二行
     * @param list
     *            统计条件数组
     * @param colSum
     *            需要合并到的列索引
     *
     */
    @SuppressWarnings("deprecation")
    public void createNormalTwoRow(List<String> list, int colSum){
        // 创建第二行
        XSSFRow row1 = sheet.createRow(1);
        row1.setHeight((short) 400);//设置高度
        XSSFCell cell2 = row1.createCell(0);//创建Excel工作表指定行的单元格
        cell2.setCellType(CellType.STRING);// 中文处理
        // 定义单元格格式，添加单元格表样式，并添加到工作簿
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER); // 指定单元格居中对齐
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);// 指定单元格垂直居中对齐
        cellStyle.setWrapText(true);// 指定单元格自动换行

        // 设置单元格字体
        XSSFFont font = wb.createFont();
        font.setBold(true);
        font.setFontName("宋体");//字体
        font.setFontHeightInPoints((short)10);//设置字体
        cellStyle.setFont(font);
        //HSSFCellStyle.ALIGN_CENTER  设定居中
        for(int i=0;i<list.size();i++){
            cteateCell(wb,row1,i,list.get(i),cellStyle);
        }
    }

    /**
     * 设置报表标题
     *
     * @param listInfo
     *            标题字符串数组
     */
    public <T> void createColumHeader(List<Map<String,T>> listInfo, List<String> list) {
        // 定义单元格格式，添加单元格表样式，并添加到工作簿
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER); // 指定单元格居中对齐
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);// 指定单元格垂直居中对齐
        cellStyle.setWrapText(true);// 指定单元格自动换行

        // 单元格字体
        XSSFFont font = wb.createFont();
        font.setBold(true);
        font.setFontName("宋体");//字体
        font.setFontHeightInPoints((short)10);//设置字体
        cellStyle.setFont(font);

        // 设置单元格背景色
        //cellStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
        //cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        XSSFCell cell3 = null;

        for (int i = 0; i < listInfo.size(); i++) {
            //循环插入数据
            XSSFRow row2 = sheet.createRow(i+2);
            row2.setHeight((short) 400);// 指定行高
            for(int j = 0; j < list.size(); j++ ) {
                cell3 = row2.createCell(j);
                cell3.setCellStyle(cellStyle);
                cell3.setCellValue(new XSSFRichTextString(listInfo.get(i).get(list.get(j))!=null?listInfo.get(i).get(list.get(j)).toString():"-"));
            }
        }
    }

    /**
     * 创建内容单元格
     *
     * @param wb
     *            XSSFWorkbook
     * @param row
     *            XSSFRow
     * @param col
     *            short型的列索引
     * @param val
     *            列值
     */
    public void cteateCell(XSSFWorkbook wb, XSSFRow row, int col,
                           String val,XSSFCellStyle cellstyle ) {
        XSSFCell cell = row.createCell(col);
        cell.setCellType(CellType.STRING);
        cell.setCellValue(new XSSFRichTextString(val));
        cell.setCellStyle(cellstyle);
    }
    /**
     * 创建合计行
     *
     * @param colSum
     *            需要合并到的列索引
     * @param list
     */
    public void createLastSumRow(int colSum, List<String> list) {
        // 定义单元格格式，添加单元格表样式，并添加到工作簿
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER); // 指定单元格居中对齐
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);// 指定单元格垂直居中对齐
        cellStyle.setWrapText(true);// 指定单元格自动换行

        // 单元格字体
        XSSFFont font = wb.createFont();
        font.setBold(true);
        font.setFontName("宋体");
        font.setFontHeight((short) 250);
        cellStyle.setFont(font);
        // 获取工作表最后一行
        XSSFRow lastRow = sheet.createRow((short) (sheet.getLastRowNum() + 1));
        XSSFCell sumCell = lastRow.createCell(0);//创建Excel工作表指定行的单元格

        sumCell.setCellValue(new XSSFRichTextString("合计"));
        sumCell.setCellStyle(cellStyle);
        // 合并 最后一行的第零列-最后一行的第一列
        sheet.addMergedRegion(new CellRangeAddress(sheet.getLastRowNum(), (short) 0,
                sheet.getLastRowNum(), (short) colSum));// 指定合并区域

        for (int i = 2; i < (list.size() + 2); i++) {
            // 定义最后一行的第三列
            sumCell = lastRow.createCell(i);
            sumCell.setCellStyle(cellStyle);
            // 定义数组 从0开始。
            sumCell.setCellValue(new XSSFRichTextString(list.get(i-2)));
        }
    }
    /**
     * 输入EXCEL文件
     *
     * @param fileName
     *            文件名
     */
    public void outputExcel(String fileName) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File(fileName));
            wb.write(fos);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public XSSFWorkbook getWb() {
        return wb;
    }
    public void setWb(XSSFWorkbook wb) {
        this.wb = wb;
    }
    public XSSFSheet getSheet() {
        return sheet;
    }
    public void setSheet(XSSFSheet sheet) {
        this.sheet = sheet;
    }


    /**
     * 设置表格样式
     */
    public static void setHSSFWorkbook(HSSFWorkbook wb, HSSFSheet sheet,
                                 String[] excelHeader, int[] excelHeaderWidth, String titleName){
        HSSFFont font1 = wb.createFont();//标题字体
        HSSFCellStyle style1 = wb.createCellStyle();//标题样式
        HSSFFont font2 = wb.createFont();//列名字体
        HSSFCellStyle style2 = wb.createCellStyle();//列名样式
        font1.setFontName("宋体");
        font1.setFontHeightInPoints((short) 16);//设置字体大小
        // 设置居中样式
        style1.setAlignment(HorizontalAlignment.CENTER); // 水平居中
        style1.setVerticalAlignment(VerticalAlignment.CENTER); // 垂直居中
        style1.setFont(font1);//设置标题的字体和大小
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, excelHeader.length - 1));//合并
        HSSFRow row = sheet.createRow((int) 0);//第一行
        HSSFCell cell = row.createCell(0);//标题
        cell.setCellValue(titleName);//标题
        cell.setCellStyle(style1);//标题样式
        row = sheet.createRow(1);//第二行
        font2.setFontName("宋体");
        font2.setFontHeightInPoints((short) 14);//设置字体大小
        style2.setAlignment(HorizontalAlignment.CENTER); // 水平居中
        style2.setVerticalAlignment(VerticalAlignment.CENTER); // 垂直居中
        style2.setFont(font2);
        style2.setWrapText(true);//设置自动换行
        // 设置列宽度（像素）
        for (int i = 0; i < excelHeaderWidth.length; i++) {
            sheet.setColumnWidth(i, 50 * excelHeaderWidth[i]);
        }
        // 添加表格头
        for (int i = 0; i < excelHeader.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(excelHeader[i]);
            cell.setCellStyle(style2);
        }
    }
}
