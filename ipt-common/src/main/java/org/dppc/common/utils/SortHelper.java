package org.dppc.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.dppc.common.entity.SortParam;
import org.springframework.data.domain.Sort;

public class SortHelper {

    public static final String ASC = "asc";
    public static final String DESC = "desc";

    public static Sort convert(SortParam sortParam){
        if(sortParam !=null){
            if(sortParam.getDirection().equals(DESC)){
                return new Sort(Sort.Direction.DESC,sortParam.getProperty());
            }else if(sortParam.getDirection().equals(ASC)){
                return new Sort(Sort.Direction.ASC,sortParam.getProperty());
            }
        }
        return null;
    }

    public static boolean isEmpty (SortParam sortParam){
        if(sortParam == null || StringUtils.isBlank(sortParam.getDirection()) || StringUtils.isBlank(sortParam.getProperty()) ){
            return true;
        }
        return false;
    }

}
