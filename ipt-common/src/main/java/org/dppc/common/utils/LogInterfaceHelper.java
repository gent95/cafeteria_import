package org.dppc.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.dppc.api.admin.dto.LogInterface;
import org.dppc.api.gateway.vo.RequestParams;
import org.dppc.api.gateway.vo.RequestVO;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

/**
 * Created by yanqi on 2018/4/21.
 */
public class LogInterfaceHelper {
    /**
     * 获取request提交的属性
     * @param request
     * @return
     */
    public static LogInterface getLogInterface(HttpServletRequest request) {
        LogInterface logInterface = new LogInterface();
        logInterface.setRequestTime(Calendar.getInstance().getTime());
        logInterface.setUrl(request.getRequestURI());
        logInterface.setRequestMethod(request.getMethod());
        logInterface.setHost(ClientHelper.getClientIp(request));
        return logInterface;
    }

    /**
     * 填充参数
     * @param logInterface
     * @param requestVO
     */
    public static void fillData( LogInterface logInterface,RequestVO requestVO) {
        logInterface.setBizType(requestVO.getBIZ_TYPE());
        logInterface.setReqId(requestVO.getREQ_ID());
        if (requestVO.getPARAM() instanceof Map) {
            Map param = (Map) requestVO.getPARAM();
            Collection values = param.values();
            for (Object value : values) {
                if (value instanceof List) {
                    logInterface.setUploadCount ( ((List) value).size() );
                }
            }
        }
        requestVO.setPARAM("#");
        logInterface.setRequestParams(JsonHelper.toJSon(requestVO));

    }

    /**
     * 转换请求内容
     * @param body
     * @return
     * @throws IOException
     */
    public static RequestParams preProcessParams(String body) throws IOException {
        String resquestVoStr = body.substring(0, body.lastIndexOf("}") + 1);
        String token = body.substring(body.lastIndexOf("}") + 1);
        if (StringUtils.isEmpty(resquestVoStr) || StringUtils.isEmpty(token)) {
            return null;
        }
        RequestVO requestVO = JsonHelper.readJson(resquestVoStr, RequestVO.class);
        return new RequestParams(requestVO, token);
    }
}
