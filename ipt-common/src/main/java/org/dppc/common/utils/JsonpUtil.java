package org.dppc.common.utils;


import org.springframework.util.StringUtils;

/**
 * @描述 : Jsonp工具类
 * @作者 : yanggang
 * @日期 : 2016/9/18
 * @时间 : 9:02
 */
public class JsonpUtil {
    /**
     * @描述 : 获取jsonp请求的返回数据
     * @作者 : yanggang
     * @时间 : 2016/9/18 9:14
     */
    public static <T> String getResult(String callback, T t) {
        if (StringUtils.isEmpty(callback)) {
            return JsonHelper.toJSon(t);
        }
        StringBuilder sb = new StringBuilder();
        return sb.append(callback).append("(").append(JsonHelper.toJSon(t)).append(");").toString();
    }
}
