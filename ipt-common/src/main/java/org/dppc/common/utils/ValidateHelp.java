package org.dppc.common.utils;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.Map;

/**
 * @描述 :  验证帮助类
 * @作者 :	zhumh
 * @日期 :	2018/11/29
 * @时间 :	13:49
 */
public class ValidateHelp {
    /**
     * @Author lhw
     * @Description 将错误消息格式化为Map对象
     * @Date 14:13 2019/5/8
     **/
    public static Map<String, Object> toErrMap(BindingResult result){
        Map<String, Object> map = new HashMap<String, Object>();
        for(FieldError fieldError : result.getFieldErrors()){
            map.put(fieldError.getField(), fieldError.getDefaultMessage());
        }
        return map;
    }
    /**
     * @Author lhw
     * @Description 将错误消息格式化为json字符串
     * @Date 14:13 2019/5/8
     **/
    public static String toErrJsonString(BindingResult result){
        Map<String, Object> map =  toErrMap(result);
        return JsonHelper.toJSon(map);
    }
}
