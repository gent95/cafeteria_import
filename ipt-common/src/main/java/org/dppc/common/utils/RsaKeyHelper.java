package org.dppc.common.utils;

import org.springframework.util.StreamUtils;

import java.io.DataInputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * Created by ace on 2017/9/10.
 */
public class RsaKeyHelper {

    /**
     * 获取公钥
     *
     * @param filename
     * @return
     * @throws Exception
     */
    public PublicKey getPublicKey(String filename) throws Exception {
        DataInputStream dis = new DataInputStream(this.getClass().getClassLoader().getResourceAsStream(filename));
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(new X509EncodedKeySpec(StreamUtils.copyToByteArray(dis)));
    }

    /**
     * 获取密钥
     *
     * @param filename
     * @return
     * @throws Exception
     */
    public PrivateKey getPrivateKey(String filename) throws Exception {
        DataInputStream dis = new DataInputStream(getClass().getClassLoader().getResourceAsStream(filename));
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(new PKCS8EncodedKeySpec(StreamUtils.copyToByteArray(dis)));
    }
}

