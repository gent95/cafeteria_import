package org.dppc.common.utils;

import org.springframework.beans.BeanUtils;

/**
 * @描述 : 复制实体类属性值到VO实体
 * @作者 :	zhumh
 * @日期 :	2018/5/14
 * @时间 :	11:35
 */
public class CopyPropertiesHelper {

    /**
     * 属性复制
     *
     * @param source
     * @param clazz
     * @return
     */
    public static <T> T copyProperties(Object source, Class<T> clazz) {
        if (source == null) {
            return null;
        }
        try {
            T target = BeanUtils.instantiate(clazz);
            BeanUtils.copyProperties(source, target);
            return target;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
