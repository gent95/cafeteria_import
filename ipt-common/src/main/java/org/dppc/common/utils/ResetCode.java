package org.dppc.common.utils;

import org.apache.commons.lang3.RandomUtils;

import java.util.Random;
import java.util.UUID;

/**
 * @描述：设置校验码
 * @作者：lichao
 * @日期：2017/12/13
 * @时间：16:48
 */
public class ResetCode {
    public static String[] chars = new String[] {"1", "2", "3", "4", "5",
            "6", "7", "8", "9"};
    public static String getResetCode() {
        StringBuffer shortBuffer = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            int i1 = random.nextInt(chars.length);
            shortBuffer.append(chars[i1]);
        }
        return shortBuffer.toString();

    }
}
