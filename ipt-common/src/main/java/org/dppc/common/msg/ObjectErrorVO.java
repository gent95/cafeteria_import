package org.dppc.common.msg;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @描述 :  校验错误对象
 * @作者 :  Zhao Yun
 * @日期 :  2017/11/8
 * @时间 :  13:36
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ObjectErrorVO {

    /** Zhao Yun 2017/11/8 校验的对象 */
    private String objectName;
    /** Zhao Yun 2017/11/8 错误的消息 */
    private String Message;

    @Override
    public String toString() {
        return "{" +
                objectName +
                Message  +
                "}";
    }

}