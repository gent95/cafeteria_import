package org.dppc.common.msg.auth;


import org.dppc.common.enums.ExceptionEnum;
import org.dppc.common.msg.BaseResponse;

/**
 * Created by ace on 2017/8/23.
 */
public class TokenErrorResponse extends BaseResponse {
    public TokenErrorResponse(String message) {
        super(ExceptionEnum.EX_TOKEN_ERROR.getStatus(), message);
    }
}
