package org.dppc.common.msg;

import lombok.*;

import java.util.List;

/**
 * @描述 :  分页返回
 * @作者 :  Zhao Yun
 * @日期 :  2017/11/15
 * @时间 :  09:28
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PageResponse<T> extends BaseResponse {

    /** Zhao Yun 2017/11/15 数据 */
    private List<T> data;
    /** Zhao Yun 2017/11/15 当前页 */
    private Integer currentPage;
    /** Zhao Yun 2017/11/15 总页数 */
    private Integer totalPages;
    /** Zhao Yun 2017/11/15 数据总数 */
    private Long totalRecords;

}
