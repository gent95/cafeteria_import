package org.dppc.common.msg;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.common.entity.EditorImage;

/**
 * @描述： layui table组件返回数据
 * @作者： gao jing
 * @日期： 2018/2/27.
 * @时间： 9:50.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class LayuiEditorResponse extends BaseResponse {

    /** 错误码. */
    private Integer code;

    /** 提示信息. */
    private String msg;

    /** 具体的内容. */
    private EditorImage data;

    public enum  CodeType {

        SUCCESS("成功", 0)
        , FAIL("失败", 1);

        private String name;
        private int value;

        CodeType(String name, int value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public int getValue() {
            return value;
        }
    }
}
