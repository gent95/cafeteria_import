package org.dppc.common.msg.auth;


import org.dppc.common.enums.ExceptionEnum;
import org.dppc.common.msg.BaseResponse;

/**
 * Created by ace on 2017/8/25.
 */
public class TokenForbiddenResponse  extends BaseResponse {
    public TokenForbiddenResponse() {
        super(ExceptionEnum.EX_USER_NOT_ALLOW.getStatus(), ExceptionEnum.EX_USER_NOT_ALLOW.getMsg());
    }
}
