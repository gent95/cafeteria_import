package org.dppc.common.msg;

/**
 * 返回基础类
 */
public class BaseResponse {
    private int status = 200;
    private String msg;

    public BaseResponse(int status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public BaseResponse() {
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "status=" + status +
                ", msg='" + msg + '\'' +
                '}';
    }
}
