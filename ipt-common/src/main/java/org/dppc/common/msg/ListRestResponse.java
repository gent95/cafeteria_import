package org.dppc.common.msg;

import java.util.List;


/**
 * @描述： 返回类型定义
 * @作者： 颜齐
 * @日期： 2017/7/27.
 * @时间： 11:36.
 */
public class ListRestResponse<T> extends BaseResponse{

    private List<T> data;

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

}
