package org.dppc.common.handler;

import io.jsonwebtoken.ExpiredJwtException;
import org.dppc.common.enums.ExceptionEnum;
import org.dppc.common.exception.BaseException;
import org.dppc.common.exception.StockNumberException;
import org.dppc.common.exception.UserNoBindCafeteriaException;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.security.SignatureException;


/**
 * 错误拦截
 */
@ControllerAdvice("org.dppc")
@ResponseBody
public class GlobalExceptionHandler {
    private Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    @ExceptionHandler(BaseException.class)
    public BaseResponse baseExceptionHandler(HttpServletResponse response, BaseException ex) {
        return new BaseResponse(ex.getStatus(), ex.getMessage());
    }

    /**
     * @描述 :  处理BindResult
     * @作者 :  Zhao Yun
     * @日期 :  2017/11/9
     * @时间 :  8:50
     */
    @ExceptionHandler(BindException.class)
    public BaseResponse bindExceptionHandler(HttpServletResponse response, BindException ex) {
        return ResponseHelper.error(ex.getBindingResult());
    }

    /**
     * @描述 :  处理BindResult
     * @作者 :  Zhao Yun
     * @日期 :  2017/11/9
     * @时间 :  8:50
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public BaseResponse bindExceptionHandler(HttpServletResponse response, MethodArgumentNotValidException ex) {
        return ResponseHelper.error(ex.getBindingResult());
    }

    /**
     * @描述 : token过期
     * @作者 : yanggang
     * @日期 : 2017/12/19
     * @时间 : 14:07
     */
    @ExceptionHandler(ExpiredJwtException.class)
    public BaseResponse otherExceptionHandler(ExpiredJwtException ex) {
        logger.error(ex.getMessage(),ex);
        return new BaseResponse(ExceptionEnum.EX_USER_INVALID.getStatus(), ex.getMessage());
    }

    /**
     * @描述 : token错误
     * @作者 : yanggang
     * @日期 : 2017/12/19
     * @时间 : 14:07
     */
    @ExceptionHandler(SignatureException.class)
    public BaseResponse otherExceptionHandler(SignatureException ex) {
        logger.error(ex.getMessage(),ex);
        return new BaseResponse(ExceptionEnum.EX_TOKEN_ERROR.getStatus(), ex.getMessage());
    }

    /**
     * @描述 : token为空
     * @作者 : yanggang
     * @日期 : 2017/12/19
     * @时间 : 14:07
     */
    @ExceptionHandler(IllegalArgumentException.class)
    public BaseResponse otherExceptionHandler(IllegalArgumentException ex) {
        logger.error(ex.getMessage(),ex);
        return new BaseResponse(ExceptionEnum.EX_TOKEN_NULL.getStatus(), ex.getMessage());
    }

    /**
     * @Author lhw
     * @Description 库存异常统一处理
     * @Date 16:25 2019/5/24
     **/
    @ExceptionHandler(StockNumberException.class)
    public BaseResponse otherExceptionHandler(StockNumberException ex) {
        logger.error(ex.getMessage(),ex);
        return new BaseResponse(ExceptionEnum.EX_STOCK_NUMBER.getStatus(), ex.getCustomMessage());
    }

    /**
     * @Author lhw
     * @Description 库存异常统一处理
     * @Date 16:25 2019/5/24
     **/
    @ExceptionHandler(UserNoBindCafeteriaException.class)
    public BaseResponse userNoBindCafeteriaException(UserNoBindCafeteriaException ex) {
        logger.error(ex.getMessage(),ex);
        return new BaseResponse(ExceptionEnum.EX_USER_NO_BIND_CAFETERIA.getStatus(), ExceptionEnum.EX_USER_NO_BIND_CAFETERIA.getMsg());
    }

    @ExceptionHandler(Exception.class)
    public BaseResponse otherExceptionHandler(HttpServletResponse response, Exception ex) {
        logger.error(ex.getMessage(),ex);
        return new BaseResponse(ExceptionEnum.EX_OTHER_ERROR.getStatus(), ex.getMessage());
    }
}
