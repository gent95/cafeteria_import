package org.dppc.common.handler;

import org.springframework.util.StreamUtils;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

public class BufferedServletRequestWrapper extends HttpServletRequestWrapper {
    private byte[] buffer;

    public BufferedServletRequestWrapper(HttpServletRequest request) throws IOException {
        super(request);
        InputStream is = request.getInputStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte buff[] = new byte[1024];
        int read;
        while ((read = is.read(buff)) > 0) {
            baos.write(buff, 0, read);
        }
        this.buffer = baos.toByteArray();
    }

    public BufferedServletRequestWrapper(HttpServletRequest request, byte[] buffer) throws IOException {
        super(request);
        this.buffer = buffer;
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        return new BufferedServletInputStream(this.buffer);
    }

    public String getBodyString() {
        return new String(buffer);
    }

    public void setBodyString(String bodyString) {
        this.buffer = bodyString.getBytes();
    }

    @Override
    public int getContentLength() {
        return this.buffer.length;
    }

    @Override
    public long getContentLengthLong() {
        return this.buffer.length;
    }
}