package org.dppc.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.common.enums.TraceTableEnum;

/**
 * @描述 :   数据交换队列用VO
 * @作者 :   GAOJ
 * @日期 :   2018/4/9 0019
 * @时间 :   13:58
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransportData {

    /** GAOJ 2018/4/9 数据类型 TraceTableEnum */
    private TraceTableEnum traceTable;

    /** GAOJ 2018/4/9 交换数据 */
    private String data;

    /** GAOJ 2018/4/9 交换数据ids */
    private String seqIds;

}
