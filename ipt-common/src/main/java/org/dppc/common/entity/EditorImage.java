package org.dppc.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @描述： layui table组件返回数据
 * @作者： gao jing
 * @日期： 2018/2/27.
 * @时间： 9:50.
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class EditorImage {

    /** 返回编辑器的图片访问路径 */
    private String src;
    /** 返回编辑器的图片名称 可选 */
    private String title;

}
