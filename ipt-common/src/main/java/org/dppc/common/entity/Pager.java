package org.dppc.common.entity;

/**
 * @ClassName Pager
 * @Description 分页请求参数
 * @Author GAOJ
 * @Data 2019/05/07 14:02
 * @Version 1.0
 **/
public class Pager {
    /**
     * 页码，第page页
     */
    private int page = 1;
    /**
     * 每页的大小
     */
    private int size = 10;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
