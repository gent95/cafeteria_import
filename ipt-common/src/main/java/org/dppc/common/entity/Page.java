package org.dppc.common.entity;

import org.apache.commons.lang3.StringUtils;
import org.dppc.common.exception.BaseException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 描述	 	  ：分页基类
 * <br>名称	  ：com.xinxkj.peops.admin.common.entity.Page
 * <br>创建人	  ：SHGU
 * <br>修改备注 ：
 *
 * @version V1.0
 */
public class Page<T> {
    /**
     * 页码，第几页
     */
    private int page = 1;
    /**
     * 总页数
     */
    @SuppressWarnings("unused")
    private Long totalPages;
    /**
     * 每页的大小
     */
    private int size = 10;
    /**
     * 总记录数
     */
    private long total;
    /**
     * 此页携带的数据
     */
    private List<T> data;
    /**
     * 此页数据Map
     */
    private Map<Long, T> dataMap;
    /**
     * 排序字段 ，如果是多个排序字段，以逗号隔开
     */
    private String sorts;
    /**
     * 排序顺序 ，如果是多个排序字段，以逗号隔开，并与排序字段sort一一对应
     */
    private String orders;

    private PageRequest pageRequest;


    public Long getTotalPages() {
        return (this.total + size - 1) / size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public String getSorts() {
        return sorts;
    }

    public void setSorts(String sorts) {
        this.sorts = sorts;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public Map<Long, T> getDataMap() {
        return dataMap;
    }

    public void setDataMap(Map<Long, T> dataMap) {
        this.dataMap = dataMap;
    }

    public void setTotalPages(Long totalPages) {
        this.totalPages = totalPages;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    /**
     * yanqi-- 分区springdata 的分页
     */
    public PageRequest getPageRequest() {
        if (this.pageRequest != null)
            return pageRequest;
        List<Sort.Order> orderList = new ArrayList<>();
        if (StringUtils.isNotEmpty(sorts) && StringUtils.isNotEmpty(sorts)) {
            String[] sortArray = sorts.split(",");
            String[] orderArray = orders.split(",");
            if (sortArray.length != orderArray.length)
                throw new BaseException(500, "orders与sorts不匹配，无法排序");
            for (int i = 0; i < orderArray.length; i++) {
                if ("desc".equalsIgnoreCase(orderArray[i]))
                    orderList.add(new Sort.Order(Sort.Direction.DESC, sortArray[i]));
                else if ("asc".equalsIgnoreCase(orderArray[i]))
                    orderList.add(new Sort.Order(Sort.Direction.ASC, sortArray[i]));
            }
        }
        if (orderList.size() != 0) {
            Sort domainSort = new Sort(orderList);
            this.pageRequest = new PageRequest(page - 1, size, domainSort);
        } else {
            this.pageRequest = new PageRequest(page-1, size);
        }
        return pageRequest;
    }

    public Page(int page, Long totalPages, int size, long total, String sorts, String orders) {
        this.page = page;
        this.totalPages = totalPages;
        this.size = size;
        this.total = total;
        this.sorts = sorts;
        this.orders = orders;
    }

    public Page(int page, Long totalPages, int size, long total, List<T> data, Map<Long, T> dataMap, String sorts, String orders) {
        this(page, totalPages, size, total, sorts, orders);
        this.data = data;
        this.dataMap = dataMap;
    }

    public Page() {
    }

    public static Page toPageVo(Page page) {
        return new Page(page.getPage(), page.getTotalPages(), page.getSize(), page.getTotal(), page.getSorts(), page.getOrders());
    }


}
