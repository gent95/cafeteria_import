package org.dppc.common.constant;

/**
 * @描述 ：分页默认页码及页面大小
 * @作者 ：wyl
 * @日期 ：2017/10/31 
 * @时间 ：16:18
 */
public class PageConstant {
    public static final String DEFAULT_PAGE = "1";
    //默认十条
    public static final String DEFAULT_PAGE_SIZE = "10";
}
