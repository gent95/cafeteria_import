package org.dppc.common.constant;

/**
 *
 */
public interface CommonConstants {
    String CONTEXT_KEY_USER_ID = "currentUserId";
    String CONTEXT_KEY_USERNAME = "currentUserName";
    String CONTEXT_KEY_USER_NAME = "currentUser";
    String CONTEXT_KEY_USER_TOKEN = "currentUserToken";
    String CONTEXT_KEY_USER_CITY_CODE = "currentUserCityCode";
    String CONTEXT_KEY_USER_TYPE = "currentUserType";
    String JWT_KEY_USER_ID = "userId";
    String JWT_KEY_NAME = "name";
    String SCHOOL_CAFTERIAS_KEY = "schoolCafterias";
}
