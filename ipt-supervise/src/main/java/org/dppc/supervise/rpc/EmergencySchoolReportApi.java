package org.dppc.supervise.rpc;

import org.dppc.supervise.entity.emergency.EmergencySchoolReport;
import org.dppc.supervise.service.emergency.EmergencySchoolReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description 学校应急事件上报
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
@RestController
@RequestMapping(value = "/api/schoolReport")
public class EmergencySchoolReportApi {

    @Autowired
    EmergencySchoolReportService emergencySchoolReportService;

    /**
     * @Author GAOJ
     * @Description 应急事件上报
     * @Date 2019/05/27 14:18
     **/
    @PostMapping(value = "/report")
    @ResponseBody
    public Boolean generateTraceCode(@RequestBody EmergencySchoolReport requestTraceCode) {
        try {
            emergencySchoolReportService.save(requestTraceCode);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
