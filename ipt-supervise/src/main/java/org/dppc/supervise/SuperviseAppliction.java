package org.dppc.supervise;

import org.dppc.auth.client.EnableIptAuthClient;
import org.dppc.dbexpand.repository.BaseRepositoryFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @描述 :
 * @作者 :	zhumh
 * @日期 :	2019/5/6
 * @时间 :	13:31
 */
@EnableJpaRepositories(basePackages = {"org.dppc"}, repositoryFactoryBeanClass = BaseRepositoryFactoryBean.class)
@ComponentScan(basePackages = {"org.dppc.supervise","org.dppc.dbexpand"})
@EnableTransactionManagement
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients({"org.dppc.auth.client.feign", "org.dppc.supervise.feign"})
@EnableIptAuthClient
@EnableHystrix
public class SuperviseAppliction {

    public static void main(String[] args) {
        SpringApplication.run(SuperviseAppliction.class, args);
    }
}
