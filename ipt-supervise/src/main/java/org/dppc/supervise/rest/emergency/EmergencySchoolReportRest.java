package org.dppc.supervise.rest.emergency;

import lombok.extern.java.Log;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.dppc.supervise.entity.emergency.EmergencySchoolReport;
import org.dppc.supervise.entity.emergency.EmergencyType;
import org.dppc.supervise.service.emergency.EmergencySchoolReportService;
import org.dppc.supervise.service.emergency.EmergencyTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.logging.Level;

/**
 * @Description 学校应急事件上报（学校直通车）
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/emergencySchoolReport")
public class EmergencySchoolReportRest {

    @Autowired
    private EmergencySchoolReportService emergencySchoolReportService;
    @Autowired
    private EmergencyTypeService emergencyTypeService;

    /**
     * @Author GAOJ
     * @Description 根据条件分页查询学校应急事件上报（学校直通车）
     * @Date 2019/05/27 14:18
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, EmergencySchoolReport emergencySchoolReport) {
        /** 根据条件分页查询集合 **/
        Page<EmergencySchoolReport> page = emergencySchoolReportService.findPageList(emergencySchoolReport,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        HashMap<Object, Object> map = new HashMap<>(2);
        if (pager.getPage() == 1) {
            map.put("emergencyTypeList", emergencyTypeService.findList(new EmergencyType()));
        }
        return ResponseHelper.success(page, map);
    }

    /**
     * @Author GAOJ
     * @Description 根据id查询学校应急事件上报（学校直通车）
     * @Date 2019/05/27 14:18
     **/
    @GetMapping("/{id}")
    public BaseResponse findEmergencySchoolReport(@PathVariable Long id) {
        return ResponseHelper.success(emergencySchoolReportService.getOneDataById(id));
    }

    /**
     * @Author GAOJ
     * @Description 添加学校应急事件上报（学校直通车）
     * @Date 2019/05/27 14:18
     **/
    @PostMapping
    public BaseResponse saveEmergencySchoolReport(@Valid @RequestBody EmergencySchoolReport emergencySchoolReport, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            emergencySchoolReportService.save(emergencySchoolReport);
            return ResponseHelper.success(emergencySchoolReport);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author GAOJ
     * @Description 局部更新学校应急事件上报（学校直通车）
     * @Date 2019/05/27 14:18
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchEmergencySchoolReport(@PathVariable Long id, @RequestBody EmergencySchoolReport emergencySchoolReport) {
        try {
            boolean result = emergencySchoolReportService.patch(emergencySchoolReport, id);
            if (result) {
                return ResponseHelper.success(emergencySchoolReport);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 根据id删除学校应急事件上报（学校直通车）
     * @Date 2019/05/27 14:18
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteEmergencySchoolReport(@PathVariable Long id) {
        try {
            emergencySchoolReportService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }
}
