package org.dppc.supervise.rest.emergency;

import lombok.extern.java.Log;
import org.dppc.common.entity.Pager;
import org.dppc.common.enums.IsDeleteEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.dppc.supervise.entity.emergency.EmergencyPlan;
import org.dppc.supervise.entity.emergency.EmergencyType;
import org.dppc.supervise.service.emergency.EmergencyPlanService;
import org.dppc.supervise.service.emergency.EmergencyTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;

/**
 * @Description 应急预案
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/emergencyPlan")
public class EmergencyPlanRest {

    @Autowired
    private EmergencyPlanService emergencyPlanService;
    @Autowired
    private EmergencyTypeService emergencyTypeService;

    /**
     * @Author GAOJ
     * @Description 根据条件分页查询应急预案
     * @Date 2019/05/27 14:18
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, EmergencyPlan emergencyPlan) {
        /** 根据条件分页查询集合 **/
        Page<EmergencyPlan> page = emergencyPlanService.findPageList(emergencyPlan,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        HashMap<Object, Object> map = new HashMap<>(2);
        if (pager.getPage() == 1) {
            map.put("emergencyTypeList", emergencyTypeService.findList(new EmergencyType()));
        }
        return ResponseHelper.success(page, map);
    }

    /**
     * @Author GAOJ
     * @Description 根据id查询应急预案
     * @Date 2019/05/27 14:18
     **/
    @GetMapping("/{id}")
    public BaseResponse findEmergencyPlan(@PathVariable Long id) {
        return ResponseHelper.success(emergencyPlanService.getOneDataById(id));
    }

    /**
     * @Author GAOJ
     * @Description 添加应急预案
     * @Date 2019/05/27 14:18
     **/
    @PostMapping
    public BaseResponse saveEmergencyPlan(@Valid @RequestBody EmergencyPlan emergencyPlan, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            emergencyPlan.setCreateDate(new Date());
            emergencyPlan.setIsDelete(IsDeleteEnum.NOT_DELETE.getValue());
            emergencyPlanService.save(emergencyPlan);
            return ResponseHelper.success(emergencyPlan);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author GAOJ
     * @Description 局部更新应急预案
     * @Date 2019/05/27 14:18
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchEmergencyPlan(@PathVariable Long id, @RequestBody EmergencyPlan emergencyPlan) {
        try {
            emergencyPlan.setUpdateDate(new Date());
            boolean result = emergencyPlanService.patch(emergencyPlan, id);
            if (result) {
                return ResponseHelper.success(emergencyPlan);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 根据id删除应急预案
     * @Date 2019/05/27 14:18
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteEmergencyPlan(@PathVariable Long id) {
        try {
            EmergencyPlan data = emergencyPlanService.getOneDataById(id);
            if (data != null) {
                data.setIsDelete(IsDeleteEnum.DELETED.getValue());
                emergencyPlanService.patch(data, id);
                return ResponseHelper.success(id);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }
}
