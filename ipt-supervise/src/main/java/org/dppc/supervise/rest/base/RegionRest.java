package org.dppc.supervise.rest.base;

import lombok.extern.java.Log;
import org.dppc.common.entity.Pager;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.dppc.supervise.entity.base.Region;
import org.dppc.supervise.service.base.RegionService;
import org.dppc.supervise.vo.EleTreeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.logging.Level;

/**
 * @Description 区域
 * @Author GAOJ
 * @Data 2019/05/24 10:43
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/region")
public class RegionRest {

    @Autowired
    private RegionService regionService;

    /**
     * @Author GAOJ
     * @Description 根据条件分页查询区域
     * @Date 2019/05/24 10:43
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, Region region) {
        /** 根据条件分页查询集合 **/
        Page<Region> page = regionService.findPageList(region,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        return ResponseHelper.success(page);
    }

    /**
     * @Author GAOJ
     * @Description 根据regionCode查询区域
     * @Date 2019/05/24 10:43
     **/
    @GetMapping("/{regionCode}")
    public BaseResponse findRegion(@PathVariable String regionCode) {
        return ResponseHelper.success(regionService.findByRegionCode(regionCode));
    }
    /**
     * @Author GAOJ
     * @Description 根据条件查询学校
     * @Date 2019/05/09 09:45
     **/
    @GetMapping("/list")
    public BaseResponse list(Region region) {
        List<Region> list = regionService.findList(region);
        return ResponseHelper.success(list);
    }
    /**
     * @Author GAOJ
     * @Description 添加区域
     * @Date 2019/05/24 10:43
     **/
    @PostMapping
    public BaseResponse saveRegion(@Valid @RequestBody Region region, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            regionService.save(region);
            return ResponseHelper.success(region);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author GAOJ
     * @Description 局部更新区域
     * @Date 2019/05/24 10:43
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchRegion(@PathVariable String id, @RequestBody Region region) {
        try {
            boolean result = regionService.patch(region, id);
            if (result) {
                return ResponseHelper.success(region);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 根据id删除区域
     * @Date 2019/05/24 10:43
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteRegion(@PathVariable String id) {
        try {
            regionService.delete(id);
            return ResponseHelper.success(id);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author GAOJ
     * @Description 根据id查询学校
     * @Date 2019/05/24 10:45
     **/
    @GetMapping("/tree/")
    public BaseResponse findSchool() {
        List<EleTreeVO> eleTreeVOS = regionService.findRegionTree();
        return ResponseHelper.success(eleTreeVOS);
    }
}
