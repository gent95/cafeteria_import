package org.dppc.supervise.rest.record;

import lombok.extern.java.Log;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.entity.Pager;
import org.dppc.common.enums.RecordAuditStatusEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.dppc.supervise.entity.record.Cafeteria;
import org.dppc.supervise.service.record.CafeteriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

/**
 * @Description 食堂
 * @Author GAOJ
 * @Data 2019/05/24 10:45
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/cafeteria")
public class CafeteriaRest {

    @Autowired
    private CafeteriaService cafeteriaService;

    /**
     * @Author GAOJ
     * @Description 根据条件分页查询食堂
     * @Date 2019/05/24 10:45
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, Cafeteria cafeteria) {
        /** 根据条件分页查询集合 **/
        Page<Cafeteria> page = cafeteriaService.findPageList(cafeteria,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        HashMap<Object, Object> map = new HashMap<>(2);
        map.put("auditStatusEnums", RecordAuditStatusEnum.values());
        return ResponseHelper.success(page, map);
    }

    /**
     * @Author GAOJ
     * @Description 根据id查询食堂
     * @Date 2019/05/24 10:45
     **/
    @GetMapping("/{id}")
    public BaseResponse findCafeteria(@PathVariable Integer id) {
        return ResponseHelper.success(cafeteriaService.getOneDataById(id));
    }

    /**
     * @Author GAOJ
     * @Description 添加食堂
     * @Date 2019/05/24 10:45
     **/
    @PostMapping
    public BaseResponse saveCafeteria(@Valid @RequestBody Cafeteria cafeteria, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            cafeteriaService.saveNew(cafeteria);
            return ResponseHelper.success(cafeteria);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author GAOJ
     * @Description 局部更新食堂
     * @Date 2019/05/24 10:45
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchCafeteria(@PathVariable Integer id, @RequestBody Cafeteria cafeteria) {
        try {
            boolean result = cafeteriaService.patch(cafeteria, id);
            if (result) {
                return ResponseHelper.success(cafeteria);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 审核食堂
     * @Date 2019/05/09 09:45
     **/
    @PatchMapping(value = "/audit")
    public BaseResponse patchCafeteria(@RequestBody Cafeteria cafeteria) {
        try {
            cafeteria.setAuditUserId(Long.valueOf(BaseContextHandler.getUserID()));
            cafeteria.setAuditUserName(BaseContextHandler.getName());
            cafeteria.setAuditDate(new Date());
            boolean result = cafeteriaService.patch(cafeteria, cafeteria.getCafeteriaId());
            if (result) {
                return ResponseHelper.success(cafeteria);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }
    /**
     * @Author GAOJ
     * @Description 根据id删除食堂
     * @Date 2019/05/24 10:45
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteCafeteria(@PathVariable Integer id) {
        try {
            Cafeteria cafeteria = cafeteriaService.getOneDataById(id);
            if (cafeteria != null && RecordAuditStatusEnum.AUDIT_PASSED.getValue() != cafeteria.getAuditStatus()) {
                cafeteriaService.delete(id);
                return ResponseHelper.success(id);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author 朱明华
     * @Description 根据条件查询所有食堂数据
     * @Date 2019/05/09 09:45
     **/
    @GetMapping("/list")
    public BaseResponse list(Cafeteria cafeteria) {
        List<Cafeteria> list = cafeteriaService.findList(cafeteria);
        return ResponseHelper.success(list);
    }
}
