package org.dppc.supervise.rest.record;

import lombok.extern.java.Log;
import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.entity.Pager;
import org.dppc.common.enums.RecordAuditStatusEnum;
import org.dppc.common.enums.SchoolNatureEnum;
import org.dppc.common.enums.SchoolTypeEnum;
import org.dppc.common.msg.BaseResponse;
import org.dppc.common.utils.ResponseHelper;
import org.dppc.supervise.entity.record.School;
import org.dppc.supervise.service.record.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

/**
 * @Description 学校
 * @Author GAOJ
 * @Data 2019/05/24 10:45
 * @Version 1.0
 **/
@Log
@RestController
@RequestMapping(value = "/school")
public class SchoolRest {

    @Autowired
    private SchoolService schoolService;

    /**
     * @Author GAOJ
     * @Description 根据条件分页查询学校
     * @Date 2019/05/24 10:45
     **/
    @GetMapping("/page")
    public BaseResponse page(Pager pager, School school) {
        /** 根据条件分页查询集合 **/
        Page<School> page = schoolService.findPageList(school,
                new PageRequest(pager.getPage() - 1, pager.getSize()));
        HashMap<Object, Object> map = new HashMap<>(4);
        map.put("schoolTypeEnum", SchoolTypeEnum.values());
        map.put("schoolNatureEnum", SchoolNatureEnum.values());
        map.put("auditStatusEnums", RecordAuditStatusEnum.values());
        return ResponseHelper.success(page, map);
    }

    /**
     * @Author GAOJ
     * @Description 根据条件查询学校
     * @Date 2019/05/09 09:45
     **/
    @GetMapping("/list")
    public BaseResponse list(School school) {
        List<School> list = schoolService.findList(school);
        return ResponseHelper.success(list);
    }

    /**
     * @Author GAOJ
     * @Description 根据id查询学校
     * @Date 2019/05/24 10:45
     **/
    @GetMapping("/{id}")
    public BaseResponse findSchool(@PathVariable Integer id) {
        return ResponseHelper.success(schoolService.getOneDataById(id));
    }

    /**
     * @Author GAOJ
     * @Description 添加学校
     * @Date 2019/05/24 10:45
     **/
    @PostMapping
    public BaseResponse saveSchool(@Valid @RequestBody School school, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseHelper.verifyError(result);
        }
        try {
            schoolService.saveNew(school);
            return ResponseHelper.success(school);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            return ResponseHelper.error();
        }
    }

    /**
     * @Author GAOJ
     * @Description 局部更新学校
     * @Date 2019/05/24 10:45
     **/
    @PatchMapping(value = "/{id}")
    public BaseResponse patchSchool(@PathVariable Integer id, @RequestBody School school) {
        try {
            boolean result = schoolService.patch(school, id);
            if (result) {
                return ResponseHelper.success(school);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 审核食堂
     * @Date 2019/05/09 09:45
     **/
    @PatchMapping(value = "/audit")
    public BaseResponse patchSchool(@RequestBody School school) {
        try {
            school.setAuditUserId(Long.valueOf(BaseContextHandler.getUserID()));
            school.setAuditUserName(BaseContextHandler.getName());
            school.setAuditDate(new Date());
            boolean result = schoolService.patch(school, school.getSchoolId());
            if (result) {
                return ResponseHelper.success(school);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        return ResponseHelper.error();
    }

    /**
     * @Author GAOJ
     * @Description 根据id删除学校
     * @Date 2019/05/24 10:45
     **/
    @DeleteMapping("/{id}")
    public BaseResponse deleteSchool(@PathVariable Integer id) {
        try {
            School school = schoolService.getOneDataById(id);
            if (school != null && RecordAuditStatusEnum.AUDIT_PASSED.getValue() != school.getAuditStatus()) {
                schoolService.delete(id);
                return ResponseHelper.success(id);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());

        }
        return ResponseHelper.error();
    }
}
