package org.dppc.supervise.entity.record;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 学校
 * @Author GAOJ
 * @Data 2019/05/24 10:45
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_spv_record_school")
public class School implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "school_id")
    private Integer schoolId;

    /** 学校名称 */
    @NotNull
    @Column(name = "school_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String schoolName;

    /** 学校代码 */
    @Column(name = "school_no", length = 30)
    @PredicateInfo
    private String schoolNo;

    /** 学校编码 */
    @Column(name = "school_code", length = 30)
    @PredicateInfo
    private String schoolCode;

    /** 学校类型 SchoolTypeEnum*/
    @Column(name = "school_type")
    @PredicateInfo
    private Integer schoolType;

    /** 学校性质 */
    @Column(name = "school_nature")
    @PredicateInfo
    private Integer schoolNature;

    /** 区域编码 */
    @NotNull
    @Column(name = "region_code", length = 6)
    @PredicateInfo(queryType = PredicateInfo.QueryType.RIGHT_LIKE)
    private String regionCode;

    /** 区域名称 */
    @Column(name = "all_region_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.RIGHT_LIKE)
    private String allRegionName;

    /** 学校地址 */
    @Column(name = "address", length = 50)
    private String address;

    /** 经度 */
    @Column(name = "lon")
    private Double lon;

    /** 纬度 */
    @Column(name = "lat")
    private Double lat;

    /** 校长 */
    @Column(name = "headmaster", length = 30)
    private String headmaster;

    /** 校长电话 */
    @Column(name = "master_phone", length = 11)
    private String masterPhone;

    /** 联系人 */
    @Column(name = "contact", length = 30)
    private String contact;

    /** 联系电话 */
    @Column(name = "contact_phone", length = 11)
    private String contactPhone;

    /** 学校图片 */
    @Column(name = "image_url", length = 100)
    private String imageUrl;

    /** 备案人id */
    @Column(name = "create_user_id")
    private Long createUserId;

    /** 备案人姓名 */
    @Column(name = "create_user_name", length = 30)
    private String createUserName;

    /** 备案日期 */
    @Column(name = "create_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_MINUTE_PATTERN, timezone = "GMT+8")
    private Date createDate;

    /** 审核状态 */
    @Column(name = "audit_status")
    @PredicateInfo
    private Integer auditStatus;

    /** 审核人id */
    @Column(name = "audit_user_id")
    private Long auditUserId;

    /** 审核人姓名 */
    @Column(name = "audit_user_name", length = 30)
    private String auditUserName;

    /** 审核日期 */
    @Column(name = "audit_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_MINUTE_PATTERN, timezone = "GMT+8")
    private Date auditDate;

    @Transient
    private Boolean nameChange;

}