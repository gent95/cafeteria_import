package org.dppc.supervise.entity.emergency;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 应急预案
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_spv_emergency_plan")
public class EmergencyPlan implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "emergency_plan_id")
    private Long emergencyPlanId;

    /** 预案名称 */
    @Column(name = "plan_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String planName;

    /** 预案主题 */
    @Column(name = "plan_theme", length = 100)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String planTheme;

    /** 事件类型 */
    @Column(name = "emergency_type")
    @PredicateInfo
    private Integer emergencyType;

    /** 预案内容 */
    @Column(name = "content", length = 4000)
    private String content;

    /** 应急组织 */
    @Column(name = "organization", length = 50)
    private String organization;

    /** 应急部门 */
    @Column(name = "department", length = 50)
    private String department;

    /** 应急人员 */
    @Column(name = "staff", length = 50)
    private String staff;

    /** 人员电话 */
    @Column(name = "staff_phone", length = 11)
    private String staffPhone;

    /** 创建时间 */
    @Column(name = "create_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_MINUTE_PATTERN, timezone = "GMT+8")
    private Date createDate;

    /** 修改时间 */
    @Column(name = "update_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_MINUTE_PATTERN, timezone = "GMT+8")
    private Date updateDate;

    /** 是否删除1.未删除0.已删除 */
    @Column(name = "is_delete")
    @PredicateInfo
    private Integer isDelete;

}