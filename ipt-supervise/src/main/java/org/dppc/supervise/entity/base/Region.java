package org.dppc.supervise.entity.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @Description 区域
 * @Author GAOJ
 * @Data 2019/05/24 10:43
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_spv_region")
public class Region implements Serializable {

    @Id
    /** 区域编码 */
    @Column(name = "region_code")
    @PredicateInfo
    private String regionCode;

    /** 区域名称 */
    @Column(name = "region_name", length = 20)
    private String regionName;

    /** 父级区域编码 */
    @Column(name = "p_code", length = 6)
    @PredicateInfo
    private String pCode;

    /** 区域等级 */
    @Column(name = "level")
    @PredicateInfo
    private Integer level;

    /** 组合名称 */
    @Column(name = "all_region_name", length = 50)
    private String allRegionName;

    @Transient
    private List<Region> sons;

}