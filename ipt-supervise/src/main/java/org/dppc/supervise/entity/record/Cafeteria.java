package org.dppc.supervise.entity.record;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 食堂
 * @Author GAOJ
 * @Data 2019/05/24 10:45
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_spv_record_cafeteria")
public class Cafeteria implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "cafeteria_id")
    private Integer cafeteriaId;

    /** 食堂编码 */
    @Column(name = "cafeteria_code", length = 20)
    @PredicateInfo
    private String cafeteriaCode;

    /** 食堂名称 */
    @Column(name = "cafeteria_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String cafeteriaName;

    /** 学校编码 */
    @Column(name = "school_code")
    @PredicateInfo
    private String schoolCode;

    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String schoolName;

    /** 负责人 */
    @Column(name = "head", length = 30)
    private String head;

    /** 负责人电话 */
    @Column(name = "head_phone", length = 11)
    private String headPhone;

    /** 食堂图片 */
    @Column(name = "image_url", length = 100)
    private String imageUrl;

    /** 备案人id */
    @Column(name = "create_user_id")
    private Long createUserId;

    /** 备案人姓名 */
    @Column(name = "create_user_name", length = 30)
    private String createUserName;

    /** 备案日期 */
    @Column(name = "create_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_MINUTE_PATTERN, timezone = "GMT+8")
    private Date createDate;

    /** 审核状态 */
    @Column(name = "audit_status")
    @PredicateInfo
    private Integer auditStatus;

    /** 审核人id */
    @Column(name = "audit_user_id")
    private Long auditUserId;

    /** 审核人姓名 */
    @Column(name = "audit_user_name", length = 30)
    private String auditUserName;

    /** 审核日期 */
    @Column(name = "audit_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_MINUTE_PATTERN, timezone = "GMT+8")
    private Date auditDate;

}