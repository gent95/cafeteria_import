package org.dppc.supervise.entity.emergency;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dppc.dbexpand.annotation.PredicateInfo;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @Description 学校应急事件上报（学校直通车）
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dppc_spv_emergency_school_report")
public class EmergencySchoolReport implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "report_id")
    private Long reportId;

    /** 学校id */
    @Column(name = "school_id")
    private Integer schoolId;

    /** 学校名称 */
    @Column(name = "school_name", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String schoolName;

    /** 地址 */
    @Column(name = "address", length = 100)
    private String address;

    /** 事件类型 */
    @Column(name = "emergency_type")
    @PredicateInfo
    private Integer emergencyType;

    /** 事件标题 */
    @Column(name = "title", length = 50)
    @PredicateInfo(queryType = PredicateInfo.QueryType.INNER_LIKE)
    private String title;

    /** 事件描述 */
    @Column(name = "content")
    private String content;

    /** 校长 */
    @Column(name = "headmaster", length = 30)
    private String headmaster;

    /** 校长电话 */
    @Column(name = "master_phone", length = 11)
    private String masterPhone;

    /** 上报时间 */
    @Column(name = "report_date")
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    @JsonFormat(pattern = DatePattern.NORM_DATETIME_MINUTE_PATTERN, timezone = "GMT+8")
    private Date reportDate;

    /** 是否删除1.未删除0.已删除 */
    @Column(name = "is_delete")
    @PredicateInfo
    private Integer isDelete;

    /** 经度 */
    @Column(name = "lon")
    private Double lon;

    /** 纬度 */
    @Column(name = "lat")
    private Double lat;

}