package org.dppc.supervise.service.emergency.impl;

import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.dppc.supervise.entity.emergency.EmergencySchoolReport;
import org.dppc.supervise.repository.emergency.EmergencySchoolReportRepository;
import org.dppc.supervise.service.emergency.EmergencySchoolReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description 学校应急事件上报（学校直通车）业务层实现类
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class EmergencySchoolReportServiceImpl extends BaseServiceImpl<EmergencySchoolReport, Long> implements EmergencySchoolReportService {

    @Autowired
    private EmergencySchoolReportRepository emergencySchoolReportRepository;

}
