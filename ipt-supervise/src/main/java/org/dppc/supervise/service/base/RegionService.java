package org.dppc.supervise.service.base;

import org.dppc.dbexpand.service.BaseService;
import org.dppc.supervise.entity.base.Region;
import org.dppc.supervise.vo.EleTreeVO;

import java.util.List;

/**
 * @Description 区域业务层接口
 * @Author GAOJ
 * @Data 2019/05/24 10:43
 * @Version 1.0
 **/
public interface RegionService extends BaseService<Region, String> {

    Region findByRegionCode(String regionCode);
    /**
     * @Author lhw
     * @Description 查询区域树
     * @Date 10:43 2019/6/6
     **/
    List<EleTreeVO> findRegionTree();
}
