package org.dppc.supervise.service.base.impl;

import io.swagger.models.auth.In;
import org.dppc.common.enums.RegionTypeEnum;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.dppc.supervise.entity.base.Region;
import org.dppc.supervise.repository.base.RegionRepository;
import org.dppc.supervise.service.base.RegionService;
import org.dppc.supervise.vo.EleTreeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description 区域业务层实现类
 * @Author GAOJ
 * @Data 2019/05/24 10:43
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class RegionServiceImpl extends BaseServiceImpl<Region, String> implements RegionService {

    @Autowired
    private RegionRepository regionRepository;

    @Override
    public Region findByRegionCode(String regionCode) {
        Region one = regionRepository.findOne(regionCode);
        recursionGetSons(one);
        return one;
    }

    @Override
    public List<EleTreeVO> findRegionTree() {
        List<EleTreeVO> eleTreeVOS ;
        List<Region> regions = regionRepository.findAll();
        eleTreeVOS = bindChildren(regions,"100000");
        return eleTreeVOS;
    }

    private List<EleTreeVO> bindChildren(List<Region> regions,  String pCode){
        List<EleTreeVO> eleTreeVOS = new ArrayList<>();
        regions.forEach(region -> {
            if (region.getPCode().equals(pCode)) {
                EleTreeVO eleTreeVO = new EleTreeVO();
                eleTreeVO.setLevel(region.getLevel() -1);
                eleTreeVO.setCode(region.getRegionCode());
                eleTreeVO.setLabel(region.getRegionName());
                eleTreeVO.setChildren(bindChildren(regions,region.getRegionCode()));
                eleTreeVOS.add(eleTreeVO);
            }
        });
        return eleTreeVOS;
    }

    private void recursionGetSons(Region region) {
        if (region.getLevel() < RegionTypeEnum.DISTRICT_COUNTY.getValue()) {
            Region query = new Region();
            query.setPCode(region.getRegionCode());
            List<Region> sons = findList(query);
            region.setSons(sons);
            for (Region re : sons) {
                recursionGetSons(re);
            }
        }
    }

}
