package org.dppc.supervise.service.record;

import org.dppc.dbexpand.service.BaseService;
import org.dppc.supervise.entity.record.Cafeteria;

/**
 * @Description 食堂业务层接口
 * @Author GAOJ
 * @Data 2019/05/24 10:45
 * @Version 1.0
 **/
public interface CafeteriaService extends BaseService<Cafeteria, Integer> {

    void saveNew(Cafeteria cafeteria);

}
