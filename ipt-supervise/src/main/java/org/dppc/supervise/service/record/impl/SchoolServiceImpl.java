package org.dppc.supervise.service.record.impl;

import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.enums.RecordAuditStatusEnum;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.dppc.supervise.entity.record.School;
import org.dppc.supervise.repository.record.SchoolRepository;
import org.dppc.supervise.service.record.SchoolService;
import org.dppc.supervise.service.syscode.SysCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @Description 学校业务层实现类
 * @Author GAOJ
 * @Data 2019/05/24 10:45
 * @Version 1.0
 **/
@Service
public class SchoolServiceImpl extends BaseServiceImpl<School, Integer> implements SchoolService {

    @Autowired
    private SchoolRepository schoolRepository;
    @Autowired
    private SysCodeService sysCodeService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveNew(School school) {
        school.setSchoolCode(school.getRegionCode() + sysCodeService.getSchoolCode());
        school.setCreateUserId(Long.valueOf(BaseContextHandler.getUserID()));
        school.setCreateUserName(BaseContextHandler.getName());
        school.setAuditStatus(RecordAuditStatusEnum.NOT_AUDIT.getValue());
        school.setCreateDate(new Date());
        schoolRepository.save(school);
    }


}
