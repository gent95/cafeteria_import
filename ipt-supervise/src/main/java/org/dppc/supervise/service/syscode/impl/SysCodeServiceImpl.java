package org.dppc.supervise.service.syscode.impl;

import org.dppc.supervise.entity.syscode.SysCode;
import org.dppc.supervise.repository.syscode.SysCodeRepository;
import org.dppc.supervise.service.syscode.SysCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description 业务层实现类
 * @Author GAOJ
 * @Data 2019/05/14 14:00
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class SysCodeServiceImpl implements SysCodeService {

    @Autowired
    private SysCodeRepository sysCodeRepository;

    public void put(SysCode sysCode) {
        sysCodeRepository.saveAndFlush(sysCode);
    }

    @Override
    public synchronized String getCafeteriaCode() {
        return getTypeOne("cafeteria_code");
    }

    @Override
    public synchronized String getSchoolCode() {
        return getTypeOne("school_code");
    }

    private String getTypeOne(String codeType) {
        SysCode sysCode = sysCodeRepository.findByCodeType(codeType);
        if (sysCode != null) {
            sysCode.setSeqNum(sysCode.getSeqNum() + 1);
            put(sysCode);
            return sysCode.getPrefix() + String.format("%0" + sysCode.getCompletion() + "d", sysCode.getSeqNum());
        }
        return null;
    }

}
