package org.dppc.supervise.service.emergency.impl;

import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.dppc.supervise.entity.emergency.EmergencyType;
import org.dppc.supervise.repository.emergency.EmergencyTypeRepository;
import org.dppc.supervise.service.emergency.EmergencyTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description 应急事件类型业务层实现类
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class EmergencyTypeServiceImpl extends BaseServiceImpl<EmergencyType, Integer> implements EmergencyTypeService {

    @Autowired
    private EmergencyTypeRepository emergencyTypeRepository;

}
