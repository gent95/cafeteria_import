package org.dppc.supervise.service.record.impl;

import org.dppc.common.context.BaseContextHandler;
import org.dppc.common.enums.RecordAuditStatusEnum;
import org.dppc.dbexpand.service.impl.BaseServiceImpl;
import org.dppc.supervise.entity.record.Cafeteria;
import org.dppc.supervise.repository.record.CafeteriaRepository;
import org.dppc.supervise.service.record.CafeteriaService;
import org.dppc.supervise.service.syscode.SysCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @Description 食堂业务层实现类
 * @Author GAOJ
 * @Data 2019/05/24 10:45
 * @Version 1.0
 **/
@Service
public class CafeteriaServiceImpl extends BaseServiceImpl<Cafeteria, Integer> implements CafeteriaService {

    @Autowired
    private CafeteriaRepository cafeteriaRepository;

    @Autowired
    private SysCodeService sysCodeService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveNew(Cafeteria cafeteria) {
        cafeteria.setCreateUserId(Long.valueOf(BaseContextHandler.getUserID()));
        cafeteria.setCreateUserName(BaseContextHandler.getName());
        cafeteria.setAuditStatus(RecordAuditStatusEnum.NOT_AUDIT.getValue());
        cafeteria.setCreateDate(new Date());
        cafeteria.setCafeteriaCode(sysCodeService.getCafeteriaCode());
        cafeteriaRepository.save(cafeteria);
    }
}
