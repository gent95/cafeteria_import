package org.dppc.supervise.service.emergency;

import org.dppc.dbexpand.service.BaseService;
import org.dppc.supervise.entity.emergency.EmergencySchoolReport;

/**
 * @Description 学校应急事件上报（学校直通车）业务层接口
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
public interface EmergencySchoolReportService extends BaseService<EmergencySchoolReport, Long> {

}
