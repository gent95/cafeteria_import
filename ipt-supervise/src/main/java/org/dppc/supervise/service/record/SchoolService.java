package org.dppc.supervise.service.record;

import org.dppc.dbexpand.service.BaseService;
import org.dppc.supervise.entity.record.School;

/**
 * @Description 学校业务层接口
 * @Author GAOJ
 * @Data 2019/05/24 10:45
 * @Version 1.0
 **/
public interface SchoolService extends BaseService<School, Integer> {

    void saveNew(School school);

}
