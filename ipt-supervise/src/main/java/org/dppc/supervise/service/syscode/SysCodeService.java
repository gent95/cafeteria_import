package org.dppc.supervise.service.syscode;

/**
 * @Description 业务层接口
 * @Author GAOJ
 * @Data 2019/05/14 14:00
 * @Version 1.0
 **/
public interface SysCodeService {

    /** 生成食堂备案编码*/
    String getCafeteriaCode();

    /** 生成学校备案编码序列*/
    String getSchoolCode();

}
