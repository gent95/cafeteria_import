package org.dppc.supervise.service.emergency;

import org.dppc.dbexpand.service.BaseService;
import org.dppc.supervise.entity.emergency.EmergencyPlan;

/**
 * @Description 应急预案业务层接口
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
public interface EmergencyPlanService extends BaseService<EmergencyPlan, Long> {

}
