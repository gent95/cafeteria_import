package org.dppc.supervise.repository.emergency;

import org.dppc.dbexpand.repository.BaseRepository;
import org.dppc.supervise.entity.emergency.EmergencyPlan;

/**
 * @Description 应急预案数据库操作层
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
public interface EmergencyPlanRepository extends BaseRepository<EmergencyPlan, Long> {

}
