package org.dppc.supervise.repository.emergency;

import org.dppc.dbexpand.repository.BaseRepository;
import org.dppc.supervise.entity.emergency.EmergencySchoolReport;

/**
 * @Description 学校应急事件上报（学校直通车）数据库操作层
 * @Author GAOJ
 * @Data 2019/05/27 14:18
 * @Version 1.0
 **/
public interface EmergencySchoolReportRepository extends BaseRepository<EmergencySchoolReport, Long> {

}
