package org.dppc.supervise.repository.syscode;

import org.dppc.dbexpand.repository.BaseRepository;
import org.dppc.supervise.entity.syscode.SysCode;

/**
 * @Description 数据库操作层
 * @Author GAOJ
 * @Data 2019/05/14 14:00
 * @Version 1.0
 **/
public interface SysCodeRepository extends BaseRepository<SysCode, Integer> {

    SysCode findByCodeType(String codeType);

}
