package org.dppc.supervise.repository.base;

import org.dppc.dbexpand.repository.BaseRepository;
import org.dppc.supervise.entity.base.Region;

/**
 * @Description 区域数据库操作层
 * @Author GAOJ
 * @Data 2019/05/24 10:43
 * @Version 1.0
 **/
public interface RegionRepository extends BaseRepository<Region, String> {

}
