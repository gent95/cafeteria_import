package org.dppc.supervise.repository.record;

import org.dppc.dbexpand.repository.BaseRepository;
import org.dppc.supervise.entity.record.School;

/**
 * @Description 学校数据库操作层
 * @Author GAOJ
 * @Data 2019/05/24 10:45
 * @Version 1.0
 **/
public interface SchoolRepository extends BaseRepository<School, Integer> {

}
