package org.dppc.supervise.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description element树形控件VO
 * @Author lhw
 * @Data 2019/6/6 10:18
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EleTreeVO {
    /** 控件显示数据 */
    private String label;
    /** 自定义属性  编码 */
    private String code;
    /** 自定义属性  id */
    private Long id;
    /** 级别 从0开始 */
    private Integer level;
    /** 子元素 */
    private List<EleTreeVO> children;
}
