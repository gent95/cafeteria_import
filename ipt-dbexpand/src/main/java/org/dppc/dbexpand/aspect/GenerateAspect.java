package org.dppc.dbexpand.aspect;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.dppc.dbexpand.annotation.GenerateValueInfo;
import org.dppc.dbexpand.util.BeanHelp;
import org.dppc.dbexpand.util.SequenceHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.Table;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

/**
 * @描述 :  生成切面
 * @作者 :  Zhao Yun
 * @日期 :  2017/08/30
 * @时间 :  12:43
 */
@Aspect
@Component
public class GenerateAspect {

    @Pointcut("execution(public * org.dppc..service..*.add*(..))")
    private void add() { }

    @Pointcut("execution(public * org.dppc..service..*.save*(..))")
    private void save() { }

    @Pointcut("execution(public * org.dppc..service..*.create*(..))")
    private void create() { }

    @Pointcut("add() || save() || create()")
    private void generate() { }

    @Autowired
    private EntityManager entityManager;

    @Before("generate()")
    public void before(JoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        for (Object arg : args) {
            final Class<?> argClass = arg.getClass();
            if (Arrays.asList(argClass.getInterfaces()).contains(List.class)) {
                List list = (List) arg;
                for (Object obj : list) {
                    final Class<?> objClass = obj.getClass();
                    setField(obj, objClass);
                }
            } else {
                setField(arg, argClass);
            }
        }
    }

    private void setField(Object arg, Class clazz) throws Throwable {
        List<Field> fields = BeanHelp.getAllFields(clazz);
        for (Field field : fields) {
            boolean accessible = field.isAccessible();
            field.setAccessible(true);
            GenerateValueInfo generateValueInfo = field.getAnnotation(GenerateValueInfo.class);
            if (generateValueInfo == null) {
                continue;
            }
            if (field.get(arg) != null){
                continue;
            }
            GenerateValueInfo.GenerateType mode = generateValueInfo.mode();
            switch (mode) {
                case UUID:
                    field.set(arg, SequenceHelp.generateUUID());
                    break;
                case SEQUENCE:
                    final String prefix = generateValueInfo.prefix();
                    Class tableClass = getTableClass(clazz);
                    if (tableClass != null && StringUtils.isNotBlank(prefix)) {
                        // noinspection Convert2Lambda
                        field.set(arg, SequenceHelp.getNextSequence(prefix, new SequenceHelp.InitHandler() {
                            @Override
                            public String initSequence() {
                                Object result = entityManager.createQuery(
                                        "SELECT max(" + field.getName() + ") FROM " + tableClass.getName())
                                        .getSingleResult();
                                return result == null ? null : result.toString();
                            }
                        }, false));
                    }
                    break;
            }
            field.setAccessible(accessible);
        }
    }

    private Class getTableClass(Class clazz) {
        return clazz == null
                ? null
                : (clazz.getAnnotation(Table.class) == null
                    ? getTableClass(clazz)
                    : clazz);
    }

}
