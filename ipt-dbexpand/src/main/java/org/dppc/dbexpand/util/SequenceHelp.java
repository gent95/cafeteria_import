package org.dppc.dbexpand.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @描述 :  序列帮助
 * @作者 :  Zhao Yun
 * @日期 :  2017/03/24
 * @时间 :  16:10
 */
public class SequenceHelp {

    private static final String DATE_PATTERN = "yyyyMMdd";
    private static final int DATE_PATTERN_LEN = DATE_PATTERN.length();
    private static final int SEQUENCE_LEN = 8;
    private static final ReentrantLock lock = new ReentrantLock();

    private static ConcurrentHashMap<String, LongAdder> sequenceMap = new ConcurrentHashMap<>();
    private static String ymd = null;


    /**
     * @描述 : 获取下一个序列
     *      @param handler   序列初始化持利器
     *      @param force     是否强制初始化
     *      @return          序列
     * @作者 : Zhao Yun
     * @日期 : 2017/03/24
     * @时间 : 17:24
     */
    public static String getNextSequence(String prefix, InitHandler handler, boolean force) {
        lock.lock();
        try {
            LongAdder sequence = sequenceMap.get(prefix);
            if (force || sequence == null) {
                int prefixLength = prefix.length();
                String initSequence = handler.initSequence();
                int sequencePos = prefixLength + DATE_PATTERN_LEN;
                if (initSequence == null || initSequence.length() < sequencePos + SEQUENCE_LEN) {
                    sequence = new LongAdder();
                } else {
                    if (ymd == null) {
                        ymd = initSequence.substring(prefixLength, sequencePos);
                    }
                    sequence = new LongAdder();
                    sequence.add(Long.valueOf(initSequence.substring(sequencePos)));
                }
            }
            String yyyyMMdd = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
            if (!yyyyMMdd.equals(ymd)) {
                ymd = yyyyMMdd;
                sequence.reset();
            }
            sequence.increment();
            sequenceMap.put(prefix, sequence);
            return String.format("%s%8s%08d", prefix, ymd, sequence.intValue());
        } finally {
            lock.unlock();
        }
    }

    public static String generateUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * @描述 :  序列初始值，主要用于项目重启导致的缓存失效
     * @作者 :	Zhao Yun
     * @日期 :	2017/3/24
     * @时间 :	17:24
     */
    public interface InitHandler {
        String initSequence();
    }

}
