package org.dppc.dbexpand.contant;

/**
 * @描述 :  常量
 * @作者 :  Zhao Yun
 * @日期 :  2017/08/30
 * @时间 :  11:37
 */
public interface Constants {

    /** Zhao Yun 2017/8/30 序列中间标识长度 */
    Integer PREFIX_LENGTH = 2;

}
