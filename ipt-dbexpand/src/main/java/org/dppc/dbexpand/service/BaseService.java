package org.dppc.dbexpand.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.List;

/**
 * @描述 : 公共service,其他service继承
 * @作者 :	zhumh
 * @日期 :	2017/10/13
 * @时间 :	14:39
 */
public interface BaseService<T,ID extends Serializable> {

    /**
     * 实体添加数据
     * @param t
     * @return
     */
    T save(T t);
    /**
     * 实体ID删除数据
     * @param id
     */
    void delete(ID id);

    /**
     *实体删除数据
     * @param t
     */
    void delete(T t);
    /**
     *实体更新数据
     * @return
     */
    T update(T t);

    /**
     * 通过id获取单条数据
     * @param id
     * @return
     */
    T getOneDataById(ID id);

    /**
     * 获取所有数据
     * @return
     */
    List<T> getAllList();

    /**
     * 获取列表数据
     */
    List<T> findList(T query);

    /**
     * 获取分页数据
     */
    Page<T> findPageList(T query, Pageable pageable);

    /**
     * 局部更新数据
     */
    boolean patch(T t, ID id);

}
