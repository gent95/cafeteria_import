package org.dppc.dbexpand.service.impl;

import org.dppc.dbexpand.repository.BaseRepository;
import org.dppc.dbexpand.service.BaseService;
import org.dppc.dbexpand.util.BeanHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.List;

/**
 * @描述 :
 * @作者 :	zhumh
 * @日期 :	2017/10/13
 * @时间 :	14:47
 */
public class BaseServiceImpl<T, ID extends Serializable> implements BaseService<T, ID> {

    @Autowired
    private BaseRepository<T, ID> baseRepository;

    @Override
    public T save(T t) {
        return baseRepository.save(t);
    }

    @Override
    public void delete(ID id) {
        baseRepository.delete(id);
    }

    @Override
    public void delete(T t) {
        baseRepository.delete(t);
    }

    @Override
    public T update(T t) {
        return baseRepository.saveAndFlush(t);
    }

    @Override
    public T getOneDataById(ID id) {
        return baseRepository.getOneById(id);
    }

    @Override
    public List<T> getAllList() {
        return baseRepository.findAll();
    }

    @Override
    public List<T> findList(T query) {
        return baseRepository.findAll((root, criteriaQuery, criteriaBuilder) ->
                BeanHelp.getPredicate(root, query, criteriaBuilder)
        );
    }

    @Override
    public Page<T> findPageList(T query, Pageable pageable) {
        return baseRepository.findAll((root, criteriaQuery, criteriaBuilder) ->
                        BeanHelp.getPredicate(root, query, criteriaBuilder)
                , pageable);
    }

    @Override
    public boolean patch(T t, ID id) {
        T old = baseRepository.findOne(id);
        if (old != null) {
            baseRepository.saveAndFlush(BeanHelp.updateEntityExceptEmptyProps(old, t));
            return true;
        } else {
            return false;
        }
    }
}
