package org.dppc.dbexpand.redis;

import org.dppc.common.utils.JsonHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @描述： redis核心操作方法
 * @作者： 颜齐
 * @日期： 2017/10/27.
 * @时间： 16:10.
 */
public class BaseRedis<T> {

    @Autowired
    protected RedisTemplate<String, String> redisTemplate;

    /**
     * 缓存单个对象
     * @param key
     * @param time
     * @param t
     */
    public void add(String key, Long time, T t) {

        redisTemplate.opsForValue().set(key, JsonHelper.toJSon(t), time, TimeUnit.MINUTES);
    }

    /**
     * 缓存集合对象
     * @param key
     * @param time
     * @param ts
     */
    public void add(String key, Long time, List<T> ts) {
        redisTemplate.opsForValue().set(key, JsonHelper.toJSon(ts), time, TimeUnit.MINUTES);
    }

    /**
     * 获取单个对象
     * @param key
     * @param valueType
     * @return
     */
    public T get(String key, Class<T> valueType) {
        T t = null;
        String json = redisTemplate.opsForValue().get(key);
        if (!StringUtils.isEmpty(json))
            t = JsonHelper.readJson(json, valueType);
        return t;
    }

    /**
     * 获取集合对象
     * @param key
     * @param valueType
     * @return
     * @throws Exception
     */
    public List<T> getList(String key, Class<T> valueType) throws Exception {
        List<T> ts = null;
        String listJson = redisTemplate.opsForValue().get(key);
        if (!StringUtils.isEmpty(listJson))
            ts = JsonHelper.readJson(listJson, List.class, valueType);
        return ts;
    }

    /**
     * 删除单个对象
     * @param key
     */
    public void delete(String key) {
        redisTemplate.opsForValue().getOperations().delete(key);
    }
}
