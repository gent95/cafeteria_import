package org.dppc.dbexpand.repository;

import org.dppc.common.entity.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


@NoRepositoryBean
public interface BaseRepository<T,ID extends Serializable> extends/* CrudRepository<T, Serializable>,*/JpaRepository<T, ID>,JpaSpecificationExecutor<T> {

	/**
	 * 
	 * @描述	 : 通过sql查询数据
	 * @作者	 : zmh
	 * @创建日期 : 2017-9-9 下午11:01:16  
	 * @param sql
	 * @return
	 */
	List<T> selectBySqlList(String sql);

	/**
	 * 通过jpql查询数据
	 * @param jpql
	 * @return
	 */
	List<T> selectByJpqlList(String jpql);

	/**
	 * 通过SQL查询数据,指定返回类型
	 * @param sql
	 * @param entityClass 返回类型
	 * @return
	 */
	List selectBySqlList(String sql,Class entityClass);

	/**
	 * 
	 * @描述	 : 通过条件查询单条数据
	 * @作者	 : zmh
	 * @创建日期 : 2017-9-10 下午6:49:35  
	 * @param entityClass
	 * @param whereJpql
	 * @param params
	 * @return
	 */
	T selectOne(Class<T> entityClass, String whereJpql, Object[] params);
	
	/**
	 * 
	 * @描述	 : 通过条件查询单条数据
	 * @作者	 : zmh
	 * @创建日期 : 2017-9-10 下午6:58:37  
	 * @param entityClass
	 * @param whereJpql
	 * @param params
	 * @param page
	 * @return
	 */
	void selectPageList(Class<T> entityClass, String whereJpql, Object[] params, Page page, String orderBy);

	/**
	 *
	 * @描述	 : 通过条件查询单条数据
	 * @作者	 : zmh
	 * @创建日期 : 2017-9-10 下午6:58:37
	 * @param entityClass
	 * @param whereJpql
	 * @param params
	 * @param page
	 * @return
	 */
	void selectPageList(Class<T> entityClass, String whereJpql, Object[] params, Page page);

	/**
	 *
	 * @描述	 : 通过条件查询单条数据
	 * @作者	 : zmh
	 * @创建日期 : 2017-9-10 下午6:58:37
	 * @param entityClass
	 * @param sql
	 * @param params
	 * @param page
	 * @return
	 */
	void selectPageSqlList(Class<T> entityClass, String sql, Object[] params, Page page);

	/**
	 *
	 * @描述	 : 通过条件查询单条数据
	 * @作者	 : zmh
	 * @创建日期 : 2017-9-10 下午6:58:37
	 * @param jqpl
	 * @param countJpql
	 * @param page
	 * @return
	 */
	void selectPageList(String jqpl,String countJpql,Page page);

	/**
	 * 查询所有数据
	 * @return
     */
	List<T> selectAll(Class<T> entityClass);

	/**
	 * @描述 :  根据原生sql获取集合数据
	 * @作者 :  Zhao Yun
	 * @日期 :  2017/11/3
	 * @时间 :  15:30
	 */
	List findByNativeSQL(String sql);

	/**
	 * @描述 :  根据原生sql获取Map(K - 字段名，值 - 数据)集合数据
	 * @作者 :  Zhao Yun
	 * @日期 :  2017/12/29
	 * @时间 :  15:26
	 */
	List<Map> findMapListByNativeSQL(String sql);

	/**
	 * @描述 :  根据原生sql获取单一结果
	 * @作者 :  Zhao Yun
	 * @日期 :  2017/11/3
	 * @时间 :  15:30
	 */
	Object findOneByNativeSQL(String sql);

	/**
	 * @描述 :  根据原生sql获取单一结果Map(K - 字段名，值 - 数据)
     * @作者 :  Zhao Yun
     * @日期 :  2017/12/29
     * @时间 :  15:26
	 */
	Map findOneMapByNativeSQL(String sql);

	/**
	 * jpql 查询总记录数据
	 * @param jpql
	 * @return
	 */
	Long findCountByJpql(String jpql);

	/**
	 * id 获取一条数据
	 * @param id
	 * @return
	 */
	T getOneById(ID id);

}
