package org.dppc.dbexpand.repository;

import org.dppc.dbexpand.repository.impl.BaseRepositoryImpl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.repository.core.RepositoryInformation;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * 
 ***************************************************************
 * @描述		:	工厂注册
 * @作者		:	zmh
 * @版本		:	v1.0
 * @日期		:	2017-9-9             
 * 
 * @修改日志	:		
 * @修改人		:	
 * @修订后版本	:	v2.0
 * @修改时间	:	  
 ****************************************************************
 */
public class BaseRepositoryFactoryBean<R extends JpaRepository<T, I>, T,
		I extends Serializable> extends JpaRepositoryFactoryBean<R, T, I>  {

    public BaseRepositoryFactoryBean(Class<? extends R> repositoryInterface) {
        super(repositoryInterface);
    }

    @SuppressWarnings("rawtypes")
	@Override
    protected RepositoryFactorySupport createRepositoryFactory(EntityManager em) {
        return new BaseRepositoryFactory(em);
    }
    
    //创建一个内部类，该类不用在外部访问
    private static class BaseRepositoryFactory<T, I extends Serializable>
            extends JpaRepositoryFactory {
    	
    	private final EntityManager em;
    	
    	public BaseRepositoryFactory(EntityManager em) {
            super(em);
            this.em = em;
        }
    	
    	//设置具体的实现类是BaseRepositoryImpl
    	@Override 
    	protected Object getTargetRepository(RepositoryInformation information) {
    		return new BaseRepositoryImpl<T, I>((Class<T>) information.getDomainType(), em);
    	}
    	
    	//设置具体的实现类的class
        @Override
        protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
            return BaseRepositoryImpl.class;
        }

    }


}
