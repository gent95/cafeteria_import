package org.dppc.dbexpand.repository.impl;

import org.dppc.common.entity.Page;
import org.dppc.dbexpand.repository.BaseRepository;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Transactional
public class BaseRepositoryImpl<T,ID extends Serializable> extends SimpleJpaRepository<T, ID> implements BaseRepository<T,ID> {

	@SuppressWarnings("unused")
	private final EntityManager em;

	public BaseRepositoryImpl(Class<T> domainClass, EntityManager em) {
		super(domainClass, em);
		this.em = em;
	}

	public List<T> selectBySqlList(String sql) {
		return em.createNamedQuery(sql).getResultList();
	}

	public List<T> selectByJpqlList(String jpql) {
		return em.createQuery(jpql).getResultList();
	}

	public List selectBySqlList(String sql,Class entityClass) {
		Query query = em.createNativeQuery(sql);
		//指定返回对象类型
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(entityClass));
		return query.getResultList();
	}

	public T selectOne(Class<T> entityClass,String whereJpql,Object[] params){
		String entityName = getEntityName(entityClass);
		Query query = em.createQuery("SELECT t FROM "+entityName + "  t " + (whereJpql == null ? "" : " WHERE " + whereJpql));
		List<T> tList = query.getResultList();
		if(tList != null && tList.size()>0){
			return tList.get(0);
		}
		return null;
	}

	public void selectPageList(Class<T> entityClass, String whereJpql,
							   Object[] params, Page page, String orderBy) {
		String entityName = getEntityName(entityClass);
		String jpql = " FROM " + entityName + " t " +  (whereJpql == null || whereJpql.equals("") ? "" : " WHERE " + whereJpql);
		Query query = em.createQuery("SELECT t " + jpql);
		setQueryParams(query, params);
		query.setFirstResult((page.getPage()-1)* page.getSize()).setMaxResults(page.getSize());
		page.setData(query.getResultList());
		query = em.createQuery("SELECT count(t) " + jpql);
		setQueryParams(query, params);
		page.setTotal((Long)query.getSingleResult());
	}

	public void selectPageList(Class<T> entityClass, String whereJpql,
							   Object[] params, Page page) {
		String entityName = getEntityName(entityClass);
		String jpql = " FROM " + entityName + " t " +  (whereJpql == null || whereJpql.equals("") ? "" : " WHERE " + whereJpql);
		Query query = em.createQuery("SELECT t " + jpql);
		setQueryParams(query, params);
		query.setFirstResult((page.getPage()-1)* page.getSize()).setMaxResults(page.getSize());
		page.setData(query.getResultList());
		query = em.createQuery("SELECT count(t) " + jpql);
		setQueryParams(query, params);
		page.setTotal((Long)query.getSingleResult());
	}

	public void selectPageSqlList(Class<T> entityClass, String sql, Object[] params, Page page) {
		Query query = em.createNativeQuery(sql);
		setQueryParams(query, params);
		query.setFirstResult((page.getPage()-1)* page.getSize()).setMaxResults(page.getSize());
		//指定返回对象类型
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(entityClass));
		page.setData(query.getResultList());

		query = em.createNativeQuery(sql);
		setQueryParams(query, params);
		page.setTotal((query.getResultList().size()));
	}

	public Long findCountByJpql(String jpql){
		Long s = (Long)em.createQuery(jpql).getSingleResult();
		return s;
	}

	@Override
	public T getOneById(ID id) {
		return em.find(getDomainClass(),id);
	}

	public void selectPageList(String jqpl,String countJpql,Page page){
		Query query = em.createQuery(jqpl);
		query.setFirstResult((page.getPage()-1)* page.getSize()).setMaxResults(page.getSize());
		page.setData(query.getResultList());
		query = em.createQuery(countJpql);
		page.setTotal((Long)query.getSingleResult());
	}

	public List<T> selectAll(Class<T> entityClass) {
		String entityName = getEntityName(entityClass);
		Query query = em.createQuery("SELECT t  FROM " + entityName);
		return query.getResultList();
	}

	/**
	 * @描述 :  根据原生sql获取集合数据
	 * @作者 :  Zhao Yun
	 * @日期 :  2017/11/3
	 * @时间 :  15:30
	 */
	@Override
	public List findByNativeSQL(String sql) {
		return em.createNativeQuery(sql).getResultList();
	}

    /**
     * @描述 :  根据原生sql获取Map(K - 字段名，值 - 数据)集合数据
     * @作者 :  Zhao Yun
     * @日期 :  2017/12/29
     * @时间 :  15:26
     */
	@Override
	public List<Map> findMapListByNativeSQL(String sql) {
		return em.createNativeQuery(sql)
				.unwrap(SQLQuery.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.list();
	}

	/**
	 * @描述 :  根据原生sql获取单一结果
	 * @作者 :  Zhao Yun
	 * @日期 :  2017/11/3
	 * @时间 :  15:30
	 */
	@Override
	public Object findOneByNativeSQL(String sql) {
		return em.createNativeQuery(sql).getSingleResult();
	}

    /**
     * @描述 :  根据原生sql获取单一结果Map(K - 字段名，值 - 数据)
     * @作者 :  Zhao Yun
     * @日期 :  2017/12/29
     * @时间 :  15:26
     */
	@Override
	public Map findOneMapByNativeSQL(String sql) {
		return (Map) em.createNativeQuery(sql)
				.unwrap(SQLQuery.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.uniqueResult();
	}

	/**
	 * 
	 * @描述	 : 查询参数设置
	 * @作者	 : zmh
	 * @创建日期 : 2017-9-10 下午7:09:36  
	 * @param query
	 * @param params
	 */
	protected void setQueryParams(Query query,Object[] params) {
		if(params != null && params.length > 0){
			for(int i=0;i<params.length;i++){
				query.setParameter(i+1,params[i]);
			}
		}
	}
	
	/**
	 * 
	 * @描述	 : 获取实体名称
	 * @作者	 : zmh
	 * @创建日期 : 2017-9-10 下午6:42:01  
	 * @param entityClass
	 * @return
	 */
	protected <T> String getEntityName(Class<T> entityClass){
		String entityName = entityClass.getSimpleName();
		Entity entity = entityClass.getAnnotation(Entity.class);
		if(entity.name() != null && !"".equals(entity.name())){
			entityName = entity.name();	
		}
		return entityName;		
	}

}
