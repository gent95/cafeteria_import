package org.dppc.dbexpand.annotation.support;

import org.dppc.dbexpand.annotation.PredicateInfo;

/**
 * @描述 :  覆盖默认的{@link PredicateInfo}注解
 * @作者 :  Zhao Yun
 * @日期 :  2017/10/27
 * @时间 :  17:10
 */
public class PredicateInfoCover {

    /** Zhao Yun 2017/10/27 要覆盖的属性名，内嵌属性使用"outer.inner"来写 */
    private String propName;
    /** Zhao Yun 2017/10/27 查询方式 */
    private PredicateInfo.QueryType queryType;

    public PredicateInfoCover(String propName, PredicateInfo.QueryType queryType) {
        this.propName = propName;
        this.queryType = queryType;
    }

    public String getPropName() {
        return propName;
    }

    public void setPropName(String propName) {
        this.propName = propName;
    }

    public PredicateInfo.QueryType getQueryType() {
        return queryType;
    }

    public void setQueryType(PredicateInfo.QueryType queryType) {
        this.queryType = queryType;
    }

    @Override
    public String toString() {
        return "PredicateInfoCover{" +
                "propName='" + propName + '\'' +
                ", queryType=" + queryType +
                '}';
    }

}
