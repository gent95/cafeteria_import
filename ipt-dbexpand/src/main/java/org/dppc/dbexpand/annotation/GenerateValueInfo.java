package org.dppc.dbexpand.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @描述 :  数据生成值
 * @作者 :  Zhao Yun
 * @日期 :  2017/08/30
 * @时间 :  12:56
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface GenerateValueInfo {

    /** Zhao Yun 2017/8/30 生成方式，[可选]，默认UUID */
    GenerateType mode() default GenerateType.UUID;
    /** Zhao Yun 2017/8/30 序列前缀，三位，mode为SEQUENCE时，必填 */
    String prefix() default "";

    /**
     * @描述 :  生成方式
     * @作者 :  Zhao Yun
     * @日期 :  2017/8/30
     * @时间 :  12:57
     */
    enum GenerateType {
        UUID
        , SEQUENCE
    }

}
